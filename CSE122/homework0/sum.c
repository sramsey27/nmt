#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

unsigned int sum_forloop(unsigned int n);

/**
* Finds the sum of 2 + 5 + 8 and so on using a for loop
* @param n, the passed in limit to calculate for
* @return the finished result (the sum)
*/
unsigned int sum_forloop(unsigned int n)
{
      unsigned int i = 1, sum = 0, loopcount = 0;

      for( ; i <= n; i++){
            sum += ((3 * i) - 1);
            loopcount++;    
      }
      printf("Loop executed = %d times  |  ", loopcount + 1);
      return sum;
}

int main() 
{
      unsigned int sum = 0, b = 0, n = 1;
     
      /*used just for 1*/
      clock_t start, end;
      start = clock();
      sum = sum_forloop(n);
      end = clock();
      printf("k = %d  |  Sum = %d  |", n, sum);
      printf("Time = %3.24lf\n", (double) (end - start) / CLOCKS_PER_SEC);


      /*used for 10^1 to 10^9*/
      for(b = 1; b <= 9; b++){
            n = 10;
            n = pow(n , b);
            clock_t start, end;
            start = clock();
            sum = sum_forloop(n);
            end = clock();
            printf("k = %d  |  Sum = %d  |", n, sum);
            printf("Time = %3.24lf\n", (double) (end - start) / CLOCKS_PER_SEC);
      }
      return 0;
}