4.

For the previous two questions answer the following questions: What is the largest k
you can use before overflow occurs? Is k the same for both functions? Which function
is preferred to calculate the sum? Why?

- 	The largest k that can be used before overflow would have to be 10^6, beyond this point the limit is reached.
	K is different in both. In sum.c the k = i where in sum_no_for.c k = n. The perferred method would have to be without the for loop as this should take less cpu cycles and significantly less time since it only has to perform the algorithum once instead of n times.