#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define MAX(x, y) ((x) >= (y) ? (x) : (y))

double newton_cube(double n, double epsilon);

/**
* Finds the square root of n using newtons method
* @param n, the value to find the cube root for
* @param epsilon, limit
* @return the finsihed result (cube root)
*/
double newton_cube(double n, double epsilon)
{
        int num_guess = 0;
        double cube = n /2.0;
        
        while (fabs((cube * cube * cube) - n) >= epsilon) {
            cube = cube - ((cube * cube * cube) - n) / (3 * (cube * cube));
            num_guess++;
        }
        
        printf("Guesses =  %d\t|", num_guess);
        return cube;
}

int main()
{
        double n = 1.0;
        for(; n <= 100; n++) {
        	double cube = 0;
        	clock_t start, end;
        	start = clock();
        	cube = newton_cube(n, 0.0000000001);
        	end = clock();
        	printf("Input = %f\t|Result =%.9f\t|", n, cube);
        	printf("Time = %3.24f\n", (double) (end - start) / CLOCKS_PER_SEC);   	
        }   
        return 0;
}