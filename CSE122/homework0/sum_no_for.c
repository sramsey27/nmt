#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

unsigned int sum_noloop(unsigned int n);

/**
* finds the sum of 2 + 5 + 8 and so on depending on limit passed in
* @param n, passed in number or calcuations
* @return the finished result (the sum)
*/
unsigned int sum_noloop(unsigned int n)
{
      unsigned int sum = 0;

            sum = 3 * n * (n + 1);
            sum = sum / 2 - n;

      return sum;
}

int main() 
{
      unsigned int sum = 0, b = 0, n = 1;
      
      /*used just for 1*/
      clock_t start, end;
      start = clock();
      sum = sum_noloop(n);
      end = clock();
      printf("k = %d\t|Sum = %d\t|", n, sum);
      printf("Time = %3.24lf\n", (double) (end - start) / CLOCKS_PER_SEC);
     
      /*used for 10^1 to 10^9*/
      for(b = 1; b <= 9; b++){
            n = 10;
            n = pow(n , b);
            clock_t start, end;
            start = clock();
            sum = sum_noloop(n);
            end = clock();
            printf("k = %d\t|Sum = %d\t|", n, sum);
            printf("Time = %3.24lf\n", (double) (end - start) / CLOCKS_PER_SEC);
      }
            
      return 0;
}