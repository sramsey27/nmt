#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define MAX(x, y) ((x) >= (y) ? (x) : (y))

double newton_sqrt(double n, double epsilon);

/**
* Finds the square root of n using newtons method
* @param n, Value to find square root for
* @param epsilon, limit
* @return the finished result (square root)
*/
double newton_sqrt(double n, double epsilon)
{
        int num_guess = 0;
        double sqr = n / 2.0;

        while (fabs((sqr * sqr) - n) >= epsilon) {
            sqr = sqr - ((sqr * sqr) - n) / (2 * sqr);
            num_guess++;
        }

        printf("Guesses =  %d\t|", num_guess);

        return sqr;
}

int main()
{
        double n = 1.0;
        for (n = 1; n <= 100; n++) {
        	double square = 0;
        	clock_t start, end;
        	start = clock();
        	square = newton_sqrt(n, 0.000000001);
        	end = clock();
        	printf("Input = %f\t|Result =%.9f\t|", n, square);
        	printf("Time = %3.24f\n", (double) (end - start) / CLOCKS_PER_SEC);   	
        }                                                                                                           return 0;
}