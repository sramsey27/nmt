#include <stdio.h>
#include <stdlib.h>

/**
* Prints Fizz if number is divisible by 3 and Buzz by 5.
* @Return nothing
*/
void fizzbuzz()
{
	int i = 0;
	/*Checks for divisibility and prints accordingly*/
	for(i = 1;i < 101; i++){
		if(i % 3 == 0){
			printf("Fizz\n");
		}
		else if (i % 5 == 0){
			printf("Buzz\n");
		}
		else{
			printf("%d\n",i);
		}
	}
}

int main()
{
	fizzbuzz();
	return 0;
}

