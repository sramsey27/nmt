#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define MAX(x, y) ((x) >= (y) ? (x) : (y))

/**
* Binary Search method to determine cube root of pass in value
* @param n, The passed in value
* @param epsilon, the epsilon.
* @return the finished result (cube root)
*/
double cube_bin(double n, double epsilon)
{
        int num_guess = 0;
        double cube = 0.0, low = 0.0, high = MAX(1.0, n);

        while (fabs(cube * cube * cube - n) >= epsilon) {

                cube = (low + high)/2.0;
                if (cube * cube * cube > n) {
                        high = cube;
                }
                else {
                        low = cube;
                }
                num_guess++;
        }
        printf("Guesses =  %d\t|", num_guess);
        return cube;
}
int main()
{
        double cube = 0.0;
        double n = 1;
        for(n = 1; n <= 100; n++){
                clock_t start, end;
                start = clock();
                cube = cube_bin(n, 0.000001);
                end = clock();
                printf("Input = %f\t|Result =%.9f\t|", n, cube);
                printf("Time = %3.24f\n", (double) (end - start) / CLOCKS_PER_SEC);
        }
        return 0;
}
