#include "heapsort.h"
#include <sys/types.h>

int heapsort(void *base, size_t nel, size_t width, int (*compar)(const void *, const void *))
{
        /* todo generic heapsort */

		struct heap_t heap;

		heap.last = nel - 1;
		heap.size = nel;
		heap.data = base;

		heapify(heap.data, heap.last);

		shrink(&heap);

        /* note you have to do pointer arthimetic with void pointers */
        /* can't use array notation, base[i] as that makes no sense as base is a void pointer  */
        /* correct way to get the i-th element is (base + i * width) */

        return 0;
}

void heapify(void *data, int last)
{
	int parent = 0, stop = 1;

	/* left and right children */
	int left = (2 * parent) + 1;
	int right = (2 * parent) + 2;

	while(stop && left <= last)
	{
		while(left <= last)
		{
			if(comp(*((int**)data + left), *((int**)data + parent)) > 0){
				swap(left, parent, data);
				stop += 1;
			}
			if(right <= last){
				if(comp(*((int**)data + right) , *((int**)data + parent)) > 0){
					swap(right, parent, data);
					stop += 1;
				}
			}
			if(parent == 0 && stop == 1){
				stop = 0;
				break;
			}
			parent++;
			stop = 1;
			left = (2 * parent) + 1;
			right = left + 1;
		}
		if (parent == 0 && stop == 0){
				break;
		}
		parent = 0;
		left = (2 * parent) + 1;
		right = left + 1;
	}
}

/**
 * Swaps two nodes child nodes with parent nodes as necessary
 * @param i the current index
 * @param heap_array the structure containing the array
 * @return nothing
 */
void swap(int child, int parent, void *data)
{
	int *temp = 0;
	temp = *((int**)data + child);
	*((int**)data + child) = *((int**)data + parent);
	*((int**)data + parent) = temp;
}

/**
 * deletes the nodes according to binary structure
 * @param i the current index
 * @param heap_array the structure containing the array
 * @return nothing
 */
void shrink(struct heap_t *heap_array)
{
	while(heap_array->last != 0){
		swap(0, heap_array->last, heap_array->data);
		heap_array->last--;
		heapify(heap_array->data, heap_array->last);
	}
}