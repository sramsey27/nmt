#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "heapsort.h"

int comp(const void *num1, const void *num2);

#define LEN 4096

/*  convert string to lowercase */
void tolowercase(char *buf)
{
    int length = strlen(buf);
	int i = 0;

	while(i < length){
		buf[i] = tolower(buf[i]);
		i++;
	}
}

/* trim off whitespace see the manpage for isspace as to what is considered whitespace */
void rstrip(char *buf)
{
    int length = strlen(buf);

    while (length > 0)
    {
    	if(isspace(buf[length]))
    		buf[length] = '\0';
    	length--;
    }
}


int main(int argc, char *argv[])
{
        char buf[LEN];


	int **list = NULL;
	int word = 1;
	int i;
	FILE *fp;

	fp = fopen(argv[1], "r");
	assert(fp);

	//list = malloc(sizeof(int*));
        /* this leaks  you need to fix*/
	while(fgets(buf, LEN, fp)) {
		/* remove new line */
		/* printf("%s\n", t);  */
		list = realloc(list, word * sizeof(int*));
		list[word - 1] = malloc(sizeof(int*));  
		*list[word - 1] = atoi(buf);
		word++;
	}
        /* overcounted */
        word--;
    
        /* print the list */
	for(i = 0; i < word; i++) 
		printf("%d\n", *list[i]);

        printf("\n");

	/* todo sort list with heapsort */
        heapsort(list, word, sizeof(int*), comp);
        /* user needs to provide comparison function */
	
        /* print the sorted list */
        for(i = 0; i < word; i++) 
		printf("%d\n", *list[i]);

		for(i = 0; i < word; i++){
			free(list[i]);
		}
		free(list);

		fclose(fp);
 
       return 0; 
}

/**
 * compares the two strings to see if they match
 * @param string 1 word from user input
 * @param string 2 word from scrabble dict
 * @return 1 if they dont match and 0 otherwise
 */
int comp(const void *num1, const void *num2)
{
	return (*(int*)num1 - *(int*)num2);
}