#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define LEN 4096
#define SEARCH 5
#define QUIT 6

struct postal_t {
        char *abrv; /*two digit abbreviation, capitalized */
        char *name; /*name in start case */
};

void hash_insert(struct postal_t *postal, char* c_name, char* s_name, unsigned key, size_t t_size);
void tolowercase(char *buf);
int rstrip(char *buf);
void startcase(char *buf);
unsigned djb_hash(void *key);
unsigned hash_search(struct postal_t *postal, unsigned key, size_t t_size, char* inp);

/**
 * convert string to lowercase 
 * @param buf the input string
 * @return nothing
 */
void tolowercase(char *buf)
{
        int length = strlen(buf);
        int i = 0;

        while(i < length){
                buf[i] = tolower(buf[i]);
                i++;
        }
}

/**
 * trim off whitespace from the right
 * @param buf, the current line to be stripped 
 * @return nothing
 */
int rstrip(char *buf)
{
        int length = strlen(buf);
        int i = 0;

        while (i < length)
        {
            if(isspace(buf[i]) != 0){
                if(isspace(buf[i + 1]) != 0){
                    return i;
                }
            }
            i++;
        }
    return 0;
}

/**
 * Converts first case of state name to uppercase
 * @param buf the city name string
 * @return nothing
 */
void startcase(char *buf)
{
    int length = strlen(buf);
    int i = 0;

    buf[i] = toupper(buf[i]);

    while(i < length){
        if(isspace(buf[i])!= 0){
            buf[i + 1] = toupper(buf[i + 1]);
        }
        i++;
    }  
}

/**
 * Generates a key based on Bernstein’s Hash
 * @param key the current element to store
 * @return generated key
 */
unsigned djb_hash(void *key)
{
    unsigned char * p = key ;
    unsigned h = 0;

    while (*p){
        h = 33 * h + *p ;
        p ++;
    }

    return h;
}

unsigned hash_search(struct postal_t *postal, unsigned key, size_t t_size, char* inp)
{
    int n = 1;

    //printf("Current = %s\tkey = %d\n", (postal + key)->abrv, key);

    if(((postal + key)->abrv) == NULL){
        return 0;
    }
    else if(strcmp((postal + key)->abrv, inp) == 0){
        return key;
    }
    else{
        while((postal + key * (n * n)) != NULL){
            key = key * (n * n);
            if(((postal + key)->abrv) == NULL){
                return 0;
            }               
            else if (strcmp((postal + key)->abrv, inp) == 0){
                return key;
            }
            n++;
        }
    }

    return 0;
}

/**
 * Inserts the element into the hash table
 * @param table the hash table itself
 * @param len the size of the table
 * param postal, the postal abbreviation
 * @return nothing
 */
void hash_insert(struct postal_t *postal, char* c_name, char* s_name, unsigned key, size_t t_size)
{

        int n = 1, i = 0;
        i = key % t_size;

        if((postal + i)->abrv == NULL && (postal + i)->name == NULL){
            (postal + i)->abrv = s_name;
            (postal + i)->name = c_name;
        }
        else{
            while((postal + ((i + (n * n)) % t_size))->abrv != NULL && (postal + ((i + (n * n)) % t_size))->name != NULL){
                n++;
            }
            (postal + (i + (n * n)) % t_size)->abrv = s_name;
            (postal + (i + (n * n)) % t_size)->name = c_name;
        }
}

int main(int argc, char *argv[])
{
    char buf[LEN];
    size_t t_size = 200;
    int i = 0;

    struct postal_t *postal = malloc(t_size * sizeof(struct postal_t));

    for(i = 0; i < t_size; i++){
        postal[i].name = NULL;
        postal[i].abrv = NULL;
    }
    
    char* c_name = NULL;
    char* s_name = NULL;

    int c_length = 0;
    unsigned key = 0, found = 0;

	FILE *fp = fopen("postal", "r");
	assert(fp);

    printf("Generating Hash Table...");
	while(fgets(buf, LEN, fp)) {

            c_length = rstrip(buf);

            c_name = malloc(sizeof(char) * c_length + 1);
            s_name = malloc(sizeof(char) * 3);

            strncpy(c_name, buf, c_length);
            c_name[c_length] = '\0';

            tolowercase(c_name);
            startcase(c_name);

            c_length = strlen(buf) - 3;

            strncpy(s_name, c_length + buf, 3);
            s_name[2] = '\0';

            key = djb_hash(s_name);

            hash_insert(postal, c_name, s_name, key, t_size);

           
        }
        printf("Done.\n\n");

        int menu = 1;
        char inp[LEN];
        char choice[LEN];

        while(menu != 0){
            printf("Enter a two-digit state abbreviation (q to quit, p to print): ");
            fgets(inp, LEN, stdin);
            sscanf(inp, "%s", choice);

            /* Quit Case*/
            if(strcmp(choice, "q") == 0){
                printf("\nFreeing memory...");

                for(i = 0; i < t_size; i++){
                    if((postal + i)->abrv != NULL){
                        free((postal + i)->abrv);
                    }

                     if((postal + i)->name != NULL){
                        free((postal + i)->name);
                    }
                }
                free(postal);

                printf("Done!\t\t");
                printf("Goodbye.\n\n");
                menu = 0;

            }
            /* Print DEBUG */
            else if(strcmp(choice, "p") == 0){
                printf("\nDEBUG PRINT:\n\n");
                for(i = 0; i < t_size; i++){
                    if(((postal + i)->abrv) == NULL){
                        continue;
                    }
                    printf("i = %d\n%s\t%s\n\n",i, (postal + i)->abrv, ((postal + i)->name));
                }
            }

            /* Hash Search */
            else{
                key = djb_hash(choice);
                key = key % t_size;

                found = hash_search(postal, key, t_size, choice);
                if(found != 0){
                printf("%s\n\n",((postal + found)->name));
                }
                else{
                    printf("State not found.\n\n");
                }
            }
        }

    fclose(fp);
    return 0;
}