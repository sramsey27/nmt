#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#define LEN 128
#define LOOKUP 1
#define QUIT 2

/* Link list within array */
struct node_t {
        char *word; /* dictionary word */
        struct node_t *next; 
};

/* Main Array */
struct list_t {
        int nnodes; /* number of nodes list contains */
        struct node_t *head; /*head of the list */
};

void tolowercase(char *buf);
void rstrip(char *buf);
unsigned djb_hash(void *key);
int table_search(struct list_t **sentinel, char* in_word, unsigned key);
struct list_t **create_hashtable(struct list_t **dict, size_t tbl_size);
struct list_t **insert_head(struct list_t **sentinel, struct node_t *node, unsigned key);
void print_node(struct node_t *node);
void free_everything(struct list_t **sentinel, size_t tbl_size);

int main(int argc, char *argv[]) 
{
        if(argv[1] == NULL){
                printf("\nNO TABLE SIZE PROVIDED!\n\n");
                exit(LEN);
        }
        size_t tbl_size = strtol(argv[1], NULL, 10);
        struct list_t **sentinel = NULL; //malloc(sizeof(struct list_t *) * tbl_size);

        //assert(sentinel);
   
        clock_t elasped_time_start, elasped_time_end;     
        
        /*load dictionary into hash table*/
        sentinel = create_hashtable(sentinel, tbl_size);

        /* This allows for the change in modes either be menu mode or DEBUG PRINT*/
        /* 1 for true, 0 for false*/
        int dump = 1;

        struct node_t *tmp = NULL;

        char inp[LEN];
        int menu = 1, choice = 0, i = 0, j = 0;
        unsigned key = 0;
        if(dump == 1){
                elasped_time_start = clock();
                for(i = 0; i < tbl_size; i++){
                        j = 0;
                        printf("%d:     ", i);
                        if(sentinel[i]->head == NULL){
                                printf("\n");
                                continue;
                        }
                        else{
                                tmp = sentinel[i]->head;
                                while(tmp != NULL){
                                        //printf("\nnnodes = %d\n j = %d\n", sentinel[i]->nnodes, j);
                                        print_node(tmp);
                                        if(j != sentinel[i]->nnodes - 1){
                                                printf(", ");
                                        }
                                        j++;
                                        tmp = tmp -> next;
                                }
                                printf("\n");
                        }
                }
                elasped_time_end = clock();
                printf("Table Size %lu ", tbl_size);
                printf("load factor: %0.2lf\n", (double) (elasped_time_end - elasped_time_start) / CLOCKS_PER_SEC);
                free_everything(sentinel, tbl_size);
        }
        else if(dump == 0){
                char in_word[LEN];

                while(menu != 0){
                        printf("\nScrabble Hash Table Lookup\n");
                        printf("1. Lookup a word:\n2. Quit The Program\n");
                        printf("Whats it going to be? : ");

                        fgets(inp, LEN, stdin);
                        sscanf(inp, "%d", &choice);

                        switch(choice){
                                case LOOKUP:
                                        printf("Enter a word: ");
                                        fgets(in_word, LEN, stdin);

                                        rstrip(in_word);
                                        tolowercase(in_word);

                                        key = djb_hash(in_word);
                                        key = key % tbl_size;

                                        if(table_search(sentinel, in_word, key) == 1){
                                                printf("\n%s is a valid Scrabble word!\n", in_word);
                                                printf("Found on index: %d\n", key);
                                        }
                                        else{
                                                printf("%s is not a valid scrabble word!\n", in_word);
                                        }
                                        break;
                                case QUIT:
                                        printf("\nFreeing memory...");
                                        free_everything(sentinel, tbl_size);
                                        printf("Done!\n");
                                        menu = 0;
                                        break;
                                default:
                                        printf("That wasn't a valid choice. Try Again.\n");
                        }
                }
        }
        return 0;
}

/**
 * convert string to lowercase 
 * @param buf the input string
 * @return nothing
 */
void tolowercase(char *buf)
{
        int length = strlen(buf);
        int i = 0;

        while(i < length){
                buf[i] = tolower(buf[i]);
                i++;
        }
}
/**
 * trim off whitespace from the right
 * @param buf, the current line to be stripped 
 * @return nothing
 */
void rstrip(char *buf)
{
        int length = strlen(buf);

        while (length > 0) {
                if(isspace(buf[length]))
                        buf[length] = '\0';
        length--;
        }

}

/**
 * Generates a key based on Bernstein’s Hash
 * @param key the current element to store
 * @return generated key
 */
unsigned djb_hash(void *key)
{
    unsigned char * p = key ;
    unsigned h = 0;

    while (*p){
        h = 33 * h + *p ;
        p ++;
    }

    return h;
}

/**
 * Determines if a word is present in the hash table
 * @param sentinel the sentinel containing the data
 * @param in_word the user input word
 * @param key the generated key/index
 * @return 1 if word is found, 0 otherwise
 */
int table_search(struct list_t **sentinel, char* in_word, unsigned key)
{
        struct node_t *tmp = NULL;

        if(sentinel[key]->head == NULL){
                return 0;
        }
        else{
                tmp = sentinel[key]->head;
                while(tmp != NULL){
                        if(strcmp(tmp->word, in_word) == 0){
                                return 1;
                        }
                        tmp = tmp->next;
                }
        }
        return 0;
}

/**
 * Generates the hash table
 * @param dict the sentinel to put data in
 * @param tbl_size the table size defined by the user
 */
struct list_t **create_hashtable(struct list_t **dict, size_t tbl_size)
{
        FILE* in_file;
        char buffer[LEN];
        unsigned key = 0;
        int cur_length = 0;

        in_file = fopen("scrabble.txt", "r");
        if (in_file == NULL){
                printf("Cannot find file. Exitting...\n\n");
                exit(10);
        }

        struct list_t **sentinel = malloc(sizeof(struct list_t*) * tbl_size);

        int i = 0;
        for(i = 0; i < tbl_size; i++){
                sentinel[i] = malloc(sizeof(struct list_t));
                sentinel[i]->head = NULL;
                sentinel[i]->nnodes = 0;
        }

        struct node_t *node = NULL;

        printf("Generating Hash Storage with a table size of %lu...", tbl_size);
        while(fgets(buffer, LEN, in_file)){

                /* Prepare the String*/
                rstrip(buffer);
                tolowercase(buffer);
                cur_length = strlen(buffer) + 1;

                /*Generate Key*/
                key = djb_hash(buffer); 
                key = key % tbl_size;

                node = malloc(sizeof(struct node_t));
                node->word = malloc(sizeof(char) * cur_length);

                strncpy(node->word, buffer, cur_length);
                insert_head(sentinel, node, key);
        }
        printf("Done.\n");
        fclose(in_file);

        return sentinel;

}

/**
 * inserts the word into the current indexes linked list
 * @param sentinel the sentinel containing the data
 * @param node the node to be inserted
 * @param key the generated key/index
 * @return the changed sentinel
 */
struct list_t **insert_head(struct list_t **sentinel, struct node_t *node, unsigned key)
{
        node->next = sentinel[key]->head;
        sentinel[key]->head = node;
        sentinel[key]->nnodes++;
        return sentinel;
}

/**
 * Prints the passed in node
 * @param node the passed in node to be printed
 */
void print_node(struct node_t *node)
{
        if (node != NULL){
                printf("%s", node->word);
        }

        else 
                printf("\nNode is currently NULL.\n");
}

/**
 * Frees everything from memory
 * @param sentinel the sentinel containing the data
 * @param tbl_size the size of the table
 */
void free_everything(struct list_t **sentinel, size_t tbl_size)
{
        struct node_t *temp = NULL;
        struct node_t *cur = NULL;
        int i = 0;
        for(i = 0; i < tbl_size; i++){
                if(sentinel[i]->head == NULL){
                        continue;
                }
                else{
                        temp = sentinel[i]->head;
                        cur = NULL;
                        while(temp != NULL){
                                cur = temp->next;
                                free(temp->word);
                                free(temp);
                                temp = cur;
                        }
                }
        }
        for(i = 0; i < tbl_size; i++){
                if(sentinel[i] != NULL){
                        free(sentinel[i]);
                }
        }
        free(sentinel);
}