#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int strchrrep(char* word);

/**
 * checks for repeated characters
 * @param word the user input
 * @return the repeated character
 */
int strchrrep(char* word)
{
	int array[128] = {0};
	int i = 0;

	for(i = 0; i < strlen(word); i++){
		array[word[i] - 'a']++;
		if(array[word[i] - 'a'] > 1){
			return word[i];
		}
	}
	return 0;
}

int main(int argc, char **argv)
{
        	
	if (argc != 2) {
		printf("error in input\n");
		printf("usage: ./repeated [STRING]\n");
		printf("where [STRING] is the string to find the first repeated character in\n");
		exit(EXIT_FAILURE);
	}
	int word = 0;

	word = strchrrep(argv[1]);

	/* print the first repeated char in argv[1] */

	if(word){
		printf("\nThe first repeated character is: %c\n\n", word);
	}
	else{
		printf("\nThere is no repeated characters.\n\n");
	}
	
	return 0;
}