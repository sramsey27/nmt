#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>

#define LEN 4096

struct heap_t {
	int last; 		/* index of last heap element in data array */
	int size;               /* number of elements in array */
	int max; 		/* allocated size of array */
	int *data;		/* the data array */

};

void heapify(int *data, int last);
void swap(int i, int min, int *data);
void shrink(struct heap_t *heap_array);

enum {INIT = 1, GROW = 2};

int main(int argc, char **argv) 
{

	char buf[LEN];
	FILE *fp = NULL;
	int i = 0;

       	if (argc != 2) {
		printf("error in input\n");
		printf("usage: ./heap [FILE]\n");
		printf("[FILE] is a list of integers one per line\n");
		exit(EXIT_FAILURE);
	}
	else {
		fp = fopen(argv[1], "r");
		assert(fp);
	}

	struct heap_t *heap = malloc(sizeof(struct heap_t));
	heap->size = INIT;
	heap->max = INIT;
	heap->data = NULL;

	while (fgets(buf, LEN, fp)) {

		/* read in data from file */
		/* assign to heap->data */

		/* grow the array as necessary */
		if (heap->size > heap->max) {
			heap->data = realloc(heap->data, GROW * heap->max *sizeof(int));
			assert(heap->data);
			heap->max = GROW * heap->max;
		}
		else if (heap->data == NULL) {
			heap->data = malloc(INIT * sizeof(int));
			assert(heap->data);
		}
		*(heap->data + i) = atoi(buf);

		heap->size++;
		i++;
	}	

	/* size is off by one */
	heap->size--;
	heap->last = heap->size - 1;

	/* Heapify the array */
	heapify(heap->data, heap->last);

	/* Printing is done in this function */
	printf("%s Sorted\n", argv[1]);
	shrink(heap);
	
	/* cleanup */
	free(heap->data);
	free(heap);
	fclose(fp);

	return 0;
}

/**
 * Converts array into heap (this was hell to get right)
 * @param heap_array the data array
 * @param last the last index of the array
 */
void heapify(int *data, int last)
{
	int parent = 0, stop = 1;

	/* left and right children */
	int left = (2 * parent) + 1;
	int right = (2 * parent) + 2;

	while(stop && left <= last)
	{
		while(left <= last)
		{
			if(*(data + left) > *(data + parent)){
				swap(left, parent, data);
				stop += 1;
			}
			if(right <= last){
				if(*(data + right) > *(data + parent)){
					swap(right, parent, data);
					stop += 1;
				}
			}
			if(parent == 0 && stop == 1){
				stop = 0;
				break;
			}
			parent++;
			stop = 1;
			left = (2 * parent) + 1;
			right = left + 1;
		}
		if (parent == 0 && stop == 0){
				break;
		}
		parent = 0;
		left = (2 * parent) + 1;
		right = left + 1;
	}
}

/**
 * Swaps two nodes child nodes with parent nodes as necessary
 * @param i the current index
 * @param heap_array the structure containing the array
 * @return nothing
 */
void swap(int child, int parent, int *data)
{
	int temp = 0;
	temp = *(data + child);
	*(data + child) = *(data + parent);
	*(data + parent) = temp;
}

/**
 * deletes the nodes according to binary structure
 * @param i the current index
 * @param heap_array the structure containing the array
 * @return nothing
 */
void shrink(struct heap_t *heap_array)
{
	while(heap_array->last != 0){
		printf("%d\n", *(heap_array->data + 0));
		swap(0, heap_array->last, heap_array->data);
		heap_array->last--;
		heapify(heap_array->data, heap_array->last);
	}
	printf("%d\n", *(heap_array->data + 0));
}