#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SEARCH 1
#define TRY 2
#define EXIT 3
#define LEN 256

struct data_t *generate_dict();
int binsearch(struct data_t *data, char* in_word, int mid_word, int upper, int lower);
char* rstrip(char* buf);
void free_stuff(struct data_t *dict_data, int j);
int tilecheck(int point_array[],char* in_word, struct data_t *data);
int points(int point_array[], char* in_word);
int check(char* in_word, char* dict_word);

struct data_t {
	int num_elements; 
	int max;
	char **data;
};

enum {INIT = 1, GROW = 2};

int main()
{
	int menu = 1, choice = 0, j = 0, highest_index = 0;
	int point_array[] = {1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 5 ,1 ,3, 1, 1, 3, 10, 1, 1, 1, 1, 4, 4, 8 ,4 ,10};
	char inp[LEN];
	char word[LEN];
	char* rword = NULL;

	/* File read in and array storage*/
	printf("Generating storage...");
	struct data_t *data = generate_dict();
	printf("Success!\n");

	while(menu != 0){
		/* Prints menu Options */
		printf("\nScrabble Menu\n");
		printf("1. Find if a word is in the scrabble dictonary\n");
		printf("2. Determine best play from tiles\n");
		printf("3. Exit Program\n");
		printf("Enter an option [1, 2, 3]: ");

		/* user input */
		fgets(inp, LEN, stdin);
		sscanf(inp,"%d", &choice);

		/* menu actions */
		switch(choice){
			case SEARCH:
				printf("\nEnter a word: ");
				fgets(word, LEN, stdin);
				rword = rstrip(word);

				if (binsearch(data, rword, data->num_elements / 2, data->num_elements, 0) == 1)
					printf("\n%s is a valid scrabble word!\n", word);
				else
					printf("\n%s is not a valid scrabble word!\n", word);	

				break;
			case TRY:
				printf("\nEnter tiles as a word: ");

				fgets(word, LEN, stdin);
				rword = rstrip(word);

				highest_index = tilecheck(point_array, rword, data);

				printf("\nThe best play is '%s' (%d points)\n", *(data->data + highest_index), points(point_array, *(data->data + highest_index)));

				break;
			case EXIT:
				printf("\nFreeing memory...");
				for(j = 0; j < data->num_elements; j++){
				free_stuff(data, j);
				}
				free(data->data);
				free(data);
				printf("Done\n");
				printf("\nExitting...\n");
				menu = 0;
				break;
			default:
				printf("\nInvalid input...Please try again.\n\n");
				break;
		}
	}
	return 0;
}

/**
 * Generates a structure filled with the scrabble dictionary and its infomation
 * @return the generated stucture
 */
struct data_t *generate_dict()
{
	char buffer[LEN];
	char* buf = NULL;

	FILE* in_file;
	in_file = fopen("scrabble.txt", "r");
	if (in_file == NULL){
		printf("Cannot find file. Exitting...");
		exit(10);
	}

	struct data_t *data = malloc(sizeof(struct data_t));
	data->num_elements = INIT;
	data->max = INIT;
	data->data = NULL;
	int i = 0;

	/* Allocates and stores as needed */
	while(fgets(buffer, LEN, in_file)) {

		/* Strips the white space */
		buf = rstrip(buffer);

		if(data->data == NULL)
			data->data = malloc(sizeof(char*));
		else if (data->num_elements > data->max) {
			data->data = realloc(data->data, (GROW * data->max) * sizeof(char*));
			data->max = GROW * data->max;
		}
		*(data->data + i) = malloc((sizeof(char) * strlen(buf)) + 1) ;
		strncpy(*(data->data + i), buffer, strlen(buf) + 1);
		i++;
		data->num_elements++;
	}
	data->num_elements--;

	/*for(i = 0; i < data->num_elements; i++)
		printf("data->data[%d] = %s", i, *(data->data + i));
	*/
	fclose(in_file);

	return data;
}

/**
 * Strips the buffer input of whitespace
 * @param buf the buffer input from in_file
 * @return the newly striped buffer
 */
char* rstrip(char* buf){

	int length = strlen(buf);
	int i = 0;

	while(i<length){
		if(buf[i]== '\t' ||
		buf[i]== '\n' ||
		buf[i]== '\f' ||
		buf[i]== '\v' ||
		buf[i]== '\r') {
		buf[i] = '\0';
		}
		i++;
		/*Really wierd coding style isn't it?*/
	}	
	return buf;
}

/**
 * determines if a word is present in the scabble dictonary recursively
 * @param *data the structure containing the data
 * @param in_word the user input word
 * @param the middle index of the dictonary
 * @param upper the highest index of the dictionary
 * @param lower the absolute lowest index of the dictonary
 * @return 1 if the word is present, 0 if not.
 */
int binsearch(struct data_t *data, char* in_word, int mid_word, int upper, int lower)
{
	int compare = strcmp(in_word, data->data[mid_word]);
	int tmp = 0;

	if(upper == lower || mid_word == lower || upper == mid_word){
		if(compare == 0)
			return 1;
		else
			return 0;
	}

	if(compare == 0)
		return 1;
	else if(compare > 0){
		tmp = ((upper - mid_word) / 2) + mid_word;
		return binsearch(data, in_word, tmp, upper, mid_word);
	}
	else if(compare < 0){
		return binsearch(data, in_word, ((mid_word - lower) / 2) + lower, mid_word, lower);
	}
	return 0;
}

/**
 * determines the index of the best play
 * @param point_array the array of values (points)
 * @param in_word user input word
 * @param *data the structure containing info on scrabble dictionary
 * @return the highest index of the best play
 */
int tilecheck(int point_array[], char* in_word, struct data_t *data)
{
	int i = 0;
	int point_tmp1 = 0, highest_index = 0, max = 0;
	for(i = 0; i < data->num_elements - 1; i++){
		//printf("%s\n", *(data->data + i));
		if((check(in_word, *(data->data + i)) == 0)){
			continue;
		}
		else {
			point_tmp1 = points(point_array, *(data->data + i));
			if (point_tmp1 > max){
				max = point_tmp1;
				highest_index = i;
			}
		}

	}
	return highest_index;
}

/**
 * determines the amount of points
 * @param point_array the values corresponding to letters
 * @param in_word the user input word
 * @return the overall sum of points
 */
int points(int point_array[], char* in_word)
{
	int length = strlen(in_word);
	int i = 0, sum = 0;

	for(i = 0; i < length; i++){
		sum += point_array[in_word[i] - 'a'];
	}

	return sum;
}

/**
 * Checks to see if the words are the same in point value
 * @param in_word the user input word
 * @param dict_word the current word in the scrabble dictionary
 * @return 1 for true, 0 for false
 */
int check(char* in_word, char* dict_word)
{
	int length1 = strlen(in_word), length2 = strlen(dict_word);
	int count1[26] = {0}, count2[26] = {0};
	int i = 0; 

	for(i = 0; i < length1; i++){
		count1[in_word[i] - 'a']++;
	}
	for(i = 0; i < length2; i++){
		count2[dict_word[i] - 'a']++;
	}

	for(i = 0; i < 26; i++){
		if(count1[i] < count2[i]){
			return 0;
		}
	}
	return 1;
}

/**
 * Frees up the scrabble dictionary from memory
 * @param dict_data the scrabble dictionary
 * @param j the current index
 * @return nothing
 */
void free_stuff(struct data_t *dict_data, int j)
{
	if (*(dict_data->data + j) != NULL){
		free(*(dict_data->data + j));
	}	
}