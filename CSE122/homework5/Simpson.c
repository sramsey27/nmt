#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double integrate(double (*f)(double x), double a, double b, int n)
{
	double h;		/* height of rectangle */
	double w;		/* width of rectangle */
	double area = 0.0;	/* area of rectangle */
	double x;		/* the domain value */
	int i;

	w = (b - a) / n;

	for (i = 1, x = a; i <= n; i++) {
		x += w;
		h = (*f)(x);
		area += (w * h);
	}

	return area;		/* area under the curve */
}

int main(void)
{
	double natlog, ex, cosx, sinx, sqrtx;
	int n = 10;

	/* integerate with sin, cos, exp */
	natlog = integrate(log, 0.5, 2, n);
	ex = integrate(exp, 0, 1, n);
	sinx = integrate(sin, 0, M_PI, n);
	cosx = integrate(cos, 0, M_PI, n);
	sqrtx = integrate(sqrt, 0, 10, n);

	/* print output */
	printf("\nintegral of log(x) from 0.5 to 2 is %lf\n", natlog);
	printf("integral of e^x from 0 to 1 is %lf\n", ex);
	printf("integral of cos(x) from 0 to PI is %lf\n", cosx);
	printf("integral of sin(x) from 0 to PI is %lf\n", sinx);
	printf("integral of sqrt(x) from 0 to 10 is %lf\n\n", sqrtx);

	return 0;
}