/**
 * @file russian.c
 * 
 * @brief Multiplies two values using the Russian Peasant Algorithm
 *
 * @details what brief said
 *
 * @author Steven Ramsey
 *
 * @date 2/20/15
 *  
 * @todo 
 *
 * @bug none
 *
 * @remark
 * 
 */

#include <stdio.h>
#include <stdlib.h>

/**
* Multiplies two values using the Russian Peasant Algorithm
* @param a the first passed in value
* @param b the second passed in value
* @return answer the final product
*/
int russian(unsigned int m, unsigned int n)
{

	int answer = 0;

	while (n != 0){
		if((m & 1) != 0){
			answer += n;
		}
		m = m / 2;
		n = n * 2;
	}
	
	return answer;
}

int main(int argc, char*argv[])
{	
	unsigned m = 0, n = 0, answer = 0;

	/*Allows command line input for simple use*/
	m = strtol(argv[1], NULL, 0);
	n = strtol(argv[2], NULL, 0);

	
	printf("%d * %d", m ,n );

	answer = russian(m, n);

	printf(" = %d\n", answer);

	return 0;
}