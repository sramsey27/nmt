/**
 * @file permutation.c
 * 
 * @brief various array algorithms for purmutation generation
 *
 * @details this program contains various ways / algorithms for generating random numbered
 			purmutatons which are limited by n / range. Each algorithm is timed for efficiency.
 *
 * @author Steven Ramsey
 *
 * @date 2/23/15
 *  
 * @todo 
 *
 * @bug none
 *
 * @remark
 * 
 */

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <limits.h>
#include <time.h>

void seed(void);
int nrandint(int range);
int *lucky(int *a_array, int range);
int *used(int *array_b, int range);
int *knuth(int *array_c, int range);
void swap(int *array_c, int *array_tmp);

int main(int argc, char*argv[])
{
        /*Command line argument for range*/
		int range = 0;
        //range = (strtol(argv[1], NULL, 0));

        /*Prepare for loops and timing... runs is the amount of loops executions*/
        int i = 0, runs = 10;
        double average = 0.0;
        seed(); 
        int pass = 0;

        /*Lucky loop*/
        range = 250;
        printf("\nbegin \"lucky\" run:\n\n");
        for(pass = 1; pass <= 3; pass++){
        	printf("run\tn\ttime\n");
	        average = 0;
	        int *lucky_array = malloc(sizeof(int) * (range + 1));
	        for(i = 0; i <= runs; i++){
	        	clock_t start, end;
	        	start = clock();
	        	lucky_array = lucky(lucky_array, range);
	        	end = clock();
	        	average = average + ((double) (end - start) / CLOCKS_PER_SEC);
	        	printf("%d\t%d\t%0.4lf\n",i , range, (double) (end - start) / CLOCKS_PER_SEC);
	        }
	        printf("\navg for n = %d for \"lucky\" is %0.4lf seconds\n\n", range, (average / runs));
	        free(lucky_array);

	        if(pass == 1)
	        	range = 500;
	        else if(pass == 2)
	        	range = 1000;
	        else if(pass == 3)
	        	range = 2000;
    	}

        /*used loop*/
        range = 2500, average = 0;
        printf("begin \"used\" run:\n");
        for(pass = 1; pass <= 6; pass++){
        	printf("\nrun\tn\ttime\n");
	        int *used_array = malloc(sizeof(int) * (range + 1)), j = 0;
	        for(i = 0; i <= runs; i++){
	        	clock_t start, end;
	        	start = clock();
	        	used_array = used(used_array, range);
	        	end = clock();
	        	average = average + ((double) (end - start) / CLOCKS_PER_SEC);
	        	for(j = 0; j <= range; j++)
	        			used_array[j] = 0;
	        	printf("%d\t%d\t%0.4lf\n",i , range, (double) (end - start) / CLOCKS_PER_SEC);
	        }
	        printf("\navg for n = %d for \"used\" is %0.4lf seconds\n", range, (average / runs));
	        free(used_array);

	        if(pass == 1)
	        	range = 5000;
	        else if(pass == 2)
	        	range = 10000;
	        else if(pass == 3)
	        	range = 20000;
	        else if(pass == 4)
	        	range = 40000;
	        else
	        	range = 80000;
    }
        /*knuth loop*/
        average = 0, range = 10000;
        printf("\nbegin \"knuth\" run\n");
        for(pass = 1; pass <= 8; pass++){
        	printf("run\tn\ttime\n");
	        int *knuth_array = malloc(sizeof(int) * (range + 1));
	        for(i = 0; i <= runs; i++){
	        	clock_t start, end;
	        	start = clock();
	        	knuth_array = knuth(knuth_array, range);
	        	end = clock();
	        	average = average + ((double) (end - start) / CLOCKS_PER_SEC);
	        	printf("%d\t%d\t%0.4lf\n",i , range, (double) (end - start) / CLOCKS_PER_SEC);
	        }
	        printf("\navg for n = %d for \"knuth\" is %0.4lf seconds\n\n", range, (average / runs));
	        free(knuth_array);

	        if(pass == 1)
		        range = 10000;
		    else if(pass == 2)
		        range = 20000;
		    else if(pass == 3)
		        range = 40000;
		    else if(pass == 4)
		        range = 80000;
		    else if(pass == 5)
		        range = 160000;
		    else if(pass == 6)
		    	range = 320000;
		    else if(pass == 7)
		    	range = 640000;
    	}
		return 0;
}

/**
 * returns a uniform random integer between 0 <= rand num <= range
 * @param range, defines the range of the random number [1,range]  
 * @return the generated random number
 */
int nrandint(int range) 
{
	return rand() % (range + 1);
}

/**
 * call this to seed the random number generator rand()
 * uses a simple seed -- the number of seconds since the epoch 
 * call once before using nrandint and similiar functions that call rand()
 */
void seed(void) 
{
  	srand((unsigned int)time(NULL));
}

/**
 * Stores an integer into the current index if integer is not already used.
 * @param a_array the array for integers to be stored in and returned
 * @param range the amount of numbers to be stored
 * @return the array with the stored numbers
 */
int *lucky(int *a_array, int range)
{
	int i = 0, ran = 0, j = 0;

	for (i = 0; i < range; i++){
		ran = nrandint(range - 1) + 1;
		if (i == 0){
        		a_array[0] = ran;
      	}
        for(j = 0; j < i; j++){
       		if(a_array[j] == ran){
       			/*decrement back one index to regenerate a different number*/
       			i--;
       			break;
       		}
        	else {
        		a_array[i] = ran;
        	}        	
        }
    }
    return a_array;
}

/**
 * Stores an integer into the array if the integer is not already used
 * @param used_array the array to store numbers in
 * @param range to be stored
 * @return the array with the stored numbers
 */
int *used(int *used_array, int range)
{
	int ran = 0, i = 0;
	int *array_a = malloc(sizeof(int) * (range + 1));

	for(i = 0; i < range; i++){
		ran = nrandint(range- 1) + 1;
		while(used_array[ran] == 1){
			ran = nrandint(range - 1) + 1;
		}
		if(used_array[ran] == 0){
			array_a[i] = ran;
			used_array[ran] = 1;
		}
	}
	free(array_a);
	return used_array;
}

/**
 * stores random integers in array that are not already there using knuth method
 * @param arracy_c the passed in array for storing the integers
 * @param range the passed in number (n)
 * @return the finished array
*/
int *knuth(int *array_c, int range)
{
	int i = 0, ran = 0;

	for (i = 1; i < range; i++){
		ran = nrandint((range - 1) + 1);
		swap(&array_c[i], &array_c[ran]);
	}

	return array_c;
}
/**
 * Swaps two array elements
 * @param arracy_c the first array element
 * @param array_tmp the seond array element
 * @return nothing
*/
void swap(int *array_c, int *array_tmp)
{
	int temp;
	temp = *array_c;
	*array_c = *array_tmp;
	*array_tmp = temp;
}