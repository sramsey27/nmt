#include "lex_word.h"

int main(int argc, char *argv[])
{
	/* Metamorphosis was run at 500 with load factor of 4.07 */
	/* Gadsby was run at 1500 with load factor of 3.32 */

    FILE* in_file;
    FILE* common_file;

    in_file = fopen(argv[1], "r");
    assert(in_file);
    common_file = fopen("1000_words.txt", "r");
    assert(common_file);

    char buf[LEN] = {0};
    int i = 0;
    unsigned key = 0;

    assert(argv[2]);
    size_t tbl_size = strtol(argv[2], NULL, 10);
    size_t tbl_size_common = 250;

    struct list_t **common = malloc(sizeof(struct list_t*) * tbl_size_common);
    common = create_hashtable(tbl_size_common, common);

    common = generate_common(common, tbl_size_common, common_file);
    fclose(common_file);

    int j = 0, c = 0, cur_length = 0;;
    cur_length = 0, key = 0, i = 0;

    struct list_t **sentinel = malloc(sizeof(struct list_t*) * tbl_size);
    sentinel = create_hashtable(tbl_size, sentinel);
    int length = 0, uncommon_count = 0;
    //i = 0;
    struct node_t *tmp = NULL; 
    j = 0, i = 0;
    char* tmp_word = NULL, *tmp_buf = NULL;
    while((c = fgetc(in_file)) != EOF){
    	if(c > 127){
    		continue;
    	}
        if(!isspace(c)){
            buf[i] = c;
            i++;
        } 
        else{
            if(i == 0){
                continue;
            }
            else{
	            buf[i] = '\0';
	            i = 0;
	            rstrip(buf);
	            tmp_word = strip_s(buf);
	           	length = strlen(tmp_word);
	        	while(length > 0){
	        		if(tmp_word[length] == '-'){
	        			if(isspace(tmp_word[length + 2])){
	        				tmp_buf = malloc(sizeof(char*) * strlen(&tmp_word[length + 2]));
	        				strcpy(tmp_buf, &tmp_word[length + 2]);
	        				key = djb_hash(&tmp_buf[length + 2]);
	        				if((table_search(common, tmp_buf, key % tbl_size_common))){
	            				buf[0] = 0;
	               				continue;           
	            			}

	        				uncommon_count = uncommon_count + insert_hash(sentinel, tbl_size, tmp_buf, key % tbl_size);
	        				free(tmp_buf);
	        			}
	        			else{
	        				tmp_buf = malloc(sizeof(char*) * strlen(&tmp_word[length + 1]));
	        				strcpy(tmp_buf, &tmp_word[length + 1]);
	        				key = djb_hash(&tmp_buf[length + 1]);
	        				if((table_search(common, tmp_buf, key % tbl_size_common))){
	            				buf[0] = 0;
	               				continue;           
	            			}
	        				uncommon_count = uncommon_count + insert_hash(sentinel, tbl_size, tmp_buf, key % tbl_size);
	        				free(tmp_buf);
	        			}
	        		}
	        		length--;
	        	}

	        	tmp_word = hyp_check(tmp_word);
	        	key = 0;
	            tolowercase(tmp_word);
	            key = djb_hash(tmp_word);
	            if(buf[0] == ' ' || buf[0] == '\n'){
	                buf[0] = 0;
	                continue;
	            }
	            if((table_search(common, tmp_word, key % tbl_size_common))){
	            	buf[0] = 0;
	                continue;           
	            }
	            else{
	            	uncommon_count = uncommon_count + insert_hash(sentinel, tbl_size, tmp_word, key % tbl_size);
	            }
	        }
        buf[0] = 0;
        }
    }
    char* uncommon_array[uncommon_count];

    j = 0;
    /* Insert uncommon into another array */
    for(i = 0; i < tbl_size; i++){
        if(sentinel[i]->head == NULL){
            continue;
        }
        else{
            tmp = sentinel[i]->head;
            while(tmp != NULL){
                uncommon_array[j] = tmp->word; 
                j++;
                tmp = tmp -> next;
            }
        }
    }

    qsort(uncommon_array, uncommon_count, sizeof(char*), comp);

    for(i = 0; i < uncommon_count; i++){
    	key = djb_hash(uncommon_array[i]);
    	key = key % tbl_size;
    	tmp = sentinel[key]->head;
    	if(tmp == NULL){
    		continue;
    	}
        printf("%s\t%d\n", uncommon_array[i], tmp->count);
    }

    //double load = (double)uncommon_count/(double)tbl_size;
    //printf("LOAD FACTOR = %0.2lf\n", load);
    
    free_everything(sentinel, common, tbl_size, tbl_size_common);
    fclose(in_file);

    return 0;
}