#include "stack.h"

/**
 * Initalizes a new stack
 * @return the newly created stack
 */
stack new_stack(void)
{
	stack new_stack = malloc(sizeof(struct stack_t));

	new_stack->count = 0;
	new_stack->head = NULL;

	return new_stack;
}

/**
 * Pushes an element to the stack
 * @param stk the structure/stack
 * @param data user data
 */
void push(stack stk, void *data)
{
	struct node_t *list = malloc(sizeof(struct node_t));

	list->data = data;
    list->next = stk->head;
    stk->head = list;
    stk->count++;
}

/**
 * pop the head off the stack then returns the element
 * @param stk the structure or entire stack
 * @return the popped element
 */
void* pop(stack stk)
{
	struct node_t *tmp = NULL;
	void* element = NULL;

	tmp = stk->head;
	stk->head = stk->head->next;
	element = tmp->data;
	free(tmp);

	stk->count--;

	return element; 
}

/**
 * returns the element within the head
 * @param stk the structure or entire stack
 * @return the element within the head
 */
void* peek(stack stk)
{
	void* element = NULL;
	element = stk->head->data;

	printf(" = %lf\n", *(float*)element);

	return element;
}

/**
 * frees the enitre stack from memory
 * @param stk the enitre stack
 */
void free_stack(stack stk)
{
	struct node_t *cur = NULL;
	struct node_t *tmp = NULL;

	if(is_stack_empty(stk) == 1){
		return;
	}

	else{
		cur = stk->head;

		while(cur != NULL){
			tmp = cur->next;
			free(cur);
			cur = tmp; 
		}
	}
	free(stk);
}

/**
 * checks to see if the stack is empty
 * @param stk the entire stack
 * @return 1 if empty, returns 0 otherwise
 */
int is_stack_empty(stack stk)
{
	if(stk->head == NULL){
		return 1;
	}
	else{
		return 0;
	}
}

/**
 * returns the count of stack elements
 * @param stk the entire stack
 * @return the current count
 */
int stack_count(stack stk)
{
	return stk->count;
}