#include "stack.h"
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

int main(int argc, char *argv[])
{
	FILE* in_file;
	in_file = fopen(argv[1], "r");
	/* Error Check */
	assert(in_file);

	stack chk_stk = new_stack();
	stack line_stack = new_stack();
	int line_count = 1, column_count = 1;

	char* input = NULL;
	int* tmp_line = NULL;

	int c;
	while((c = fgetc(in_file)) != EOF){
			if(c == ' ' || c == '\t' || c == '*' || c == '-' || isalpha(c)|| c == ';'){
				continue;
			}
			else if(c == '('){
				/* If so Push onto stack */
				input = malloc(sizeof(char));
				tmp_line = malloc(sizeof(int));
				*input = c;
				*tmp_line = line_count;
				//printf("Pushing to stack\n");
				push(chk_stk, input);
				push(line_stack, tmp_line);
			}
			else if(c == ')'){
				/* If so Pop off a ( from the stack */
				if(!is_stack_empty(chk_stk)){
					input = pop(chk_stk);
					tmp_line = pop(line_stack);
					if(input != NULL){
						free(input);
					}
					if(tmp_line != NULL){
						free(tmp_line);
					}
				}
				else{
					printf("Parenthesis Error on Line: %d\n", line_count);
				}
			}
			else if(c == '\n'){
				line_count++;
			}
			column_count++;
	}
	fclose(in_file);

	while(!is_stack_empty(chk_stk)){
			tmp_line = pop(line_stack);
			input = pop(chk_stk);
			printf("Parenthesis Error on line: %d\n", *tmp_line);
			free(tmp_line);
			free(input);
	}

	free_stack(chk_stk);
	free_stack(line_stack);
	free(chk_stk);
	free(line_stack);


	return 0;
}