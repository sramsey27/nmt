#ifndef _LEX_H_
#define _LEX_H_

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

struct node_t {
        int count;
        char * word;
        struct node_t *next; 
};

struct list_t {
        int nnodes; /* number of nodes list contains */
        struct node_t *head; /*head of the list */
};

#define LEN 1000

void rstrip(char *buf);
unsigned djb_hash(void *key);
struct list_t **create_hashtable(size_t tbl_size, struct list_t **sentinel);
int table_search(struct list_t **sentinel, char* in_word, unsigned key);
struct list_t **insert_head(struct list_t **sentinel, struct node_t *node, unsigned key);
void print_node(struct node_t *node);
void tolowercase(char *buf);
void free_everything(struct list_t **sentinel, struct list_t **common, size_t tbl_size, size_t tbl_size_common);
char* strip_s(char *buf);
int comp(const void *string1, const void *string2);
struct list_t **generate_common(struct list_t **common, size_t tbl_size_common, FILE* common_file);
char* hyp_check(char* buf);
int insert_hash(struct list_t **sentinel, size_t tbl_size, char* tmp_word, unsigned key);

#endif