#include "lex.h"
#include "stack.h"
#include <stdio.h>
#include "stack.h"
#include <stdlib.h>
#include <math.h>

int calc(stack new_stack);

#define MAX_NUM 512   //max chars in number, increase as necessary

int calc(stack new_stack)
{
        int token;
        float* product;
        float* product2;
        float* answer;
        float* input = NULL;
        char num[MAX_NUM];
        char c;

        while (1) {
                switch (token = get_tok(num, sizeof(num))) {
                case TOK_NUM:
                        input = malloc(sizeof(float));
                        *input = strtod(num, NULL);
                        push(new_stack, input);
                        //printf("TOK_NUM: %s (%lf)\n", num, strtod(num, NULL)); 
                        break;
                case TOK_MULT:
                        product = (float*)pop(new_stack);
                        product2 = (float*)pop(new_stack);
                        *product *= *product2;
                        //printf("Answer = %0.2lf\n\n", *product);
                        free(product2);
                        push(new_stack, product);
                        break;    
                case TOK_DIV:
                        product = (float*)pop(new_stack);
                        product2 = (float*)pop(new_stack);
                        //printf("\nAnswer = %0.2lf\n", *product);
                        *product /= *product2;
                        free(product2);
                        push(new_stack, product);
                        break;
                case TOK_ADD:
                        product = (float*)pop(new_stack);
                        product2 = (float*)pop(new_stack);
                        *product += *product2;
                        //printf("Answer = %0.2lf\n\n", *product);
                        free(product2);
                        push(new_stack, product);
                        break;
                case TOK_SUB:
                        product = (float*)pop(new_stack);
                        product2 = (float*)pop(new_stack);
                        *product2 -= *product;
                        //printf("Answer = %0.2lf\n\n", *product2);
                        free(product);
                        push(new_stack, product2);
                        break;
                case TOK_EXP:
                        product = (float*)pop(new_stack);
                        product2 = (float*)pop(new_stack); 
                        *product = pow(*product2, *product);
                        //printf("Answer = %0.2lf\n\n", *product);
                        free(product2);
                        push(new_stack, product);
                        break;
                case TOK_NL:
                        answer = (float*)pop(new_stack);
                        printf("Answer: %f\n", *answer);
                        free(answer);
                        return TOK_NL;
                        break;
                case TOK_QUIT:
                        printf("goodbye\n");
                        free_stack(new_stack);
                        free(new_stack);
                        exit(EXIT_SUCCESS);
                case TOK_EOF:
                        free_stack(new_stack);
                        exit(EXIT_SUCCESS);
                case TOK_ERR:
                        //eat the remaining characters in stdin
                        while ((c = getchar()) != '\n')
                                c++;
                        return TOK_ERR; 
                }
        }    
}

int main(void)
{
        int tok = TOK_NL;
        stack new_stk = new_stack();
        char *prompt = "postfix> ";

        printf("Welcome to the postfix calculator\n");
        printf("Enter a postfix expression -- all expressions are converted to floating point\n");
        printf("Enter CTRL + C to quit or type \"quit\"\n");

        while (1) {
                if (tok == TOK_NL)
                        printf("%s", prompt);
                
                tok = calc(new_stk);

                //an error occur?
                if (tok == TOK_ERR) {
                       fprintf(stderr, "error in postfix expression\n");
                       tok = TOK_NL; 
                }

        }

        return 0;
}
