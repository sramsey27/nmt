#include "lex_word.h"
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

/**
 * This ones a bit tricky, it returns the left half of a hypenated word
 * @param buf the input word
 * @return left half demlimited by hypen
 */
char* hyp_check(char* buf)
{
	int i = 0;
	char *token;
	for(i = 0; i < strlen(buf); i++){
		if(buf[i] == '-'){
			token = strtok(buf, "-");
			//printf("TOKEN = %s\n", token);
			return token;
		}
		else if(buf[i] == '-' && buf[i + 1] == '-'){
			token = strtok(buf, "--");
			return token;
		}
		else if(isspace(buf[i]) && buf[i + 1] == '-' && buf[i + 2] == '-'){
			token = strtok(buf, "-");
			rstrip(token);
			return token;
		}
	}
	return buf;
}

/**
 * Inserts passed in element into hash table
 * @param sentinel the uncommon list
 * @param tbl_size the uncommon table size
 * @param tmp_word the passed in word
 * @param key the hash table key for element to be stored
 */
int insert_hash(struct list_t **sentinel, size_t tbl_size, char* tmp_word, unsigned key)
{
	struct node_t *node = NULL;
    struct node_t *tmp = NULL;
	int found = 0, cur_length = 0, uncommon_count = 0;

	tmp = sentinel[key]->head;
    while(tmp != NULL){
     	//str_1 = strlen(tmp_word);
       	if(!strcmp(tmp->word, tmp_word)){
           	found = 1;
           	tmp->count++;
          	break;
       	}
       	tmp = tmp->next;
    }
    if(!found){
        cur_length = strlen(tmp_word) + 1;
        node = malloc(sizeof(struct node_t));
        node->count = 1;
        node->word = malloc(sizeof(char) * cur_length + 1);
        strncpy(node->word, tmp_word, cur_length + 1); 
        insert_head(sentinel, node, key);
        uncommon_count++;
    }   
	return uncommon_count; 
}

/**
 * Generates the common words hash table
 * @param common the reserved spot for the hash table
 * @param tbl_size_common the common hash table size
 * @param common_file the common words txt file
 */
struct list_t **generate_common(struct list_t **common, size_t tbl_size_common, FILE* common_file)
{

	struct node_t *common_node = NULL;
    int cur_length = 0, length = 0;
    unsigned key = 0;
    char buf[LEN];

    while(fgets(buf, LEN, common_file)){

        rstrip(buf);
        length = strlen(buf);
        key = djb_hash(buf);
        key = key % tbl_size_common;
        cur_length = strlen(buf);
        common_node = malloc(sizeof(struct node_t));
        common_node->word = malloc(sizeof(char) * cur_length + 1);

        strncpy(common_node->word, buf, cur_length + 1);

        insert_head(common, common_node, key);
    }
    return common;
}   

/**
 * convert string to lowercase 
 * @param buf the input string
 * @return nothing
 */
void tolowercase(char *buf)
{
        int length = strlen(buf);
        int i = 0;

        while(i < length){
                buf[i] = tolower(buf[i]);
                i++;
        }
}

/**
 * trim off whitespace from the right
 * @param buf, the current line to be stripped 
 * @return nothing
 */
void rstrip(char *buf)
{
    int length = strlen(buf);

    while (length > 0) {
        if(isspace(buf[length])){
            buf[length] = '\0';
        }
        if(ispunct(buf[length]) || buf[length] == '\n'){
            buf[length] = '\0';
        }
        if(isalpha(buf[length])){
            break;
        }
    length--;
    }
}

/**
 * Eliminates white space from the left
 * @param buf the input word
 * @return the address of the string without whitespace, otherwise as is.
 */
char* strip_s(char *buf)
{
    int i = 0;
    for(i = 0; i < strlen(buf); i++){  
        if(isalpha(buf[i])){
            return &buf[i];
        }
        if(isspace(buf[i]) || ispunct(buf[i]) || buf[i] == '\n'){
            buf[i] = '\0'; 
        }
    }
    return buf;
}

/**
 * Generates a key based on Bernstein’s Hash
 * @param key the current element to store
 * @return generated key
 */
unsigned djb_hash(void *key)
{
    unsigned char * p = key ;
    unsigned h = 0;

    while (*p){
        h = 33 * h + *p ;
        p ++;
    }

    return h;
}

/**
 * Determines if a word is present in the hash table
 * @param sentinel the sentinel containing the data
 * @param in_word the user input word
 * @param key the generated key/index
 * @return 1 if word is found, 0 otherwise
 */
int table_search(struct list_t **common, char* in_word, unsigned key)
{
        struct node_t *tmp = NULL;
        //int length = 0;

        if(common[key] == NULL){
                return 0;
        }
        else{
            tmp = common[key]->head;
            while(tmp != NULL){
                if(strncmp(tmp->word, in_word, strlen(in_word)) == 0){
                    return 1;
                }
            	tmp = tmp->next;
         	}
        }
        return 0;
}

/**
 * Generates the hash table
 * @param dict the sentinel to put data in
 * @param tbl_size the table size defined by the user
 */
struct list_t **create_hashtable(size_t tbl_size, struct list_t **sentinel)
{
    int i = 0;
    for(i = 0; i < tbl_size; i++){
  	    sentinel[i] = malloc(sizeof(struct list_t));
        sentinel[i]->head = NULL;
        sentinel[i]->nnodes = 0;
    }
    return sentinel;
}

/**
 * inserts the word into the current indexes linked list
 * @param sentinel the sentinel containing the data
 * @param node the node to be inserted
 * @param key the generated key/index
 * @return the changed sentinel
 */
struct list_t **insert_head(struct list_t **sentinel, struct node_t *node, unsigned key)
{
    node->next = sentinel[key]->head;
    sentinel[key]->head = node;
    sentinel[key]->nnodes++;
    return sentinel;
}

/**
 * Prints the passed in node
 * @param node the passed in node to be printed
 */
void print_node(struct node_t *node)
{
        if (node != NULL){
            printf("%s", node->word);
        }
        else{ 
            printf("\nNode is currently NULL.\n");
        }
}

/**
 * Frees both hash tables from memory
 * @param sentinel the uncommon hash
 * @param common the common words hash
 * @param tbl_size the table size of uncommon
 * @param tbl_size_common the table size of the common hash
 * @return nothing
 */
void free_everything(struct list_t **sentinel, struct list_t **common, size_t tbl_size, size_t tbl_size_common)
{
        struct node_t *temp = NULL;
        struct node_t *cur = NULL;
        int i = 0;
        for(i = 0; i < tbl_size; i++){
            if(sentinel[i]->head == NULL){
                continue;
            }
            else{
   	            temp = sentinel[i]->head;
                cur = NULL;
                while(temp != NULL){
                    cur = temp->next;
                    free(temp->word);
                    free(temp);
                    temp = cur;
                }
            }
        }
        for(i = 0; i < tbl_size; i++){
                if(sentinel[i] != NULL){
                        free(sentinel[i]);
                }
        }
        free(sentinel);

        for(i = 0; i < tbl_size_common; i++){
                if(common[i]->head == NULL){
                        continue;
                }
                else{
                        temp = common[i]->head;
                        cur = NULL;
                        while(temp != NULL){
                                cur = temp->next;
                                free(temp->word);
                                free(temp);
                                temp = cur;
                        }
                }
        }
        for(i = 0; i < tbl_size_common; i++){
                if(common[i] != NULL){
                        free(common[i]);
                }
        }
        free(common);
}

/**
 * (used for qsort) this compares two strings to see if they match or not
 * @param string1 input string 1
 * @param string2 input string 2
 * @return 0 if they match, nonzero otherwise
 */
int comp(const void *string1, const void *string2)
{
    return strcmp(*(char**)string1, *(char**)string2);

}