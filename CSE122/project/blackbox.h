#ifndef BLACKBOX_H
#define BLACKBOX_H

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

/* void function_1(unsigned long *list, unsigned long n); */
/* void function_2(unsigned long *list,  unsigned long n); */
/* void function_3(unsigned long n); */
/* void function_4(unsigned long n); */
/* void function_5(unsigned long n); */
/* void function_6(unsigned long n); */
/* void function_7(unsigned long n); */

void function_1(unsigned long n);
void function_2(unsigned long n);
void function_3(unsigned long *list,  unsigned long n);
void function_4(unsigned long n);
void function_5(unsigned long n);
void function_6(unsigned long *list, unsigned long n);
void function_7(unsigned long n);

void seed(void);
unsigned long nrandint(unsigned long range);
void swap(unsigned long *array_c, unsigned long *array_tmp);
unsigned long random_number(void);


#endif
