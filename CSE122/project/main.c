/**
 * @file main.c
 * 
 * @brief Program used to determine various function runtimes stores in blackbox
 *
 * @details Each function sotred in the blackbox is run though k times and timed with milliseconds
 * and CPU ticks which is then used to determine their runtime and complexity.
 *
 * @author Steven Ramsey
 *
 * @date 2/26/15
 *  
 * @todo Finish it
 *
 * @bug Segmentation fault possible when n is very large when reversing list
 *
 * @remark
 * 
 */

#include "blackbox.h"

int main(int argc, char*argv[])
{
	/*Command line argument input for range_n*/
	unsigned long range_n = (strtol(argv[1], NULL, 0));

	/*amount of times to repeat test*/
	unsigned long runs = 10;

	/*for random number generation*/
	seed();

	int msec, i = 0;

	/*There is a lot of commented out code due to testing purposes
	They are all working properly but I have left the random shuffle
	uncommented to demonstrate the valgrind script*/

	/*Function 1, 2, 4, 5, and 7 loop*/
	/*for(i = 1; i <= runs; i++){
		clock_t elasped_time;
		elasped_time = clock();

		//function_1(range_n);
		//function_2(range_n);
		//function_4(range_n);
		//function_5(range_n);
		//function_7(range_n);

		elasped_time = clock() - elasped_time;
		msec = (elasped_time * 1000) / CLOCKS_PER_SEC;
		printf("%ld\t",range_n);// X value, n value
		printf("%d.%03d\n", msec /1000 , msec %1000); //Y value, second
		//printf("\t%d\n", (int) elasped_time);// Y Value, ticks
		//range_n += 1; //This would be change depending on the function.*/

		int j = 0;
		for(i = 1; i <= runs; i++){

			unsigned long *f_list = malloc(range_n * sizeof(unsigned long));

			//Ascending Sort
			/*for(j = 0; j < range_n; j++){
				*(f_list + j) = j;
			}*/

			//Reverse Sort
			/*int k = 0;
			j = range_n - 1;
			for(j = range_n - 1, k = 0; j >=  0; j--, k++){
				*(f_list + k) = j;
			}*/

			//Puesdo Random
			int k = 0;
			for(j = 0; j < range_n; j++){
				*(f_list + j) = j;
			}
			for (k = range_n - 1; k > 0; k--) {
				j = random_number() % (k + 1);
				swap((f_list + k) ,(f_list + j));
			}

			clock_t elasped_time;
			elasped_time = clock();

			//function_3(f_list, range_n);
			function_6(f_list, range_n);

			elasped_time = clock() - elasped_time;
			msec = (elasped_time * 1000) / CLOCKS_PER_SEC;
			printf("%ld\t",range_n);// X value, n value
			//printf("\t%d\n", (int) elasped_time);// Y Value, ticks
			printf("%d.%03d\n", msec /1000 , msec %1000); //Y value, second
			range_n += range_n;
			//range_n += 1; //used in some cases where n had to be small
			free(f_list);

		}

	return 0;
}
/**
 * Generates a random number for the puesdo random list
 * @return randomly generated number
 */
unsigned long random_number(void)
{
	unsigned long n = 0, p = 0, r = 0;
	int i = 0;

	for(i = 0, p = 1; i < 8; i++ , p *= 256) {
		r = random() % 256;
		n += (r * p);
	}
	return n;
}

/**
 * Swap two list elements (used in pseudo random list)
 * @param list_a the first element to be switched with list_b element
 * @param list_B the second element to be switched with list_a element
 * @return nothing
 */
void swap(unsigned long *list_a, unsigned long *list_b)
{
	int temp;
	temp = *list_a;
	*list_a = *list_b;
	*list_b = temp;
}

/**
 * call this to seed the random number generator rand()
 * uses a simple seed -- the number of seconds since the epoch 
 * call once before using nrandint and similiar functions that call rand()
 */
void seed(void) 
{
  	srand((unsigned int)time(NULL));
}