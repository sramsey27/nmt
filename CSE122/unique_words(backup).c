#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define LEN 1000

struct node_t {
        int count;
        char * word;
        struct node_t *next; 
};

struct list_t {
        int word_count; /* number of nodes list contains */
        struct node_t *head; /*head of the list */
};

struct node2_t {
    char* word;
    struct node2_t *next;
};

struct common_t{
        int nnodes;
        struct node2_t *head;

};

void rstrip(char *buf);
unsigned djb_hash(void *key);
struct list_t **create_hashtable(size_t tbl_size, struct list_t **sentinel);
int table_search(struct common_t **sentinel, char* in_word, unsigned key);
struct list_t **insert_head(struct list_t **sentinel, struct node_t *node, unsigned key);
struct common_t **insert_head2(struct common_t **common, struct node2_t *node, unsigned key);
void print_node(struct node_t *node);
void tolowercase(char *buf);
void free_everything(struct list_t **sentinel, struct common_t **common, size_t tbl_size, size_t tbl_size_common);
char* strip_s(char *buf);
int comp(const void *string1, const void *string2);

int main(int argc, char *argv[])
{

    FILE* in_file;
    FILE* common_file;

    in_file = fopen(argv[1], "r");
    assert(in_file);
    common_file = fopen("1000_words.txt", "r");
    assert(common_file);

    char buf[LEN] = {0};
    int i = 0, length = 0;
    assert(argv[2]);
    size_t tbl_size = strtol(argv[2], NULL, 10);

    size_t tbl_size_common = 250;
    unsigned key = 0;

    struct common_t **common = malloc(sizeof(struct common_t*) * tbl_size);
    for(i = 0; i < tbl_size_common; i++){
        common[i] = malloc(sizeof(struct common_t));
        common[i]->head = NULL;
        common[i]->nnodes = 0;
    }

    struct node2_t *common_node = NULL;
    int cur_length = 0;

    i = 0;
    while(fgets(buf, LEN, common_file)){

        rstrip(buf);
        length = strlen(buf);

        key = djb_hash(buf);
        key = key % tbl_size_common;

        cur_length = strlen(buf);

        common_node = malloc(sizeof(struct node2_t));
        common_node->word = malloc(sizeof(char) * cur_length + 1);
        strncpy(common_node->word, buf, cur_length + 1);

        insert_head2(common, common_node, key);
        i++;
    }
    fclose(common_file);

    struct node2_t *tmp2 = NULL;
    int j = 0;

    int c;
    cur_length = 0;
    key = 0;
    i = 0;

    struct node_t *node = NULL;
    struct node_t *tmp = NULL;

    struct list_t **sentinel = malloc(sizeof(struct list_t*) * tbl_size);
    sentinel = create_hashtable(tbl_size, sentinel);

    i = 0;
    int found = 0, uncommon_count = 0;
    char* tmp_word = NULL;
    while((c = fgetc(in_file)) != EOF){
        if(!isspace(c)){
            buf[i] = c;
            i++;
        }
        
        else{
            if(i == 0){
                continue;
            }
            buf[i] = '\0';
            i = 0;
            rstrip(buf);
            tmp_word = strip_s(buf);
            //printf("%s\n", tmp_word);
            key = djb_hash(tmp_word);
            key = key % tbl_size;
            if(buf[0] == ' ' || buf[0] == '\n'){
                buf[0] = 0;
                continue;
            }
            if((table_search(common, tmp_word, key))){
                //printf("%s is in common\n", tmp_word);            
            }
            else{
                tmp = sentinel[key]->head;
                //printf("%s NOT COMMON\n", tmp_word);
                found = 0;
                while(tmp != NULL){
                    if(!strcmp(tmp->word, tmp_word)){
                        //printf("%s FOUND\n", tmp_word);
                        found = 1;
                        tmp->count++;
                        break;
                    }
                    tmp = tmp->next;
                }
                if(!found){
                        //printf("%s NOT FOUND\n", tmp_word);
                        cur_length = strlen(tmp_word) + 1;
                        node = malloc(sizeof(struct node_t));
                        node->count = 1;
                        node->word = malloc(sizeof(char) * cur_length + 1);
                        strncpy(node->word, tmp_word, cur_length + 1); 
                        insert_head(sentinel, node, key);
                        uncommon_count++;  
                        //printf("UNCOMMON COUNT = %d", uncommon_count);
                }
            }
        buf[0] = 0;
        }
    }

    printf("COUNT = %d\n", uncommon_count);

    char* uncommon_array[uncommon_count];

    /*for(i = 0; i < uncommon_count; i++){
        uncommon_array[i] = malloc(sizeof(char) * 30);
    }*/

    int counter = 0;
    int count_w[uncommon_count];

    for(i = 0; i < tbl_size; i++){
        if(sentinel[i]->head == NULL){
            continue;
        }
        else{
            tmp = sentinel[i]->head;
            while(tmp != NULL){
                uncommon_array[j] = tmp->word;
                count_w[j] = tmp->count;
                //printf("%s\n", uncommon_array[j]); 
                j++;
                tmp = tmp -> next;
            }
        }
    }
    printf("J = %d\n", j);

    /*for(i = 0, counter = 0; i < tbl_size; i++){
        tmp = sentinel[i]->head;
        while(tmp != NULL){
            //printf("TMP->WORD = '%s'\n", tmp->word);
            //printf("UNCOMMON_ARRAY = '%s'\n", uncommon_array[counter - 1]);
            strncpy(uncommon_array[counter], tmp->word, 30);

            count_w[i] = tmp->count;
            tmp = tmp->next;
            counter++;
          }
    }*/
    qsort(uncommon_array, uncommon_count, sizeof(char*), comp);

    for(i = 0; i < uncommon_count; i++){
        printf("%s\t%d\n", uncommon_array[i], count_w[i]);
    }
    

    //fclose(in_file);
    //char* word_array = NULL;

    /*while(i < tbl_size){
        if(sentinel[key] == NULL){
            continue;
        }
        strcpy((word_array + i * 30), sentinel[key];
        i++;
    }*/

    free_everything(sentinel, common, tbl_size, tbl_size_common);
    sentinel = NULL;
}    

/**
 * convert string to lowercase 
 * @param buf the input string
 * @return nothing
 */
void tolowercase(char *buf)
{
        int length = strlen(buf);
        int i = 0;

        while(i < length){
                buf[i] = tolower(buf[i]);
                i++;
        }
}

/**
 * trim off whitespace from the right
 * @param buf, the current line to be stripped 
 * @return nothing
 */
void rstrip(char *buf)
{
        int length = strlen(buf);

        while (length > 0) {
                if(isspace(buf[length])){
                    buf[length] = '\0';
                }
                if(ispunct(buf[length]) || buf[length] == '\n'){
                        buf[length] = '\0';
                }
                if(isalpha(buf[length])){
                    break;
                }
        length--;
        }

}


char* strip_s(char *buf)
{
        int i = 0;
        for(i = 0; i < strlen(buf); i++){  
            if(isalpha(buf[i])){
                return &buf[i];
            }
            if(isspace(buf[i]) || ispunct(buf[i]) || buf[i] == '\n'){
                buf[i] = '\0'; 
            }
        }

}

/**
 * Generates a key based on Bernstein’s Hash
 * @param key the current element to store
 * @return generated key
 */
unsigned djb_hash(void *key)
{
    unsigned char * p = key ;
    unsigned h = 0;

    while (*p){
        h = 33 * h + *p ;
        p ++;
    }

    return h;
}

/**
 * Determines if a word is present in the hash table
 * @param sentinel the sentinel containing the data
 * @param in_word the user input word
 * @param key the generated key/index
 * @return 1 if word is found, 0 otherwise
 */
int table_search(struct common_t **common, char* in_word, unsigned key)
{
        struct node2_t *tmp = NULL;

        if(common[key] == NULL){
                return 0;
        }
        else{
                tmp = common[key]->head;
                while(tmp != NULL){
                        if(strcmp(tmp->word, in_word) == 0){
                                return 1;
                        }
                        tmp = tmp->next;
                }
        }
        return 0;
}

/**
 * Generates the hash table
 * @param dict the sentinel to put data in
 * @param tbl_size the table size defined by the user
 */
struct list_t **create_hashtable(size_t tbl_size, struct list_t **sentinel)
{

        int i = 0;
        for(i = 0; i < tbl_size; i++){
                sentinel[i] = malloc(sizeof(struct list_t));
                sentinel[i]->head = NULL;
                sentinel[i]->word_count = 0;
        }

        return sentinel;

}

/**
 * inserts the word into the current indexes linked list
 * @param sentinel the sentinel containing the data
 * @param node the node to be inserted
 * @param key the generated key/index
 * @return the changed sentinel
 */
struct list_t **insert_head(struct list_t **sentinel, struct node_t *node, unsigned key)
{
        node->next = sentinel[key]->head;
        sentinel[key]->head = node;
        sentinel[key]->word_count++;
        return sentinel;
}

struct common_t **insert_head2(struct common_t **common, struct node2_t *node, unsigned key)
{
        node->next = common[key]->head;
        common[key]->head = node;
        common[key]->nnodes++;
        return common;
}

/**
 * Prints the passed in node
 * @param node the passed in node to be printed
 */
void print_node(struct node_t *node)
{
        if (node != NULL){
                printf("%s", node->word);
        }

        else 
                printf("\nNode is currently NULL.\n");
}

void free_everything(struct list_t **sentinel, struct common_t **common, size_t tbl_size, size_t tbl_size_common)
{
        struct node_t *temp = NULL;
        struct node_t *cur = NULL;
        int i = 0;
        for(i = 0; i < tbl_size; i++){
                if(sentinel[i]->head == NULL){
                        continue;
                }
                else{
                        temp = sentinel[i]->head;
                        cur = NULL;
                        while(temp != NULL){
                                cur = temp->next;
                                free(temp->word);
                                free(temp);
                                temp = cur;
                        }
                }
        }
        for(i = 0; i < tbl_size; i++){
                if(sentinel[i] != NULL){
                        free(sentinel[i]);
                }
        }
        free(sentinel);
}

int comp(const void *string1, const void *string2)
{
    return strcmp(*(char**)string1, *(char**)string2);

}