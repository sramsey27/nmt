/**
 * @file fib_error.c
 * 
 * @brief file Generates the Fibonacci seq.
 *
 * @details Generates the Fibonacci seq while storing
 * its values in an array then comparing two different 
 * methods of getting the values
 *
 * @author Steven Ramsey
 *
 * @date 2/5/15
 *  
 * @todo 
 *
 * @bug none
 *
 * @remark
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

unsigned max_n(void);
double fib(unsigned n);
unsigned long * fib_array(unsigned n);

int main()
{
	unsigned int largest_n = 0, diff = 0;
	double error_p = 0.0;
	unsigned long true_val = 0, measured = 0;
	unsigned long *fib_a = NULL;
	
	largest_n = max_n();
	fib_a = fib_array(largest_n);
	
	int i = 0;
	printf("f [0]  =  %d\n", 0);
	for(i = 1; i < largest_n; i++){
		printf("f [%d]  =  %ld\n", i , fib_a[i - 1]);
	}

	for(i = 1; i < largest_n; i++){
		true_val = fib_a[i - 1]; //True Value
		measured = (unsigned long) fib(i - 1);// Measured Value

		if(measured == true_val){
			continue;
		}
		else {
			error_p = ((measured - true_val) / ((double)true_val))* 100; 
			diff = measured - true_val;
			printf("\nError in term %d\nTerms differ by %d\nError percent = %0.17lf%%\n", i, diff, error_p);
		}
	}
	free(fib_a);
	return 0;
}

/**
* determines the largest n that can be used before overflow
* @return n, largest value
*/
unsigned max_n(void)
{
	double n = 0.0;

	n = ((log(sqrt(5) * (ULONG_MAX)))/(log((1 + sqrt(5))/2)));
	n = (unsigned int) n;

	return n;
}

/**
* uses provided formula to find the n-th Fibonacci Term
* @param n, largest n before overflow
* @return n-th fibonacci term
*/
double fib(unsigned n)
{
	double nth_term = 0.0;

	nth_term = (pow(((1 + sqrt(5)) / 2), n + 1)) - (pow(((1 - sqrt(5)) / 2), n + 1));
	nth_term = (nth_term * (1 / sqrt(5)));

	return nth_term;
}

/**
* dynamically allocates an array to store fibonacci values
* @param n, largest n value
* @return array of fibonnacci values
*/
unsigned long * fib_array(unsigned n)
{
	unsigned long *f_array = malloc(sizeof(long) * (n + 1));
	int i = 2;

	f_array[0] = 1;
	f_array[1] = 1;

	for(; i < n + 1; i++) {
		f_array[i] = f_array[i - 1] + f_array[i - 2];
	}

	return f_array;
}