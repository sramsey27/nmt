/**
 * @file foo.c
 * 
 * @brief file Calcuates sums for various functions.
 *
 * @details Calcuates the sums for various functions
 * and printing them out in this format n|sum|seconds
 * 
 * @author Steven Ramsey
 *
 * @date 2/5/15
 *  
 * @todo 
 *
 * @bug none
 *
 * @remark
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>
#include <time.h>

unsigned long foo1(unsigned long n);
unsigned long foo2(unsigned long n);
unsigned long foo3(unsigned long n);
unsigned long foo4(unsigned long n);
unsigned long foo5(unsigned long n);
unsigned long foo6(unsigned long n);

int main()
{
	/*The code was restored to work as it does in foo.out*/
	unsigned long n = 1, sum = 0; 
	unsigned long power = 1; 
		/*for foo 1*/
		printf("foo1\n");
		for( ; n <= 1000000000; n = pow(10, power++)) {
		//for(n = 1000000000 ; n <= 10000000000; n += 1000000000){
			clock_t start, end;
    		start = clock();
   			sum = foo1(n);
    		end = clock();
    		printf("%ld|%ld|", n, sum);
    		printf("%3.24lf\n", (double) (end - start) / CLOCKS_PER_SEC);
    	}
		/*for foo 2*/
		n = 1, power = 1, sum = 0;
		printf("foo2\n");
		for( ; n <= 100000; n = pow(10, power++)) {
		//or(n = 100000 ; n <= 1000000; n += 100000){
			clock_t start, end;
    		start = clock();
   			sum = foo2(n);
    		end = clock();
    		printf("%ld|%ld|", n, sum);
    		printf("%3.24lf\n", (double) (end - start) / CLOCKS_PER_SEC);
    	}
    	
    	/*for foo 3*/
    	n = 1, power = 1, sum = 0;
		printf("foo3\n");
		for( ; n <= 1000; n = pow(10, power++)) {
		//for(n = 1000 ; n <= 10000; n += 1000){
			clock_t start, end;
    		start = clock();
   			sum = foo3(n);
    		end = clock();
    		printf("%ld|%ld|", n, sum);
    		printf("%3.24lf\n", (double) (end - start) / CLOCKS_PER_SEC);
    	}
    	/*for foo 4*/
    	n = 1, power = 1, sum = 0;
		printf("foo4\n");
		for( ; n <= 100000; n = pow(10, power++)) {
		//for(n = 100000; n <= 1000000; n += 100000){
			clock_t start, end;
    		start = clock();
   			sum = foo4(n);
    		end = clock();
    		printf("%ld|%ld|", n, sum);
    		printf("%3.24lf\n", (double) (end - start) / CLOCKS_PER_SEC);
    	}

    	/*for foo 5*/
    	n = 1, power = 1, sum = 0;
		printf("foo5\n");
		for( ; n <= 100; n = pow(10, power++)) {
    	//for(n = 100; n <= 1000; n += 100){
			clock_t start, end;
    		start = clock();
   			sum = foo5(n);
    		end = clock();
    		printf("%ld|%ld|", n, sum);
    		printf("%3.24lf\n", (double) (end - start) / CLOCKS_PER_SEC);
    	}

    	/*for foo 6*/
    	n = 1, power = 1, sum = 0;
		printf("foo6\n");
		for( ; n <= 100; n = pow(10, power++)) {
		//for(n = 100; n <= 1000; n += 100){
			clock_t start, end;
    		start = clock();
   			sum = foo6(n);
    		end = clock();
    		printf("%ld|%ld|", n, sum);
    		printf("%3.24lf\n", (double) (end - start) / CLOCKS_PER_SEC);
    	}
	return 0;
}
/**
* First segment of code 
* @param n, the amount of times the loop can run
* @return, the added up sum
*/
unsigned long foo1(unsigned long n)
{
	unsigned long sum = 0, i = 0;
	for(i = 0; i < n; i++){
		sum++;
	}
	return sum;

}
/**
* Second segment of code 
* @param n, the amount of times the loop can run
* @return, the added up sum
*/
unsigned long foo2(unsigned long n)
{

	unsigned long sum = 0, i = 0, j = 0;
	for (i = 0; i < n; i++){
		for (j = 0; j < n; j++)
		sum++;
	}
	return sum;
}

/**
* Third segment of code 
* @param n, the amount of times the loop can run
* @return, the added up sum
*/
unsigned long foo3(unsigned long n)
{
	unsigned long sum = 0, i = 0, j = 0;
	for (i = 0; i < n; i++){
		for (j = 0; j < n * n; j++){
			sum++;
		}
	}
	return sum;
}

/**
* Fourth segment of code 
* @param n, the amount of times the loop can run
* @return, the added up sum
*/
unsigned long foo4(unsigned long n)
{
	unsigned long sum = 0, i = 0, j = 0;
	for (i = 0; i < n; i++){
		for(j = 0; j < i; j++){
			sum++;
		}
	}
	return sum;
}

/**
* Fifth segment of code 
* @param n, the amount of times the loop can run
* @return, the added up sum
*/
unsigned long foo5(unsigned long n)
{
	unsigned long sum = 0, i = 0, j = 0, k = 0;
	for (i = 0; i < n; i++){
		for(j = 0; j < i * i; j++){
		for(k = 0; k < j; k++)
				sum++;
		}
	}
	return sum;
}

/**
* Sixth segment of code 
* @param n, the amount of times the loop can run
* @return, the added up sum
*/
unsigned long foo6(unsigned long n)
{
	unsigned long sum = 0, i = 0, j = 0, k = 0;
	for (i = 1; i < n; i++){
		for(j = 1; j < i * i; j++){
			if (j % i == 0){
				for(k = 0; k < j; k++){
					sum++;
				}
			}
		}
	}
	return sum;
}