/**
 * @file fib.c
 * 
 * @brief file Generates the Fibonacci seq.
 *
 * @details Generates the Fibonacci seq based on
 * user input.
 * 
 * @author Steven Ramsey
 *
 * @date 2/5/15
 *  
 * @todo 
 *
 * @bug none
 *
 * @remark
 * 
 */

#include <stdio.h>
#include <stdlib.h> /* atoi */
#include <unistd.h> /* getopt and optarg */
#include <limits.h>
#include <math.h>

unsigned max_n(void);
int check_n(unsigned n, unsigned max_n);
unsigned long * fib_array(unsigned n);

int main(int argc, char **argv)
{
        unsigned n = -1;
        int c;
        unsigned long *fi_array = NULL; 

        /* pass a string of options to getopt
         * the colon after a letter signifies that the option expects an argument 
         */

        while((c = getopt(argc, argv, "n:h:")) != -1) {
                switch(c) {
                case 'n':
                        n = (unsigned)atoi(optarg);
                        unsigned int max_num = max_n() - 1;
                        int control = check_n(n, max_num);
                        if((control) == -1) {
                                return -1;
                        }
                        else if (control == 0) {
                                if (n == 0) {
                                        printf("The %d Fibonacci term is %d.\n.", n, 0);
                                }
                                else {
                                fi_array = fib_array(max_num);
                                printf("The %d Fibonacci term is %ld.\n", n, fi_array[n - 1]);
                                free(fi_array);
                                }
                        }
                        break;
                case 'h':
                        printf("usage:\nFibonacci term = -n [INTEGER]\n");
                        return 0;
                default:
                        printf("something went wrong\n");
                        return -1;
                }
        }
        return 0;
}
/**
* finds the maximum n before overflow
* @return, the maximum value
*/
unsigned max_n(void)
{   
        double n = 0.0;

        n = ((log(sqrt(5) * (ULONG_MAX)))/(log((1 + sqrt(5))/2)));
        n = (unsigned int) n;

        return n;
}

/**
* checks if the inputted number is able to be calcuated without overflow
* @param, the user input number
* @param, max_n the max number before overflow
* @return, returns -1 if n is too small or large (before overflow), return 0 otherwise.
*/
int check_n(unsigned n, unsigned max_n)
{
        if (n > max_n){
                printf("\nNumber is too large! -- Overflow will occur!\n");
                printf("The largest Fibonacci term that can be calculated is %d.\n\n", max_n);
                        return -1;
        }
        else if (n < 0){
                printf("Number entered was less than 0! Exitting...\n\n");
                return -1;
        }
        else{
                return 0;
        }
}

/**
* Processes the Fibonacci sequence and store values in array
* @param n the passed in nth term to be calcuated
* @return the array containing the fibonacci array
*/
unsigned long * fib_array(unsigned n)
{
    unsigned long *f_array = malloc((sizeof(unsigned long) * n) + 1);
    int i = 2;

    f_array[0] = 1;
    f_array[1] = 1;

    for(; i < n; i++) {
        f_array[i] = f_array[i - 1] + f_array[i - 2];
    }

    return f_array;
}