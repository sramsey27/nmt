package com.networks.one;

public class Frame {
	private byte[] frame;
	
	private int source;
	private int destination;
	private int size;
	private String data;
	private int priority;
	
	public Frame(int source, int destination, int size, String data, int priority) {
		if (source < 1 || source > 255) {
			System.out.println("error in source");
		}
		if (destination < 1 || destination > 255) {
			System.out.println("error in destination");
		}
		if (size < 0 || size > 255) {
			System.out.println("error in size");
		}
		if (data.length() < 1 || data.length() > 255) {
			System.out.println("error in source");
		}
		
		this.source = source;
		this.destination = destination;
		this.size = size;
		this.data = data;
		this.priority = priority;
		
		frame = new byte[259];
		
		frame[0] = (byte) source;
		frame[1] = (byte) destination;
		frame[2] = (byte) size;
		frame[258] = (byte) priority;
		
		byte[] array = data.getBytes();
		for (int i = 0; i < data.length(); i++)
		frame[i + 3] = array[i];
		
		//System.out.println("Frame made successfully");
	}
	
	public Frame(byte[] frame) {
		this.frame = frame;
		this.source = getSource(frame);
		this.destination = getDestination(frame);
		this.size = getSize(frame);
		this.data = getData(frame);
		this.priority = getPriority(frame);
	}
	
	public static int getPriority(byte[] frame) {
		return (int) frame[258] & 0xFF;
	}
	
	public byte[] getFrame() {
		return frame;
	}
	
	public static int getSource(byte[] frame) {
		return (int) frame[0] & 0xFF; //& 0xFF makes sure result is unsigned since byte is signed
	}
	
	public static int getDestination(byte[] frame) {
		return (int) frame[1] & 0xFF;
	}
	
	public static int getSize(byte[] frame) {
		return (int) frame[2] & 0xFF;
	}
	
	public static String getData(byte[] frame) {
		int size = getSize(frame);
		byte[] data = new byte[size];
		for (int i = 0; i < size; i++) {
			data[i] = frame[i + 3];
		}
		return new String(data);
	}
	
	public static int getFrameSize(byte[] frame) {
		return Frame.getSize(frame) + 3;
	}

	public int getSource() {
		return source;
	}

	public int getDestination() {
		return destination;
	}

	public int getSize() {
		return size;
	}

	public String getData() {
		return data;
	}
	
	public int getPriority() {
		return priority;
	}
}
