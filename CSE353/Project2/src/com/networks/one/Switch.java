package com.networks.one;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.LinkedBlockingDeque;

public class Switch implements Runnable {

	public int serverport; // what port the serversocket is on
	public ServerSocket serversocket = null; // the socket the switch is listening on
	public boolean running = true; 
	public Thread runningThread; 
	
	public Address[] table; // this table keeps track of the names and what port they are listening on
	
	/*
	 * This is the array list of the messages in the queue
	 * All the messages that are received are collected into this queue and are sent later 
	 * This queue is intended to fix the overwrite issue explained on main
	 */
	static LinkedBlockingDeque<Frame> tmp; 
	
	/*
	 * Switch Constructor 
	 * @port what port you want the switch to listen on
	 */
	public Switch(int port){
		this.serverport = port;
		this.table = new Address[256];
		
		// init ip table to -1
		for(int i = 0; i < 256; i++){
			table[i] = new Address(-1, -1);
		}
		
		tmp = new LinkedBlockingDeque<Frame>();
	}
	
	
	// start handing out ports at 60,000
	int port = 60000;
	
	
	/*
	 * This method is used to hand out available ports to the node
	 * All nodes must use this method before sending messages to the 
	 * switch
	 */
	public void networkconnect(Node a){
		if(a.name > 256){
			// node ip is not valid
		}
		
		// valid node ip
		else if(a.name < 256 && a.name > 0){
			// hasn't been used yet we can use it now
			if(table[a.getName()].getName() == -1){
				table[a.getName()].setName(a.getName());
				table[a.getName()].setPort(port);
				a.setPort(port);
				port++;
			}
			//System.out.println("Node " + a.getName() + " was assigned port " + port);
		}
		
		else {
			// something went wrong
		}
	}
	
	
	/*
	 * This message will send out messages to the nodes after all the messages have been queued
	 * This is again in hopes of removing the overwrite issue 
	 */
	synchronized void messageall(){
		/*
		 * break up message node#:message 
		 * result[0] holds who to send message to
		 * result[1] holds message
		 */
		
		/*
		 * Loop through each message in queue
		 * connect to the node listed in the message according to the routing table 
		 */
		for(Frame x : tmp){
			//result = x.split(":");
			
			//System.out.println("Messageall");
			/*
			 * do some waiting for race conditions 
			 */
			try {
				Thread.sleep(100);
			} catch (InterruptedException e){
				e.printStackTrace();
			}
			
			try (
					//Socket echoSocket = new Socket("localhost", table[Integer.parseInt(result[0])].getPort());
					Socket echoSocket = new Socket("localhost", table[x.getDestination()].getPort());
				     PrintWriter out =
				                new PrintWriter(echoSocket.getOutputStream(), true);
				            BufferedReader in =
				                new BufferedReader(
				                    new InputStreamReader(echoSocket.getInputStream()));
			)
			{
			
				// output message to node
				byte[] tmp = x.getFrame();
				out.println(new String(tmp));
			} catch (IOException e){
				e.printStackTrace();
			}
		}
	}
	
	
	/*
	 * This is the method that listens to incoming connections
	 * when a connection is made it forks a new thread 
	 * WorkerRunnable which was previously sending messages to 
	 * nodes but they are all writing files at the same time and 
	 * cause conflicts so I used the messageall method to fix this 
	 */
	@Override
	public void run() {
		synchronized(this){
			this.runningThread = Thread.currentThread();
		}
		System.out.println("Switch is On");
		openServerSocket();
		while(isRunning()){
			Socket clientsocket = null;
			try {
				clientsocket = this.serversocket.accept();
			} catch(IOException e){
				if(isRunning() == false){
					System.out.println("Switch is off");
					return;
				}  throw new RuntimeException(
	                    "Error accepting client connection", e);
	            }
	            new Thread(new WorkerRunnable(clientsocket, this.table)).start();
			}
		System.out.println("Switch has stopped");
		}
	

	/*
	 * Method used to stop the switch from listening 
	 */
	public synchronized void stop(){
		this.running = false;
		try {
			this.serversocket.close();
		} catch(IOException e) {
			throw new RuntimeException("Switch cannot be closed");
		}
	}
	
	/*
	 * Check if the switch is on
	 */
	public synchronized boolean isRunning() {
		return this.running;
	}
	
	/*
	 * This is used only to start the switch 
	 */
    public void openServerSocket() {
        try {
            this.serversocket = new ServerSocket(this.serverport);
        } catch (IOException e) {
            throw new RuntimeException("Cannot open port", e);
        }
    }
	
}