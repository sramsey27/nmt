package com.networks.one;

public class Address {
	int name;
	int port;
	
	public Address(){
		
	}
	
	public Address(int name, int port){
		this.name = name;
		this.port = port;
	}

	public int getName() {
		return name;
	}

	public void setName(int name) {
		this.name = name;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
}