package com.marcus.one;


import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

/*
 * @file Node.java
 * @author Marcus Armijo
 * @description This is the node class it is able to listen on it's own port
 * as well as communicate with the RingHub or the Switch depending on the message format
 * Node class will determine which method to use to communicate with other nodes 
 */

public class Node implements Runnable {
	String name; // node name needs to be a number from 1 - 255
	Integer port; // what port it should listen to 
	ArrayList<Frame> messages; // buffered messages from input file
	private boolean running; // if it is listening or not
	private ServerSocket serversocket; // Socket that actually listens for input 
	@SuppressWarnings("unused")
	private Thread runningThread; // if the thread is running or not used for stopping the node
	
	/*
	 * Copy of SyncWriter from main used for concurrency stopping nodes from writing to same file 
	 * at once 
	 */
	SyncWriter writer;
	
	public Node(String name, SyncWriter writer){
		this.name = name;
		this.messages = new ArrayList<Frame>();
		this.writer = writer;
	}
	
	
	/*
	 * Dump of all messages it has stored 
	 */
	void printmessages(){
		System.out.println("");
		System.out.println("Messages from " + this.name);
		for(Frame x : messages){
			System.out.println(x.toString());
		}
	}
	
	/*
	 * This is used to examine the Frame and send it through 
	 * the Ring if A for type 
	 * send through Switch if C 
	 */
	void message(){
		for(Frame a: messages){
			// go through hub
			if(a.AC == true){
				fmessage(a.toString());
			}
			
			// go through switch
			else {
				smessage(a.toString());
			}
		}
	}
	
	/*
	 * This is the message method for sending a message through 
	 * the Ring main difference here is the port it sends the message
	 * Ring is listening on 1024
	 * Switch is listening on 1025
	 * could probably consolidate but this is easier if I want to change anything 
	 */
	void fmessage(String message){
		try (
				Socket echoSocket = new Socket("localhost", 1024);
				BufferedReader stdIn =
		                new BufferedReader(
		                    new InputStreamReader(new FileInputStream("node" + this.name + ".txt")));
			     PrintWriter out =
			                new PrintWriter(echoSocket.getOutputStream(), true);
			            BufferedReader in =
			                new BufferedReader(
			                    new InputStreamReader(echoSocket.getInputStream()));
		)
		{
			//TOKEN FRAME?
			out.println(message);
			
		} catch (IOException e){
			
		}
	}
	
	/*
	 * This is the method to send messages through the switch 
	 */
	void smessage(String message){
		try (
				Socket echoSocket = new Socket("localhost", 1025);
				BufferedReader stdIn =
		                new BufferedReader(
		                    new InputStreamReader(new FileInputStream("node" + this.name + ".txt")));
			     PrintWriter out =
			                new PrintWriter(echoSocket.getOutputStream(), true);
			            BufferedReader in =
			                new BufferedReader(
			                    new InputStreamReader(echoSocket.getInputStream()));
		)
		{
			out.println(message);
			
		} catch (IOException e){
			
		}
	}
	
	/*
	 * This method is how the node listens for data sent from the switch 
	 * every time someone connects a new ClientRunnable Thread is created 
	 * This thread handles writing to the output file. The data the switch sends the node
	 */
	@Override
	public void run() {
		synchronized(this){
			this.runningThread = Thread.currentThread();
		}
		System.out.println("Node " + this.name + " is listening on " + this.port);
		openServerSocket();
		while(isRunning()){
			Socket clientsocket = null;
			//System.out.println("Someone connected");
			try {
				clientsocket = this.serversocket.accept();
			} catch(IOException e){
				if(isRunning() == false){
					System.out.println("Node is off");
					return;
				}  throw new RuntimeException(
	                    "Error accepting client connection", e);
	            }
	           	new Thread(new ClientRunnable(clientsocket, this, this.writer)).start();
				
			}
		System.out.println("Node has stopped");
		
	}
	
	
	/*
	 * Stuff below is how to start, stop etc the node form listening and close
	 * the sockets cleanly 
	 */
	public synchronized void stop(){
		this.running = false;
		try {
			this.serversocket.close();
		} catch(IOException e) {
			throw new RuntimeException("Node cannot be closed");
		}
	}
	
	public synchronized boolean isRunning() {
		return this.running;
	}
	
    public void openServerSocket() {
        try {
            this.serversocket = new ServerSocket(this.port);
            this.running = true;
        } catch (IOException e) {
            throw new RuntimeException("Cannot open port", e);
        }
    }
    
}
