package com.marcus.one;

/*
 * @file main.c
 * @author Marcus Armijo
 * @description This file starts the RingHub, Switch, and Inits the Nodes
 * the proceeds to have all the files read their messages at once and then send 
 * their messages through the appropriate means
 */
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;



public class Main {

	public static void main(String[] args) throws InterruptedException {
		
		ArrayList<Node> nodes = new ArrayList<Node>(); // list containing all nodes 
		
		RingHub ringhub = new RingHub(1024);	// start ringhub on 1024 for listening
		Switch switcher = new Switch(1025);		// start switch on 1025 for listening
		
		// start the listening 
		new Thread(ringhub).start(); 
		new Thread(switcher).start();
		
		
		// Nodes start on port 65,000
		// for availability reasons
		int ports = 65000;
		
		/*
		 * SyncWrite prevents nodes from editing the same output file at once
		 * All writing of a file must be completed before another node starts to write
		 * to avoid conflicts 
		 */
		SyncWriter writer = new SyncWriter();
		
		// Add nodes to an Array
		for(int i = 1; i <= 3; i++){
			nodes.add(new Node("" + i, writer));
			nodes.get(i - 1).port = ports;
			System.out.println(ports);
			ports++; // just increment the ports 
		
		}
		
		// give RingHub a list of the nodes as well it will use this as a ring structure N -> 1 -> N
		// give switcher a list of the nodes it will use this as the switch table
		ringhub.nodes = nodes; 
		switcher.nodes = nodes;
		
		/*
		 * This coutdownlatch is used to insure all the nodes have read their files 
		 * and buffered their messages 
		 */
		final CountDownLatch latch = new CountDownLatch(nodes.size());
		
		
		// read messages into nodes message buffers
		// Each node at this point is reading their files into their messages array 
		// it all happends at once for spead
		for(Node a: nodes){
			//new Thread(new MessageBuffer(a));
			// might need to change this to a 1000 to wait longer yo
			new Thread(new MessageBuffer(a, 1000, latch)).start();;
		}
		
		// wait for all the nodes to read their messages into their buffers
		try {
			latch.await();
		} catch (InterruptedException ie){
			
		}
		
		// start all the nodes for listening 
		for(Node a: nodes){
			new Thread(a).start();
		}
		
		
		// send out all the nodes messages they have buffered 
		for(Node a: nodes){
			a.message();
		}
		
		
		/*
		 * A simple wait for laggers probably don't need at all can try 
		 * increasing if there is problems
		 */
		try {
			Thread.sleep(2000);
		} catch (Exception e){
			
		}
		
		/*
		 * Stop and close all the node ports
		 * shutdown the RingHub
		 * shutdown the Switch 
		 */
		for(Node a: nodes){
			a.stop();
		}
		ringhub.stop();
		switcher.stop();
	}
}
