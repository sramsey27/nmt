package com.marcus.one;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

/*
 * @author Marcus Armijo
 * This is a thread used to handle messages the Switch received from nodes 
 * it received messages from nodes and sends them to their destined node in the 
 * Table. This allows multiple nodes to send messages at the same time as well as allows
 * the Switch to send multiple messages as once. 
 */

public class SwitchWorkable implements Runnable {
	Socket clientSocket;
	ArrayList<Node> nodelist;
	
	SwitchWorkable(Socket c, ArrayList<Node> node){
		this.clientSocket = c;
		this.nodelist = node;
	}
	
	@Override
	public void run() {
		try (
			PrintWriter out = new PrintWriter(this.clientSocket.getOutputStream(), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));
		) { 

			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				messageNode(inputLine);
	        }
	       clientSocket.close();
		} catch (IOException e) {
			//e.printStackTrace();
		}
		
	}
	
	void messageNode(String message){

		Integer port;
		Integer tmp;
		String result[];
		result = message.split(",");
		tmp = Integer.parseInt(result[1]);
	
		port = nodelist.get(tmp - 1).port;
		
		try (	
				Socket echoSocket = new Socket("localhost", port);
			    PrintWriter out = new PrintWriter(echoSocket.getOutputStream(), true);
			)
		{
			out.println(message);
		} catch (IOException e){
			//e.printStackTrace();
		}

	
	}
}
