package com.marcus.one;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.CountDownLatch;

/*
 * This is a thread to allow the nodes to all read their input files at once
 * A countdownlatch is used to keep track of when all the nodes finish so execution can continue
 * not really much to it other than it's just a concurrent way of reading all the files 
 */
public class MessageBuffer implements Runnable {
	Node a;
	private final int timeToStart;
	private final CountDownLatch latch;


	public MessageBuffer(Node a, int timeToStart, CountDownLatch latch){
		this.a = a;
		this.timeToStart = timeToStart;
		this.latch = latch;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			Thread.sleep(timeToStart);
		} catch (InterruptedException ex){
			
		}
		
		try (
				BufferedReader stdIn = new BufferedReader( new InputStreamReader(new FileInputStream("node" + a.name + ".txt")));
			)
		{
			// it worked code
			String userInput;
            while ((userInput = stdIn.readLine()) != null) {
            	 a.messages.add(new Frame(userInput, a.name));
            	//a.messages.add(userInput);
            }
            latch.countDown();
            
		} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	
}