package com.marcus.one;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

/*
 * @file Switch.java 
 * @author Marcus Armijo
 * @description This file implements a switch it both receives messages and is able to send
 * them to the correct node at it's port. 
 */
public class Switch implements Runnable {
	
	public int serverport; // what port the server socket is on
	public ServerSocket serversocket = null; // the socket the switch is listening on
	public boolean running = true; 
	public Thread runningThread; 
	
	ArrayList<Node> nodes; // Table of nodes 
	
	public Switch(int serverport){
		this.serverport = serverport;
		nodes = new ArrayList<Node>();
	}
	
	@Override
	public void run() {
		synchronized(this){
			this.runningThread = Thread.currentThread();
		}
		System.out.println("Switch is On");
		openServerSocket();
		while(isRunning()){
			Socket clientsocket = null;
			try {
				clientsocket = this.serversocket.accept();
			} catch(IOException e){
				if(isRunning() == false){
					System.out.println("Switch is off");
					return;
				}  throw new RuntimeException(
	                    "Error accepting client connection", e);
	            }
	            new Thread(new SwitchWorkable(clientsocket, nodes)).start();
				//System.out.println("Hey the switch gets messages too");
			}
		System.out.println("Switch has stopped");
		}
	

	/*
	 * Method used to stop the switch from listening 
	 */
	public synchronized void stop(){
		this.running = false;
		try {
			this.serversocket.close();
		} catch(IOException e) {
			throw new RuntimeException("Switch cannot be closed");
		}
	}
	
	/*
	 * Check if the switch is on
	 */
	public synchronized boolean isRunning() {
		return this.running;
	}
	
	/*
	 * This is used only to start the switch 
	 */
    public void openServerSocket() {
        try {
            this.serversocket = new ServerSocket(this.serverport);
        } catch (IOException e) {
            throw new RuntimeException("Switch: Cannot open port", e);
        }
    }
}


