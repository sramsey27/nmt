package com.marcus.one;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

/*
 * @file SyncWriter
 * @author Marcus Armijo
 * @description this file is used to insure the Nodes do not write to the same file at the same time
 * it used synchronized methods to allow locking and preventing nodes from overwriting messages
 * use these methods when a node recieves a packet that was meant for it 
 */

public class SyncWriter {
	private static final SyncWriter inst = new SyncWriter();

    SyncWriter() {
        super();
    }

    public synchronized void writeToFile(String str, Node a) throws IOException {
        // Do whatever
    	//System.out.println("GOT THE MESSAGE WRITE TO FILE");
    	
    	File file = new File("node" + a.name + "output.txt");
		
		if(!file.exists()){
			file.createNewFile();
		}

		PrintWriter pw = new PrintWriter(new FileOutputStream(file,true));
		
		Frame x = new Frame(str);
		
		//System.out.println(x.source + "," + x.AorC() + "," + x.data);
		pw.append(x.source + "," + x.AorC() + "," + x.data);
		pw.append("\n");
     
		pw.close();
    }

    public SyncWriter getInstance() {
        return inst;
    }
}
