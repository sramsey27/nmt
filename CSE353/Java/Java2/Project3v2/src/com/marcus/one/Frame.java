package com.marcus.one;

/*
 * @file Frame.java
 * @author Marcus Armijo
 * @description this File represents the Frame that is sent as data
 * it includes constructors and methods to convert a line into a frame
 * it is useful to convert a input like into understandable frame units
 * I just used Strings out of laziness it should be bytes or something
 * but really isn't every just a string of bytes at the end? 
 */
public class Frame {
	String source; // who sent it
	String des; // where is it going 
	String data; // what is the actual message
	Boolean AC; // true for A false for C - A for RingHub, C for switch
	int CRC;
	
	Frame(String source, String des, String data, Boolean AC, int CRC){
		this.source = source;
		this.des = des;
		this.data = data;
		this.AC = AC;
		this.CRC = CRC;
	}
	
	/*
	 * This is mainly used when a node is sending a message 
	 * and it needs to add it's src in the frame 
	 */
	Frame(String input, String who){
		String x[];
		x = input.split(",");
		this.source = who;
		this.des = x[0];
		
		if(x[1].equals("A")){
			this.AC = true;
		} else {
			this.AC = false;
		}
		
		if(x.length == 3){
			this.data = x[2];
		} 
		
		if(x.length < 3){
			this.data = " ";
		}
		
		Integer size = 0;
		if(x.length > 3){
			
			size += x[0].length();
			size += x[1].length();
			size += 2;
			
			String hey = input.substring(size);
			this.data = hey;
		}
		
		//Added CRC which is basically an error check for robustness
		if(x.length > size + 3){
			this.CRC = size + 3;
		}
		
	}
	
	Frame(){
		
	}
	
	/*
	 * This is mainly used to break up data that was already
	 * put into the correct form structure 
	 */
	Frame(String message){
		String x[];
		//System.out.println("FRAME RECIEVED: " + message);
		x = message.split(",");
		this.source = x[0];
		this.des = x[1];
		if(x[2].equals("A")){
			this.AC = true;
		} else {
			this.AC = false;
		}
		//this.data = x[3];
		if(x.length == 3){
			this.data = x[2];
		} 
		
		if(x.length < 3){
			this.data = " ";
		}
	
		if(x.length > 3){
			Integer size = 0;
			size += x[0].length();
			size += x[1].length();
			size += x[2].length();
			size += 3;
			
			String hey = message.substring(size);
			this.data = hey;
		
			this.CRC = size + this.data.length();
			
		}	
	}
	
	/*
	 * This method is used when sending the packet
	 * puts the Frame back into a single string so it can be written
	 * all at once. 
	 */
	@Override
	public String toString(){
		String x;
		if(this.AC == true){
			x = "A";
		} else {
			x = "C";
		}
		return source + "," + des + "," + x + "," + data;
	}
	
	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public Boolean getAC() {
		return AC;
	}

	public void setAC(Boolean aC) {
		AC = aC;
	}
	
	public void setCRC(int CRC){
		this.CRC = CRC;
	}
	
	public int getCRC(){
		return CRC;
	}
	
	String AorC(){
		if(this.AC == true){
			return "A";
		} else {
			return "C";
		}
	}
}
