package com.marcus.one;

public class RingFrame {

	Boolean AC;
	String FC, DA, SA, Data, FS;
	int CRC;
	Frame SwitchFrame;
	int size;
	
	//AC - Access Control
	//FC - Frame Control
	//SA - Source Address
	//size - size of data
	//Data/.. self 
	//CRC - Cyclic Redundancy Check
	//FS - Frame Status
	
	RingFrame(String input){
		
		String x[];
		SwitchFrame = new Frame(input);
		
		this.AC = SwitchFrame.getAC();
		this.DA = SwitchFrame.getDes();
		this.SA = SwitchFrame.getSource();
		this.Data = SwitchFrame.getData();
		this.CRC = SwitchFrame.getCRC();
		
	}

	public Boolean getAC() {
		return AC;
	}

	public void setAC(Boolean AC) {
		this.AC = AC;
	}

	public String getFC() {
		return FC;
	}

	public void setFC(String fC) {
		this.FC = fC;
	}

	public String getDA() {
		return DA;
	}

	public void setDA(String dA) {
		this.DA = dA;
	}

	public String getSA() {
		return SA;
	}

	public void setSA(String sA) {
		this.SA = sA;
	}

	public String getData() {
		return Data;
	}

	public void setData(String data) {
		this.Data = data;
	}

	public int getCRC() {
		return CRC;
	}

	public void setCRC(int CRC) {
		this.CRC = CRC;
	}

	public String getFS() {
		return FS;
	}

	public void setFS(String fS) {
		this.FS = fS;
	}

	public Frame getSwitchFrame() {
		return SwitchFrame;
	}

	public void setSwitchFrame(Frame switchFrame) {
		this.SwitchFrame = switchFrame;
	}
	
}
