package com.networks.one;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

/*
 * WorkerRunnable is used any time someone connects to the switch 
 * It receives input sent from the node's input file 
 * right now all it does is sends every message to the tmp queue 
 * again this is in hopes of being able to sync all the messages later on 
 */
public class WorkerRunnable implements Runnable {

	public Socket client = null; 
	public Address[] table; // this is the ip table node name, node port
	ArrayList<String> tmp; // this stores all the messages 
 
	public WorkerRunnable(Socket clientsocket, Address[] table, ArrayList<String> tmp){
		this.client = clientsocket;
		this.table = table;
		this.tmp = tmp;
	}
	
	@Override
	public void run() {
		//System.out.println("\nSwitch has recieved communication request");
		
		try (
			PrintWriter out = new PrintWriter(this.client.getOutputStream(), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(this.client.getInputStream()));
		) { 
			String inputLine;
			//String result[];
			//System.out.println("\nSwitch Received: ");
			while ((inputLine = in.readLine()) != null) {
				
				/*
				 * Previously I was sending messages to node request directly here 
				 */
				//System.out.println(inputLine);
				//result = inputLine.split(":");
				//message(Integer.parseInt(result[0]), result[1]);
				
				
				tmp.add(inputLine); // add message to the message queue 
	        }
	       client.close();
		} catch (IOException e) {
			
		}
		
	}
	
	
	/*
	 * This sends the message received by Node a to Node B
	 * This is a lazy method now keep in mind the frame byte array 
	 * is not being used 
	 */
	void message(int name, String text){
		try (
				Socket echoSocket = new Socket("localhost", table[name].getPort());
			     PrintWriter out =
			                new PrintWriter(echoSocket.getOutputStream(), true);
			            BufferedReader in =
			                new BufferedReader(
			                    new InputStreamReader(echoSocket.getInputStream()));
		)
		{
		
		out.println(name + ":" + text);
		} catch (IOException e){
			e.printStackTrace();
		}
	}

}
