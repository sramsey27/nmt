package com.networks.one;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;


/*
 * what I did
 * 
 * I pretty much did the whole implementation of the star so I need you guys to take care of the rest which sounds fair to me
 * so if you have questions text me about them, I think I left pretty good comments along the way 
 * 
 * 
 * Okay so what I need you guys to do
 * 
 *  - needs to write main so it can take in x amount of nodes and run based off the user input
 *  - needs to change the sending from strings to frames 
 *  - need a script to generate x amount of node files and then run the program for x amount of nodes 
 *  - needs to write up the report 
 *  - need to make a power point report for the TA meeting
 *  - do some bug testing , there is very little error checking and assumes things to be correct so you can add cases 
 *  - Do other Bullshit he asked for like flooding (This is optional) I really don't care
 * 
 * !! if you understand how this code works it's pretty easy please try and implement any extra credit
 *    or any other task he wants that I have not accomplished 
 */
public class Main {

	public static void main(String[] args) throws IOException, InterruptedException {
		
		int numofnodes = 0, nodeline = 0;
		
		Scanner input = new Scanner(System.in);
		System.out.printf("Enter the number of nodes to initalize: ");
		numofnodes = input.nextInt();
		System.out.printf("Enter number of lines for generator: ");
		nodeline = input.nextInt(); 
		System.out.println("");
		input.close();
		
		/*Build String Array for ProcessBuilder*/
		List<String> command = new ArrayList<String>();
		command.add("./generator");
		command.add(Integer.toString(numofnodes));
		command.add(Integer.toString(nodeline));
		
		System.out.println("Running Script - Generator...");
		try{
			ProcessBuilder pb = new ProcessBuilder(command);
			pb.start();
			TimeUnit.SECONDS.sleep(5);
		}catch(IOException err){
			System.out.println("Script Error: " + err + "\nMake sure it is in the same directory.");
			System.out.println("This will not work on Windows, Unix only");
			System.exit(2);
		}
		
		/*
		 * Start the switch interface
		 */
		Switch s = new Switch(1024);
		new Thread(s).start();
		
		if(numofnodes > 255 || numofnodes < 1){
			s.stop();
			System.out.println("Invalid number of nodes, must not be > 255 or < 1. Try Again.");
			System.exit(1);
		}
		
		/* Initalize N number of nodes*/
		Node[] nodes = new Node[numofnodes];
		
		for(int i = 0, j = 1; i < numofnodes; i++, j++){
			nodes[i] = new Node(j);
			s.networkconnect(nodes[i]);
			new Thread(nodes[i]).start();
		}
		for(int i = 0; i < numofnodes; i++){
			nodes[i].message();
		}
		
		/*Node one = new Node(1);
		s.networkconnect(one);
		new Thread(one).start();
		
		Node two = new Node(2);
		s.networkconnect(two);
		new Thread(two).start();
		
		Node three = new Node(3);
		s.networkconnect(three);
		new Thread(three).start();*/
		/*
		 * Sends each of the files node#.txt to switch
		 */
		//nodes[0].message();
		//nodes[1].message();
		//nodes[2].message();
		
		/*
		 * Just wait for things to finish before closing down
		 */
		try {
			Thread.sleep(300);
		} catch (InterruptedException e){
			e.printStackTrace();
		}
		
	
		/*
		 * This prints out the queue of messages send to the server
		 */
		
		System.out.println();
		System.out.println("Messages");
		for(String x : s.tmp){
			System.out.println(x);
		}
		System.out.println();
		
		/*
		 * Sends all messages from que to the nodes 
		 * this was in an attempt to fix the timing issues
		 */
		s.messageall();
		
		
		// Stop everything 
		s.stop();
		for(int i = 0; i < numofnodes; i++){
			nodes[i].stop();
		}
	}

}