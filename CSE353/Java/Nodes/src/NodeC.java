/**
 * Implements Socket communication between NodeB and NodeC by transferring data lines from config files.
 * 
 * @author Steven Ramsey
 * 
 * @version NodeC - Project 1
 * 
 * @bugs none
 */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class NodeC extends Thread{

	private Thread T1;
	private String thread_name;
	int attempts = 0;
	
	/*Constructor for Object Class Instantiation and threading*/
	NodeC(String name) {
	      thread_name = name;
	      System.out.println("Initalizing Thread: " + thread_name);
	}
	
	/*Initalize the thread for this class*/
	public void start () {
	      if (T1 == null) {
	         T1 = new Thread (this, thread_name);
	         T1.start ();
	      }	
	}	
	
	
	/*Run when NODEC.start() is called by separate thread*/
	public void run(){	
		int portnumberC = 0;
		String serv_line;
		
		/*Read information within the Config File and initalize socket*/
		try {	
			
			/*Line by Line get port information and assign socket port*/
			FileInputStream config_stream = new FileInputStream("ConfigC.txt");
			
			@SuppressWarnings("resource")
			BufferedReader buffer = new BufferedReader(new InputStreamReader(config_stream));
			portnumberC = Integer.parseInt(buffer.readLine());
			
			ServerSocket inline = new ServerSocket(portnumberC);
			Socket input_socket = inline.accept();
			
			BufferedReader br = new BufferedReader(new InputStreamReader(input_socket.getInputStream()));;
			
			while((serv_line = br.readLine())!= null){ /*Print Received Data*/
				System.out.println("NodeC Recieved: " + serv_line);
				Thread.sleep(1000);
			}

			System.out.println("Transfer Complete! Have a good day :)");
			inline.close(); /*Close Server Socket, no longer needed*/
				
		} catch (IOException | InterruptedException e1) {
			if(attempts > 10){
				try {
					Thread.sleep(2000);
					attempts++;
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
	}
}
}