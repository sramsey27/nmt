/**
 * Implements Socket communication between NodeA and NodeC by transferring data lines from config files.
 * 
 * @author Steven Ramsey
 * 
 * @version NodeB - Project 1
 * 
 * @bugs none
 */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.*;


public class NodeB extends Thread {

	private String thread_name;
	private Thread T1;
	private int attempts = 0;
	
	/*Constructor for Object Class Instantiation and threading*/
	NodeB(String name) {
	      thread_name = name;
	      System.out.println("Initalizing Thread: " + thread_name);
	}
	
	/*Initalize the thread for this class*/
	public void start () {
	      if (T1 == null) {
	         T1 = new Thread (this, thread_name);
	         T1.start ();
	      }	
	}	
	
	public void run(){ /*Initalize the program when NODEB.start is called*/
		
		int portnumberC = 0, portnumberA = 0;
		String temp_line = null;
		String serv_line;

			try {
				
				/*Read information within the Config File and initalize socket*/
				FileInputStream config_stream = new FileInputStream("ConfigB.txt");
				
				@SuppressWarnings("resource")
				BufferedReader buffer = new BufferedReader(new InputStreamReader(config_stream));
				portnumberA = Integer.parseInt(buffer.readLine());
				portnumberC = Integer.parseInt(buffer.readLine());
				
				ServerSocket inline = new ServerSocket(portnumberA);
				Socket input_socket = inline.accept();
				
				BufferedReader br = new BufferedReader(new InputStreamReader(input_socket.getInputStream()));;
				
				while((serv_line = br.readLine())!= null){ /*Print received data*/
					System.out.println("NodeB Recieved: " + serv_line);
					Thread.sleep(1000); 
				}

				inline.close(); /*close the server socket, no longer needed*/
						
				Socket com_socketB = new Socket(InetAddress.getLocalHost(), portnumberC);
				if(!com_socketB.isConnected() && attempts <= 10){ /*Make sure socket is good*/
					Thread.sleep(1000);
					attempts++;
				}
				else if (attempts > 10){
					System.out.println("Too Many Attempts, closing NODEB");
					System.exit(-1);
				}
				PrintWriter output = new PrintWriter(com_socketB.getOutputStream(), true);
				while((temp_line = buffer.readLine())!= null){
					output.println(temp_line);	/*Send the current line*/
					Thread.sleep(1000); /*Just to see the action happening*/
				}
				com_socketB.close();
				
		} catch (IOException | InterruptedException e1) {
			if(attempts > 10){
				try {
					Thread.sleep(2000);
					attempts++;
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			else{
				System.out.println("Too Many Connection attempts!! Terminating...");
				e1.printStackTrace();
				System.exit(-1);
			}
		}
	}
}
