/**
 * Implements Socket communication between NodeA to NodeB by transferring data lines from config files.
 * 
 * @author Steven Ramsey
 * 
 * @version NodeA - Project 1
 * 
 * @bugs none
 */

import java.net.*;
import java.io.*;

public class NodeA extends Thread {
	
	private Thread T1;
	private String thread_name;
	
	/*Constructor for Object Class Instantiation and threading*/
	NodeA(String name) { 
	      thread_name = name;
	      System.out.println("Initalizing Thread: " + thread_name);
	}
	
	/*Initalize the thread for this class*/
	public void start () {
	      if (T1 == null) {
	         T1 = new Thread (this, thread_name);
	         T1.start ();
	      }	
	}	
	
	/*Run when NODEA.start() is called by separate thread*/
	public void run(){ 
		
		int portnumberA = 0, attempts = 0; //Initial port number for A
		String temp_line;
		Socket com_socketA = null;
		
		/*Read information within the Config File and initalize socket*/
		try{ 
			FileInputStream config_stream = new FileInputStream("ConfigA.txt");	
			BufferedReader buffer = new BufferedReader(new InputStreamReader(config_stream));
			
			/*Line by Line get port information and assign socket port*/
			portnumberA = Integer.parseInt(buffer.readLine());
			com_socketA = new Socket(InetAddress.getLocalHost(), portnumberA);
			
			/*Prepare output by PrintWriter*/
			PrintWriter output = new PrintWriter(com_socketA.getOutputStream());
			output.flush(); 
			
			/*Output the data lines via PrintWriter*/
			while((temp_line = buffer.readLine())!= null){
						output.println(temp_line);	
						Thread.sleep(1000); /*Just to slow down the action happening*/
			}
			
			/*Closed sockets and buffers as they are no longer needed*/
			buffer.close();
			output.close();
			com_socketA.close();
			
			} catch (Exception err){
				if (attempts > 10){ /*Only try 10 times to reconnect, otherwise kill the program*/
					System.out.println("Too Many Attempts, closing NODE_A");
					System.exit(-1);
				}
				else{
					/*If exception occurred, try again after 2 seconds and increment attempt*/	
					try {
						Thread.sleep(2000);
						attempts++; /*increment connect attempts*/
					} catch (InterruptedException e) {
						e.printStackTrace();
					}		
				}
			}	
		}
}