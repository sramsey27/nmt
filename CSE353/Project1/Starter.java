/**
 * The main function for the Node Communication, it initializes the nodes on separate threads. Allowing for multiple socket communication.
 * 
 * @author Steven Ramsey
 * 
 * @version Starter - Project 1
 * 
 * @bugs none
 */

import java.io.*;

public class Starter {
	
	public static void main(String[] args) throws IOException, InterruptedException {

		Thread NODE_A = new NodeA("NODE_A"); /*Make a Thread Object from each class*/
		Thread NODE_B = new NodeB("NODE_B");
		Thread NODE_C = new NodeC("NODE_C");
		
		NODE_A.start(); /*Start Node A Thread*/
		NODE_B.start(); /*Start Node B Thread*/
		NODE_C.start(); /*Start Node C Thread*/	
		
	}
}
