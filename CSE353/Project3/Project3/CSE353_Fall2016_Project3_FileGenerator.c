#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void printHelp(char *);

int main(int argc, char ** argv)
{
	int numNodes, numLines, i, j, k, length, dest, trafficType;
	char fname[15];

	if (argc != 3)
	{
		printHelp(argv[0]);
		exit(1);
	}
	srand(time(NULL));
	numNodes = atoi(argv[1]);
	numLines = atoi(argv[2]);

	if ((numNodes < 2) || (numNodes > 255))
	{
		printf("Number of nodes is out of range (given %d, should be 2-255)\n",numNodes);
		printHelp(argv[0]);
		exit(1);
	}

	printf("Generating data for %d nodes, %d lines each.\n",numNodes,numLines);
	for (i=1;i<=numNodes;i++)
	{
		sprintf(fname,"node%d.txt",i);
		FILE * f = fopen(fname,"w");
		if (f == NULL)
		{
			perror("Cannot open output file");
			exit(2);
		}
		// printf("writing %s\n",fname);
		for (j=0;j<numLines;j++)
		{
			length = (rand() % 100);
			trafficType = (rand() % 2);
			// printf("\tlen %d, type %d, ",length,trafficType);
			do
				dest = (rand() % numNodes+1);
			while (dest == i);
			// printf("dest: %d,",dest);

			fprintf(f,"%d,%c,",dest, (trafficType == 0 ? 'A':'C'));
			for (k=0;k<length;k++)
			{
				fputc((char)(rand() % 93) + 32,f);
			}
			fputc('\n',f);
			// printf("data\n");
		}
		fclose(f);
		f = NULL;

	}
	printf("Done.\n");
	return 0;
}

void printHelp(char * progName)
{
	printf("CSE353 Project 2 File Generator\n");
	printf("Usage: %s [Number of Nodes] [Number of Lines]\n",progName);
	printf("\tNumber of Nodes: Number of nodes to generate data for\n");
	printf("\tNumber of Lines: Number of lines to generate for each file.\n");
}
