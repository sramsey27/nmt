package com.pizza;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.concurrent.LinkedBlockingDeque;

/*
 * WorkerRunnable is used any time someone connects to the switch 
 * It receives input sent from the node's input file 
 * right now all it does is sends every message to the tmp queue 
 * again this is in hopes of being able to sync all the messages later on 
 */
public class WorkerRunnable implements Runnable {

	public Socket client = null; 
	public Address[] table; // this is the ip table node name, node port
	LinkedBlockingDeque<Frame> tmp; // this stores all the messages 
 
	public WorkerRunnable(Socket clientsocket, Address[] table){
		this.client = clientsocket;
		this.table = table;
	}
	
	@Override
	public void run() {
		//System.out.println("\nSwitch has recieved communication request");
		
		try (
			//PrintWriter out = new PrintWriter(this.client.getOutputStream(), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(this.client.getInputStream()));
			DataInputStream in2 = new DataInputStream(this.client.getInputStream());
		) { 
			String inputLine;
			//String result[];
			//System.out.println("\nSwitch Received: ");
			while ((inputLine = in.readLine()) != null) {
            	if (inputLine.length() < 3) {
            		break;
            	}
				//System.out.println("Reiv: " + inputLine);
				/*
				 * Previously I was sending messages to node request directly here 
				 */
				//System.out.println(inputLine);
				//result = inputLine.split(":");
				//message(Integer.parseInt(result[0]), result[1]);

			//synchronized(in2){
			//	in2.readFully(inb);
			byte[] temp = inputLine.getBytes();
			Frame infr = new Frame(temp);

			//System.out.println("Recv from: " +infr.getSource() + " --> " + infr.getDestination()  + ":" + infr.getData());

			synchronized (Switch.tmp){
				try {
					if (Frame.getPriority(temp) == 0) {
						Switch.tmp.putLast(infr);
					}
					else {
						Switch.tmp.putFirst(infr);
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} // add message to the message queue 
			}
			//	i++;
	       // }
			}
	       client.close();
			
		} catch (IOException err) {
			err.printStackTrace();
		}
		
	}
	
	


}
