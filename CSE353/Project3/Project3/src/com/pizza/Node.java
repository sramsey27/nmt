package com.pizza;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;


public class Node implements Runnable {
	public int name;
	public int port;
	public ServerSocket serversocket = null;
	public Thread runningThread;
	public boolean running = true;
	
	Node(){
		
	}
	
	Node(int name){
		this.name = name;
	}
	
	
	/*
	 * This method is used to send messages to the switch 
	 * the switch must be on port 1024 for this connection to work
	 * any time you need to send a message
	 * a connection is made and it reads from that nodes input file
	 * input file is based on node + node# + .txt
	 * reads that file line by line and outputs to the switch 
	 */
	void message() throws InterruptedException{
		String filename = "node" + this.name + ".txt";
		try (
				Socket echoSocket = new Socket("localhost", 1024);
				//BufferedReader stdIn =
		         //       new BufferedReader(
		           //         new InputStreamReader(new FileInputStream("node" + this.name + ".txt")));
				BufferedReader stdIn = new BufferedReader(new FileReader(filename));
			     PrintWriter out =
			                new PrintWriter(echoSocket.getOutputStream(), true);
			            BufferedReader in =
			                new BufferedReader(
			                    new InputStreamReader(echoSocket.getInputStream()));
				DataOutputStream out2 = new DataOutputStream(echoSocket.getOutputStream());
		)
		{
			String userInput;
			int sep = 0;
			int count = 0;
			
            while ((userInput = stdIn.readLine()) != null) {
            	if (userInput.length() < 3) {
            		break;
            	}
            	count++;
            	sep = userInput.indexOf(':');
            	System.out.println(this.name + "-->" + userInput.substring(0, sep) + " : " + userInput.substring(sep + 1));
            	Frame frout = new Frame(this.name, Integer.parseInt(userInput.substring(0, sep)), userInput.substring(sep+1).length() ,userInput.substring(sep + 1), 0);
            	byte[] outb = frout.getFrame();
            	
            	
            	
            //	synchronized(out2){
            //		out2.write(outb);
            //		out2.flush();
            //	}
            	
            	
            	
            	//Thread.sleep(10);            	
            	
            	//System.out.println("Send: " + frout.getData());
            	out.println(new String(outb));
            }
            System.out.println("\n" + count + filename + "\n");
		} catch (IOException e){
			e.printStackTrace();
		}
	}
	
	
	
	/*
	 * Getters and Setters 
	 */
	public int getName() {
		return name;
	}

	public void setName(int name) {
		this.name = name;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	
	/*
	 * This method is how the node listens for data sent from the switch 
	 * every time someone connects a new ClientRunnable Thread is created 
	 * This thread handles writing to the output file. The data the switch sends the node
	 */
	@Override
	public void run() {
		synchronized(this){
			this.runningThread = Thread.currentThread();
		}
		//System.out.println("Node " + this.name + " is listening on " + this.port);
		openServerSocket();
		while(isRunning()){
			Socket clientsocket = null;
			try {
				clientsocket = this.serversocket.accept();
			} catch(IOException e){
				if(isRunning() == false){
					System.out.println("Node is off");
					return;
				}  throw new RuntimeException(
	                    "Error accepting client connection", e);
	            }
	            new Thread(new ClientRunnable(clientsocket, this)).start();
				
			}
		System.out.println("Node has stopped");
		
	}
	
	
	/*
	 * Stuff below is how to start, stop etc the node form listening and close
	 * the sockets cleanly 
	 */
	public synchronized void stop(){
		this.running = false;
		try {
			this.serversocket.close();
		} catch(IOException e) {
			throw new RuntimeException("Node cannot be closed");
		}
	}
	
	public synchronized boolean isRunning() {
		return this.running;
	}
	
    public void openServerSocket() {
        try {
            this.serversocket = new ServerSocket(this.port);
        } catch (IOException e) {
            throw new RuntimeException("Cannot open port", e);
        }
    }
	
}