package com.pizza;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;


/*
 * This class is what the Client uses any time they get input from the switch
 * this classes purpose is the write the input from the switch to an output file
 */
public class ClientRunnable implements Runnable {
	public Socket client = null;
	public Node a = null;
	
	public ClientRunnable(Socket clientsocket, Node a){
		this.client = clientsocket;
		this.a = a;
	}
	
	
	/*
	 * Any time someone connects to this node it reads in the input creates an output if needed
	 * and writes the message to the output file 
	 * 
	 */
	@Override
	public void run() {
		//System.out.println("\nNode " + a.getName() + " has received communication request");
		
		try (
			//PrintWriter out = new PrintWriter(this.client.getOutputStream(), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(this.client.getInputStream()));
		
		) { 
			File file = new File("node" + a.getName() + "output.txt");

			PrintWriter pw = new PrintWriter(new FileOutputStream(file,true));
			

			String inputLine;	
			
			/*
			 * Append the message to the output file 
			 */
			while ((inputLine = in.readLine()) != null) {
				byte[] tmp = inputLine.getBytes();
				Frame f = new Frame(tmp);
				pw.append(f.getSource() + ":" + f.getData());
				pw.append("\n");
	        }
			pw.close();;
	       client.close();
		} catch (IOException e) {
			
		}
		
	}
}
