Steven Ramsey
1/26/17
CSE325 

  # Set up the stack pointer and call into C.
  movl    $start, %esp
  call    bootmain

Value of %esp becomes 0x7c00 (print output of x/24x $esp)

start:

When $esp becomes 0x7c00, it becomes the stack pointer.

At 0x00007c48 in ?? () 
0x7c00:	0x8ec031fa	0x8ec08ed8	0xa864e4d0	0xb0fa7502 
0x7c10:	0xe464e6d1	0x7502a864	0xe6dfb0fa	0x16010f60
0x7c20:	0x200f7c78	0xc88366c0	0xc0220f01	0x087c31ea
0x7c30:	0x10b86600	0x8ed88e00	0x66d08ec0	0x8e0000b8
0x7c40:	0xbce88ee0	0x00007c00	0x0000e2e8	0x00b86600
0x7c50:	0xc289668a	0xb866ef66	0xef668ae0	0x9066feeb

This occurs from these lines in bootblock.asm:

movl    $start, %esp
    7c43:	bc 00 7c 00 00       	mov    $0x7c00,%esp  
 call    bootmain
    7c48:	e8 e2 00 00 00       	call   7d2f <bootmain>

from this point we step into the bootmain.c file.  The bootloader instructions.
The Call in bootblock.asm shows the value of 0x7d2f as the call to bootmain which is given to the $eip register in the next step. 

Once these instructions pass, the entry to the kernel occurs when the eip register obtains the 0x10000c address,
from this point the stack now looks like this...

0x7bcc:	0x00007db7	0x00000000	0x00000000	0x00000000  
0x7bdc:	0x00000000	0x00000000	0x00000000	0x00000000
0x7bec:	0x00000000	<-- Callee EBX saved
		0x00000000	<-- Callee ESI saved
		0x00000000	<-- Callee EDI saved
		0x00000000	<-- EBP of bootasm
0x7bfc:		0x00007c4d	0x8ec031fa	0x8ec08ed8	0xa864e4d0
0x7c0c:	0xb0fa7502	0xe464e6d1	0x7502a864	0xe6dfb0fa
0x7c1c:	0x16010f60	0x200f7c78	0xc88366c0	0xc0220f01

The zeroed values are space made by bootmain for the stack , however starting from 0x7bec the first 3 are callees 
which are saved by bootmain, the last zero address just before 0x7bfc is the EBP of the bootasm frame, this is 
also where bootmain ESP is intialized aswell.

0x7bcc:  	0x00007db7 	<-- EIP for return to bootmain (shouldn't happen according to the comments in code)
				it is also the top of the stack...

0x7bfc:		0x00007c4d 	<-- return address
		0x8ec031fa	<-- 1st Parameter
		0x8ec08ed8	<-- 2nd Parameter
		0xa864e4d0	<-- 3rd Parameter