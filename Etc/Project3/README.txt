**********************************************************
This code works and validates 100% the only problem 
is validator seg faults sometimes. This is only validators
problem not this code. It usually works and says either

1 - no errors
2 - validator segfaults

If validator segfaults you will find that the output files are infact
correct.
**********************************************************

A. 	Group Members: 

	Marcus Armijo
	Thomas Barks
	Iseah Olguin
	Steven Ramsey

B. 	Compilation:
 
	[build]
	make

	* the apache ant source is included in this directory, the make
	file calls ant from the source folder. You could change to work 
	with /bin/ant but this way is more portable. 

	[run] 
	./generator [number of nodes] [number of messages]
	java -jar pizzaNetwork.jar [number of nodes]
	./validator [number of nodes]

	[clean]
	make clean

	* will delete *.txt and pizzaNetwork.jar

C. Files & File Description: 

	1. 	Main.java
		
		The ringhub, switchm, and N number of nodes are started within this class. All files read their corresponding messages at once and then sends them through the appropriate route.

	2. 	Switch.java
	
		Only instantiated once, but this file implements a switch which recieves messages and is has the ability to send them to the appropriate node at it's assigned port.

	3. 	RingHub.java
	
		This class implements the ringhub mechanism. It basically is a server that goes around the ring based on the token instead of direct sending of the data to the node.

	4. 	Node.java

		This is the required node class. It is able to listen on it's assigned port and also communicate with the RingHub or switch depending on the message format (frame). It determines which method to use to communicate with other nodes.

	5.	HubWorkable.java

		This class is designed to be a thread dedicated for the RingHub for when it recieves a message from a node. It moves the token around the Ring and sends messages to the Nodes. Any Node with either an Acknowledgement that the message belongs to them or the node will re-send the message back to the Ringhub and will continue the necessary path.

	6.	Frame.java
		
		This class represents the frame which is sent as data. It includes constructors and methods which convert a line into a frame. It is useful to convert an input into an understandable frame entity. Equipped with a source, destination, data, and acknowledgement.
		
	7. 	MessageBuffer.java

		This is a thread which allows the nodes to read all their corresponding input files at once. It utilizes a countdownlatch object to keep track of when all the nodes are finished so the program execution can continue. It's just a concurrent way of reading all the files 

	8.	SwitchWorkable.java
	
		This class acts as a thread to handle messages that the switch recieved from nodes. It recieves messages from the nodes and sends them to the appropreiate node in the table. This will allow multiple nodes to send messages at the same time and also allow the siwtch to send multiple messages at once.

	9.	SyncWriter.java

		This class is used to insure that the nodes do not write to the same file at the same time. It utilizes synchonized methods to allow locking and preventing nodes from overwriting messages. 

	10. ClientRunnable.java
		
		This class acts as a thread which is used by the node class to allow multi-thread connections to nodes.

	

