


/*
 * @file RingHub.java
 * @author Marcus Armijo
 * @description This class is used to implement the RingHub
 * which is basically just a server that goes around the ring based of a token 
 * instead of directly sending the data to the Node 
 */
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class RingHub implements Runnable {
	
	public int serverport; // what port the server socket is on
	public ServerSocket serversocket = null; // the socket the switch is listening on
	public boolean running = true; 
	public Thread runningThread; 
	int token = 0;

	ArrayList<Node> nodes;
	
	public RingHub(int serverport){
		this.serverport = serverport;
		nodes = new ArrayList<Node>();
	}

	public synchronized void tokenIncrease(){
		if(token == nodes.size() - 1){
			token = 0;
		} else {
			token++;
		}
	}
	
	/*
	 * While this is running any connection is gets it will start a new Thread to handle
	 * the communication called HubWorkAble 
	 */
	@Override
	public void run() {
		synchronized(this){
			this.runningThread = Thread.currentThread();
		}
		System.out.println("HUB is On");
		openServerSocket();
		while(isRunning()){
			Socket clientsocket = null;
			try {
				clientsocket = this.serversocket.accept();
			} catch(IOException e){
				if(isRunning() == false){
					System.out.println("Hub is off");
					return;
				}  throw new RuntimeException(
	                    "Error accepting client connection", e);
	            }
	            new Thread(new HubWorkable(clientsocket, nodes, token)).start();
				tokenIncrease();
			}
		System.out.println("HUB has stopped");
		}
	

	/*
	 * Method used to stop the switch from listening 
	 */
	public synchronized void stop(){
		this.running = false;
		try {
			this.serversocket.close();
		} catch(IOException e) {
			throw new RuntimeException("HUB cannot be closed");
		}
	}
	
	/*
	 * Check if the switch is on
	 */
	public synchronized boolean isRunning() {
		return this.running;
	}
	
	/*
	 * This is used only to start the switch 
	 */
    public void openServerSocket() {
        try {
            this.serversocket = new ServerSocket(this.serverport);
        } catch (IOException e) {
            throw new RuntimeException("HUB: Cannot open port", e);
        }
    }
}
