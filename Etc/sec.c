#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int main(void) {
        struct tm* tm_info;
        time_t t;
        time(&t);
        char buf[50];

        tm_info = localtime(&t);

        printf("Number of seconds since the Epoch: %lld\n\n", (long long)time(NULL));

        printf("Localtime\n");
        printf("hours: %d\nminutes: %d\nseconds: %d\nyear: %d\nmonth: %d\n",
                tm_info->tm_hour,
                tm_info->tm_min,
                tm_info->tm_sec,
                tm_info->tm_year,
                tm_info->tm_mon);
        printf("day of the month: %d\nday of the week: %d\njulian: %d\ndaylight savings time: %d\n",
                tm_info->tm_mday,
                tm_info->tm_wday,
                tm_info->tm_yday,
                tm_info->tm_isdst);

        strftime(buf, 50, "%A, %B %d, %Y %I:%M:%S %p", tm_info);
        puts(buf);


        tm_info = gmtime(&t);

        printf("\nCoordinated Universal Time (UTC)\n");
        printf("hours: %d\nminutes: %d\nseconds: %d\nyear: %d\nmonth: %d\n",
                tm_info->tm_hour,
                tm_info->tm_min,
                tm_info->tm_sec,
                tm_info->tm_year,
                tm_info->tm_mon);
        printf("day of the month: %d\nday of the week: %d\njulian: %d\ndaylight savings time: %d\n",
                tm_info->tm_mday,
                tm_info->tm_wday,
                tm_info->tm_yday,
                tm_info->tm_isdst);
        strftime(buf, 50, "%A, %B %d, %Y %I:%M:%S %p", tm_info);
        puts(buf);

        return 0;
}