<?xml version="1.0"?>

<xsl:stylesheet version="1.0"  
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
	<html  xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<title>My Resume</title>
		</head>
		<style type="text/css">
			body{
				margin:27px;
				background-color:#181919;
				font-family: verdana,helvetica,sans-serif;
				color: white;
			}
		</style>
		<body>
			<h1 align="center">My Resume: XML Edition</h1><hr/>
				
				<font size="5"> <strong>Summery</strong> </font> <br/><br/>
					<xsl:apply-templates select="resume/Bio"/>
				<br/><br/> <hr/>

				<font size="5"><strong> Skills/Strengths </strong> </font>
				<div><br/>
					<xsl:value-of select="resume/Skills_Strengths/Pro_Languages"/><br/>
					<xsl:value-of select="resume/Skills_Strengths/Software"/><br/>
					<xsl:value-of select="resume/Skills_Strengths/Professional"/><br/>
				</div><br/><hr/>

				<font size="5"> <strong> Education </strong> </font> <br/><br/>
					<span>
					<xsl:value-of select="resume/Education/College/Name"/><br/>
					<xsl:value-of select="resume/Education/College/Degree"/><br/>
					<xsl:value-of select="resume/Education/College/Dates"/><br/>
					</span><br/><hr/>

				<font size="5"> <strong> Work Experience </strong> </font> <br/> <br/> 

					<span>
					<xsl:value-of select="resume/WorkExp/Job1/Name_Local"/><br/>
					<xsl:value-of select="resume/WorkExp/Job1/Description"/><br/>
					<xsl:value-of select="resume/WorkExp/Job1/Date"/><br/>
					</span><br/>

					<xsl:value-of select="resume/WorkExp/Job2/Name_Local"/><br/>
					<xsl:value-of select="resume/WorkExp/Job2/Description"/><br/>
					<xsl:value-of select="resume/WorkExp/Job2/Date"/><br/>

		</body>
	</html>
</xsl:template>

<xsl:template match="resume/Bio">
	<xsl:apply-templates/>
</xsl:template>

</xsl:stylesheet>