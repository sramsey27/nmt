#include <stdio.h>

float sum_coins(int p, int n, int d, int q);

float sum_coins(int p, int n, int d, int q)
{
	
	float sum = ((q * 0.25) + (d * 0.10) + (n * 0.05) + (p * 0.01));
	return sum;
}
int main(void)
{

	int pennies = 13;
	int nickels = 7;
	int dimes = 18;
	int quarters = 27;

	float change = sum_coins(pennies, nickels, dimes, quarters);

	printf("\nThe total amount of change is %.2f dollars.\n\n",change);

	return 0;
}
