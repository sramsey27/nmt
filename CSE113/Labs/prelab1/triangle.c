#include <stdio.h>

int tri(int a, int b);
int triangle(int c, int d);

int tri(int a, int b)
{
	if (a + b > 180) {
		printf("Angle too big");
		return 0;
	}

	if (a + b < 180) {
		return 180 - a + b;
	}
	return 0;
}

int triangle(int c, int d)
{
	if (c + d > 180) {
		printf("Angle too big");
		return 0;
	}

	if (c + d < 180){
		return 180 - c + d;
	}
	return 0;
}

int main(void)
{
	int a = 45;
	int b = 45;

	int tri_sum = tri(a, b);

	int c = 30;
	int d = 60;

	int tri2_sum = triangle(c, d);

	printf("\nThe missing angle for a triangle with two angles equaling %d and %d is %d degrees.\n",a, b, tri_sum);
	printf("\nThe missing angle for a triangle with two angles equaling %d and %d is %d degrees.\n",c, d, tri2_sum);
 
	return 0;
}
