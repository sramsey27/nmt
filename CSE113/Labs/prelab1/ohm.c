#include <stdio.h>

int volts(void)
{
	int i = 1;
	int r = 4000;
	int out1 = i*r;
	return out1;
}

int amps(void)
{
	int v = 15;
	int r = 1000;
	int output = r/v;
	return output;
}

int ohms(void)
{
	int v = 120;
	int i = 15;
	int out3 = v/i;
	return out3;
}

int main(void)
{
	int amp_out = amps();
	int volt_out = volts();
	int ohm_out = ohms();
	
	printf("\nThe amount of amps is %d.\n",amp_out);
	printf("The amount of volts is %d.\n",volt_out);
	printf("The amount of ohms is %d.\n\n",ohm_out);

	return 0;
}

	
