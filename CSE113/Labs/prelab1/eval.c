#include <stdio.h>

float expression(int n);

float expression(int n)
{
	
	float a = n/3+2/3;
	return a;

}

int main(void)
{

	/* all of the values for n (0, 1, 5 and 10) was respectively tested.*/	

	int n = 10;
	float y = expression(n);
	printf("\nWhen n = %d, The expression equals %.2f.\n\n", n, y);

	return 0;
}

