#include <stdio.h>

float dollars_euro(float dollars);
float euro_dollars(float euros);

float dollars_euro(float dollars)
{
	float euro = dollars * 0.75;
	return euro;
}

float euro_dollars(float euros)
{
	float dollars_out = euros * 1.33;
	return dollars_out;
}

int main(void)
{
	float dollars = 100.25;
	float euros = 100.25;
	
	float dollar_exchange = dollars_euro(dollars);
	float euro_exchange = euro_dollars(euros);

	printf("\n%.2f dollars converts to %.2f euros.\n",dollars, dollar_exchange);
	printf("\n%.2f euros converts to %.2f dollars.\n\n",euros, euro_exchange);

	return 0;

}
 

 
