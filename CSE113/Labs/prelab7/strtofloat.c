#include <stdio.h>

float str_to_float(const char string[])
{
    float result= 0;
    int i, point_pos, intvalue;

    for (i = 0; string[i] != '\0'; i++) {
        //printf("%c\n", string[i]);
        intvalue = string[i] - '0';

        if (string[i] == '.') {
            point_pos = 1; 
        }

        else {
        
            if (point_pos == 1){
                result = (result) / (10.0f);
            }
        
        result = result * 10.0f + intvalue; 

        }  
        //printf("\nresult = %f", result
    }

    return result;
}

int main(void)
{
    printf("%f\n", str_to_float("55.55") + str_to_float("66.6666"));
    printf("%f\n", str_to_float("0055.5500") + str_to_float("000066.66660000"));
    printf("%f\n", str_to_float(".000005") + str_to_float("0.00000600"));
    printf("%f\n", str_to_float("12345") + str_to_float("54321"));

    return 0;
}
