/**
 * @file functions.h
 * 
 * @author Walter Flores
 *
 * @date 11/20/13
 *  
 * Assignment: Lab final 
 *
 * @brief header for functions.c
 *
 * @details see funcitons.c for details
 *  
 * @bugs none
 *
 * @todo 
 */

#ifndef FINAL_H_
#define FINAL_H_
#define LEN 4096

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct person_t{
	char *name;
	int age;
	int weight;
	enum vehicle;
	float alcohol;
	float EBAC;
	struct person_t *next;
};

struct info_t{
	struct person_t *head;
};

struct info_t *create_list();
struct person_t *create_node();
void insert(struct info_t *list, struct person_t *node);
int count_nodes(struct info_t *list);
//void delete_list(struct info_t *list);
//struct person_t *over_21(struct info_t *list, char name);
//struct person_t *under_21(struct info_t *list, char name);
//struct person_t *commercial(struct info_t *list, char name);
void ReadFile(struct info_t *list, char *filename);
void PrintFile(struct info_t *list);

#endif
