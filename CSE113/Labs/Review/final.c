/**
 * @file final.c
 * 
 * @author Walter Flores
 *
 * @date 11/20/13
 *  
 * Assignment: Lab Final
 *
 * @brief a program that tests the functions in functions.c
 *
 * @details 
 *  
 * @bugs none
 *
 * @todo none
 */

#include "functions.h"

int main()
{
	char buf[LEN];
	struct info_t *list = create_list();
	struct person_t *nodeon = list->head;
	printf("Enter file name: ");
	fgets(buf, LEN, stdin);

	ReadFile(list, buf);
	PrintFile(list);

	while(nodeon){
		printf("%s, %d, %d\n", nodeon->name, nodeon->age, nodeon->weight);
		nodeon->nodeon->next;
	}
	delete_list(list);
	free(list);
	list = NULL;

	return 0;
}
