/**
 * @file functions.c
 * 
 * @author Walter Flores
 *
 * @date 11/20/13
 *  
 * Assignment: Lab Final 
 *
 * @brief a program that reads in a file and outputs info to a different file
 *
 * @details 
 *  
 * @bugs none
 *
 * @todo none
 */

#include "functions.h"

/**
 *
 *@description creates a new list
 *@param none
 *@return a pointer to a new list
 */
struct info_t *create_list()
{
	struct info_t *list = malloc(sizeof(struct info_t));
	list->head = NULL;
	return list;	
}

/**
 *
 *@description creates a node
 *@param none
 *@return, the node created
 */
struct person_t *create_node()
{
	struct person_t *node = malloc(sizeof(struct person_t));
	node->name = NULL;
	node->next = NULL;
	return node;
}

/**
 *
 *@description inserts a node at the tail of list
 *@param struct info_t *list, the list
 *@param struct person_t *node, node inserted
 */
void insert(struct info_t *list, struct person_t *node)
{
	if(NULL == list->head){
		list->head = node;
	}
	struct person_t *nodeon = list->head;
	while(NULL != nodeon->next){
		nodeon = nodeon->next;
	}
	nodeon->next = node;
	node->next = NULL;
}
		
/**
 *
 *@description counts nodes in list
 *@param count, variable count
 *@return, the number of nodes
 */
int count_nodes(struct info_t *list)
{
	int count = 0;

	if(NULL == list->head){

	}
	struct person_t *nodeon = list->head;
	while(NULL != nodeon){
		count++;
		nodeon = nodeon->next;
	}
	return count;
}

/**
 *
 *@description deletes list
 *@param struct person_t *nodeon, where node is currently on
 *@param struct person_t *delete_node, node deleted
 */
void delete_list(struct info_t *list)
{
	struct person_t *nodeon = list->head;
	struct person_t *delete_node = NULL;
	
	if(NULL == list->head){
		return;
	}
	while(NULL != nodeon){
		if(NULL != nodeon->name){
			free(nodeon->name);
		}
		delete_node = nodeon;
		nodeon = nodeon->next;
		free(delete_node);
	}	
	list->head = NULL;
}

/**
 *
 *@description calculate's estimated blood alcohol content(EBAC)
 *@param none
 */
void EBAC(struct person_t *node)
{
	node->EBACe = 0.0f;
	node->EBAC = (float)(((node->alcohol * 5.14f) / (weight * (0.55f))) - 0.015f);
}

/**
 *
 *@description finds all persons over the age of 21 that are guilty
 *@param 
 *@return, # of people over 21 DUI
 */
struct person_t *over_21(struct info_t *list, char name)
{
	int over_21 = list->head->age;
	struct person_t *ov_21 = list->head;
	struct person_t *nodeon = list->head;
	
	while(nodeon){
		if(name == nodeon->name && ov_21 < nodeon->age){
			over_21 = nodeon->age;
			ov_21 = nodeon;
		}
		nodeon = nodeon->next;
	}
	return ov_21;
}

/**
 *
 *@description finds all persons under the age of 21 that are guilty
 *@param 
 *@return, # of people under 21 DUI
 */
struct person_t *under_21(struct info_t *list, char name)
{
	int min = list->head->EBACs;
	struct person_t *minx = list->head;
	struct person_t *nodeon = list->head;
	
	while(nodeon){
		if(name == nodeon->name && min > nodeon->EBACe){
			min = nodeon->EBACe;
			minx = nodeon;
		}
		nodeon = nodeon->next;
	}
	return minx;
}

/**
 *
 *@description DUI's of people driving commericial vehicles
 *@param
 *@return, # of people commerical driving DUI
 */
struct person_t *commercial(struct info_t *list, char name)
{
	int com = 0;
	int count = 0;
	struct person_t *nodeon = list->head;
		
	while(nodeon){
		if(name == nodeon->name){
			com += nodeon-EBACe;
			count++;
		}
		nodeon = nodeon->next;
	}
	return count;
}

/**
 *
 *@description reads file
 *@param
 */
void ReadFile(struct info_t *list, char *filename)
{
	char buf[LEN];
	char *tmp = NULL;
	struct node_t *nodeon = NULL;
	FILE *file = fopen(filename, "r");
	
	while(fgets(buf, LEN, file)){
		if('\n' == buf[strlen(buf) - 1]){
			buf[strlen(buf) - 1] = '\0';
		}
		/*makes record*/
		nodeon = create_node();
		/*reads name*/
		tmp = strtok(buf, ",");
		nodeon->name = malloc(sizeof(char) * (strlen(tmp) + 1));
		strcpy(nodeon->name, tmp);
		/*reads age*/
		tmp = strtok(NULL, ",");
		//nodeon->a = tmp[0];
		/*reads weight*/
		tmp = strtok(NULL, ",");
		/*reads vehicle*/
		tmp = strtok(NULL, ",");
		nodeon->name = tmp[0];
		/*reads alcohol*/
		EBAC(nodeon);
		/*node inserted to list*/
		insert(list, node);
	}
	fclose(file);
}

void PrintFile(struct info_t *list)
{
	char *filename = "./output.txt";
	FILE *file = fopen( filename, "w" );
	struct person_t *minx = under_21(list, name);
	struct person_t *ov_21 = over_21(list, name);
	
	fprintf(file, "Number of people: %d\n", count_nodes(list));
	fprintf(file, "Number of people guilty of DUI: %d\n", count_nodes(list));
	fprintf(file, "Number of people over 21 guilty of DUI: %d\n", ov_21->name, ov_21->EBACe);
	fprintf(file, "Number of people under 21 guilty of DUI: %d\n", minx->name, minx-EBACe);
	fprintf(file, "Number commericial guilty of DUI: %d\n", com->name, com->EBACs);
	fclose(file);
}

