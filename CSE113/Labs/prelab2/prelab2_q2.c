#include <stdio.h>

int main()
{
	/*Defines variable type*/
	int input = 0;
	
	/*Takes user input for an integer*/
	printf("Enter an integer: \n");
	scanf("%d", &input);
	
	
	/* Similar to if statements, if the input
	 matches one of the corresponding cases,
	 the nested lines will execute.*/
	
	switch (input) {
	case 1:
		printf("One is the loneiest number.\n");
		break;
	case 3:
		printf("Three is just as bad as one. ");
		printf("But worse, Because it's 3!\n");
		break;
	case 7:
		printf("Seven is lucky.\n");
		break;
		
	/*This line below executes if the input doesn't
	 match any of the above cases*/
	
	default:
		printf("I can't sing about %d.\n", input);
	}
	
	return 0;
}