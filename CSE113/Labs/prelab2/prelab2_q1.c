#include <stdio.h>

int main()
{
	/* States variable type and assigns number*/
	int input = 0;
	
	/*User input*/
	printf("Enter an interger: \n");
	scanf("%d", &input);
	
	/*Checks to see if the number (input) matches 
	one of the corresponding conditions*/
	
	if (input == 1) {
		printf("First is the worst.\n");
	}
	
	else if (input == 2) {
		printf("Second is the best. ");
		printf("Because it's the first prime!\n");
	}
	else if (input == 5) {
		printf("Fifth has to lift!\n");
	}
	
	/*if the number doesn't match any of these
	 this line below executes*/
	else {
		printf("I can't sing about %d.\n", input);
	}

	return 0;
	
}