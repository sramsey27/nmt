#include <stdio> 

/* There was 7 errors within this source code rendering it 
broken. */

/* for starters, the <stdio> is suppose to have an .h
at the end of it like so: <stdio.h>.*/

void main()
{

/* main () is usally defined as an integer (int), not 
as void, this means the program won't represent anything.*/

	char input;
	
	printf("Enter a character: \n");
	scanf("%f", input);
	
	/* %f is for floats, %c (for chars) would 
	work better here. */
	
	switch (input) {
	case 'a':
		  
		printf("A is for apple. \n");
		
		/*There should be a break; after each
		case. There isn't one here. */
		
	case 'b'; /* < -- There is a semi colon instead
		  of a colon here. */
		  
		printf("B is for Banana!/n");
		
		/* The new line character isn't correct.
		Instead of /n, it is \n. */
	
		break;
		
	default: 
		printf("The letter %d isn't important!\n", input);
		
		/* The token %d is for integers, %c is
		more suitable here. */

	}
	
	return 0;

}


