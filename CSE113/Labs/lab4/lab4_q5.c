#include <stdio.h>
#include <stdlib.h>

#define Execute 1
#define Quit 2

int get_color(void);

enum color
{
	RED = 1, ORANGE , YELLOW, GREEN, BLUE, INDIGO, VIOLET
};

/**
 * Recieves color choice from user
 * @return number value for corresponding color
 */
int get_color(void)
{
	int color_num;

	printf("\nEnter one of the following colors:");
	printf(" \n1. Red\n2. Orange\n3. Yellow\n4. Green\n5. Blue\n6. Indigo\n7. Violet\n");
	printf("\nChoice: ");
	scanf("%d", &color_num);

	return color_num;
}

int main (void)
{

	int menu, a;

	printf("\nWelcome!\nEnter 1 or 2 depending on your choice.\n");

	while (1) {
		printf ("\n1. Enter a color of your choice\n");
		printf ("2. Quit\n");
		printf ("\nChoice: ");
		scanf ("%d",&menu);

		/* Uses Defined variables for switch */
		switch (menu) {
		case Execute:
			a = get_color();
			switch (a){
			case RED:
				printf("\nYou chose Red.\n");
				break;
			case ORANGE:
				printf("\nYou chose Orange.\n");
				break;
			case YELLOW:
				printf("\nYou chose Yellow.\n");
				break;
			case GREEN:
				printf("\nYou chose Green.\n");
				break;
			case BLUE:
				printf("\nYou chose Blue.\n");
				break;
			case INDIGO:
				printf("\nYou chose Indigo.\n");	
				break;
			case VIOLET:
				printf("\nYou chose Violet.\n");
				break;
			default:
				printf("\nJust like seeing the color nine.\n");
				break;
			}
			break;
		case Quit:
			printf ("\nQuiting...\n\n");
			exit (EXIT_SUCCESS);
			break;
		default:
			printf ("\nInvalid Choice! Please Try Again!\n");
			break;
		}
	}

	return 0;

}

