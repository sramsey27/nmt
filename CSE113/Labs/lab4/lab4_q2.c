#include <stdio.h>

enum color
{
	RED = 18, ORANGE, YELLOW = 5, GREEN, BLUE, INDIGO = 14, VIOLET
};

int main(void)
{

	printf("\nRED = %d\nORANGE = %d\nYELLOW = %d\nGREEN = %d\n", RED, ORANGE, YELLOW, GREEN);
	printf("BLUE = %d\nINDIGO = %d\nVIOLET = %d\n\n", BLUE, INDIGO, VIOLET);

	return 0;
}