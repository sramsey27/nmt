#include <stdio.h>

enum month 
{
	JAN = 1, FEB, MAR, APR, MAY, JUNE, JULY,
	AUG, SEPT, OCT, NOV, DEC
};

/**
 * Collects user input
 * @return nothing
 */
int get_month(void)
{
	int Mon;
	printf("\nEnter the Month:\nExample: 2 for Feburary.\nMonth = ");
	scanf("%d",&Mon);
	return Mon;

}
int main(void)
{
	int sel_month = get_month();
	switch (sel_month){
	case JAN:
		printf("\nUser Selected January.\n\n");
		break;
	case FEB:
		printf("\nUser Selected Feburary.\n\n");
		break;
	case MAR:
		printf("\nUser Selected March.\n\n");
		break;
	case APR:
		printf("\nUser Selected April.\n\n");
		break;
	case MAY:
		printf("\nUser Selected May.\n\n");
		break;
	case JUNE:
		printf("\nUser Selected June.\n\n");
		break;
	case JULY:
		printf("\nUser Selected July.\n\n");
		break;
	case AUG:
		printf("\nUser Selected August.\n\n");
		break;
	case SEPT:
		printf("\nUser Selected September.\n\n");
		break;
	case OCT:
		printf("\nUser Selected October.\n\n");
		break;
	case NOV:
		printf("\nUser Selected November.\n\n");
		break;
	case DEC:
		printf("\nUser Selected December.\n\n");
		break;
	default:
		printf("\nInvalid Month.\n\n");
	}
	
	return 0;
}