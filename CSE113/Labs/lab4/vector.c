#include "vector.h"
#include <stdio.h>

#define SIZE 10

/* Place all of your functions here for your Array Problems, #1-8 */

/**
 * Multiplies the array a[] by the multiple and prints the result.
 * @param a the array to be multiplied
 * @param multiple the number the array gets multiplied by
 * @return nothing
 */
void multiply_array(int a[], int multiple)
{
	int i = 0;
	for(i = 0; i < SIZE; i++){
		a[i] *= multiple;
		printf("%d, ", a[i]);	
	}

}

/**
 * Adds array a1[] by the number (add).
 * @param a1 the array to be added upon
 * @param add number that is added to each index
 * @return nothing
 */
void add_array(int a1[], int add)
{
	int i = 0;
	for(i = 0; i < SIZE; i++){
		a1[i] += add;
		printf("%d, ", a1[i]);
	}

}

/**
 * Replaces array a2 with b2
 * @param a2 array to be replaced
 * @param b2 replacement array
 * @return nothing
 */
void replace_array(int a2[], int b2[])
{
	int i = 0;
	for(i = 0; i < SIZE; i++){
		b2[i] = a2[i];
		printf("%d, ",b2[i]);
	}

}

/**
 * Adds array a3 and b3 together then puts them in array c3
 * @param a3 array of numbers (to be added)
 * @param b3 array of numbers (to be added)
 * @param c3 the holding array for the sum
 * @return nothing
 */
void sumof_arrays(int a3[], int b3[], int c3[])
{
	int i = 0;
	for(i = 0; i < SIZE; i++){
		c3[i] = a3[i] + b3[i];
		printf("%d, ", c3[i]);
	}

}

/**
 * Multiplies array a4 by array b4 then is stored in array c4
 * @param a4 an array of numbers (to be multiplied)
 * @param b4 an array of numbers (to be multiplied)
 * @param c4 storing array for product
 * @return nothing
 */
void product_arrays(int a4[], int b4[], int c4[])
{
	int i = 0;
	for(i = 0; i < SIZE; i++){
		c4[i] = a4[i] * b4[i];
		printf("%d, ", c4[i]);
	}
}

/**
 * Multiplies two arrays (a5, b5) resulting in an inverted product stored in array c5.
 * @param a5 an array of numbers (to be multiplied)
 * @param b5 an array of numbers (to be multiplied)
 * @param c5 holding array for inverted product
 * @param size_in the size of the arrays (determined by a5)
 * @return nothing
 */
void invert_array(int a5[], int b5[], int c5[], size_t size_in)
{
	int i = 0;
	for(i = 0; i < size_in; i++)
		c5[i] = a5[i] * b5[size_in- 1 -i];
		
	
	for(i = 0; i < size_in; i++)
		printf("%d, ", c5[i]);

}

/**
 * Reverses the order of array a
 * @param a an array of numbers to be reversed
 * @return nothing
 */
void reverse_array(int a[])
{
	int temp, i;
	for (i = 0; i < (SIZE / 2); i++) {
		temp = a[i];
		a[i] = a[SIZE - i - 1];
		a[SIZE - i - 1] = temp;
		}
	for (i = 0; i < SIZE; i++)
		printf("%d, ", a[i]);
}

/**
 * Calcuates random number
 * @param n the limiting number
 * @return the random number
 */
int get_random_int(int n)
{
	return random() % n;
}

/**
 * Creates a random generated array
 * @param size_ar size of array
 * @return nothing
 */
void random_array(size_t size_ar)
{
	int a,b,c,d,e,i;
	srandom(time(NULL));

	a = get_random_int(50);
	b = get_random_int(50);
	c = get_random_int(50);
	d = get_random_int(50);
	e = get_random_int(50);

	int ar[] = {a, b, c, d, e};

	for(i = 0; i < size_ar; i++)
		printf("%d, ", ar[i]);

}

