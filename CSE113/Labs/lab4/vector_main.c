#include <stdio.h>
#include "vector.h"

#define SIZE 10

int main(void)
{
	/*Write code for your main() for your Array Problems 1-8 here*/
	
	int multiple = 5;
	int add = 5;
	int i;

	int a[] = {1,2,3,4,5,6,7,8,9,3};

	int a1[] = {1,2,3,4,5,6,7,8,9,3};

	int a2[] = {1,2,3,4,5,6,7,8,9,3};
	int b2[] = {0,0,0,0,0,0,0,0,0,0};

	int a3[] = {1,2,3,4,5,6,7,8,9,3};
	int b3[] = {10,10,10,10,10,10,10,10,10,10};
	int c3[] = {0,0,0,0,0,0,0,0,0,0};

	int a4[] = {1,2,3,4,5,6,7,8,9,3};
	int b4[] = {10,10,10,10,10,10,10,10,10,10};
	int c4[] = {0,0,0,0,0,0,0,0,0,0};

	int a5[] = {1,2,3,4,5};
	int b5[] = {6,7,8,9,10};
	int c5[] = {0,0,0,0,0};

	int a6[] = {1,2,3,4,5,6,7,8,9,10};

	printf("\nMultiply_Array:\na[] = ");
	multiply_array(a, multiple);

	printf("\n\nAdd_Array:\na1[] = ");
	add_array(a1, add);

	printf("\n\nReplace_Array:\nOriginal: ");
	for(i = 0; i < SIZE; i++){
		printf("%d, ", b2[i]);
	}

	printf("\nModifyed: ");
	replace_array(a2, b2);

	printf("\n\nSumof_Arrays:\nc3[] = ");
	sumof_arrays(a3, b3, c3);

	printf("\n\nProduct_array:\nc4[] = ");
	product_arrays(a4, b4, c4);

	size_t size_invert = sizeof(a5)/sizeof(int);

	printf("\n\nInvert_array:\nc5[] = ");
	invert_array(a5, b5, c5, size_invert);
	printf("%zu", size_invert);

	printf("\n\nReversed Array:\na[6] = ");
	reverse_array(a6);

	printf("\n\nRandom Generated Array: \nar[] = ");
	random_array(size_invert);

	printf("\n");

	return 0;
}

