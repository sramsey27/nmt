#include <stdio.h>

struct date_t get_date(struct date_t date);
void zodiac(struct date_t date);

enum month
{
	JAN = 1, FEB, MAR, APR, MAY, JUNE, JULY,
	AUG, SEPT, OCT, NOV, DEC
};

struct date_t
{
	unsigned short Mon, Day, Year;
};

/**
 * Collects date from user
 * @param date structure containing variables for date
 * @return the date entered
 */
struct date_t get_date(struct date_t date)
{
	printf("\nEnter a date using the following format: mm/dd/yy\n\nDate: ");
	scanf("%02hu/%02hu/%02hu", &date.Mon, &date.Day, &date.Year);
	return date;

}

/**
 * Prints zodiac name corresponding to entered date
 * @param date the entered date stored in the structure
 * @return nothing
 */
void zodiac(struct date_t date)
{
	switch (date.Mon){
	case JAN:
		printf("January %hu, ", date.Day);
		if (date.Day <= 20)
			printf("Capricorn\n");
		
		else if (date.Day > 20)
			printf("Aquarius\n\n");
		else
			printf("\nError\n");
		break;
	case FEB:
		printf("Febuary %hu, ", date.Day);
		if (date.Day <= 19)
			printf("Aquarius\n");
		else if (date.Day > 19)
			printf("Pisces\n");
		else
			printf("\nError\n");
		break;
	case MAR:
		printf("March %hu, ", date.Day);
		if (date.Day <= 20)
			printf("Pisces\n");
		else if (date.Day > 20)
			printf("Aries\n");
		else
			printf("\nError\n");
		break;
	case APR:
		printf("April %hu, ", date.Day);
		if (date.Day <= 20)
			printf("Aries\n");
		else if (date.Day > 20)
			printf("Taurus\n");
		else
			printf("\nError\n");
		break;
	case MAY:
		printf("May %hu, ", date.Day);
		if (date.Day <= 21)
			printf("Taurus\n");
		else if (date.Day > 21)
			printf("Gemini\n");
		else
			printf("\nError.\n");
		break;
	case JUNE:
		printf("June %hu, ", date.Day);
		if (date.Day <= 21)
			printf("Gemini\n");
		else if (date.Day > 21)
			printf("Cancer\n");
		else
			printf("\nError.\n");
		break;
	case JULY:
		printf("July %hu, ", date.Day);
		if (date.Day <= 22)
			printf("Cancer\n");
		else if (date.Day > 22)
			printf("Leo\n");
		else
			printf("\nError.\n");
		break;
	case AUG:
		printf("August %hu, ", date.Day);
		if (date.Day <= 23)
			printf("Leo\n");
		else if (date.Day > 23)
			printf("Virgo\n");
		else
			printf("\nError.\n");
		break;
	case SEPT:
		printf("September %hu, ", date.Day);
		if (date.Day <= 22)
			printf("Virgo\n");
		else if (date.Day > 22)
			printf("Libra\n");
		else
			printf("\nError.\n");
		break;
	case OCT:
		printf("October %hu, ", date.Day);
		if (date.Day <= 23)
			printf("Libra\n");
		else if (date.Day > 23)
			printf("Scorpio\n");
		else
			printf("\nError.\n");
		break;
	case NOV:
		printf("November %hu, ", date.Day);
		if (date.Day <= 22)
			printf("Scorpio\n");
		else if (date.Day > 22)
			printf("Sagittarius\n");
		else
			printf("\nError.\n");
		break;
	case DEC:
		printf("December %hu, ", date.Day);
		if (date.Day <= 20)
			printf("Sagittarius\n");
		else if (date.Day > 20)
			printf("Capricorn\n");
		else
			printf("\nError.\n");
		break;
	default:
		printf("\nError.\n");
	}
}

int main(void)
{
	int quit;

	printf("\nWelcome!\nEnter 1 or 2 depending on your choice.\n");

	/* Provides the menu choices, I decided to do a do while loop just as 
	   as an experiment, works like it should. */
	do {
		int menu = 0;

		printf("\n1. Enter a birthday\n");
		printf("2. Quit \n");
		printf("Choice: ");
		scanf ("%d",&menu);

		if (menu == 2){
			printf("\nQuiting...\n\n");
			quit = 5;
		}
		else if (menu == 1){

			struct date_t date;
			date = get_date(date);
	
			printf("Date Entered: %hu/%hu/%hu\n\n", date.Mon, date.Day, date.Year);
	
			zodiac(date);
		}
		else {
			printf("\nInvalid Choice! Please try again.\n");
		}
		
	}while (!quit);

	return 0;
}
