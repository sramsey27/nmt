#include "array_struct.h"
#include <stdio.h>

/**
 * Hold the values for the subjects infomation
 * @param data holds the array
 * @param index the index of the stored data (a call basically)
 * @param id the subjects id char
 * @param years the year of the corresponding subject
 * @param inches the height of the corresponding subject
 * @return nothing
 */
void fill_array(struct data_t data[], int index, char id, int years, int inches)
{
	(data[index]).subject = id;
	(data[index]).age = years;
	(data[index]).height = inches;
}

/**
 * Finds the minimum age of all subjects
 * @param data the array containing subject data
 * @return the index of where the minimum age was found
 */
int min_age(struct data_t data[])
{
	
	int i = 0, min_a, tmp;
	min_a = data[0].age;
	for(i = 0; i <= 4; i++){
		if (data[i].age < min_a){
			min_a = data[i].age;
			tmp = i;
		}
	}
	
	return tmp;
}

/**
 * Finds the minimum height of all subjects
 * @param data the array containing subject data
 * @return the index where the minimum height was found
 */
int min_height(struct data_t data[])
{
	int i = 0;
	int min_h = data[0].height;
	int tmp;
	for(i = 0; i < 5; i++){
		if (data[i].height < min_h) {
			min_h = data[i].height;
			tmp = i;
		}
		
	}
	return tmp;
}

/**
 * Finds the max age of all subjects
 * @param data the array containing subject data
 * @return the index of where the max age was found
 */
int max_age(struct data_t data[])
{
	int i, max_a = 0, tmp;

	for(i = 0; i < 5; i++){
		if (data[i].age > max_a){
			max_a = data[i].age;
			tmp = i;
		}
	}
	return tmp;
	
}

/**
 * Finds the max height of all subjects
 * @param data the array containing subject data
 * @return the index of where the max height was found
 */
int max_height(struct data_t data[])
{
	int i, max_h = 0, tmp;

	for(i = 0; i < 5; i++){
		if (data[i].height > max_h){
			max_h = data[i].height;
			tmp = i;
		}
	}

	return tmp;
	
}

/**
 * Finds the average age of all subjects
 * @param data the array containing subject data
 * @return the index the average age of the subjects
 */
int average_age(struct data_t data[])
{
	int i = 0, av_a = 0, size = 5, sum = 0;
	for (i = 0; i < 5; i++){
		sum += data[i].age;
	}
	av_a = sum / size;
	return av_a;


}

/**
 * Finds the average height of all subjects
 * @param data the array containing subject data
 * @return the average height of the subjects
 */ 
int average_height(struct data_t data[])
{
	int i = 0, av_h = 0, size = 5, sum = 0;
	for (i = 0; i < 5; i++){
		sum += data[i].height;
	}
	av_h = sum / size;
	return av_h;
}

/**
 * Prints the struct data of the subjects
 * @param data the array containing subject data
 * @return nothing
 */
void print_structure(struct data_t data)
{
	printf("\nSubject: %c\nAge: %d\nHeight: %d\n", data.subject, data.age, data.height);
}

int main(void)
{

	struct data_t data[SIZE];
	
	int minimum_a, minimum_h, max_ag, max_hei, average_a, average_h;

	/* data initialization calls  - this matches the function prototype in array_struct.h*/
	fill_array(data, 0, 'A', 23, 74);
	fill_array(data, 1, 'B', 22, 64);
	fill_array(data, 2, 'C', 19, 68);
	fill_array(data, 3, 'D', 20, 76);
	fill_array(data, 4, 'E', 22, 62);
	
	minimum_a = min_age(data);
	print_structure(data[minimum_a]);

	minimum_h = min_height(data);
	print_structure(data[minimum_h]);

	max_ag = max_age(data);
	print_structure(data[max_ag]);

	max_hei =  max_height(data);
	print_structure(data[max_hei]);


	average_a = average_age(data);

	printf("\nThe average age is %d years old.\n", average_a);

	average_h = average_height(data);

	printf("\nThe average height is %d inches.\n\n", average_h);
	
	return 0;
	
	
}

