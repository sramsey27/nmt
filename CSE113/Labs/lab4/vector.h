#ifndef VECTOR_H_
#define VECTOR_H_

#include <stdlib.h>
#include <time.h>

/* Place all of your function prototypes here for your Array Problems, #1-8 */

void multiply_array(int a[], int multiple);
void add_array(int a1[], int add);
void replace_array(int a2[], int b2[]);
void sumof_arrays(int a3[], int b3[], int c3[]);
void product_arrays(int a4[], int b4[], int c4[]);
void invert_array(int a5[], int b5[], int c5[], size_t size);
void reverse_array(int a[]);
int get_random_int(int n);
void random_array(size_t size);

#endif
