#ifndef SDL_H
#define SDL_H

#include "SDL/SDL.h"

void sdl_init(int width, int height, SDL_Surface *screen);
/* size must be 2, 4, 8, or 16 otherwise defaults to 2 */
void sdl_render_life(unsigned char **life, int width, int height, SDL_Surface *screen, int color, int size); 
int sdl_set_color(unsigned char red, unsigned char blue, unsigned char green);
void sdl_test(int width, int height, int m, int n, int size, SDL_Surface *screen);

#endif
