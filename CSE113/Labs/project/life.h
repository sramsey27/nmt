/**
 * @file life.h
 * 
 * @brief a header file for life.c
 *
 * @author Steven Ramsey
 *
 * @date 4/19/14
 *
*/

#ifndef LIFE_H_
#define LIFE_H_

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define LEN 256
#define EXIT_PROGRAM 1227

unsigned char **initialize_matrix(int rows, int cols);
char* rstrip(char* buf); 
void read_file(unsigned char** matrix_a, char* filename, int cols, int rows, int cy, int cx);
void clear_matrix(unsigned char** matrix, int cols, int rows);
void swap(unsigned char** matrix_a, unsigned char** matrix_b, int cols, int rows);
void delete_matrix(unsigned char** matrix_a, unsigned char** matrix_b, int rows);
void hedge(unsigned char** ma, unsigned char** mb, int cols, int rows);
void torus(unsigned char** ma, unsigned char** mb, int cols, int rows);
void klein_bottle(unsigned char** ma, unsigned char** mb, int cols, int rows);

#endif
