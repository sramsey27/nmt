#include <stdlib.h>
#include "SDL/SDL.h" 
#include "sdl.h"
#include "life.h"

int main(int argc, char *argv[])
{
	SDL_Surface *screen;
	int color = 200;
	int width = 640;
	int height = 480;
	int size = 4;
	//int m = -6; 	/*These are not needed for the code to work*/
	//int n = 4;
	int red = 255;
	int green = 255;
	int blue = 255;

	printf("\nSTATUS:\n------------------------------------------");

	/*Command line arguments*/
	char* filename = NULL;
	int edge_mode = 0, tsize = 0, cord_x = 0, cord_y = 0, i = 0;
	for(i = 1; i < argc; i++){ 	/*Gets rid of the need for getopt*/
		if(argv[i][0] == '-'){ 	/*Works no matter where the flag is*/
			switch(argv[i][1]){
				case 'w':
					/*Assign resolution width*/
					printf("\nWidth = %s", argv[i + 1]);
					width = atoi(argv[i + 1]);
					break;
				case 'h':
					/*Assign resolution height*/
					printf("\nHeight = %s\n", argv[i + 1]);
					height = atoi(argv[i + 1]);
					break;
				case 'r':
					/*Color red, keeps user from exceeding 255*/
					if(atoi(argv[i + 1]) > 255){
						printf("\n-r cannot exceed 255!");
						printf("\n%d used instead.\n", red);
						break;
					}
					else{
						printf("\nRed value = %s\n", argv[i + 1]);
						red = atoi(argv[i + 1]);
						break;
					}
				case 'g':
					/*Color green, keeps user from exceeding 255*/
					if(atoi(argv[i + 1]) > 255){
						printf("\n-g cannot exceed 255!");
						printf("\n%d used instead.\n", green);
						break;
					}
					else{
						printf("Green value = %s\n", argv[i + 1]);
						green = atoi(argv[i + 1]);
						break;
					}
				case 'b':
					/*Color blue, keeps user from exceeding 255*/
					if(atoi(argv[i + 1]) > 255){
						printf("\n-b cannot exceed 255!");
						printf("\n%d used instead.\n", blue);
						break;
					}
					else{
						printf("Blue value = %s\n", argv[i + 1]);
						blue = atoi(argv[i + 1]);
						break;
					}
				case 's':
					/*Keeps segmentation fault at bay*/
					tsize = atoi(argv[i + 1]);
					if(tsize == 2 || tsize == 4 || tsize == 8 || tsize == 16) {
						printf("\nSprite Size = \"%d\"\n", tsize);
						size = tsize;
						break;
					}
					else {
						printf("\nSize must be 2,4,8, or 16!\n");
						printf("%d used instead.\n", size);
						break;
					}
				case 'f':
					/*Allocates space for user input for file*/
					printf("\nFilename: \"%s\"\n", argv[i + 1]);
					filename = malloc(sizeof(char) * strlen(argv[i + 1]) + 1);
					strcpy(filename, argv[i + 1]);

					if(filename == NULL){
						printf("\nFile reading error...\nExitting...");
						exit(EXIT_PROGRAM);
					}

					break;
				case 'o':
					sscanf(argv[i + 1], "%d,%d",&cord_x, &cord_y);
					printf("\nCoordinates \"(%d,%d)\"",cord_x, cord_y);
					printf(" achknowledged.\n");
					break;
				case 'e':
					/*Assigned edge mode depending on input*/
					printf("\nEdge mode = \"%s\" achknowledged.\n", argv[i + 1]);
					if(strcmp(argv[i + 1], "hedge") == 0)
						edge_mode = 1;
					else if(strcmp(argv[i + 1], "Hedge") == 0)
						edge_mode = 1;
					else if(strcmp(argv[i + 1], "torus") == 0)
						edge_mode = 2;
					else if(strcmp(argv[i + 1], "Torus") == 0)
						edge_mode = 2;
					else if(strcmp(argv[i + 1], "klein") == 0)
						edge_mode = 3;
					else if(strcmp(argv[i + 1], "Klein") == 0)
						edge_mode = 3;
					else{
						printf("\nInvalid Entry for Edge mode.\n");
						printf("\"Edge mode = torus\" used by default.\n");
					}
					break;
				case 'H':
					/*The Help menu accessible by -H help*/
					printf("\nCommands\t\tDesription\n");
					printf("\n\t-w\tis the width of screen resolution (integer)\n");
					printf("\t-h\tis the height of screen resolution (integer).\n\n");
					printf("\t\tNOTE:\tIf no resolution size is provided.");
					printf(" the\n\t\t\tdefault is 640X480\n\n");
					printf("-r, -g, -b\tare colors. Red, Green, Blue (integer)\n\n");
					printf("\t\tNOTE:\tValues must not exceed 255 or default values\n");
					printf("\t\t\twill take place!\n\n");
					printf("\t-s\tis the size of the sprites. (integer)\n\n");
					printf("\t\tNOTE:\tsize must be 2, 4, 8 or 16");
					printf(" or size will\n\t\t\tdefault to 4!\n\n");
					printf("\t-f\tis the filename. (string)\n\n\t\t");
					printf("NOTE:\tIf no filename is provided");
					printf(" the program will\n\t\t\texit.\n\n");
					printf("\t-o\tare the inital corrdinates (x,y).\n");
					printf("\t\tNOTE:\tIf no Coordinates are provided they will\n");
					printf("\t\t\tdefault to (0,0)\n");
					printf("\t-e\tis the edge mode.\thedge, torus, or klein");
					printf("\n\n\t\tNOTE:\tThis program is only");
					printf(" compataible with hedge,\n\t\t\ttorus, and klein bottle");
					printf(" edge cases. If no\n\t\t\tmode is provided hedge will be");
					printf(" selected by\n\t\t\tdefault.\n\n");
					
					if(filename != NULL)
						free(filename);
					return 0;
				}
			}
		/*Move to the next argument*/
		i++;
	}
	/*Error checking for filename*/
	if (filename == NULL){
		printf("\nNo File name Provided.\n\"./engine/gosperglidergun_106.lif\" used by default.\n");
		filename = malloc(sizeof(char) * strlen("./engine/gosperglidergun_106.lif") + 1);
		strcpy(filename, "./engine/gosperglidergun_106.lif");
	}
	if(edge_mode == 0){
		printf("\nNo Edge Mode provided.\n\"torus\" used by default.\n");
	}

        /* Initialize SDL's subsystems - in this case, only video. */
	if (SDL_Init(SDL_INIT_VIDEO) < 0) 
	{
		fprintf(stderr, "Unable to init SDL: %s\n", SDL_GetError());
		exit(1);
	}

        /* Register SDL_Quit to be called at exit; makes sure things are */
        /* cleaned up when we quit. */
	atexit(SDL_Quit);
    
        /* Attempt to create a screen with width x height window with 32bit pixels. */
	screen = SDL_SetVideoMode(width, height, 32, SDL_SWSURFACE);
  
        /* If SDL_SetVideoMode fails return error. */
	if (screen == NULL) 
	{
		fprintf(stderr, "Unable to set 640x480 video: %s\n", SDL_GetError());
		free(filename);
		exit(1);
	}

	sdl_init(width, height, screen);

	/* your life initialization code here */

	/*Decclaring the matrices*/
	unsigned char** matrix_a = NULL;
	unsigned char** matrix_b = NULL;

	/*adjust size of the spites depending on screen resolution and size*/
	int rows = width / size;
	int columns = height / size;

	/*Initializes the Matrices*/
	matrix_a = initialize_matrix(rows, columns);
	matrix_b = initialize_matrix(rows, columns);

	/*This makes the user coordinates to scale with the resolution*/
	cord_y = cord_y / size;
	cord_x = cord_x / size;

	/*Reads the file and fills the matrix with info from file*/
	read_file(matrix_a, filename, columns, rows, cord_y, cord_x);

	/* change the color of the sprite */
	color = sdl_set_color(red, green, blue);

/* Main loop: loop forever. */
	while (1)
	{
		/* change the modulus value to slow the rendering */
		if (SDL_GetTicks() % 20 == 0){
			//sdl_test(width, height, m, n, size, screen);
			sdl_render_life(matrix_a, width, height, screen, color, size);
		
		/*Makes sure matrix_b is free before reswaping*/
		clear_matrix(matrix_b, columns, rows);
		
		/*Checks for selected edge mode, uses torus as default*/
		switch(edge_mode){
			case 1:
				hedge(matrix_a, matrix_b, columns, rows);
				break;
			case 2: 
				torus(matrix_a, matrix_b, columns, rows);
				break;
			case 3:
				klein_bottle(matrix_a, matrix_b, columns, rows);
				break;
			default:
				torus(matrix_a, matrix_b, columns, rows);
		}

		/*Swaps the matrices*/
		swap(matrix_a, matrix_b, columns, rows);
		}

 /* Poll for events, and handle the ones we care about. 
  * You can click the X button to close the window
 */
		SDL_Event event;
		while (SDL_PollEvent(&event)) 
		{
			switch (event.type) 
			{
			case SDL_KEYDOWN:
				break;
			case SDL_KEYUP:
				 /* If escape is pressed, return (and thus, quit) */
				printf("\nESC key pressed.\n");
				if (event.key.keysym.sym == SDLK_ESCAPE){
					printf("Cleaning up memory...");
					delete_matrix(matrix_a, matrix_b, rows);
					free(filename);
					printf("Done!\n\nExitting...\n\n");
					return 0;
				}
				break;
			case SDL_QUIT:
				printf("\nSDL window closed\nCleaning up memory...");
				delete_matrix(matrix_a, matrix_b, rows);
				free(filename);
				printf("Done.\nExitting...\n\n");
				return(0);
			}
		}
	}

	return 0;
}