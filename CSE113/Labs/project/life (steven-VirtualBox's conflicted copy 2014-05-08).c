/**
 * @file life.c
 * 
 * @brief file containing the functions for the game of life
 *
 * @details this file contains each function for the
 * game of life including, initialzing the matrix, 
 * and functions for each of the shapes.
 *
 * @author Steven Ramsey
 *
 * @date 4/19/14
 *  
 * @todo program the game of life using c syntax
 *
 * @bug none
 *
 * @remark Segmentation fault when size does not equal 2, 4, 8 , or 16
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "life.h"

/**
 * Initalizes the matrix setting it in memory
 * @param rows the number of rows of calculated
 * @param cols the number of columns calculated
 * @return the created matrix
 */
unsigned char **initialize_matrix(int rows, int cols)
{	
	/*Naming it matrix helps me understand the logic*/
	int i = 0, j = 0; 
	unsigned char **matrix = NULL;

	/*Row allocation*/
	matrix = malloc(rows * sizeof(int*));

	if(!matrix){
		return NULL;
	}

	/*Column allocation*/
	for(i = 0; i < rows; i++){
		matrix[i] = malloc(cols * sizeof(int));

		/*Checks for malloc failure*/
		if (!matrix[i]){
			for(j = 0; j < i; j++){
				free(matrix[j]);
			}
			free(matrix);
			return NULL;
		}

	}
	clear_matrix(matrix, cols, rows);
	return matrix;
}
/**
 * Deletes/frees the passed in matrices
 * @param matrix_a the first matrix holding original cell data
 * @param martix_b the secondary matrix holding previous cell data
 * @param rows thenumber of rows present in the matrix
 * @return nothing
 */
void delete_matrix(unsigned char** matrix_a, unsigned char** matrix_b, int rows)
{
	int i = 0;
	for(i = 0; i < rows; i++){
		free(matrix_a[i]);
		free(matrix_b[i]);
	}
	free(matrix_a);
	free(matrix_b);
}
/**
 * Runs through all the elements in the matrix setting them to 0.
 * @param matrix the passed in matrix to be cleared/zeroed
 * @param cols the number of columns present in the matrix
 * @param rows the number of rows present in the matrix
 * @return nothing
 */
void clear_matrix(unsigned char** matrix, int cols, int rows)
{
	int i = 0, j = 0;

	for(i = 0; i < rows; i++){
		for(j = 0; j < cols; j++){
			matrix[i][j] = 0;
		}
	}
}
/**
 * Swaps matrix a and b
 * @param matrix_a the first matrix holding original cell data
 * @param martix_b the secondary matrix holding previous cell data
 * @param cols the number of columns present in the matrix
 * @param rows the number of rows present in the matrix
 * @return nothing
 */
void swap(unsigned char** matrix_a, unsigned char** matrix_b, int cols, int rows)
{
	int i = 0, j = 0;

	for(i = 0; i < rows; i++){
			for(j = 0; j < cols; j++){
				matrix_a[i][j] = matrix_b[i][j]; 
			}
	}
}
/**
 * Reads in a file determining the format then assigns variables to the matrix.
 * @param matrix_a the first matrix holding original cell data
 * @param filename the filename given from command line
 * @param cols the number of columns present in the matrix
 * @param rows the number of rows present in the matrix
 * @param cy the passed in inital y coordinate (the offset)
 * @param cx the passed in intial x coordinate (the offset)
 * @return nothing
 */
void read_file(unsigned char** matrix_a, char* filename, int cols, int rows, int cy, int cx)
{
	FILE* infile;
	char buffer[LEN];
	char* buf = NULL;
	int type_file = 0;
	int x = 0, y = 0;

	infile = fopen(filename, "r");

	/*Check for file reading failure*/
	if(infile == NULL){
		printf("\nFILE COULD NOT BE FOUND OR BE READ!\n\n");

		exit(EXIT_PROGRAM);
	}

	/*Reads First Line of input file for file version*/
	fgets(buffer, LEN, infile);
	buf = rstrip(buffer);

	if(strcmp(buf, "#Life 1.06") == 0){
		type_file = 1;
	}
	if(strcmp(buf, "#Life 1.05") == 0){
		type_file = 2;
	}

	/*1.06 file case*/
	if(type_file == 1){
		while(fgets(buffer, LEN, infile)){
			sscanf(buffer, "%d %d", &y, &x);

			/*This is the offset being added by the user*/
			y = cy + y;
			x = cx + x;

			if(((rows / 2) + y) >= rows){
				continue;
			}
			if((cols / 2 + x) >= cols){
				continue;
			}
			if(((rows / 2) + y) < 0 || ((cols / 2) + x) < 0){
				continue;
			}
			matrix_a[(rows / 2) + y][(cols / 2) + x] = 1;
		}
	}
	/*1.05 file case*/
	if(type_file == 2){
			printf("\nSorry this program is not compatible with this type of file yet.\n");

			return;

	}

	/*Close the file Since processing is done*/
	fclose(infile);
}
/**
 * Strips the file of whitespace to prevent error
 * @param buf the passed in buffer to be striped
 * @return the striped buffer
 */
char* rstrip(char* buf)
{
	int length = strlen(buf) - 1;
	int i = 0;

	while(i<length){
		if(buf[i]== '\t' ||
		buf[i]== '\n' ||
		buf[i]== '\f' ||
		buf[i]== '\v' ||
		buf[i]== '\r') {
		buf[i] = '\0';
		}
		i++;
	}	
	return buf;
}
/**
 * Runs the program using the hedge edge case
 * @param ma the first matrix holding original cell data
 * @param mb the secondary matrix holding previous cell data
 * @param cols the number of columns present in the martix
 * @param rows the number of rows present in the matrix
 * @return nothing
 */
void hedge(unsigned char** ma, unsigned char** mb, int cols, int rows)
{
	int i = 0, j = 0, neigh_count = 0, x = 0, y = 0;

	/*Scans through matrix for neighbors*/
	for(i = 0; i < rows; i++){
		for(j = 0; j < cols; j++){
			for(x = -1; x < 2; x++){
				for(y = -1; y < 2; y++){
					/*Edge Cases*/
					if((i + x) >= rows){
						continue;
					}
					if((j + y) >= cols){
						continue;
					}
					if((i + x) < 0 || (j + y) < 0){
						continue;
					}
					if (ma[i + x][j + y] == 1){
						neigh_count++;
					}
				}
			}
			/*Game of life Rules*/
			if(ma[i][j] == 1){
				neigh_count--;
			}
			if(neigh_count == 3){
				mb[i][j] = 1;
			}
			else if (neigh_count == 2){
				mb[i][j] = ma[i][j];
			}
			else{
				mb[i][j] = 0;
			}
			neigh_count = 0;
		}
	}
}
/**
 * Runs the program using the torus edge case
 * @param ma the first matrix holding original cell data
 * @param mb the secondary matrix holding previous cell data
 * @param cols the number of columns present in the matrix
 * @param rows the number of rows present in the matrix
 * @return nothing
 */
void torus(unsigned char** ma, unsigned char** mb, int cols, int rows)
{
	int i = 0, j = 0, x = 0, y = 0, neigh_count = 0; 

	/*Scans through matrix for neighbors*/
	for(i = 0; i < rows; i++){
		for(j = 0; j < cols; j++){
			for(x = -1; x < 2; x++){
				for(y = -1; y < 2; y++){
					/*Edge Cases*/
					if (ma[(i + x + rows) % rows][(j + y + cols) % cols]){
						neigh_count++;
					}
				}
			}
			/*Game of life Rules*/
			if(ma[i][j] == 1){
				neigh_count--;
			}
			if(neigh_count == 3){
				mb[i][j] = 1;
			}
			else if (neigh_count == 2){
				mb[i][j] = ma[i][j];
			}
			else{
				mb[i][j] = 0;
			}
			neigh_count = 0;
		}
	}

}
/**
 * Runs the program using the klien bottle edge case
 * @param ma the first matrix holding original cell data
 * @param mb the secondary matrix holding previous cell data
 * @param cols the number of columns present in the matrix
 * @param rows the number of rows present in the matrix
 */
void klein_bottle(unsigned char** ma, unsigned char** mb, int cols, int rows)
{
	int i = 0, j = 0, x = 0, y = 0, neigh_count = 0; 

	/*Scans through matrix for neighbors*/
	for(i = 0; i < rows; i++){
		for(j = 0; j < cols; j++){
			for(x = -1; x < 2; x++){
				for(y = -1; y < 2; y++){
					/*Edge Cases*/
					if ((i + x) < 0 || (i + x) >= rows){
						if(ma[(rows + x + i) % rows][cols - 1 -((cols + j + y) % cols)])
							neigh_count++;
					}
					else{
						if(ma[((rows + x + i) % rows)][((cols + j +y) % cols)])
							neigh_count++;
					}
				}
			}
			/*Game of life Rules*/
			if(ma[i][j] == 1){
				neigh_count--;
			}
			if(neigh_count == 3){
				mb[i][j] = 1;
			}
			else if (neigh_count == 2){
				mb[i][j] = ma[i][j];
			}
			else{
				mb[i][j] = 0;
			}
			neigh_count = 0;
		}
	}

}