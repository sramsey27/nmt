/**
 *@file credit.c
 *@author Steven ramsey
 *@date 2/18/2014
 *@brief determine if a credit card is valid
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NUM_DIGITS 16
#define VALID 0
#define INVALID -1

void convert_card(int card[], char num[], int num_digits);
void print_card(int card[], int size);
void multiply_array (int card[], int size);
void sum_array (int card[], int size);

int main(void)
{
	int card[NUM_DIGITS];
	char visa[] = "4012888888881881"; /* valid card */
	/*char visa[] = "4012888888881882";*/  /*invalid card */
	
	/* The size of the array */
	size_t size = sizeof (visa)/ sizeof(char) - 1;
	
	convert_card(card, visa, size);
	print_card(card, size);  
	multiply_array(card, size);
	sum_array(card, size);
	
	return 0;
}

/**
 * Converts the char (visa) into a series of numbers (not strings)
 * @param card dedicated amount of space for array
 * @param num the numbers of the card
 * @param num_digits the number of digits the card holds
 * @return void
 */ 
void convert_card(int card[], char num[], int num_digits)
{
	int i;

	for(i = 0; i < num_digits; i++) 		
		card[i] = num[i] - '0';
	
}

/**
 * Prints the numbers in the card and the corresponding index (for testing purposes)
 * @param card dedicated amount of space for array
 * @param size the size of the array (after conversion)
 * @return void
 */
void print_card(int card[], int size)
{
	int i;

	printf("\nCard number: ");
	for (i = 0; i < size; i++){
		/*printf("card[%d] = %d\n", i, card[i]);*/
		printf("%d ", card[i]);
		}
}

/**
 * Multiplies every other number that the card consist of.
 * @param card dedicated amount of space for array
 * @param size the size of the array / card (after conversion)
 * @return void
 */ 
void multiply_array (int card[], int size)
{
	int i;
	for (i = size - 2; i >= 0; i -= 2){
		card[i] = 2 * card[i];
		
		/*printf("\n%d", card[i])*/;
	}
}

/**
 * adds the elements of the array and splits double numbers into seperate
 * numbers for adding the prints if 
 * @param card dedicated amount of space for array
 * @param size the size of the array / card (after conversion)
 * @return void
 */
void sum_array (int card[], int size)
{
	int sum = 0;
	int i;

	/* Splits the double numbers into two*/
	for ( i = 0; i < size; i++){
		if (card[i] >= 10) {
			card[i] = card[i] - 9;
		}
		/*Adds all numbers*/
		sum += card[i];	
	}
	/* Checks if Valid or not */ 
	if (sum % 10 == 0) 
		printf("\n\nValid Card.\n\n");
	else if (sum % 10 == 1) 
		printf("\n\nInvalid Card.\n\n");
} 