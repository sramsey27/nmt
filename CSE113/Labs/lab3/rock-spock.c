/**
 * @file rock-spock.c
 * @author Steven Ramsey
 * @date 2 / 18 / 14
 * @brief Play Rock-paper-scissors-lizard-Spock against the machine 
 */
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define COMPUTER 8088
#define PLAYER 1000

#define CHOICES 5

#define ROCK 0
#define PAPER 1
#define SCISSORS 2
#define LIZARD 3
#define SPOCK 4

void move(int who, int move);
int winner(int computer, int player);
void print_winner(int winner, int comp_move, int player_move);
int nrand(int range);
void seed(void);


int main(void)
{
	int computer;
	int player;
	int win;

	/* set up the seed value for the random number generator */
	/* call only once */
	seed();

        /* todo - add a while loop to keep playing */
	printf("\nEnter a move:\n");
	printf("0 for ROCK\n");
	printf("1 for PAPER\n");
	printf("2 for SCISSORS\n");
	printf("3 for LIZARD\n");
	printf("4 for SPOCK\n");
	printf("5 to have SPOCK eat a ROCK while chasing a LIZARD and quit\n"); 
	printf("\nMove: ");
	scanf("%d", &player);
  
        /* todo - error check input */
	/* todo -- exit from program when player selects 5 */
	/* otherwise play a game of rock-paper-scissors-lizard-spock */

        /* debug -- you don't need move() to play game  */
	if (player == 5){
		printf("\nI don't understand how Spock can eat a rock, and why is he chasing a lizard?\n");
		printf("Exiting...\n\n");
		return 0;	
	}
	else
		move(PLAYER, player);

		/* generate random number for computers play */
		computer = nrand(CHOICES);

		/*debug -- you don't need this to play the game */
		move(COMPUTER, computer); 

		win = winner(computer, player);

		print_winner(win, computer, player);

        /* todo --implement function winner() */
        /* todo --implement function print_winner() */

	return 0;
}

/** prints the player's or computer's  move to stdin 
 * used in debugging
 * @param who is it the computer or the player's move?
 * @param move what move did they make
 * @return void
 */
void move(int who, int move)
{
	if (who == COMPUTER) 
		printf("Computer's play is ");
	else 
		printf("\nPlayer's play is ");


	switch(move) {
	case ROCK:
		printf("ROCK\n");
		break;
	case PAPER:
		printf("PAPER\n");
		break;
	case SCISSORS:
		printf("SCISSORS\n");
		break;
	case SPOCK:
		printf("SPOCK\n");
		break;
	case LIZARD:
		printf("LIZARD\n");
		break;
    	}
}

/** 
 * determines the winner either COMPUTER or PLAYER
 * @param computer the computer's move
 * @param player the player's move
 * @return the winner of the game
 */
int winner(int computer, int player)
{
	/* todo - determine the winner; use switch statements */
	int play_win = 1, comp_win = 2, tie = 3;

	switch (player){
	case SPOCK:
		switch (computer){
		case SPOCK:
			return tie;
			break;
		case LIZARD:
			return comp_win;
			break;
		case ROCK:
			return play_win;
			break;
		case PAPER:
			return comp_win;
			break;
		case SCISSORS:
			return play_win;
			break;
		default:
			printf("Error");
			return 0;
		}
	case LIZARD:
		switch (computer){
		case SPOCK:
			return play_win;
			break;
		case LIZARD:
			return tie;
			break;
		case ROCK:
			return comp_win;
			break;
		case PAPER:
			return play_win;
			break;
		case SCISSORS:
			return comp_win;
			break;
		default:
			printf("Error");
			return 0;
		}
	case ROCK:
		switch (computer){
		case SPOCK:
			return comp_win;
			break;
		case LIZARD:
			return play_win;
			break;
		case ROCK:
			return tie;
			break;
		case PAPER:
			return comp_win;
			break;
		case SCISSORS:
			return play_win;
			break;
		default:
			printf("Error");
			return 0;
		}
	case PAPER:
		switch (computer){
		case SPOCK:
			return play_win;
			break;
		case LIZARD:
			return comp_win;
			break;
		case ROCK:
			return play_win;
			break;
		case PAPER:
			return tie;
			break;
		case SCISSORS:
			return comp_win;
			break;
		default:
			printf("Error");
			return 0;
		}
	case SCISSORS:
		switch (computer){
		case SPOCK:
			return comp_win;
			break;
		case LIZARD:
			return play_win;
			break;
		case ROCK:
			return comp_win;
			break;
		case PAPER:
			return play_win;
			break;
		case SCISSORS:
			return tie;
			break;
		default:
			printf("Error");
			return 0;
		
		}
	default:
		printf("Invalid");
		return 0;
	}

}

/** 
 * prints the winner of the game to stdin 
 * @param winner who won the computer or player
 * @param comp_move what move did the computer make
 * @param play_move what move did the player make
 * @return void
 */
void print_winner(int winner, int comp_move, int player_move)
{
    /*todo - use a switch statement 
 	switch {}
    print Computer Wins! or Player Wins!

    And how they won. Use the phrases below*/

    switch (winner){
	case 1:
		printf("Player Wins!\n");
		switch (player_move){
		case LIZARD:
			switch (comp_move){
			case SPOCK:
				printf("Lizard poisons Spock!\n\n");
				break;
			case PAPER:
				printf("Lizard eats paper!\n\n");
				break;
			default:
				printf("There was an error.\n\n");
			}
			
			break;
		case ROCK:
			switch (comp_move){
			case LIZARD:
				printf("Rock crushes lizard!\n\n");
				break;
			case SCISSORS:
				printf("Rock crushes scissors.\n\n");
				break;
			default:
				printf("There was an error.\n\n");
			}
			
			break;
		case PAPER:
			switch (comp_move){
			case ROCK:
				printf("Paper covers rock!\n\n");
				break;
			case SPOCK:
				printf("Paper disproves Spock!\n\n");
				break;
			default:
				printf("There was an error.\n\n");
			}
			
			break;
		case SCISSORS:
			switch (comp_move){
			case PAPER:
				printf("Scissors cuts paper!\n\n");
				break;
			case LIZARD:
				printf("Scissors decapitates lizard!\n\n");
				break;
			default:
				printf("There was an error.\n\n");
			}
			
			break;
		case SPOCK:
			switch (comp_move){
			case SCISSORS:
				printf("Spock smashes scissors!\n\n");
				break;
			case ROCK:
				printf("Spock vaporizes rock!\n\n");
				break;
			default:
				printf("There was an error.\n\n");
			}
			
			break;
		default:
			printf("It's a tie!\n\n");	
		}
		break;
	case 2:
		printf("Computer Wins!\n");
		switch (comp_move){
		case LIZARD:
			switch (player_move){
			case SPOCK:
				printf("Lizard poisons Spock!\n\n");
				break;
			case PAPER:
				printf("Lizard eats paper!\n\n");
				break;
			case ROCK:
				printf("Rock crushes lizard!\n\n");
				break;
			default:
				printf("There was an error.\n\n");
			}
			
			break;
		case ROCK:
			switch (player_move){
			case LIZARD:
				printf("Rock crushes lizard!\n\n");
				break;
			case SCISSORS:
				printf("Rock crushes scissors.\n\n");
				break;
			case SPOCK:
				printf("Spock vaporizes rock!\n\n");
				break;
			default:
				printf("There was an error.\n\n");
			}
			
			break;
		case PAPER:
			switch (player_move){
			case ROCK:
				printf("Paper covers rock!\n\n");
				break;
			case SPOCK:
				printf("Paper disproves Spock!\n\n");
				break;
			default:
				printf("There was an error.\n\n");
			}
			
			break;
		case SCISSORS:
			switch (player_move){
			case PAPER:
				printf("Scissors cuts paper!\n\n");
				break;
			case LIZARD:
				printf("Scissors decapitates lizard!\n\n");
				break;
			default:
				printf("There was an error.\n\n");
			}
			
			break;
		case SPOCK:
			switch (player_move){
			case SCISSORS:
				printf("Spock smashes scissors!\n\n");
				break;
			case ROCK:
				printf("Spock vaporizes rock!\n\n");
				break;
			default:
				printf("There was an error.\n\n");
			}
			
			break;
		default:
			printf("It's a tie!\n\n");	
		}
		break;

	case 3:
		printf("It's a tie! No one wins this one.\n\n");
		break;
	break;
	default:
		printf("Error\n");
    }
}

/**
 * returns a uniform random integer n, between 0 <= n < range
 * @param range defines the range of the random number [0,range)  
 * @return the generated random number
 */
int nrand(int range) 
{
	return rand() % range;
}

/**
 * call this to seed the random number generator rand()
 * uses a simple seed -- the number of seconds since the epoch 
 * call once before using nrand and similar functions that call rand()
 */
void seed(void) 
{
  	srand((unsigned int)time(NULL));
}
