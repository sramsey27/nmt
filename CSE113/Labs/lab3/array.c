/**
 * @file array.c
 * 
 * @author Steven Ramsey
 *
 * @date 2/11/2014
 *  
 * Assignment: Lab 3 
 *
 * @brief Functions for fundamental array algorithms. 
 *
 * @details The functions implemented include basic counting, 
 * summing, searching, and basic statistical methods (average,
 * median, the max, the min). Sorting is implemented via
 * bubble sort, a not very efficient sort.
 *  
 * @bugs none
 *
 * @todo implement floating point versions of the functions
 */


#include "array.h"
#include <stdio.h>

#define LT 10
#define GT 20

/**
 * prints the array
 * @param a an array of numbers
 * @param size of array
 * @return void
 */
void print_array(int a[], size_t size)
{
	int i;
	
	for(i = 0; i < size; i++)
		printf("a[%d] = %d\n", i, a[i]);

}

void print_summary(size_t size, int maximum, int min, int mid, int lt_count, int gt_count, int l_search, int sum, int average, int even, int odd, int divis, int divisable, int median)
{
	printf("\nSummary:\nSize of array = %zu elements\n", size);
	printf("Maximum of array = %d\n", maximum);
	printf("Minimum of array = %d\n", min);
	printf("Midpoint of the array = %d\n", mid);
	printf("There are %d numbers within the array that are less than %d.\n", lt_count, mid);
	printf("There are %d numbers within the array that are greater than %d.\n", gt_count, mid);
	if (l_search != -1)
		printf("Midpoint (%d) found at index %d.\n", mid, l_search);
	else
		printf("Midpoint (%d) not found in array.\n", mid);
	printf("The sum of the numbers in the array is %d.\n", sum);
	printf("The average value of the elements in the array is %d.\n", average);
	printf("There are %d numbers that are even.\n", even);
	printf("There are %d odd that are odd.\n", odd);
	printf("There are %d numbers that are divisable by %d.\n", divis, divisable); 
	printf("The median of the of the array is %d.\n\n", median);
}

/**
 * finds the max number within the array
 * @param a the array of numbers
 * @param size size of array
 * @return the maximum number of the array
 */
int find_max (int a[], size_t size)
{
	int max, i;
	max = a[1];
	
	for (i = 2; i < size; i++)
		if (a[i] > max)
			max = a[i];
	return max;
}

/**
 * finds the minimum number of the array
 * @param a an array of numbers
 * @param size size of array
 * @return the minimum number of the array
 */
int find_min(int a[], size_t size)
{
	int min, i;
	min = a[1];
	
	for (i = 0; i < size; i++)
		if (a[i] < min)
			min = a[i];
	return min;
}

/**
 * finds the midpoint of the array using the minimum and max numbers
 * @param min the minimum number of the array
 * @param max the maximum number of the array
 * @return the midpoint of the array
 */
int midpoint(int min, int max)
{
	return (min + max) / 2;
}

/**
 * finds how many numbers that are less than or greater than the midpoint
 * @param a an array of numbers
 * @param size size of array
 * @param type type of variable that determines calculation guidelines
 * @param mid the midpoint of the array
 * @return The count of numbers greater than or less than the midpoint
 */
int get_count(int a[], size_t size, int type, int mid)
{
	int i = 0, count = 0;
	switch (type) {
	case LT:
		for (i = 0; i < size; i++)
			if (a[i] < mid)
				count += 1;
		break;
		
	case GT:
		for (i = 0; i < size; i++)
			if (a[i] > mid)
				count += 1;
		break;
	
	default:
		printf("Invalid count = %d\n", count);
	}
	return count;	
}

/**
 * finds what index the midpoint lies on if it exist
 * @param a an array of numbers
 * @param max the maximum number of the array
 * @param mid the midpoint of the array
 * @param size the size of the array
 * @return the index if midpoint is found, -1 if it isn't found
 */
int linear_search(int a[], int max, int mid, size_t size)
{
	int i = 0, index;
	while (i <= size && mid != a[i])
			i += 1;
	
	if (i <= max)
		index = i;
	else
		index = -1;
	
	return index;
}

/**
 * find the sum amount of the elements/numbers in the array
 * @param a an array of numbers
 * @param size the size of the array
 * @return the sum of the element/numbers.
 */
int sum_array(int a[], size_t size)
{
	int i, sum = 0;
	for (i = 0; i <= size; i++)
		sum += a[i];
	return sum;
}

/**
 * finds the average number of the array
 * @param sum the sum of the elements/numbers within the array
 * @param size the size of the array
 * @return the average number of the array
 */
int average_array(int sum, size_t size)
{
	int aver = sum / size;
	return aver;
}

/**
 * sorts the array in acending order using the bubble sort algorithm
 * @param a an array of numbers
 * @param size the size of the array
 * @return void
 */
void bubble_sort(int a[], size_t size)
{
	int i = 0, o = 0, temp;
	
	for (i = 0; i < size; i++){
		for (o = size - 1 ; o > i; o--){
		if (a[o-1] > a[o]){
			temp = a[o];
			a[o-1] = a[o];
			a[o] = temp;
			}
		}
	}
	/*for(i = 0; i < size; i++)
		printf("Bubble: %d\n", a[i]);*/
}

/**
 * sorts the array in accending order using the insertion sort algorithm
 * @param a an array of numbers
 * @param size the size of the array
 * @return void
 */
void insertion_sort(int a[], size_t size)
{
	int x = 0, i = 0, k = 0;
	for (i = 0; i < size; i++){
		x = a[i];
		k = i - 1;
		while ((k >= 0) && (a[k] > x)){
			a[k + 1] = a[k];
			k--;
		}
		a[k + 1] = x;
	/*printf("Insertion: %d\n", x);*/
	}
}

/**
 * reverses the order of the array
 * @param a an array of numbers
 * @param size the size of the array
 * @return void
 */
void reverse(int a[], size_t size)
{
	int temp, i;
	for (i = 0; i < (size / 2); i++) {
		temp = a[i];
		a[i] = a[size - i - 1];
		a[size - i - 1] = temp;
		}
	/*for (i = 0; i < size; i++)
		printf("Reverse: %d\n", a[i]);*/

}

/**
 * runs through insertion_sort algorithm (to sort) then find the median of the array
 * @param a an array of numbers
 * @param size the size of the array
 * @return the median of the array (when sorted)
 */
int median(int a[], size_t size)
{
	int med = 0, i = 0;
	
	int x = 0, k = 0;
	for (i = 0; i < size; i++){
		x = a[i];
		k = i - 1;
		while ((k >= 0) && (a[k] > x)){
			a[k + 1] = a[k];
			k--;
		}
		a[k + 1] = x;
	}
	
	if (size % 2 == 0)
		med = (a[size / 2] + a[(size / 2) - 1]) / 2; 
	else
		med = a[(size - 1) / 2];
	
	return med;
}

/**
 * find the amount of numbers that are even in the array
 * @param a an array of numbers
 * @param size the size of the array
 * @return the amount of even numbers (the counted amount)
 */
int even_count(int a[], size_t size)
{
	int count = 0, i;
	for(i = 1; i < size; i++)
		if (a[i] % 2 == 0)
			count += 1;
	return count;
}

/**
 * find the amount of numbers that are odd in the array
 * @param a an array of numbers
 * @param size the size of the array
 * @return the amount of even numbers (the counted amount)
 */
int odd_count(int a[], size_t size)
{
	int count = 0, i;
	for(i = 1; i < size; i++)
		if (a[i] % 2 == 1)
			count += 1;
	return count;
}

/**
 * find the amount of numbers divisible by a given number
 * @param a an array of numbers
 * @param size the size of the array
 * @param divisible the given divisible
 * @return the amount of numbers divisible by the given number (the counted amount)
 */
int divisible_count(int a[], size_t size, int divisible)
{
	int count = 0, i;
	for(i = 1; i < size; i++)
		if (a[i] % divisible)
			count += 1;
	return count;
}
