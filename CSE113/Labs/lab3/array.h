/**
 * @file array.h
 * 
 * @author Steven Ramsey
 *
 * @date 2/11/2014
 *  
 * Assignment: Lab 3 
 *
 * @brief header file for array.c
 *
 * @details see array.c for details
 *  
 * @bugs none
 *
 * @todo add floating point versions of the functions
 */

#ifndef ARRAY_H_
#define ARRAY_H_

#include <stdlib.h> 		/* for size_t declaration */


/* prints out the elements of an array, one element per line*/
void print_array(int a[], size_t size);
void print_summary(size_t size, int maximum, int min, int mid, int lt_count, int gt_count, int l_search, int sum, int average, int even, int odd, int divis, int divisable, int median);
int find_max (int a[], size_t size);
int find_min(int a[], size_t size);
int midpoint(int min, int max);
int get_count(int a[], size_t size, int type, int mid);
int linear_search(int a[], int max, int mid, size_t size);
int sum_array(int a[], size_t size);
int average_array(int sum, size_t size);
void bubble_sort(int a[], size_t size);
void insertion_sort(int a[], size_t size);
void reverse(int a[], size_t size);
int median(int a[], size_t size);
int even_count(int a[], size_t size);
int odd_count(int a[], size_t size);
int divisible_count(int a[], size_t size, int divisible);

#endif
