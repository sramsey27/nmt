#include <stdio.h>
#include <stdlib.h>

#define MALLOC_FAILED -99

void print_array(int *a, size_t size);
void print_summary(size_t size, int *maximum, int *min, int *sum, int *average, int *median);
void find_max (int *a, int *max, size_t size);
void find_min(int *a, int *min, size_t size);
void sum_array(int *a, int *sum, size_t size);
void average_array(int *sum, int *average, size_t size);
void median(int *a, int *med, size_t size);

int main(void)
{
	/*Data declaration*/
	int *max = NULL;
	int *min = NULL; 
	int *sum = NULL; 
	int *average = NULL;
	int *med = NULL;

	/*Assignment of array*/
	//int t[]=  {4, 6, 2, 4, 9, 11, 14, 16, 1, 15 ,3};
	int *a;
	/*Determines size of array for use in functions*/
	int size = 11;

	max = malloc(sizeof(int));
	min = malloc(sizeof(int));
	sum = malloc(sizeof(int));
	average = malloc(sizeof(int));
	med = malloc(sizeof(int));
	a = malloc(size * sizeof(int));

	*(a + 0) = 4;
	*(a + 1) = 6;
	*(a + 2) = 2;
	*(a + 3) = 4;
	*(a + 4) = 9;
	*(a + 5) = 11;
	*(a + 6) = 14;
	*(a + 7) = 16;
	*(a + 8) = 1;
	*(a + 9) = 15;
	*(a + 10) = 3;

	if (a == NULL){
		printf("\nMalloc Failed.\n");
		exit(MALLOC_FAILED);
	}
	
	/*print_array(a, size);*/
	find_max(a, max, size);
	find_min(a, min, size);
	sum_array(a, sum, size);
	average_array(sum, average, size);
	median(a, med, size);
	print_summary(size, max, min, sum, average, med);

	free(max);
	free(min);
	free(sum);
	free(average);
	free(med);
	free(a);
	
	return 0;
}
/**
 * prints the array
 * @param a an array of numbers
 * @param size of array
 * @return void
 */
void print_array(int *a, size_t size)
{
	int i;
	
	for(i = 0; i < size; i++)
		printf("a[%d] = %d\n", i, *(a + i));

}

void print_summary(size_t size, int *maximum, int *min, int *sum, int *average, int *median)
{
	printf("\nSummary:\nSize of array = %zu elements\n", size);
	printf("Maximum of array = %d\n", *maximum);
	printf("Minimum of array = %d\n", *min);
	printf("The sum of the numbers in the array is %d.\n", *sum);
	printf("The average value of the elements in the array is %d.\n", *average);
	printf("The median of the of the array is %d.\n\n", *median);
}

/**
 * finds the max number within the array
 * @param a the array of numbers
 * @param size size of array
 * @return the maximum number of the array
 */
void find_max (int *a, int *max, size_t size)
{
	int i;
	*max = *(a + 1);
	
	for (i = 2; i < size; i++)
		if (*(a + i) > *max)
			*max = *(a + i);
}

/**
 * finds the minimum number of the array
 * @param a an array of numbers
 * @param size size of array
 * @return the minimum number of the array
 */
void find_min(int *a, int *min, size_t size)
{
	int i;
	*min = *(a + 1);
	
	for (i = 0; i < size; i++)
		if (*(a + i) < *min)
			*min = *(a + i);
}


/**
 * find the sum amount of the elements/numbers in the array
 * @param a an array of numbers
 * @param size the size of the array
 * @return the sum of the element/numbers.
 */
void sum_array(int *a, int *sum, size_t size)
{
	int i;
	for (i = 0; i <= size; i++)
		*sum += *(a + i);
}

/**
 * finds the average number of the array
 * @param sum the sum of the elements/numbers within the array
 * @param size the size of the array
 * @return the average number of the array
 */
void average_array(int *sum, int *aver, size_t size)
{
	*aver = *sum / size;
}


/**
 * runs through insertion_sort algorithm (to sort) then find the median of the array
 * @param a an array of numbers
 * @param size the size of the array
 * @return the median of the array (when sorted)
 */
void median(int *a, int *med, size_t size)
{
	int i = 0;
	
	int x = 0, k = 0;
	for (i = 0; i < size; i++){
		x = *(a + i);
		k = i - 1;
		while ((k >= 0) && (*(a + k) > x)){
			*(a + (k + 1)) = *(a + k);
			k--;
		}
		*(a +(k + 1)) = x;
	}
	
	if (size % 2 == 0)
		*med = (*(a + (size / 2)) + *(a + (size / 2 - 1))) / 2;
	else
		*med = *(a + (size - 1) / 2);
}
