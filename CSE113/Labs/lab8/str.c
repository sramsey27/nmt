#include "str.h"
#include <stdio.h>

/**
 * Counts the length of string
 * @param *s The given string
 * @return the length of the string
 */
int str_len(char *s)
{
	int count = 0;
	while(*s != '\0'){
		count++;
		s++;
	}
	/* this is so it compiles */
	return count;
}

/* array version */
/* concantenate t to the end of s; s must be big enough */
void str_cat(char s[], char t[])
{
	int i, j;

	i = j = 0;
	while(s[i] != '\0') 	/*find end of s*/
		i++;
	while((s[i++] = t[j++]) != '\0')  /*copy t*/
		;
}

/* array version */
void squeeze(char s[], int c)
{
	int i, j;
	
	for(i = j = 0; s[i] != '\0'; i++)
		if (s[i] != c)
			s[j++] = s[i];

	s[j] = '\0';
}

/* pointer version */

/**
 * Removes character that is passes in through c
 * @param *s Pointer containing string
 * @param c the letter to be removed from string.
 * @return nothing
 */ 
void psqueeze(char *s, int c)
{
	int i, j;

	for (i = j = 0; *(s + i) != '\0'; i++)
		if (*(s + i) != c)
			*(s + (j++)) = *(s + i);

	*(s + j) = '\0';
}

/* array verion */
void reverse(char s[])
{
	int i, j;

	for(i = 0, j = str_len(s) - 1; i < j; i++, j-- ) {
		/*change this so it calls cswap*/
                cswap(&s[i], &s[j]); 
		/*c = s[i];
		s[i] = s[j];
		s[j] = c; */
	}
	
}

/*pointer version*/

/**
 * Reverses given string using cswap (a seperate function)
 * @param *s a pointer holding a string
 * @return nothing
 */
void preverse(char *s)
{
	int i, j;

	for(i = 0, j = str_len(s) - 1; i < j; i++, j--) {
                cswap((s + i), (s + j));		
                /*c = *(s + i);
		*(s + i) = *(s + j);
		*(s + j) = c; */ 
	}
}

/* copy n chars of src into dest */

/**
 * Copies from src to dest
 * @param *dest holding pointer for copy
 * @param *src The source string
 * @param The length of string
 * @return nothing
 */
void pstr_ncpy(char *dest, char *src, int n)
{
	int i;
	for(i = 0; i <= n; i++)
		*(dest + i) = *(src + i); 
}

/* concantenate t to the end of s; s must be big enough! */
void pstr_cat(char *s, char *t)
{
	int i;

	i = 0;
	while(*s != '\0') 	/* find end of s */
		s++;
	while(*(t + i) != '\0'){
		*(s + (i)) = *(t + (i));
		i++; /* copy t */
		} 
}

/**
 * Swaps c with d
 * @param *c the given character
 * @param *d destination for swap
 * @return nothing
 */
void cswap(char *c, char *d)
{
	int tmp;
	tmp = *c;
	*c = *d;
	*d = tmp;
}

/**
 * Determines the index address of c if found
 * @param *s The given string
 * @param c the letter to be found
 * @return nothing but 0, changes are pointers
 */
char *pindex(char *s, int c)
{
	int i;
	for(i = 0; i < str_len(s); i++)
		if (c == *(s + i))
			return (s + i);
	return 0;
}

/**
 * Compares the two given strings
 * @param *s a given string
 * @param *t a given string
 * @param n the length of the shortest sting
 * @return returns -1 if less than, 1 if greater than and 0 if equal.
 */
int pstr_ncmp(char *s, char *t, int n)
{
        while(n != 0){
                if(*s < *t)
                        return -1;
                if(*s > *t)
                        return 1;
        s++;
        t++;
        n--;
        }
        return 0;             
}
