#include <stdio.h>

int main(void)
{
	double p = 3.14159;
	double *ptr = NULL;
	double x = 2.71828;
	
	printf("\nInital Pointer address: %p\t\tInitial Pointer value is currently NULL\n", ptr);
	printf("p initial address: %p\tp's inital value: %f\n", &p , p);
	printf("x inital address: %p\tx inital value: %f\n", &x, x);
	
	ptr = &p;
	
	printf("\nAfter obtaining p's address:\nPointer address: %p\t\tPointer value: %f", ptr, *ptr);

	*ptr = x;
	
	printf("\n\nAfter Swtiching with x:\nPointer address: %p\t\tPointer value: %f\n\n", &p, p);
	
	return 0;
}

	
	