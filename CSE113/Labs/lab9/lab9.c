#include <stdio.h>
#include <stdlib.h>
#define EXIT_PROGRAM 10

struct node_t *create_node(double n, struct node_t *head);
void print_node(struct node_t *node);
void print_list(struct node_t *head);
struct node_t *insert_head(struct node_t *head, struct node_t *node);
struct node_t *insert_tail(struct node_t *head, struct node_t *node);
struct node_t *insert_middle(struct node_t *head, struct node_t *node);
int count_nodes(struct node_t *head);
struct node_t *delete_node(struct node_t *head, double n);
void delete_list(struct node_t *head);

struct node_t {
	double x;
	struct node_t *next;

};

int main()
{
	int quit = 0;
	struct node_t *head = NULL;
	struct node_t *new_node = NULL;

	/* menu code */
	do {
		double n = 0.0;
		int pos = 0;
		int responce, sec_responce;
		responce = 0;

		printf("\nPlease Enter a number corresponding to your answer:");
		printf("\n1: Enter a number.\n2. Delete a number.\n3. Print all numbers.\n");
		printf("4. Tell how many items in the list.\n5. End Program.\n");

		printf("Answer: ");
		scanf("%d",&responce);

		switch (responce){
		case 1:
			sec_responce = 0;
			printf("\nEnter a number you wish to enter the list.\nAnswer: ");
			scanf("%lf", &n);
			new_node = create_node(n, head);
			printf("\nWhat would you like put the number?\n1: Enter number at the head.");
			printf("\n2: Enter number in the middle.\n3. Enter number at the tail.");
			printf("\nAnswer: ");

			scanf("%d", &sec_responce);

			switch (sec_responce){
			case 1:
				printf("\n%lf put as the head.\n", n);
				head = insert_head(head, new_node);
				break;
			case 2:
				printf("\n%lf added in the middle.\n", n);
				head = insert_middle(head, new_node, pos);	
				break;
			case 3:
				printf("\n%lf put as the tail:\n", n);
				head = insert_tail(head, new_node);
				break;
			default:
				free(new_node);
				printf("\nInvalid Answer.\n");
			}
		break;

		case 2:
			if(head == NULL){
				printf("\nThere is nothing to delete!\n");
				break;
			}
			printf("\nDelete a node.\n");
			printf("\nEnter a number you wish to delete: ");
			scanf("%lf", &n);
			//new_node = create_node(n, head);
			head = delete_node(head, n);
			break;

		case 3:
			printf("\nCurrent list. Including corresponding addresses:\n");
			print_list(head);
			break;

		case 4:
			if (count_nodes(head) < 2){
				printf("\nThere is %d node in the list.", count_nodes(head));
				printf(" %d if null is included.\n", count_nodes(head) + 1);
			}
			else {
				printf("\nThere are %d nodes in the list.", count_nodes(head));
				printf(" %d if null is included.\n", count_nodes(head) + 1);
			}
			break;

		case 5:
			printf("\nQuitting....\n");
			delete_list(head);
			quit = 1;
			break;
		/*Makes sure user inputs proper values for the menu logic*/
		default:
			printf("\nInvalid Choice! Please try again.\n");
		}
		
	}while (!quit);
	
	return 0;

}

/**
 * Creates a node to insert into the list
 * @param n The number that is converted in to a node
 * @param head for use in delete_list call for memory clean up
 * @return the created node
*/
struct node_t *create_node(double n, struct node_t *head)
{
	struct node_t *new_node = malloc(sizeof(struct node_t));

	if(new_node == NULL){
		/*Just in case malloc happens to fail*/

		printf("\nMalloc Failed\nCleaning up...\n");

		/* Acts as a failsafe to prevent memory leak*/

		delete_list(head);
		printf("Exiting...\n");
		exit(EXIT_PROGRAM);
	}
	new_node->x = n;
	new_node->next = NULL;
	return new_node;
}

/**
 * Prints the last added node
 * @param *node the node to be printed
 * @return nothing
*/
void print_node(struct node_t *node)
{
	if (node != NULL)
		printf("[%lf]", node->x);
	else 
		printf("\nNode is currently NULL.\n");
}

/**
 * Runs through the list and prints the node
 * @param *node The structure holding the list
 * @return nothing
*/
void print_list(struct node_t *head)
{
	struct node_t *tmp = head;
	while(tmp != NULL){
		print_node(tmp);
		printf("-->");
		tmp = tmp -> next;
	}
	printf("NULL\n");
	tmp = head;
	while(tmp != NULL){
		printf("[%p]", tmp);
		printf("-->");
		tmp = tmp -> next;
	}
	printf("NULL\n");
}

/**
 * Inserts a node to the head of the list
 * @param *head the head of the list
 * @param *node the node to be added
 * @return the list
*/
struct node_t *insert_head(struct node_t *head, struct node_t *node)
{
	node->next = head;
	head = node;
	return head;
}

/**
 * Inserts a node to the tail of the list
 * @param *head the head of the list 
 * @param *node the node to be added
 * @return the list
*/
struct node_t *insert_tail(struct node_t *head, struct node_t *node)
{
	if(head == NULL)
		return head;
	struct node_t *tmp = head;
		while(tmp->next != NULL){
			tmp = tmp->next;
		}
		tmp->next = node;
	return head;
}

/**
 * Inserts a node to the middle of the list
 * @param *head the head of the list
 * @param *node the node to be added
 * @param pos the position to be entered
 * @return the head of the list
*/
struct node_t *insert_middle(struct node_t *head, struct node_t *node)
{
	int cur_pos = 0;
	struct node_t *cur = head;

	if(head == NULL){
		return insert_head(head, node);
	}
    if(head->next == NULL){
    	insert_tail(head, node);
    	return head;
    }
    while(cur->next != NULL){
        if(cur_pos == pos){
            node->next = cur->next;
            cur->next = node;
            return head;
            }
        cur = cur->next;
        cur_pos++;
    }
	return head;
}
/**
 * Increments count for every element in list
 * @param *head the head of the list
 * @return the count
*/
int count_nodes(struct node_t *head)
{
	int count = 0;
	if(head == NULL){
		return 0;
	}
	else {
		
		struct node_t *tmp = head;
		while(tmp->next != NULL){
			count++;
			tmp = tmp->next;
		}
	}
	return count + 1;
}

/**
 * Deletes specified node
 * @param *head the head of the list
 * @param n the number of the corresponding node
 * @return the list (after freeing), or NULL if all else fails.
*/
struct node_t *delete_node(struct node_t *head, double n)
{
	struct node_t *cur = head;
	struct node_t *prev = NULL;

	if(cur->x == n) {
		prev = cur;
		cur = cur->next;
		free(prev);
		return cur;
	}

	while(cur != NULL){
		if(cur->x == n){
			prev->next = cur->next;
			free(cur);
			return head;
		}
		prev = cur;
		cur = cur->next;
	}
	return NULL;
}
/**
 * Deletes entire list from memory to prevent memory leaks
 * @param *head the head of the list
 * @return nothing
*/
void delete_list(struct node_t *head)
{
	struct node_t *cur = head;

	while(head != NULL){
		cur = head->next;
		free(head);
		head = cur;
	
	}

}