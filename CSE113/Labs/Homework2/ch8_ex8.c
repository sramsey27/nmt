#include <stdio.h>
#include <math.h>

/**
* Determines the absolute value
* @param x number to find the squareroot of
* @return absolute value of x
*/

float absoluteValue(float x)
{
	if (x < 0)
		x = -x;
	return x;
}

/**
* Calculates the square root of the given number
* @param x the given number to calculate with
* @param epsilon limits the loop
*/

float squareroot(float x, const float epsilon)
{
	/*moved epsilon out of function to main*/ 
	float guess = 1.0;

	/* Runs through the loop until guess is less then epsilon*/
	while (absoluteValue(guess * guess - x) >= epsilon)
		guess = ((x / guess + guess) / 2.0);
	
	return guess;
}

/**
* Calculates the discriminant of the quadratic formula
* @param b value of b
* @param a value of a
* @param c value of c
* @return the results as long as it isn't less than 0, if 0 it returns 0
*/
 
int discriminant(float b, float a, float c)
{
	float result = pow(b, 2) - (4 * (a * c));
	if (result > 0)
		return result;
	else
		printf("The roots are imaginary.\n");
		return 0;

}

/**
* Calculates the squareroot for the most part, also checks if discriminant is to 0
* @param a value for a
* @param epsilon value of epsilon
* @param dis the result of the discriminant function 
* @return the results of the function call of squareroot with discriminant value
*/

float quadratic(int a, const float epsilon, float dis) 
{
	float quad;
	
	if (dis == 0)
		return 0;
	else
		quad = squareroot(dis, epsilon);
		return quad;
}
int main (void)
{
	/*Values for a, b, c to be substituted into the formula*/
	float a = 5, b = 6, c = 1;
	
	const float epsilon = .00001;
	
	/*Assigns discrim the value of the returned value of discriminant
	* for easier use*/
	float discrim = discriminant(b, a, c);
	
	/* carries out the division part of the formula as well as the adding and substracting for both values*/
	float quad1 = (-b - quadratic(a, epsilon, discrim)) / (2 * a);
	float quad2 = (-b + quadratic(a, epsilon, discrim)) / (2 * a);
	
	printf("\nX = %.2f, %.2f when a = %.2f, b = %.2f, and c = %.2f.\n\n", quad2, quad1, a, b, c);

	return 0;
	
}

	
	
	
		
