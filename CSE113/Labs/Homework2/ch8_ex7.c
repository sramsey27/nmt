#include <stdio.h>


/**
* Raises an integer to a positive interger power
* @param x the integer to be raised
* @param n the power to be applied
* @return the raised power of the integer
*/

long x_to_the_n(int x, int n) 
{
	int i;
	long power = x;


	if(! n == 0)
		for(i = 1; i < n; i++)
			power = power * power;
	else
		power = 1;

	return power;
}
int main(void)
{
	/* Two different test to show it work when n = 0 */
	int x = 2, n = 0;
	int y = 5, m = 3;

	printf("\nThe result when the integer is %d and the exponent is %d equals %li.\n\n", x, n, x_to_the_n(x, n));
	printf("\nThe result when the integer is %d and the exponent is %d equals %li.\n\n", y, m, x_to_the_n(y, m));

	return 0;
}
