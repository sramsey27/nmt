#include <stdio.h>

/**
*  calcuates the nth triangular number
*  @param n the number of times to run through the loop.
*  @return the nth triangular number
*/

int calculateTriangularNumber (int n)
{
	/*For #2*/
	int i, triangularNumber = 0;

	for (i = 1; i <= n; ++i )
		triangularNumber += i;

	return triangularNumber;
}	
/**
* Calls in the new version of the function
* @return the value the function returned
* @remarks Didn't exactly know what to do except make it call the function, by the book.
*/

int old_triangle (void)
{
	/*For #2*/
	return calculateTriangularNumber
}	

int main (void)
{
	int n = 15;
	printf("\nWhat is the triangular number?: ");
	scanf("%i", &n);
	printf("\nTriangular number %i is %i \n", n, calculateTriangularNumber (n));
	printf("\nOld Function:\nTriangular number %i is %i \n", n, old_triangle());

	return 0;
}
