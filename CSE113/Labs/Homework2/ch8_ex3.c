#include <stdio.h>

/**
* Determines the absolute value
* @param x number to find the squareroot of
* @return absolute value of x
*/

float absoluteValue(float x)
{
	if (x < 0)
		x = -x;
	return x;
}

/**
* Calculates the square root of the given number
* @param x the given number to calculate with
* @param epsilon limits the loop
*/

float squareroot(float x, const float epsilon)
{
	float guess = 1.0;
	/* Runs through the loop until guess is less then epsilon*/
	while (absoluteValue(guess * guess - x) >= epsilon)
		guess = (x / guess + guess) / 2.0;
	
	return guess;
}
int main (void)
{
	/*moved epsilon here to pass into function as a parameter*/
	const float epsilon = .00001;
	printf("squareroot (2.0) = %f\n", squareroot(2.0, epsilon));
	printf("squareroot (144.0) = %f\n", squareroot(144.0, epsilon));
	printf("squareroot (17.5) = %f\n", squareroot(17.5, epsilon));

	return 0;
}
