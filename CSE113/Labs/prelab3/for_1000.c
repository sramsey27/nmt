#include <stdio.h>

/**
* Prints out numbers from 0 to 1000.
* @return nothing
*/

int main (void)
{

	int i = 0;
	/*Runs through each number printing out each*/
	for (i = 0; i < 1001 ; i++)
		printf("%d\n",i);
	
	return 0;
}
