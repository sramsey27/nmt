#include <stdio.h>

#define LENGTH 1000

/**
* stores numbers 1 to 1000 in an array
* then prints out the array.
* @return nothing
*/

int main(void)
{
	/*Uses predefined LENGTH for array size*/
	int a[LENGTH];
	int i;

	/*Runs through 1 to 1000 and prints each number*/
	for (i = 0; i < LENGTH; i++)
		printf("%d\n", a[i] = i + 1);
		

	return 0;
}
