#include <stdio.h>

/**
* Multplies numbers in within the array that is
* between start and stop.
* @param a array of numbers
* @param start a number where the process begins
* @param stop a nummber where the process stops.
* @return the product of the numbers muliplied.
*/

int multiply_range(int a[], int start, int stop)
{
	int i;
	int product = 1;

	/*runs through all elements of array to find numbers in between
	* start and stop, then finds the product.*/
	for (i = start; i <= stop; i++)
		product = *(a+i) * product; 
	
	return product;
}
int main(void)
{
	/*Hard coded array and variables, also type declaration, then prints results*/
	int a[] = {6, 3, 5, 2, 3, 2, 4};
	int start = 1;
	int stop = 4;

	/*Prints results*/
	printf("\nTest 1:\n\nStart = %d\nStop = %d\nProduct = %d\n\n", start, stop , multiply_range(a, start, stop));  
	return 0;
}
		
		

