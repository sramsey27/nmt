#include <stdio.h>

/**
*  Prints out numbers from 1000 to 1
*  @returns nothing
*/

int main (void)
{
	/*Hardcoded i value*/
	int i = 1001;

	/*Deducts one each run through then prints*/
	while ((--i) != 0)
		printf("%d\t",i);

	return 0;
}
