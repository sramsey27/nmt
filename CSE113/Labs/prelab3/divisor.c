#include <stdio.h>

/**
* Checks for divisible number in array and
* and returns 1 if divisible and 0 if not
* then prints to screen.
* @param a array of assigned numbers
* @param div blank array used as number placeholer
* @param size size of array
* @param divisor used for for divisiblility.
* @return nothing
*/

void array_mod(int a[], int div[], size_t size, int divisor)
{
	int i;
	for (i = 0; i < size; i++){
		if (!(a[i] % divisor))
			div[i] = 1;
		else
			div[i] = 0;
		printf("%d \t %d\n", a[i], div[i]);
	}
}

int main(void)
{

	/*Hardcoded array and divisor with type declaration*/
	int a[] = {13, 44, 85, 23, 72, 99, 100, 108, 222, 1084};
	int divisor = 4;
	size_t size;

	/*Determine size of array*/
	size = sizeof(a)/sizeof(int);
	
	int div[size];

	array_mod(a, div, size, divisor);

	return 0;	
}
	

	
