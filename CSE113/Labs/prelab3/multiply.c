#include <stdio.h>

/**
* Determines numbers in between start
* and stop and multiplies them. 
* @param start, starting point for process
* @param stop, ending point for process
* @return product of the numbers in between
*/

int multiply_range(int start, int stop)
{
	int i;
	int product = 1;
	for (i = start; i <= stop; i++)
		product = i * product; 
	
	return product;
}

int main(void)
{
	/*Hardcoded variables and type declaration.*/
	int start = 3, start_2 = 19; 
	int stop = 7, stop_2 = 18;

	/*Prints out the results*/
	printf("\nTest 1:\n\nStart = %d\nStop = %d\nProduct = %d\n\n", start, stop , multiply_range(start, stop));
	printf("\nTest 2:\n\nStart = %d\nStop = %d\nProduct = %d\n\n", start_2, stop_2 , multiply_range(start_2, stop_2));  
	return 0;
}
		
		

