#include <stdio.h>

#define LENGTH 1000

/**
* Prints out array according to size.
* @param a array of numbers
* @param len size of array
* @return nothing
*/

int print_array(int a[], size_t len)
{
	int i;
	for(i = 0; i < len; i++)
		printf("a[%d] = %d\n",i, a[i]);
	return 0;
}

int main(void)
{
	int i;
	int a[LENGTH];
	size_t len;

	/*assigns numbers 1 to 1000 in an array */
	for (i = 0; i < LENGTH; i++)
		a[i] = i + 1;

	/*determines size of array */
	len = sizeof(a)/sizeof(int);

	printf("The array has %zu elements.\n", len);

	/*calls function */ 
	print_array(a, len); 

	return 0;

}
