#include <stdio.h>

#define PRINT3(x,y,z) printf(#x "=%d\t" #y "=%d\t" #z "=%d\n", x, y, z)

int main(void)
{
	int x, y, z;
	
	x = y = z = 1;
	
	++x || ++y && ++z;
	PRINT3(x,y,z);
	
	x = y = z = 1;
	++x && ++y || ++z;
	PRINT3(x,y,z);
	
	x = y = z = 1;
	++x && ++y && ++z;
	PRINT3(x,y,z);
	
	x = y = z = -1;
	++x && ++y || ++z;
	PRINT3(x,y,z);
	
	x = y = z = -1;
	++x || ++y && ++z;
	PRINT3(x,y,z);
	
	x = y = z = -1;
	++x && ++y && ++z;
	PRINT3(x,y,z);
	
	return 0;

	
}