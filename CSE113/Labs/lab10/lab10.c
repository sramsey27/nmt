#include <stdio.h>
#include <stdlib.h>
#define END_PROGRAM 50

struct info_t *create_sentinel(struct info_t *sentinel);
struct node_t *create_node(int n, struct info_t *sentinel);
struct info_t *insert_head(struct info_t *sentinel, struct node_t *node);
struct info_t *insert_tail(struct info_t *sentinel, struct node_t *node);
struct info_t *insert_middle(struct info_t *sentinel, struct node_t *node);
int compare(int x, int y);
struct info_t *hailstorm(struct info_t *sentinel, int number);
struct info_t *create_sorted_list(struct info_t *sentinel, int number);
struct info_t *create_even_list(struct info_t *sentinel);
struct info_t *create_odd_list(struct info_t *sentinel);
int count_nodes(struct info_t *sentinel);
int find_max(struct info_t *sentinel);
double find_ratio(struct info_t *odd_list, struct info_t *even_list);
void print_node(struct node_t *node);
void print_list(struct info_t *sentinel);
void print_reverse_list(struct info_t *sentinel);
void delete_list(struct info_t *sentinel);

struct info_t {
	unsigned int count;
	struct node_t *head;
	struct node_t *tail;
};

struct node_t {
        unsigned int n; 
        struct node_t *next;
        struct node_t *prev;
};

int main()
{
        struct info_t *sentinel = NULL; 
        struct info_t *even_list = NULL, *odd_list = NULL, *sorted_list = NULL;
        int user_input = 0, user_number = 0, max = 0, stop_time = 0;
        float ratio = 0.0;

        printf("\nHello! To get started, enter a number corresponding to the command.\n");
        
	do {
		/*Reports sentinel status on screen*/
		printf("____________________________________\n");
		if(sentinel == NULL)
			printf("\nStatus: Sentinel currently not loaded.\n");
		else{
			printf("\nStatus: Sentinel is loaded.\n");
		}

		user_input = 0;
		printf("\n1. Enter a number to begin with.\n2. Print the hailstone sequence.\n");
		printf("3. Print the hailstone sequence in reverse order.\n");
		printf("4. Print the hailstone sequence in sorted order.\n");
		printf("5. Print the hailstone sequence in reverse sorted order.\n6. Print the stopping time (node amount)\n");
		printf("7. Find the max element in the sequence.\n");
		printf("8. Create a sequence of the even numbers based on the current list.\n");
		printf("9. Create a sequence of odd numbers based on the current list.\n");
		printf("10. Print all even elements\n11. Print all odd elements\n");
		printf("12. Find the ratio of even to odd numbers\n");
		printf("13. Exit the program\n");
		printf("Answer: ");

		scanf("%d", &user_input);

		printf("\n____________________________________\n");

		switch(user_input){
			case 1:
				if(sentinel != NULL){
					delete_list(sentinel);
				}
				printf("\nPlease enter a number\nNumber = ");
				scanf("%d", &user_number);
				if(user_number == 0){
					printf("\nHailstone sequence cannot be calculated with 0!\n");
					break;
				}
				sentinel = create_sentinel(sentinel);
				printf("\nThe number %d has been acknowledged.\n", user_number);
				sentinel = hailstorm(sentinel, user_number);
				break;
			case 2:
				if(sentinel == NULL){
					printf("\nNo list has been made yet! Please option 1 to create one.\n");
					break;
				}
				printf("Inital Order\n");
				print_list(sentinel);
				break;	
			case 3:
				if(sentinel == NULL){
					printf("\nNo list has been made yet! Please option 1 to create one.\n");
					break;
				}
				if(sentinel->tail == NULL){
                			printf("NULL");
                			break;
                		}
				printf("Reversed Order\n");
				print_reverse_list(sentinel);
				break;
			case 4:
				if(sorted_list != NULL){
					delete_list(sorted_list);
				}
				if(sentinel == NULL){
					printf("\nNo list has been made yet! Please option 1 to create one.\n");
					break;
				}
				printf("Current list in sorted order: \n");
				sorted_list = create_sorted_list(sentinel, user_number);
				print_list(sorted_list);
				break;
			case 5:
				if(sorted_list != NULL){
					delete_list(sorted_list);
				}
				/*This wasn't like this originally, this was made this way
				to satisfy the lab script*/
				if(sentinel == NULL){
					printf("\nNo list has been made yet! Please option 1 to create one.\n");
					break;
				}
				sorted_list = create_sorted_list(sentinel, user_number);
				printf("Current list in reversed sorted order: \n");
				print_reverse_list(sorted_list);
				break;
			case 6:
				if(sentinel == NULL){
					printf("\nNo list has been made yet! Please option 1 to create one.\n");
					break;
				}
				printf("\nStopping Time: \n");
				stop_time = count_nodes(sentinel);
				printf("%d nodes\n", stop_time);
				break;
			case 7:
				if(sentinel == NULL){
					printf("\nNo list has been made yet! Please option 1 to create one.\n");
					break;
				}
				max = find_max(sentinel);
				printf("\nThe Max element is %d.\n", max);
				break;
			case 8:
				if(even_list != NULL){
					delete_list(even_list);
				}
				even_list = create_even_list(sentinel);
				printf("\nEven list created, use option 10 to print.\n");
				break;
			case 9:
				if(odd_list != NULL){
					delete_list(odd_list);
				}
                                odd_list = create_odd_list(sentinel);
                                printf("\nOdd list created, use option 11 to print.\n");
				break;
			case 10:
				if(even_list == NULL){
					printf("\nList has not been created yet!\n");
					break;
				}
				printf("All Even Elements:\n");
				print_list(even_list);
				break;
			case 11:
				if(odd_list == NULL){
					printf("\nList has not been created yet!\n");
					break;
				}
				printf("Odd sequence: \n");
				print_list(odd_list);
				break;
			case 12:
				if(odd_list == NULL){
					printf("\nBoth even and odd list must exist for this function to work!\n");
					break;
				}
				if(even_list == NULL){
					printf("\nBoth even and odd list must exist for this function to work!\n");
					break;
				}
                    		ratio = find_ratio(even_list, odd_list);
                    		printf("The Ratio is %.2f%%.\n", ratio);
				break;
			case 13:
				printf("\nCleaning up...\n");
				if(sentinel != NULL)
					delete_list(sentinel);
				if(even_list != NULL)
					delete_list(even_list);
				if(odd_list != NULL)
					delete_list(odd_list);
				if(sorted_list != NULL)
					delete_list(sorted_list);
				printf("\nFinished!\n\nGoodbye!\n\n");
				break;
			default:
				printf("\nInvalid Choice Try Again!\n");
		}
	}while(user_input != 13);

	return 0;
}
/**
 * Creates a sentinal that keeps the info of the list
 * @return the newly created sentinal
 */
struct info_t *create_sentinel(struct info_t *sentinel)
{
	struct info_t *new_sentinel;
	new_sentinel = malloc(sizeof(struct info_t));

	if(new_sentinel == NULL){
		printf("\nMalloc Failed at 'create_sentinal'\nCleaning up memory....\nQuitting...\n");
		free(new_sentinel);
		delete_list(sentinel); /* just in case*/
		exit(END_PROGRAM);
	}

	new_sentinel->head = NULL;
	new_sentinel->tail = NULL;
	new_sentinel->count++;

	return new_sentinel;
}
/**
 * Creates a new node for the list
 * @param num the number the node will inherit
 * @return the nude node
 */
struct node_t *create_node(int n, struct info_t *sentinel)
{
	struct node_t *new_node;
	new_node = malloc(sizeof(struct node_t));

	if(new_node == NULL){
		printf("\nMalloc failed at 'create_node'\nCleaning Up...\nQuitting...\n");
		delete_list(sentinel); /* Better safe than sorry*/
		exit(END_PROGRAM);
	}

	new_node->n = n;
	new_node->next = NULL;
	new_node->prev = NULL;
	
	return new_node;
}
/**
 * Inserts node at the head of the list
 * @param *sentinal the info_t structure holding data about the list
 * @param *node the node to be added to be added as the head
 * @return the changes made to the sentinal
*/
struct info_t *insert_head(struct info_t *sentinel, struct node_t *node)
{
	if(sentinel->head == NULL) {
		sentinel->head = node;
		sentinel->tail = node;
		return sentinel;
	}

	node->next = sentinel->head;
	sentinel->head->prev = node;
	sentinel->head = node;
	sentinel->count++; /*Keeps track of how many nodes there are*/
	return sentinel;
}
/**
 * Inserts the given node to the end (tail) of the list
 * @param *sentinal The sentinal holding the list info
 * @param *node the node to be added as the head
 * @return the changes made
*/
struct info_t *insert_tail(struct info_t *sentinel, struct node_t *node)
{
	if(sentinel->tail == NULL) {
		sentinel->tail = node;
		sentinel->head = node;
		return sentinel;
	}

	node->prev = sentinel->tail;
	sentinel->tail->next = node;
	sentinel->tail = node;
	sentinel->count++;
	return sentinel;
}
/**
 * Inserts node into the middle of the list
 * @param *sentinal The structure that holds the info of the list
 * @param *node the node to be added to the middle
 * @return changes made to sentinal
*/
struct info_t *insert_middle(struct info_t *sentinel, struct node_t *node)
{
	struct node_t *tmp = sentinel->head;

	if(tmp == NULL){
		return insert_head(sentinel, node);
	}
	while(tmp != NULL){
		//if(tmp->n >= node->n){
		if(compare(tmp->n, node->n) == 1 || 0) {
			if(tmp == sentinel->head){
				return insert_head(sentinel, node);
			}
			node->prev = tmp->prev;
			tmp->prev->next = node;
			node->next = tmp;
			tmp->prev = node;
			sentinel->count++;
			return sentinel;
		}
		tmp = tmp->next;
	}
	insert_tail(sentinel, node);
	return sentinel;
}
/**
 * Used by insert_middle to compare the numbers
 * @param x number within node to be compare with y
 * @param y number within next node to be compared with x
 * @return the number corresponding to the condition
*/
int compare(int x, int y)
{
	if(x < y){
        	return -1;
    	}
	else if(x == y){
        	return 0;
    	}
    	else {
        	return 1;
    	}
}
/**
 * Creates a list consisting of the hailstone sequence of the given number
 * @param *sentinal the sentinal holding the list info
 * @param number the number to calculate the hailstone sequence with
 * @return the new list
*/
struct info_t *hailstorm(struct info_t *sentinel, int number)
{
	struct node_t *new_node = NULL;

	new_node = create_node(number, sentinel);
	insert_tail(sentinel, new_node);

	while(number != 1) {
		if ((number % 2) == 0){
			number = number / 2;
			new_node = create_node(number, sentinel);
			insert_tail(sentinel, new_node);
		}
		else if ((number % 2) == 1) {
			number = (3 * number) + 1;
			new_node = create_node(number, sentinel);
			insert_tail(sentinel, new_node);
		}
	}
	return sentinel;
}
/**
 * Prints the reverse order of the current list
 * @param *sentinal The sentinal holding the info for the list
 * @return nothing
*/
void print_reverse_list(struct info_t *sentinel)
{
    	struct node_t *tmp = sentinel->tail;
        
    	while(tmp != NULL){
		printf("[%d] -->", tmp->n);
		tmp = tmp->prev;     
	}         
	printf("NULL\n");
}
/**
 * Creates a sorted list of the hailstone sequence
 * @param sentinal the sentinal holding the info of the list
 * @param number the number the hailstone sequence request
 * @return the sorted list
*/
struct info_t *create_sorted_list(struct info_t *sentinel, int number)
{
	struct info_t *sorted = malloc(sizeof(struct info_t));
	struct node_t *new_node = NULL;

        while(number != 1){
            	if(number % 2 == 0){
                	new_node = create_node(number, sentinel);
                	number = (number / 2);
                }
                else {
                        new_node = create_node(number, sentinel);
                        number = (3 * number) + 1;
                }	
		sorted = insert_middle(sorted, new_node);
	}		
        new_node = create_node(1, sentinel); /*To compensate for the number one*/
        sorted = insert_head(sorted, new_node);
        return sorted;	
}
/**
 * Creates a seperate sorted list of the hailstone sequence
 * @param *sentinal the sentinal holding the infomation of the list
 * @return the even list 
*/
struct info_t *create_even_list(struct info_t *sentinel)
{
    	struct node_t *node = sentinel->head;
    	struct info_t *evens = create_sentinel(sentinel);

    	while(node != NULL){
    		if(node->n % 2 == 0){
        	insert_tail(evens, create_node(node->n, sentinel));
        	}
    		node = node->next; 
    	}
    	return evens;                       
}
/**
 * Creates a list of the odd numbers of the hailstone sequence
 * @param *sentinal the sentinal holding the infomation of the list
*/
struct info_t *create_odd_list(struct info_t *sentinel)
{
	struct node_t *node = sentinel->head;
	struct node_t *new_node = NULL;
	struct info_t *odds = create_sentinel(sentinel);

	while(node != NULL){
        if(node->n % 2 == 1){
		new_node = create_node(node->n, sentinel);
		insert_tail(odds, new_node);
        }
        node = node->next; 
    }
    return odds;                       
}
/**
 * Counts the number of nodes in the list (Stopping time)
 * @param *sentinal The sentinal containing info on the list
 * @return the count
*/
int count_nodes(struct info_t *sentinel)
{
	int count = 0;
	if(sentinel->head == NULL){
		return 0;
	}
	else {
		struct node_t *tmp = sentinel->head;
		while(tmp->next != NULL){
			count++;
			tmp = tmp->next;
		}
	}
	return count + 1;
}
/**
 * Calculates the ratio of odd to evens or vise versa depending on conditions
 * @param *odd_list The list containing the odd numbers
 * @param *even_list The list containing the even list
 * @return the calcuated ratio
*/
double find_ratio(struct info_t *odd_list, struct info_t *even_list)
{
	double ratio;

	if(odd_list->count > even_list->count){
		printf("\nThe Odd to Even ratio is %d to %d. ", odd_list->count, even_list->count);
		ratio = ((double)odd_list->count) / ((double)even_list->count); 
	}
	else if (even_list->count > odd_list->count){
		printf("\nThe Even to Odd ratio is %d to %d. ", even_list->count, odd_list->count);
		ratio = ((double)even_list->count) / ((double)odd_list->count);
	}

	return ratio * 10; /*So it gives a percentage*/
}

/**
 * Finds the maxmium number in the list and returns it
 * @param *sentinel the sentinel holding the info of the list
*/
int find_max(struct info_t *sentinel)
{
	struct node_t *node = sentinel->head;
	int max = sentinel->head->n;
	
	while(node != NULL){
		if(node->n > max){
			max = node->n;
		}
	node = node->next;
	}
	return max;
}
/**
 * Prints the entire list it takes in
 * @param *sentinal the sentinal holding list info
 * @return nothing
*/
void print_list(struct info_t *sentinel)
{
	struct node_t *tmp = sentinel->head;
	while(tmp != NULL){
		print_node(tmp);
		printf("-->");
		tmp = tmp -> next;
	}
	printf("NULL\n");
}
/**
 * Prints the current node (helper function)
 * @param *node the node to be printed
 * @return nothing
*/
void print_node(struct node_t *node)
{
	if (node != NULL)
		printf("[%d]", node->n);
	else 
		printf("\nNode is currently NULL.\n");
}
/**
 * Deletes the list entirely
 * @param *sentinel the sentinel that holds info of the list
 * @return nothing
*/
void delete_list(struct info_t *sentinel)
{
	struct node_t *current = sentinel->head;

	while(sentinel->head != NULL){
        	current = sentinel->head;
        	sentinel->head = sentinel->head->next;
        	free(current);
    	}
    	free(sentinel);
}