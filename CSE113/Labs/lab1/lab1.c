#include <stdio.h>
#include <math.h>

/* Prototypes for functions in program */
int area_rectangle(int h, int w);
int perimeter_rectangle(int h, int w);
float diagonal_rectangle(int h, int w);
float area_circle(float r);
float circumference(float r);
float area_triangle(float b, float h);
float hypotenuse(float b, float h);
float perimeter_triangle(float b, float h, float c);
int exterior_angle(int n);
int interior_angle(int n);
int area_polygon(int n, float s);

/* determines area of rectangle */
int area_rectangle(int h, int w)
{
	return w * h;
} 

/* Calculates perimeter of rectangle */
int perimeter_rectangle(int h, int w)
{
	return (2 * w) + (2 * h);
}

/* Calculates the diagonal going across the rectangle */
float diagonal_rectangle(int h, int w)
{
	return sqrt(pow(w, 2) + pow(h, 2));
}

/* Calculates area of the circle */
float area_circle(float r) 
{
	return M_PI * pow(r, 2);
}

/* Calculates the circumference of the circle */
float circumference(float r)
{
	return 2 * M_PI * r;
}

/* Calculates the area of the triangle */
float area_triangle(float b, float h)
{
	return 0.5 * b * h;
}

/* Calculates hypotenuse of the triangle */
float hypotenuse(float b, float h)
{
	return sqrt(pow(b, 2) + pow(h, 2));
}

/* Calculates perimeter of triangle using hypotenuse function output */
float perimeter_triangle(float b, float h, float c)
{
	return b + h + c; 
}

/* Calculates what the exterior angle of the polygon */
int exterior_angle(int n)
{
	return 360/n;
}

/* Calculates the interior angle of the polygon */
int interior_angle(int n)
{
	return 180*(n-2)/n;
}

/* Calculates area of the polygon */
int area_polygon(int n, float s)
{
	int area = (pow(s, 2) * n) / (4 * tan(M_PI / n));
	return area;
}

int main(void)
{

	/* Declares types of variables before use of scanf */
	int rect_h;
	int rect_w;
	float cir_r;
	float tri_b;
	float tri_h;
	int poly_sides;
	float poly_side_l;
	
	/* Prompts user for various requirements (inputs) for proper calculations of the corresponding shapes */
	printf("\nHello! Welcome to the geometry calculator!\nMade by Steven Ramsey.\n\nAnswer the questions below:\n");
	printf("\nRectangles:\nWhat is the height of the rectangle? : ");
	scanf("%d", &rect_h);
	printf("Now what is the width of the same rectangle? : ");
	scanf("%d", &rect_w);
	printf("\nCircles:\nWhat is the radius of the circle? : ");
	scanf("%f", &cir_r);
	printf("\nTriangles:\nWhat is the base of the triangle? : ");
	scanf("%f", &tri_b);
	printf("Now what is the height of the same triangle? : ");
	scanf("%f", &tri_h);
	printf("\nPolygons:\nHow many sides does the polygon have? : ");
	scanf("%d", &poly_sides);
	printf("Now what is the length of those sides? : ");
	scanf("%f", &poly_side_l);
	
	/*int rect_h = 25;
	int rect_w = 30;

	float cir_r = 30.0;

	float tri_h = 12.2;
	float tri_b = 15.6;

	int poly_sides = 7;
	float poly_side_l = 0.8;*/

	/* For use in function perimeter_triangle, prepares for input into function. */
	float hypo = hypotenuse(tri_b, tri_h);

	/* Shows results of the corresponding calculations for each shape mentioned, also function calls. */
	printf("\nHere is the results:\n");
	
	printf("\nRectangles:\nThe area of rectangle with width = %d and height = %d is %d.\n",rect_w, rect_h, area_rectangle(rect_h, rect_w));

	printf("The perimeter of a rectangle with width = %d and height = %d is %d.\n", rect_w, rect_h, perimeter_rectangle(rect_h, rect_w));

	printf("The diagonal of a rectangle with width = %d and height = %d is %.2f.", rect_w, rect_h, diagonal_rectangle(rect_h, rect_w));
	
	printf("\n\nCircles:\nArea of a circle with a radius of %.2f is %.2f.\n", cir_r, area_circle(cir_r));

	printf("The circumference of a circle with a radius of %.2f equals %.2f.\n", cir_r, circumference(cir_r));

	printf("\nTriangles:\nArea of a triange with a base = %.2f and a height = %.2f is %.2f.\n", tri_b, tri_h, area_triangle(tri_b, tri_h));

	printf("Hypotenuse of a triange with a base = %.2f and a height = %.2f equals %.2f.\n", tri_b, tri_h, hypo);

	printf("The perimeter of a triangle with a base = %.2f, height = %.2f, and a hypotenuse = %.2f is %.2f.\n", tri_b, tri_h, hypo, perimeter_triangle(tri_b, tri_h, hypo));

	printf("\nPolygons:\nThe exterior angle of a polygon with %d sides equals %d degrees.\n", poly_sides, exterior_angle(poly_sides));

	printf("The interior angle of a polygon with %d sides equals %d degrees.\n", poly_sides, interior_angle(poly_sides));
    
	printf("The area of a polygon with %d sides with the length of %.2f on each is %d.\n\n", poly_sides, poly_side_l, area_polygon(poly_sides, poly_side_l));
	
	return 0;

}
