#ifndef ZOMBIE_H_
#define ZOMBIE_H_

#include <stdlib.h>

#define LEN 1024
#define SIZE 5

struct data_t {
	char dead;
	enum {MONDAY = 1, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY,
		SATURDAY, SUNDAY} day;
	int toes;
	float blood;
	int hour;
	int min;
	int sec;
};

void zombie_array(struct data_t zombie[], int toes, int hour, int min, int sec, float blood, int index);
void get_time(struct data_t zombie[], char buffer[], int a);
void get_date(struct data_t zombie[], int a, char buffer[]);
void get_toes(struct data_t zombie[], int a, char buffer[]);
void get_blood(struct data_t zombie[], int a, char buffer[]);
void print_data(struct data_t zombie[], int a);

#endif
