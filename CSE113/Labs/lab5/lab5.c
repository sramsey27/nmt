 /**
 * @file lab5.c
 * 
 * @author Steven Ramsey
 *
 * @date 3/1/2014
 *  
 * Assignment: Lab 5 
 *
 * @brief a program that keeps a log of zombie infomation within a array 
 		  structure.
 *
 * @details This program takes user input to keep a log of every zombie the
 		    user perhaps encountered. Its conditions change depending on the
 		    starting condition of the zombie.
 *  
 * @bugs sscanf can be buggy with its input method
 *
 * @todo Implement more error checking, fixing the sscanf bug.
 */

#include <stdio.h>
#include "zombie.h"

int main(void)
{
	int a = 0;
	char buffer[LEN]; /* Prepares char buffer */
	char choice_input;
	char point = 0;
	struct data_t zombie[SIZE];

	/* While loop provides menu choices */
	while(1) {
		printf("\n___________________________\n");
		printf("\n1. Enter new zombie infomation.\n2. Display current Zombie infomation\n3. Return to fighting zombies (exit)\n");
		printf("Choice: ");
		fgets(buffer, LEN, stdin);
		sscanf(buffer, "%c", &choice_input);

		/* Executes the corresponding choice */
		switch (choice_input) {

		/* Executes data fetching depending on zombie starting condition */
		case '1':
			point = 0;
			printf("___________________________\n");
			printf("\nWas the Zombie found dead? Y/N?\nAnswer: ");
			fgets(buffer, LEN, stdin);
			sscanf(buffer, "%c", &zombie[a].dead);

			if (zombie[a].dead == 'Y' || zombie[a].dead == 'y') {
				get_toes(zombie, a, buffer);
			}
			else {
				get_blood(zombie, a, buffer);	
			}
			get_date(zombie, a, buffer);
			get_time(zombie, buffer, a);
			a = a + 1;
			if (a == 5) {
				point = 1;
				a = 0;
			}
			break;
					
		/* Executes printing of zombie data */
		case '2':
			if (point) {
				print_data(zombie, SIZE);
			}
			else {
				print_data(zombie, a);
			}
			break;
					
		/* Returns 0 to exit program gracefully */
		case '3':
			printf("\nStay Alert! Keep a watch out for zombies!!\n\nGOODBYE and GOODLUCK!\n\n");
			return 0;
		default:
			printf("\nPlease enter only numbers 1 - 3 only!\n");
		}
	}
	return 0;
}
