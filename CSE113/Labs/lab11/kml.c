#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "kml.h"

/**
 * Strips the buffer input of whitespace
 * @param buf the buffer input from in_file
 * @return the newly striped buffer
 */
char* rstrip(char* buf){

	int length = strlen(buf) - 1;
	int i = 0;

	while(i<length){
		if(buf[i]== '\t' ||
		buf[i]== '\n' ||
		buf[i]== '\f' ||
		buf[i]== '\v' ||
		buf[i]== '\r') {
		/*Really wierd coding style isn't it?*/
		buf[i] = '\0';
		}
		i++;
	}	
	return buf;
}
/**
 * Gets the city, state, and country from current entry and stores it in the structure.
 * @param i current structure entry
 * @param buffer the current line
 * @param city_info the structure holding the city info
 * @return nothing
 */
void set_city_state_country(int i, char* buffer, struct geo_city_t *city_info)
{
	char* tmp = NULL;
	char* tmp_2 = NULL;
	int tmp_length = 0; 

	tmp = strtok(buffer, ",");
	tmp_2 = strtok(NULL, ",");
	sscanf(tmp_2, " %s", tmp_2);
	tmp_length = strlen(tmp_2);

	/*Puerto Rico Case*/
	if(strcmp(tmp_2, "Puerto") == 0){
		city_info[i].city = malloc(sizeof(tmp) + 1);
		city_info[i].city = strncpy(city_info[i].city, tmp, strlen(tmp) + 1);
		city_info[i].state = malloc(sizeof(tmp_2));
		city_info[i].state = strncpy(city_info[i].state, "PR", strlen("PR")  + 1);
		city_info[i].country = malloc(sizeof(char) * strlen("United States") + 1);
		city_info[i].country = strncpy(city_info[i].country, "United States", strlen("United States") + 1);
	}
	/*State Case*/
	else if(tmp_length == 2){
		city_info[i].city = malloc(sizeof(tmp));
		city_info[i].city = strncpy(city_info[i].city, tmp, strlen(tmp) + 1);
		city_info[i].state = malloc(sizeof(tmp_2));
		city_info[i].state = strncpy(city_info[i].state, tmp_2, strlen(tmp_2) + 1);
		city_info[i].country = malloc(sizeof(char) * strlen("United states") + 1);
		city_info[i].country = strncpy(city_info[i].country, "United States", strlen("United States") + 1);
	}
	/*DC Case*/
	else if (tmp_length == 4){
		city_info[i].city = malloc(sizeof(buffer));
		city_info[i].city = strncpy(city_info[i].city, buffer, strlen(buffer) + 1);
		city_info[i].state = malloc(sizeof("DC") * strlen("DC") + 1);
		city_info[i].state = strncpy(city_info[i].state, "DC", strlen("DC") + 1);
		city_info[i].country = malloc(sizeof(char) * strlen("United states") + 1);
		city_info[i].country = strncpy(city_info[i].country, "United States", strlen("United States") + 1);
	}
	/*Other countries other than U.S*/
	else{
		city_info[i].city = malloc(sizeof(tmp));
		city_info[i].city = strncpy(city_info[i].city, tmp, strlen(tmp) + 1);
		city_info[i].state = NULL;
		city_info[i].country = malloc(sizeof(tmp_2));
		city_info[i].country = strncpy(city_info[i].country, tmp_2, strlen(tmp_2) + 1);
	}
}
/**
 * Grabs the latitude and puts it in the structure
 * @param buf the current line
 * @param i current structure entry
 * @param city_info the structure holding the city info
 * @return nothing
 */
void set_latitude(char* buf, int i, struct geo_city_t *city_info)
{
	char direction;
	int deg, mins;
	float latitude;

	sscanf(buf, "%d %d %c", &deg, &mins, &direction);
	city_info[i].orilati_deg = deg;
	city_info[i].orilati_mins = mins;
	latitude = (float)deg + (float)mins / 60.0;
	
	if(direction == 'N'){
		city_info[i].latitude = latitude;
		city_info[i].lodirection = direction;
	}
	else {
		city_info[i].latitude = -1.0 * latitude;
		city_info[i].lodirection = direction;
	}
}
/**
 * Grabs the longitude and puts it in the structure
 * @param buf the current line
 * @param i current structure entry
 * @param city_info the structure holding the city info
 * @return nothing
 */
void set_longitude(char* buf, int i, struct geo_city_t *city_info)
{
	char direction;
	int deg, mins;
	float longitude;

	sscanf(buf, "%d %d %c", &deg, &mins, &direction);

	city_info[i].orilong_deg = deg;
	city_info[i].orilong_mins = mins;

	longitude = (float)deg + (float)mins / 60.0;
	if(direction == 'E'){
		city_info[i].longitude = longitude;
		city_info[i].ladirection = direction;
	}
	else {
		longitude = -1.0 * longitude;
		city_info[i].longitude = longitude;
		city_info[i].ladirection = direction;
	}
}
/**
 * Outputs requested data to location.out
 * @param file_out the file to be written to
 * @param city_info the structure holding the city info
 * @param i the current index
 * @return nothing
 */
void location_out(FILE* file_out, struct geo_city_t *city_info, int i)
{
	fprintf(file_out, "%s|", city_info[i].city);

	if(city_info[i].state == NULL){
		fprintf(file_out, "%s|", "-");
	}
	else{
		fprintf(file_out, "%s|", city_info[i].state);
	}
	fprintf(file_out,"%s|", city_info[i].country);
	fprintf(file_out, "Latitude %d %d ", city_info[i].orilati_deg, city_info[i].orilati_mins);
	fprintf(file_out, "%c|", city_info[i].ladirection);
	fprintf(file_out, "longitude %d %d ", city_info[i].orilong_deg, city_info[i].orilong_mins);
	fprintf(file_out, "%c\n", city_info[i].lodirection);
}
/**
 * Starts the first few lines of a usual KML file
 * @param kml_out the file to be written to
 * @return nothing
 */
void begin_kml(FILE* kml_out)
{
	printf("\nWriting Beginning of KML File...");
	fputs("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n", kml_out);
	fputs("<kml xmlns=\"http://earth.google.com/kml/2.1\">\n", kml_out);
	fputs("<Document>\n\t<name>Lab11 output</name>\n", kml_out);
	printf("Done\n");
}
/**
 * Creates the body of the KML file using the data from the input file
 * @param kml_out the file to be written to
 * @param lo the current city
 * @param lo2 the current state
 * @param lo3 the current country
 * @param lon the current longitude
 * @param lat the current latitude
 * @return nothing
 */
void kml_placemark(FILE* kml_out, char* lo, char* lo2, char* lo3, float lon, float lat)
{
	fputs("\n\t<Placemark>\n\t\t<name>", kml_out);
	if(lo2 != NULL){
		fprintf(kml_out, "%s, %s",lo, lo2);
	}
	else{
		fprintf(kml_out, "%s, %s",lo, lo3);
	}
	fputs("</name>\n\t\t<Point>\n\t\t\t<coordinates>", kml_out);
	fprintf(kml_out, "%.2f, %.2f, 0</coordinates>\n",lon, lat);
	fputs("\t\t</Point>\n\t</Placemark>\n",kml_out);
}
/**
 * Appends the last section of the KML file
 * @param kml_out the file to be written to
 * @return nothing
 */ 
void kml_end(FILE* kml_out)
{
	fputs("</Document>\n</kml>", kml_out);
}
/**
 * frees the structure completely element by element then frees the
 * the structure itself
 * @param city_info the structure holding the city info
 * @param j the current index
 * @return nothing
 */
void free_stuff(struct geo_city_t *city_info, int j)
{
	free(city_info[j].city);

	if (city_info[j].state != NULL){
		free(city_info[j].state);
	}
	
	free(city_info[j].country);
}