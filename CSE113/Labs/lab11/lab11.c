#include "kml.h"
#define LEN 256
#define EXIT_PROGRAM 1227

int main()
{
	struct geo_city_t *city_info = NULL;
	int i = 0, line_count = 0, element_count = 1;
	char* buf = NULL;

	char buffer[LEN];
	FILE* in_file;
	FILE* out_file;
	FILE* kml_out;
	
	/*Initalizes files for program*/
	in_file = fopen("location.txt", "r");
	out_file = fopen("location.out", "w");
	kml_out = fopen("location.kml", "w");

	/*Checks for allocation failure*/
	if(in_file == NULL || out_file == NULL || kml_out == NULL){
		printf("\nError in file creation or file reading!\n");
		printf("Ending Program...\n\n");
		exit(EXIT_PROGRAM);
	}
	/*Begins the KML file*/
	begin_kml(kml_out);

	/*Allocates space for structure*/
	city_info = malloc(sizeof(struct geo_city_t));

	/*Checks for allocation failure*/
	if (city_info == NULL){
		printf("Malloc Failed at City Info.\nExitting...\n\n");
		free(city_info);
		exit(EXIT_PROGRAM);
	}

	int mem_count = 1;

	printf("Writing body of KML File...");
	/*Initializes file read through*/
	while(fgets(buffer, LEN, in_file)){

		/*Skips the first three lines*/
		if(line_count < 3){
			line_count++;
			continue;
		}
		/*Realloc more memory each time*/
		if (i + 1 >= mem_count){
			element_count++;
			mem_count *= 2;
			city_info = realloc(city_info, (sizeof(struct geo_city_t) * mem_count));
		}

		/*Strips file of whitespace*/
		buf = rstrip(buffer);

		/*City Line*/
		if(line_count % 3 == 0){
			set_city_state_country(i, buf, city_info);
		}
		/*Latitude Line*/
		else if(line_count % 3 == 1){
			set_latitude(buf, i, city_info);
		}
		/*Longitude Line*/
		else if(line_count % 3 == 2) {
			set_longitude(buf, i, city_info);
		}

		/*Writes to KML only after each city entry*/
		if (line_count % 3 == 2) {

			/*Writes infomation to the KML file*/

			kml_placemark(kml_out, city_info[i].city, city_info[i].state, city_info[i].country, city_info[i].longitude, city_info[i].latitude);
			location_out(out_file, city_info, i);
			/*Increments index after each entry*/
			i++;
		}

		/*Used only for first 3 line skips*/
		line_count++;
	}
	printf("Done.\nWriting end of KML File...");

	/*Writes the Ending of the KML file*/
	kml_end(kml_out);

	printf("Done.\nOutput called location.kml\n");
	printf("Finished writing location.out.\n");

	/*Runs through the structure freeing the elements along the way*/
	int j = 0;
	while(j != i){
		free_stuff(city_info, j);
		j++;
	}
	/*Finally frees the structure itself*/
	free(city_info);

	fclose(in_file);
	fclose(out_file);
	fclose(kml_out);

	printf("\nProgram Finished Successfully!\n\n");

	return 0;

}
