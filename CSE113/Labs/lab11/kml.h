#ifndef KML_H
#define KML_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*Structure holding the city infomation.*/
struct geo_city_t {
	char *city;
	char *state; /*The postal code*/
	char *country;
	char lodirection;
	char ladirection;
	int orilong_deg;
	int orilong_mins;
	int orilati_deg;
	int orilati_mins;
	float latitude;
	float longitude;
};

/*There is more in the structure due to location_out*/

char *rstrip(char* buffer);
void set_city_state_country(int i, char* buffer, struct geo_city_t *city_info);
void set_latitude(char* buf, int i, struct geo_city_t *city_info);
void set_longitude(char* buf, int i, struct geo_city_t *city_info);
void location_out(FILE* file_out, struct geo_city_t *city_info, int i);
void begin_kml(FILE* kml_out);
void kml_placemark(FILE* kml_out, char* lo, char* lo2, char* lo3, float lon, float lat);
void kml_end(FILE* kml_out);
void free_stuff(struct geo_city_t *city_info, int j);

#endif

