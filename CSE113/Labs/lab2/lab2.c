#include <stdio.h>
#include <math.h>
#include <stdlib.h>

/* Prototypes for functions in program */
int area_rectangle(int h, int w);
int perimeter_rectangle(int h, int w);
float diagonal_rectangle(int h, int w);
float area_circle(float r);
float circumference(float r);
float area_triangle(float b, float h);
float hypotenuse(float b, float h);
float perimeter_triangle(float b, float h);
int exterior_angle(int n);
int interior_angle(int n);
int area_polygon(int n, float s);

/**
* determines area of rectangle
* @param h height of the rectangle
* @param w width of the rectangle
* @return the area of the rectangle.
*/

int area_rectangle(int h, int w)
{
	return w * h;
} 

/**
* Calculates perimeter of rectangle
* @param h height of rectangle
* @param w width of rectangle
* @return perimeter of rectangle
*/

int perimeter_rectangle(int h, int w)
{
	return (2 * w) + (2 * h);
}

/**
* Calculates the diagonal going across the rectangle
* @param h height of rectangle
* @param w width of rectangle
* @return length of the diagonal
*/

float diagonal_rectangle(int h, int w)
{
	return sqrt(pow(w, 2) + pow(h, 2));
}

/**
* Calculates area of the circle
* @param r radius of the circle
* @return area of the circle
*/

float area_circle(float r) 
{
	return M_PI * pow(r, 2);
}

/**
* Calculates the circumference of the circle
* @param r radius of the circle
* @return the circumference of the circle
*/

float circumference(float r)
{
	return 2 * M_PI * r;
}

/**
* Calculates the area of the triangle
* @param b the length of the base
* @param h height of the triangle
* @return the area of the circle
*/

float area_triangle(float b, float h)
{
	return 0.5 * b * h;
}

/**
* Calculates hypotenuse of the triangle
* @param b the length of the base
* @param h the height of the triangle
* @return the hypotenuse of the triangle
*/

float hypotenuse(float b, float h)
{
	return sqrt(pow(b, 2) + pow(h, 2));
}

/**
* Calculates perimeter of triangle using hypotenuse function output 
* @param b the length of the base
* @param h the height of the triangle
* @return the perimeter of the triangle
*/

float perimeter_triangle(float b, float h)
{
	float c;
	/* calls function hypotenuse and assigns
	return value to c*/
	c = hypotenuse(b, h);
	return b + h + c; 
}

/**
* Calculates what the exterior angle of the polygon 
* @param n the number of sides the polygon has
* @return the length of the exterior angle
*/

int exterior_angle(int n)
{
	return 360/n;
}

/**
* Calculates the interior angle of the polygon
* @param n the number of sides the polygon has
* @return the length of the interior angle
*/

int interior_angle(int n)
{
	return 180*(n-2)/n;
}

/**
* Calculates area of the polygon
* @param n the number of sides the polygon has
* @param s the length of the sides
* @return the area of the polygon
*/

int area_polygon(int n, float s)
{
	return (pow(s, 2) * n) / (4 * tan(M_PI / n));
}

int main(void)
{

	/* Declares types of variables and character array
	 * before use of program.*/
	int rect_h, rect_w;
	float cir_r;
	float tri_b, tri_h;
	int poly_sides;
	float poly_side_l;
	int tmp_input, tmp_r, tmp_c, tmp_p, tmp_t;
	int choice_r, choice_c, choice_t, choice_p;
	int char_input;
	float hypo;
	char e[1024];
	
	/*Tells users what to input*/
	printf("\nHello! Welcome to the geometry calculator!\n");
	printf("Made by Steven Ramsey.\n\nEnter the one of the following.\nCircles: c\nRectangles: r\n");
	printf("Triangles: t\nRegular Polygons: p\n\n");
	printf("What shape are we working with today? : ");
	
	/*Request for a char input*/
	while ((tmp_input = getchar()) != '\n')
		char_input = tmp_input;
	
	/*Depending on user input, the switch will take place accordingly*/
	switch (char_input) {
	case 'R':
	case 'r':
		/*Rectangle*/
		printf("\nRectangles:\nWhat is the height of the rectangle? : ");
		scanf("%d", &rect_h);
		printf("Now what is the width of the same rectangle? : ");
		scanf("%d", &rect_w);
		
		printf("\nOkay, What would you like to calculate?\n");
		printf("\nArea: A\nPerimeter: P\nDiagonal: D\n\nWhat's your ");	
		printf("choice?: ");
		
		/*Request char input for a particular shape calculation.*/
		fgets(e, 1024, stdin);
		while ((tmp_r = getchar()) != '\n')
			choice_r = tmp_r;
		
		/*Calls functions depending on user input, and prints results*/
		switch (choice_r) {
		case 'A':
		case 'a':
			/*Area of rectangle*/
			printf("\nThe area of rectangle with width = ");
			printf("%d and height = %d is %d.\n\n",rect_w, rect_h, area_rectangle(rect_h, rect_w));
			break;
		case 'P':
		case 'p':
			/*Perimeter of rectangle*/
			printf("\nThe perimeter of a rectangle with width ");
			printf("= %d and height = %d is %d.\n\n", rect_w, rect_h, perimeter_rectangle(rect_h, rect_w));
			break;
		case 'D':
		case 'd':
			/*Diagonal of rectangle*/
			printf("\nThe diagonal of a rectangle with width ");
			printf("= %d and height = %d is %.2f.\n\n", rect_w, rect_h, diagonal_rectangle(rect_h, rect_w));
			break;
			
		/*If user inputs invalid char, the program will exit*/
		default:
			printf("\nInvalid Choice. Exiting...\n\n");
			break;
		}
		break;	
	case 'C':
	case 'c':
		/*Circle*/
		printf("\nCircles:\nWhat is the radius of the circle? : ");
		scanf("%f", &cir_r);
		
		printf("\nOkay, What would you like to calculate?\n");
		printf("\nArea: A\nCircumference: C\n");
		printf("\nWhat's your choice?: ");
		
		/*User char input*/
		fgets(e, 1024, stdin);
		while ((tmp_c = getchar()) != '\n')
			choice_c = tmp_c;
		
		/*Calls functions depending on user input, and prints results*/
		switch (choice_c) {
		case 'A':
		case 'a':
			/*Area of Circle*/
			printf("\nArea of a circle with a radius ");
			printf("of %.2f is %.2f.\n\n", cir_r, area_circle(cir_r));
			break;
		case 'C':
		case 'c':
			/*Circumference of Circle*/
			printf("\nThe circumference of a circle with a radius "); 
			printf("of %.2f equals %.2f.\n\n", cir_r, circumference(cir_r));
			break;
		default:
			printf("\nInvalid Input. Exitng...\n\n");
			break;
		}
		break;
	case 'T':
	case 't':
		/*Triangle*/
		printf("\nTriangles:\nWhat is the base of the triangle? : ");
		scanf("%f", &tri_b);
		printf("Now what is the height of the same triangle? : ");
		scanf("%f", &tri_h);
		printf("\nOkay, What would you like to calculate?\n");
		printf("\nArea: A\nHypotenuse: H\nPerimeter: P\n");
		printf("\nWhat's your Choice?: ");
		
		/*used to shorten print statement*/
		hypo = hypotenuse(tri_b, tri_h);
		
		fgets(e, 1024, stdin);
		while ((tmp_t = getchar()) != '\n')
			choice_t = tmp_t;
		
		/*Calls functions based on user input*/
		switch (choice_t) {	
		case 'A':
		case 'a':
			/*Area of Triangle*/
			printf("\nArea of a triange with a base "); 
			printf("= %.2f and a height = %.2f is %.2f.\n\n", tri_b, tri_h, area_triangle(tri_b, tri_h));
			break;
		case 'H':
		case 'h':
			/*Hypotenuse of Triangle*/
			printf("\nHypotenuse of a triange with a base "); 
			printf("= %.2f and a height = %.2f equals %.2f.\n\n", tri_b, tri_h, hypo);
			break;
		case 'P':
		case 'p':
			/*Perimeter of Triangle*/
			printf("\nThe perimeter of a triangle with a base ");
			printf("= %.2f, height = %.2f, and a hypotenuse = %.2f is %.2f.\n\n", tri_b, tri_h, hypo, perimeter_triangle(tri_b, tri_h));
			break;
		default:
			printf("\nInvalid Input. Exiting...\n\n");
			break;
		}
		break;
	case 'P':
	case 'p':
		/*Regular Polygons*/
		printf("\nPolygons:\nHow many sides does the polygon have? : ");
		scanf("%d", &poly_sides);
		printf("Now what is the length of those sides? : ");
		scanf("%f", &poly_side_l);
		printf("\nOkay, What would you like to calculate?\n");
		printf("\nInterior Angle: I\nExterior Angle: E\nArea: A\n");
		printf("\nWhat's your choice?: ");
		
		/*User input*/
		fgets(e, 1024, stdin);
		while ((tmp_p = getchar()) != '\n')
			choice_p = tmp_p;
		
		switch (choice_p) {
		case 'I':
		case 'i':
			/*Interior Angle of polygon*/
			printf("\nThe interior angle of a polygon with ");
			printf("%d sides equals %d degrees.\n\n", poly_sides, interior_angle(poly_sides));
			break;
		case 'E':
		case 'e':
			/*Exterior Angle of polygon*/
			printf("\nThe exterior angle of a polygon ");
			printf("with %d sides equals %d degrees.\n\n", poly_sides, exterior_angle(poly_sides));
			break;
		case 'A':
		case 'a':
			/*Area of Polygon*/
			printf("\nThe area of a polygon with %d sides with the ", poly_sides);
			printf("length of %.2f on each is %d.\n\n", poly_side_l, area_polygon(poly_sides, poly_side_l));\
			break;
		default:
			/*Invalid Input*/
			printf("Invalid Input. Exiting...\n\n");
			break;
		}
		break;
	default:
		printf("\nInvalid Input. Exiting...\n\n");
	}
	
	return 0;
	
}
