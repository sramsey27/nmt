#ifndef PREDICTOR21264_H
#define PREDICTOR21264_H

/*
  Define all your tables and their sizes here.
  All tables must be statically-sized.
  Please don't call malloc () or any of its relatives within your
  program.  The predictor will have a budget, namely (64K + 256) bits
  (not bytes).  That encompasses all storage (all tables, plus GHR, if
  necessary -- but not PC) used for the predictor.  That may not be
  the amount of storage your predictor uses, however -- for example,
  you may implement 2-bit predictors with a table of ints, in which
  case the simulator will use more memory -- that's okay, we're only
  concerned about the memory used by the simulated branch predictor.
*/

#define LOCALSIZE 1024
#define GLOBALSIZE 4096

struct LocalHistory{
        unsigned int hist_entry : 10; //unsigned int of size 10 bits
};

struct LocalPredict{
        unsigned int pred_entry: 3; //3 bit saturating counter
};

struct GlobalHistory{
        unsigned int g_entry: 12; //12 bit register
};

struct GlobalPredict{
        unsigned int g_table : 2; //2bit saturating counter
};

struct PredictTable{
        unsigned int p_table: 2; // 2 bit saturating counter
};

struct local_global{
        bool global;
        bool local;
};

/*
  Initialize the predictor.
*/
void init_predictor ();

/*
  Make a prediction for conditional branch instruction at PC 'pc'.
  Returning true indicates a prediction of taken; returning false
  indicates a prediction of not taken.
*/
bool make_prediction (unsigned int pc);

/*
  Train the predictor the last executed branch at PC 'pc' and with
  outcome 'outcome' (true indicates that the branch was taken, false
  indicates that the branch was not taken).
*/
void train_predictor (unsigned int pc, bool outcome);

#endif
