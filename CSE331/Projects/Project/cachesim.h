/**
 * filename: cachesim.h
 *
 * description: The header file for cachesim.cpp
 *
 * author: Steven Ramsey
 *
 * class: 		CSE331
 * instructor:	Zheng
 * assignment:	Lab Project #1
 * 
 * assigned: 	Oct 17, 2016
 * due:			Oct 31, 2016
 */

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <cstring>
#include <sstream>
#include <vector>
#include <math.h>
#include <algorithm>
#include <list>
#include <climits>
#include <iomanip>
#include <cstdio>

/*Additional features*/
bool debugmode = false; /*Show infomation helpful for debugging, 0 -> off, 1 -> on*/
bool display_hits = false; /*flag to display number of hits along with calculations*/
bool display_config = false; /*Display configuration infomation*/
bool output_to_file = true; /*Output to file when true, out to terminal stdout when false*/

struct config{ /*Stores infomation of configuration file*/
	unsigned int linesize;
	unsigned int writemode;
	unsigned int datasize;
	unsigned int policy;
	unsigned int misspenalty;
	unsigned int allocatemode;
};

struct result{ /*Keeps track of hits/misses/cycles etc...*/
	unsigned int total_load_hits;
	unsigned int total_store_hits;
	unsigned int total_load_misses;
	unsigned int total_store_misses;
	unsigned int total_misses;
	unsigned int cycles;
	unsigned int memory_access;
};

struct calculations{ /*Final calculations, may be removed*/
	double total_hitrate;
	double load_hitrate;
	double store_hitrate;
	double runtime_cycles;
	double AMAT;
};

/*Cache Architecture*/
struct block_t{
	unsigned int valid;
	unsigned int tag;
	unsigned int dirty;
	unsigned int time_stored;
	unsigned int time_accessed;
};

struct cacheset_t{
	int time_stamp;
	block_t **blocks;
}; 

/*Prototypes*/
config readin_config(config cacheinfo, char config_name[]);
result trace_process(config cacheinfo, result stats, char trace_name[]);
void config_display(config cacheinfo, char trace_name[], char config_name[]);
void display_results(result stats, calculations final_stats, config cacheinfo);
calculations get_calcuations(struct result cacheinfo ,struct calculations final_stats);
