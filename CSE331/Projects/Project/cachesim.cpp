/**
 * filename: cachesim.cpp
 *
 * description: An implementation of a cache simulator which supports no-write-allocate/write-through and
 * 				write-allocate/write-back modes. Supported replacement policies: FIFO and random placement.
 *				Support for direct-mapped is also present. Chosen modes are based on configuration files
 *				which also contains various cache infomation such as Associativity, block size, etc.
 *
 * author: Steven Ramsey
 *
 * class: 		CSE331
 * instructor:	Zheng
 * assignment:	Lab Project #1
 * 
 * assigned: 	Oct 17, 2016
 * due:			Oct 31, 2016
 */

using namespace std;
#include "cachesim.h"

/**
 * This is where the actual simulation occurs, the creation of the cache and placement algorithms
 * happen within this function.
 * @param cacheinfo structure holding the cache infomation brought in from config file
 * @param stats structure holding number of hits/misses and cycles, populated here in this function.
 * @param trace_name the path of the trace file for processing
 * @return the stats/results structure with hit/miss/cycle infomation
 */
result trace_process(config cacheinfo, result stats, char trace_name[]) {

	bool found = false;
	char temp_string[255], *mode, *address, *In_lastaccess_temp; /*for file extractions*/
	unsigned int instruction_count = 0, set_count = 0, offset_size = 0, frame_count = 0; /*Various cache infomation*/
	unsigned int index_size = 0, setIndex = 0 , tag_size = 0, temp_tag = 0; /*Various block infomation*/
	unsigned int mem_access = 0, Associativity = 0, cachesize = 0, cur_line = 0;
	unsigned long int min_time = ULONG_MAX, tempindex = 0, x = 0;
	unsigned int replacement = 0;

	/*The cache*/
	struct cacheset_t *cache;

	srand(time(NULL)); /*For random placement, random number generation*/

	/*Prevent garbage infomation*/ 
	stats.total_load_hits = 0;
	stats.total_store_hits = 0;
	stats.total_load_misses = 0;
	stats.total_store_misses = 0;
	stats.cycles = 0;
	stats.memory_access = 0;

	ifstream infile;
	infile.open(trace_name); /*Open trace file*/

	if(!infile){ /*Make sure the file opens before doing anything else*/
		cout << "Cannot open trace file: \"" << trace_name << "\" \n";
		exit(2);
	}

	/*-------Calculate various cache infomation--------*/
	if(cacheinfo.writemode == 0 || cacheinfo.writemode == 1){ /*Fully associative or direct-mapped*/
		Associativity = 1;
	}
	else{
		Associativity = cacheinfo.writemode;
	}
	frame_count = (cacheinfo.datasize * 1000)/ cacheinfo.linesize;
	if(Associativity == 1){
		set_count = frame_count;
	}
	else {
		set_count = frame_count/Associativity; 
	}
	cachesize = set_count * cacheinfo.linesize; //cachesize
	index_size = log2(set_count);
	offset_size = log2(cacheinfo.linesize);
	tag_size = 32 - offset_size - index_size;
	/*------------End of calcuations-------------------*/

	if(debugmode){/*Display calculated block infomation*/
		cout << "Frame count = " << frame_count << "\nSet Count = " << set_count << "\n";
		cout << "Index size = " << index_size << "\nTag size = " << tag_size << "\noffset size = " << offset_size << "\n";
	}

	/*---------Build the cache ----------- 2nd overhaul*/
	cache = new cacheset_t; /*Allocate new cache structure*/
	cache->time_stamp = 0; /*timestamp for FIFO operations*/
	cache->blocks = new block_t*[Associativity];
	for(unsigned int i = 0; i < Associativity; i++){
		cache->blocks[i] = new block_t[set_count * cacheinfo.linesize];
	}
	for (unsigned int i = 0; i < Associativity; i++){
		for(unsigned int j = 0; j < cachesize; j++){
			cache->blocks[i][j].valid = 0;
			cache->blocks[i][j].tag = 0;
			cache->blocks[i][j].dirty = 0;
			cache->blocks[i][j].time_stored = 0;
			cache->blocks[i][j].time_accessed = 0;
		}
	}
	/*----------End of building----------- 2-D array of structures based on assciativity*/

	/*Start running through the trace file and do operations while doing so*/

	while(infile){

		temp_tag = 0; setIndex = 0;
		cache->time_stamp++;
		found = false; /*until proven otherwise*/
		min_time = ULONG_MAX;
		tempindex = 0, x = 0;

		infile.getline(temp_string, 255);

		 /*Prevent last line seg fault and invalid file*/
		if(infile == NULL || temp_string == '\0'){
			break;
		}

		/*split the line into pieces*/
		mode = strtok(temp_string, " ");
		address = strtok(NULL, " ");
		In_lastaccess_temp = strtok(NULL, " ");
		mem_access = strtoul(address, 0, 16); /*hex to int address*/
		instruction_count = atoi(In_lastaccess_temp); /*instruction count since last mem access*/

		/*Identify the tag and set Index*/
		x = mem_access % cacheinfo.linesize;
		tempindex = (mem_access/cacheinfo.linesize)%set_count;
		setIndex = (tempindex*cacheinfo.linesize) + x;
		temp_tag = (mem_access/cacheinfo.linesize)/set_count;
		stats.cycles = stats.cycles + instruction_count;

		if(debugmode){ /*Don't proceed with cache operations, used for checking calcuations*/
			continue;
		}

		/*----------Check to see if tag at index exist------------*/
		for(cur_line = 0; cur_line < Associativity; cur_line++){
			if(cache->blocks[cur_line][setIndex].valid == 1){
				if(cache->blocks[cur_line][setIndex].tag == temp_tag){
					cache->time_stamp++;
					found = true;
				}
			}
		}
		/*----------------Done searching------------------*/

		if(!found){ /*CACHE MISS*/

			stats.cycles = stats.cycles + cacheinfo.misspenalty;
			for(cur_line = 0; cur_line < Associativity && (cache->blocks[cur_line][setIndex].valid == 1); cur_line++);

			if(cacheinfo.allocatemode == 0 && strcmp("l", mode) == 0){ //NOWRITE ALLOCATE
					if(cur_line != Associativity){
						cache->blocks[cur_line][setIndex].tag = temp_tag;
						cache->blocks[cur_line][setIndex].valid = 1;
						cache->blocks[cur_line][setIndex].time_stored = cache->time_stamp;
						cache->blocks[cur_line][setIndex].time_accessed = cache->time_stamp;
					}	
					else if(Associativity == 1){
						cur_line = 0;
						cache->blocks[cur_line][setIndex].tag = temp_tag;
						cache->blocks[cur_line][setIndex].valid = 1;
						cache->blocks[cur_line][setIndex].time_stored = cache->time_stamp;
						cache->blocks[cur_line][setIndex].time_accessed = cache->time_stamp;
					}
					else if(cacheinfo.policy == 1){ //FIFO
						for(unsigned int i = 0; i < Associativity; i++){
							if(cache->blocks[i][setIndex].time_stored < min_time){
								min_time = cache->blocks[i][setIndex].time_stored;
								replacement = i;
							}
						}
						cache->blocks[replacement][setIndex].tag = temp_tag;
						cache->blocks[replacement][setIndex].valid = 1;
						cache->blocks[replacement][setIndex].time_stored=cache->time_stamp;
						cache->blocks[replacement][setIndex].time_accessed = cache->time_stamp;
					}
					else if(cacheinfo.policy == 0){ //random placement
						replacement = rand()%Associativity;
						cache->blocks[replacement][setIndex].tag = temp_tag;
						cache->blocks[replacement][setIndex].valid = 1;
						cache->blocks[replacement][setIndex].time_stored=cache->time_stamp;
						cache->blocks[replacement][setIndex].time_accessed = cache->time_stamp;
					}
					goto count;
			}
			if(cacheinfo.allocatemode == 0 && strcmp("s", mode) == 0){
					goto count;
			}
			if(cur_line != Associativity){
				cache->blocks[cur_line][setIndex].tag = temp_tag;
				cache->blocks[cur_line][setIndex].valid = 1;
				cache->blocks[cur_line][setIndex].time_stored = cache->time_stamp;
				cache->blocks[cur_line][setIndex].time_accessed = cache->time_stamp;
			}	
			else if(Associativity == 1){ /*Direct-mapped*/
				cur_line = 0;
				cache->blocks[cur_line][setIndex].tag = temp_tag;
				cache->blocks[cur_line][setIndex].valid = 1;
				cache->blocks[cur_line][setIndex].time_stored = cache->time_stamp;
				cache->blocks[cur_line][setIndex].time_accessed = cache->time_stamp;
			}
			else if(cacheinfo.policy == 1){ /*FIFO Placement Policy*/
				replacement = 0;
				for(unsigned int i = 0; i < Associativity; i++){
					if(cache->blocks[i][setIndex].time_stored < min_time){
						min_time = cache->blocks[i][setIndex].time_stored;
						replacement = i;
					}
				}
				cache->blocks[replacement][setIndex].tag = temp_tag;
				cache->blocks[replacement][setIndex].valid = 1;
				cache->blocks[replacement][setIndex].time_stored=cache->time_stamp;
				cache->blocks[replacement][setIndex].time_accessed = cache->time_stamp;
			}
			else if(cacheinfo.policy == 0){ /*Random placement policy*/
				replacement = rand()%Associativity;
				cache->blocks[replacement][setIndex].tag = temp_tag;
				cache->blocks[replacement][setIndex].valid = 1;
				cache->blocks[replacement][setIndex].time_stored=cache->time_stamp;
				cache->blocks[replacement][setIndex].time_accessed = cache->time_stamp;
			}
count:
			if(strcmp("l", mode) == 0){
				stats.total_load_misses++;
				if(cacheinfo.allocatemode == 1){
					cache->time_stamp = cache->time_stamp + 2;
				}
			}
			else if(strcmp("s", mode) == 0){
				stats.total_store_misses++;
			}
		}
		else if (found) { /*CACHE HIT*/
			stats.cycles++;
			if(strcmp("l", mode) == 0){
				stats.total_load_hits++;

				if(cacheinfo.allocatemode == 0){
					for(cur_line = 0; cur_line < Associativity && (cache->blocks[cur_line][setIndex].valid == 1); cur_line++);

					if(cur_line != Associativity){
						cache->blocks[cur_line][setIndex].tag = temp_tag;
						cache->blocks[cur_line][setIndex].valid = 1;
						cache->blocks[cur_line][setIndex].time_stored = cache->time_stamp;
						cache->blocks[cur_line][setIndex].time_accessed = cache->time_stamp;
					}	
					else if(Associativity == 1){ /*Direct-mapped*/
						cur_line = 0;
						cache->blocks[cur_line][setIndex].tag = temp_tag;
						cache->blocks[cur_line][setIndex].valid = 1;
						cache->blocks[cur_line][setIndex].time_stored = cache->time_stamp;
						cache->blocks[cur_line][setIndex].time_accessed = cache->time_stamp;
					}
					else if(cacheinfo.policy == 1){ /*FIFO Placement Policy*/
						replacement = 0;
						for(unsigned int i = 0; i < Associativity; i++){
							if(cache->blocks[i][setIndex].time_stored < min_time){
								min_time = cache->blocks[i][setIndex].time_stored;
								replacement = i;
							}
						}
						cache->blocks[replacement][setIndex].tag = temp_tag;
						cache->blocks[replacement][setIndex].valid = 1;
						cache->blocks[replacement][setIndex].time_stored=cache->time_stamp;
						cache->blocks[replacement][setIndex].time_accessed = cache->time_stamp;
					}
					else if(cacheinfo.policy == 0){ /*Random placement policy*/
						replacement = rand()%Associativity;
						cache->blocks[replacement][setIndex].tag = temp_tag;
						cache->blocks[replacement][setIndex].valid = 1;
						cache->blocks[replacement][setIndex].time_stored=cache->time_stamp;
						cache->blocks[replacement][setIndex].time_accessed = cache->time_stamp;
					}
				}
				else if(cacheinfo.allocatemode == 1){ /*Write Allocate - Write back, work with dirty bit*/
					for(cur_line = 0; cur_line < Associativity && (cache->blocks[cur_line][setIndex].valid == 1); cur_line++);

					if(cur_line != Associativity){
						if(cache->blocks[cur_line][setIndex].dirty == 1){
							continue;
						}
						cache->blocks[cur_line][setIndex].tag = temp_tag;
						cache->blocks[cur_line][setIndex].valid = 1;

						if(cacheinfo.allocatemode == 1){
							cache->blocks[cur_line][setIndex].dirty = 1;
						}
						cache->blocks[cur_line][setIndex].time_stored = cache->time_stamp;
						cache->blocks[cur_line][setIndex].time_accessed = cache->time_stamp;
					}	
					else if(Associativity == 1){ /*Direct-mapped*/
						cur_line = 0;
						if(cache->blocks[cur_line][setIndex].dirty == 1){
							continue;
						}
						cache->blocks[cur_line][setIndex].tag = temp_tag;
						cache->blocks[cur_line][setIndex].valid = 1;	
						if(cacheinfo.allocatemode == 1){
							cache->blocks[cur_line][setIndex].dirty = 1;
						}
						cache->blocks[cur_line][setIndex].time_stored = cache->time_stamp;
						cache->blocks[cur_line][setIndex].time_accessed = cache->time_stamp;
					}
					else if(cacheinfo.policy == 1){ /*FIFO Placement Policy*/
						replacement = 0;
						for(unsigned int i = 0; i < Associativity; i++){
							if(cache->blocks[i][setIndex].dirty == 1){
								break;
							}
							if(cache->blocks[i][setIndex].time_stored < min_time){
								min_time = cache->blocks[i][setIndex].time_stored;
								replacement = i;
							}
						}
						if(cache->blocks[replacement][setIndex].dirty == 1){ //dont write the block
								continue;
						}
						cache->blocks[replacement][setIndex].tag = temp_tag;
						cache->blocks[replacement][setIndex].valid = 1;
						if(cacheinfo.allocatemode == 1){
							cache->blocks[replacement][setIndex].dirty = 1;
						}
						cache->blocks[replacement][setIndex].time_stored=cache->time_stamp;
						cache->blocks[replacement][setIndex].time_accessed = cache->time_stamp;
					}
					else if(cacheinfo.policy == 0){ /*Random placement policy*/
						replacement = rand()%Associativity;
						cache->blocks[replacement][setIndex].tag = temp_tag;
						cache->blocks[replacement][setIndex].valid = 1;
						cache->blocks[replacement][setIndex].time_stored=cache->time_stamp;
						cache->blocks[replacement][setIndex].time_accessed = cache->time_stamp;
					}
				}
			}
			else if(strcmp("s", mode) == 0){
				stats.total_store_hits++;
				cache->time_stamp = cache->time_stamp + 2;
			}
		}
	}

	/*Free up the cache so you can have your memory back :)*/
	for(unsigned int i = 0; i <= Associativity; i++){
		free(cache->blocks[i]);	
	}
	free(cache->blocks);
	free(cache);

	infile.close();
	return stats;
}

/**
 * Read in the configuration file and populate config structure with data
 * @param cacheinfo structure holding the cache infomation brought in from config file
 * @param config_name the configuration file pathname for processing
 * @return the populated cacheinfo structure
 */
config readin_config(config cacheinfo, char config_name[]){

		char temp_string[255];
		ifstream infile;
		infile.open(config_name);

		if(!infile){ /*Make sure the file opens before doing anything else*/
			cout << "Cannot open/find: \"" << config_name << "\"\n"; 
			exit(2);
		}

		/*Store config infomation assume format stays the same*/
		for(int i = 1; infile; i++){
			infile.getline(temp_string, 255);
			switch(i){
				case 1:
					cacheinfo.linesize = atoi(temp_string);
					break;
				case 2:
					cacheinfo.writemode = atoi(temp_string);
					break;
				case 3:
					cacheinfo.datasize = atoi(temp_string);
					break;
				case 4:
					cacheinfo.policy = atoi(temp_string);
					break;
				case 5:
					cacheinfo.misspenalty = atoi(temp_string);
					break;
				case 6:
					cacheinfo.allocatemode = atoi(temp_string);
					break;
			}
		}
		infile.close();

		if(debugmode){
			cout << "readin_config function complete \n";
		}
		return cacheinfo;
}

/**
 * Calculate the total hitrate, load hitrate, store hitrate, and Average Memory Access Time
 * @param stats structure holding the results gathered from the simulation
 * @param final_stats the structure for holding the calcuated results from this function
 * @param cacheinfo the structure holding the cache configuration obtained from config file
 * @return the populated calcuations structure
 */
calculations get_calcuations(struct result stats ,struct calculations final_stats, config cacheinfo){

	final_stats.total_hitrate = 0;
	final_stats.load_hitrate = 0;
	final_stats.store_hitrate = 0;
	final_stats.runtime_cycles = 0;
	final_stats.AMAT = 0;

	double totalhits = (stats.total_store_hits + stats.total_load_hits + stats.total_store_misses + stats.total_load_misses); 
	double hits = (double) (stats.total_load_hits + stats.total_store_hits);
	double misses = (double) (stats.total_load_misses + stats.total_load_misses);
	double temp = (double) ((hits * 1.0) + (misses * cacheinfo.misspenalty));

	final_stats.total_hitrate = (double)((stats.total_store_hits + stats.total_load_hits) * 100 / totalhits);
	final_stats.load_hitrate = (double)(stats.total_load_hits * 100)/(stats.total_load_hits + stats.total_load_misses);
	final_stats.store_hitrate = (double)(stats.total_store_hits * 100)/(stats.total_store_hits + stats.total_store_misses);
	final_stats.runtime_cycles = stats.cycles;
	final_stats.AMAT = (double) temp *1.0/(hits + misses);

	return final_stats;	
}

/**
 * Displays the cache configuration
 * @param cacheinfo the structure holding the cache infomation
 * @param trace_name the trace file pathname
 * @param config_name the config/conf pathname
 */
void config_display(config cacheinfo, char trace_name[], char config_name[]){

		cout << "\nConfig Filename: \"" << config_name << "\"\n";
		cout << "Trace Filename: \"" << trace_name << "\"\n";
		cout << "\nConfiguration......\n---------------------------\n";

		if ((cacheinfo.linesize % 2) == 0) {
			cout << "Line Size/Block size = " << cacheinfo.linesize << " byte Blocks\n";
		}
		else {
			cout << "Line Size --> INCONSISTENT LINE SIZE\n";
		}
		if(cacheinfo.writemode == 0){
			cout << "Associativity --> \"Fully-Associative\"\n";
		}
		else if (cacheinfo.writemode == 1){
			cout << "Associativity --> \"Direct-Mapped\"\n";
		}
		else if ((cacheinfo.writemode % 2) == 0) {
			cout << "Associativity --> " << cacheinfo.writemode << "-way set associative\n";
		}
		else {
			cout << "Write Mode --> INVALID ASSOCIATIVITY\n";
		}
		cout << "Data_Size = " << cacheinfo.datasize << "KB Cache\n";
		if(cacheinfo.policy == 0){
			cout << "Replacement Policy --> \"Random Placement\"\n";
		}
		else if (cacheinfo.policy == 1){
			cout << "Replacement Policy --> \"FIFO (First-In-First-Out)\"\n";
		}
		else {
			cout << "Replacement Policy --> INVALID POLICY\n";
		}
		cout << "Miss penalty = " << cacheinfo.misspenalty << " cycles\n";
		if(cacheinfo.allocatemode == 1){
			cout << "Write Mode --> \"Write-Allocate/write-back\"\n";
		}
		else if (cacheinfo.allocatemode == 0){
			cout << "Write Mode --> \"No-Write-Allocate/write-through\"\n";
		}
		else {
			cout << "Write Mode --> INVALID ALLOCATE MODE\n";
		}
		cout << "---------------------------\n\n";
}

/**
 * Displays the hit/miss/cycle results (if enabled) along with the final calcuations
 * @param stats the structure golding the hit/miss/cycles results from the simulation
 * @param final_stats the structure holding the final calculations based on stats
 */
void display_results(result stats, calculations final_stats){

	if(display_hits){ /*Present in header file*/
		cout << "\nCache Results.....\n---------------------------\n";
		cout << "Total Load Hits = " << stats.total_load_hits << "\n";
		cout << "Total Store Hits = " << stats.total_store_hits <<"\n";
		cout << "Total Load Misses = " << stats.total_load_misses << "\n";
		cout << "Total Store Misses = " << stats.total_store_misses << "\n";
		cout << "Total Cycles = " << stats.cycles << "\n\n";
		cout << "TOTAL HITS: " << stats.total_load_hits + stats.total_store_hits << "\n";
		cout << "TOTAL MISSES: " << stats.total_store_misses + stats.total_load_misses << "\n";
	}
	cout << "Total Hit Rate = " << setprecision(4) << final_stats.total_hitrate << "%";
	cout << "\nLoad Hit Rate = " << setprecision(4) << final_stats.load_hitrate << "%";
	cout << "\nStore Hit Rate = " << setprecision(4) << final_stats.store_hitrate << "%";
	cout << "\nTotal Run Time = " << stats.cycles << " cycles";
	cout << "\nAverage Memory Access Latency = " << setprecision(3) << final_stats.AMAT<< "\n"; 

}

/**
 * Displays usage infomation when non sufficent input happens
 */
void display_help(){
	cout << "This program accepts command line arguments like so:\n";
	cout << "\tArgument 1: config file pathname: \"path/to/file\"\n";
	cout << "\tArgument 2: trace file pathname: \"path/to/file\"\n";
	cout << "\tEx: ./cachesim lab1_confs/confs/test.conf traces/test.trace\n";
}

int main(int argc, char *argv[]){

	if(argv == NULL || argc < 2){
		cout << "NON-SUFFICENT ARGUMENTS PROVIDED\n";
		display_help();
		exit(2);
	}

	config cacheinfo;
	result stats;
	calculations final_stats;

	char trace_name[50], config_name[50];

	strcpy(config_name, argv[1]);
	strcpy(trace_name, argv[2]);

	/*Parse the trace name and concatenate ".out" for output file*/
	if(output_to_file){
		char *cstr, outname;
		istringstream temp(trace_name);
		string temp2;
		while(temp.get(outname)){
			if(outname == '/'){
				while(temp.get(outname)){
					temp2 = temp2 + outname;
				}
			}
		}
		cstr = (char*)temp2.c_str();
		strcat(cstr, ".out");
		cout << "OUTPUT: " << cstr << "\n";
		freopen(cstr,"w",stdout);
	}
	/*--------------parse output file name done------------------*/

	cacheinfo = readin_config(cacheinfo, config_name);
	if(display_config){
		config_display(cacheinfo, trace_name, config_name);
	}
	stats = trace_process(cacheinfo, stats, trace_name);

	final_stats = get_calcuations(stats, final_stats, cacheinfo);
	display_results(stats, final_stats);

	return 0;
}