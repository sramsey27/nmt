Cache simulator
By Steven Ramsey

SUCCESSFULLY COMPILES ON ADA USING g++.

To install and run:
	The following folders MUST be present within the same directory as the compiled binary file
		1. traces (folder containing .traces files [needed for proper parsing for output file]).
		2. lab1_confs (or at least some sort of conf file to operate)
	
	To complile:
		In the terminal type: Make

	To Run:
		./cachesim <config file path> <traces/test.trace>
		NOTE: The trace file MUST be obtained from the "traces" directory for proper string parsing

	If issues compiling: try "make clean" then attempt to recompile. 

Description:
	An implementation of a cache simulator which supports no-write-allocate/write-through and
	write-allocate/write-back modes. Supported replacement policies: FIFO and random placement.
	Support for direct-mapped is also present. Chosen modes are based on configuration files
	which also contains various cache infomation such as Associativity, block size, etc

	This program will outout a file with a name based off the input trace file 
	(of the format: "test.trace.out") with the following calculated infomation:
	
		Load Hit Rate  (in %)
		Store Hit Rate (in %)
		Total Run Time (in cycles)
		Average Memory Access Latency (AMAT)

Capabilities:
	This flexiability of this program allows for the following features to be enabled/disabled:

	NOTE: These can be found within the header file source code. Changes will take effect after
	compiling again.

	--> debugmode: Show/limit some functionality which was useful for debugging: 
			true = enabled, false = disabled (DEFAULT is false/disabled)

	--> display hits: output number of hits/misses (loads or stores) and cycles to stdout
			true = enabled, false = disabled (DEFAULT is false/disabled)

	--> display config: output the interpreted cache infomation brought in from the config file to stdout
			true = enabled, false = disabled (DEFAULT is false/disabled)

	--> output to file: output stdout to a .out file based on the name of the trace file
			true = enabled --> to file, false = disabled --> to stdout terminal (DEFAULT is true/enabled)

	
