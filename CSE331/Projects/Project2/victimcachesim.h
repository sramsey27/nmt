/**
 * filename: victimcachesim.h
 *
 * description: The header file for victimcache.cpp
 *
 * author: Steven Ramsey
 *
 * class: 		CSE331
 * instructor:	Zheng
 * assignment:	Lab Project #2
 * 
 * assigned: 	Oct 31, 2016
 * due:			Nov 14, 2016
 */

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <cstring>
#include <sstream>
#include <vector>
#include <math.h>
#include <algorithm>
#include <list>
#include <climits>
#include <iomanip>
#include <cstdio>
#include <unistd.h>

/*Additional features*/
bool debugmode = false; /*Show infomation helpful for debugging, 0 -> off, 1 -> on*/
bool display_hits = false; /*flag to display number of hits along with calculations*/
bool display_config = true; /*Display configuration infomation*/
bool output_to_file = true; /*Output to file when true, out to terminal stdout when false*/
bool process_trace = true; /*Process infomation from trace file for caches*/

struct config{ /*Stores infomation of configuration file*/
	unsigned int blocksize;
	unsigned int victim_entries;
	unsigned int writemode; //default 1
	unsigned int datasize;
	unsigned int policy;
	unsigned int misspenalty;
	unsigned int allocatemode;
};

struct cachestats{
	unsigned int main_size;
	unsigned int victim_size;
	unsigned int main_setcount;
	unsigned int victim_setcount;
	unsigned int offset_size;
	unsigned int index_size;
	unsigned int tag_size;
};

struct result{ /*Keeps track of hits/misses/cycles etc...*/
	unsigned int total_load_hits;
	unsigned int total_store_hits;
	unsigned int total_load_misses;
	unsigned int total_store_misses;
	unsigned int total_misses;
	unsigned int cycles;
	unsigned int memory_access;
};

struct calculations{ /*Final calculations, may be removed*/
	double total_hitrate;
	double load_hitrate;
	double store_hitrate;
	double runtime_cycles;
	double AMAT;
};

/*Cache Architecture*/
struct block_t{
	bool valid;
	bool dirty;
	unsigned int tag;
	int time_accessed;
	int time_stored;
};

struct cacheset_t{
	int time_stamp;
	block_t *blocks;
}; 

/*Prototypes*/
result trace_process(config cacheinfo, result stats, char trace_name[]);
void config_display(config cacheinfo, char trace_name[]);
void display_results(result stats, calculations final_stats, config cacheinfo);
calculations get_calcuations(struct result cacheinfo ,struct calculations final_stats);
bool readcache(unsigned long int mem_access, unsigned int set_count, cacheset_t *cur_cache);
bool writebackcache(unsigned long int mem_access, unsigned int set_count, cacheset_t *cur_cache);
bool writetocache(unsigned long int mem_access, unsigned int set_count, cacheset_t *cur_cache, bool policy, config cacheinfo);
bool swapcaches(cacheset_t *cache1, cacheset_t *cache2, unsigned long int mem_access);
config store_input(config cacheinfo, char *argv[]);