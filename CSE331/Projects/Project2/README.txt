Victim Cache simulator
By Steven Ramsey

SUCCESSFULLY COMPILES ON ADA USING g++ 4.7.

To install and run:

	The following folders MUST be present within the same directory as the compiled binary file
		1. traces (folder containing .traces files [needed for proper parsing for output file]).
	
	To complile:
		In the terminal type: make all

	To Run:
		./victimcachesim <traces/test.trace> <block size> <# of victim entries> <Replacement Policy> <Allocate mode>

		Where:
			<block size> is in bytes (an int), <# of victim entries> (int), <Replacement Policy> is 0, 1, 2, <Allocate mode> is 0, 1

			NOTE: <Replacement policy> 0 --> Random placement, 1 --> FIFO, 2 --> LRU
				  <Allocate Mode> 0 --> No-Write-Allocate, 1 --> Write-Allocate 

		NOTE: The trace file MUST be obtained from the "traces" directory for proper string parsing

	If issues compiling: try "make clean" then attempt to recompile. 

Description:
	An implementation of a cache simulator which supports no-write-allocate/write-allocate write-back modes. 
	Supported replacement policies: Random placement, FIFO, and LRU.
	Scrictly direct-mapped main cache.
	Support for user input of: block size (in bytes), number of victim entries, replacement policy mode, and write allocate mode.
	Supported allocate modes: No-Write-Allocate and Write-Allocate

	This program will outout a file with a name based off the input trace file 
	(of the format: "test.trace.out") with the following calculated infomation:
	
		Load Hit Rate  (in %)
		Store Hit Rate (in %)
		Total Run Time (in cycles)
		Average Memory Access Latency (AMAT)

Capabilities:
	This flexiability of this program allows for the following features to be enabled/disabled:

	NOTE: These can be found within the header file source code. Changes will take effect after
	compiling again.

	--> debugmode: Show/limit some functionality which was useful for debugging: 
			true = enabled, false = disabled (DEFAULT)

	--> display hits: output number of hits/misses (loads or stores) and cycles to stdout
			true = enabled, false = disabled (DEFAULT)

	--> display config: output the interpreted cache infomation brought in from the config file to stdout
			true = enabled (DEFAULT), false = disabled

	--> output to file: output stdout to a .out file based on the name of the trace file
			true = enabled --> to file (DEFAULT), false = disabled --> to stdout terminal

	--> process_trace: Ability to enable/disable processing of the trace file and cache operations
			true = enabled (DEFAULT), false = disabled 

	
