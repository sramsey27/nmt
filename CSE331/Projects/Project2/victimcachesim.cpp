/**
 * filename: victimcachesim.cpp
 *
 * description: An implementation of a direct-mapped cache simulator which supports a victim cache
 * 				including the following replacement policies: Random Placement, FIFO, and LRU.
 *
 * author: Steven Ramsey
 *
 * class: 		CSE331
 * instructor:	Zheng
 * assignment:	Lab Project #2
 * 
 * assigned: 	Oct 31, 2016
 * due:			Nov 14, 2016
 */

/* Changes
 * 1. Removed the config file read in as it is not needed anymore
 * 2. Adding checking for input files
 * 3. Fixed bugs with FIFO and LRU
 */

using namespace std;
#include "victimcachesim.h"
struct cacheset_t *maincache;
struct cacheset_t *victimcache;
struct cachestats info;
bool victim_on = true; /*didn't work with 0 entries orginally, this fixed that*/

/**
 * This is where the actual simulation occurs, the creation of the cache and placement algorithms
 * happen within this function.
 * @param cacheinfo structure holding the cache infomation brought in from config file
 * @param stats structure holding number of hits/misses and cycles, populated here in this function.
 * @param trace_name the path of the trace file for processing
 * @return the stats/results structure with hit/miss/cycle infomation
 */
result trace_process(config cacheinfo, result stats, char trace_name[]) {

	unsigned long int mem_access = 0, main_setIndex = 0, instruction_count = 0;;

	/*Prevent garbage infomation*/ 
	stats.total_load_hits = 0;
	stats.total_store_hits = 0;
	stats.total_load_misses = 0;
	stats.total_store_misses = 0;
	stats.cycles = 0;
	stats.memory_access = 0;

	ifstream infile;
	infile.open(trace_name); /*Open trace file*/

	if(!infile){ /*Make sure the file opens before doing anything else*/
		cout << "Cannot open trace file: \"" << trace_name << "\" \n";
		exit(2);
	}

	info.main_setcount = (cacheinfo.datasize * 1000)/ cacheinfo.blocksize;	
	info.main_size = info.main_setcount * cacheinfo.blocksize; //changed the calcuation algorithm
	info.offset_size = log2(cacheinfo.blocksize);
	info.index_size = log2(info.main_size/(cacheinfo.blocksize * 1));
	info.tag_size = 32 - info.offset_size - info.index_size;
	if(cacheinfo.victim_entries == 0){
		info.victim_setcount = 0;
		victim_on = false;
	}
	else{
		info.victim_setcount = cacheinfo.victim_entries;
	}
	
	if(debugmode){/*Display calculated block infomation*/
		cout << "Depending on blocksize, set count will change --> cachesize/blocksize\n";
		cout << "Set count = " << info.main_setcount << " cachesize = " << info.main_size << " bytes \n";
		cout << "Index size = " << info.index_size << "\nTag size = " << info.tag_size << "\noffset size = " << info.offset_size << "\n";
		cout << "Total Address Sum: " << info.index_size+info.tag_size+info.offset_size << " bits \n";
		//exit(5);
	}

	/*Create the caches*/
	maincache = new cacheset_t;
	maincache->blocks = new block_t[info.main_setcount];
	
	for(unsigned int i = 0; i < info.main_setcount; i++){
		//cout << "maincache set: " << i + 1 << " Initialized\n";
		maincache->blocks[i].valid = false;
		maincache->blocks[i].dirty = false;
		maincache->blocks[i].tag = 0;
		maincache->blocks[i].time_accessed = 0;
		maincache->blocks[i].time_stored = 0;
	}

	if(victim_on){ //only if there are entries for victim, else seg fault will occur

		victimcache = new cacheset_t;
		victimcache->blocks = new block_t[info.victim_setcount];

		for(unsigned int j = 0; j < info.victim_setcount; j++){
			//cout << "victimcache set: " << j + 1 << " Initialized\n";
			victimcache->blocks[j].valid = false;
			victimcache->blocks[j].dirty = false;
			victimcache->blocks[j].tag = 0;
			victimcache->blocks[j].time_accessed = 0;
			victimcache->blocks[j].time_stored = 0;
		}
	}
	

	if(!process_trace){
		cout << "\nProcess Trace is off, no cache processing will be done.\n";
		exit(666);
	}

	/*Start running through the trace file and do operations while doing so*/
	string mode, address, instru;

	cout << "Processing Trace...\n";

	while(infile){

		if(debugmode){
			usleep(200000); /*Slow down processing to see what is happening*/
		}

		main_setIndex = 0;
		stringstream tempstring, tempstring2;
		stats.cycles = stats.cycles + instruction_count;

		infile >> mode; //easier way to parse trace file
		infile >> address;
		infile >> instru;

		/*for a blank line, break the loop since there is nothing to parse*/
		if ((mode == "")){ 
			break;
		}

		tempstring << std::hex << address;
		tempstring >> mem_access;
		tempstring2 << instru;
		tempstring2 >> instruction_count;

		if(debugmode){
			cout << "Address: " << address << " converted to --> " << mem_access << "\n";
			cout << "Mode: " << mode << "\tInstruction count: " << instruction_count << "\n";
		}

		maincache->time_stamp = maincache->time_stamp++;
		if(victim_on){
			victimcache->time_stamp = victimcache->time_stamp++;
		}
		
		if(mode == "l"){ //LOAD
			if(readcache(mem_access, info.main_setcount, maincache)){ //True if found in maincache
				stats.total_load_hits++;
			}
			else if(readcache(mem_access, info.victim_setcount, victimcache) && victim_on){ //True if found in victimcache
				stats.total_load_hits++;
				swapcaches(maincache, victimcache, mem_access);
			}
			else{
				stats.total_load_misses++;
				stats.cycles = stats.cycles + cacheinfo.misspenalty;
				writetocache(mem_access, info.main_setcount, maincache, false, cacheinfo);
				writetocache(mem_access, info.victim_setcount, victimcache, true, cacheinfo);
			}
		}
		else if (mode == "s"){ //STORE
			if(writebackcache(mem_access, info.main_setcount, maincache)){
				stats.total_store_hits++;
			}
			else if(writebackcache(mem_access, info.victim_setcount, victimcache) && victim_on){
				stats.total_store_hits++;
				swapcaches(maincache, victimcache, mem_access);
			}
			else{
				stats.total_store_misses++;
				stats.cycles = stats.cycles + cacheinfo.misspenalty;

				if(cacheinfo.allocatemode == 1){ //For Write allocatte, if no-write do nothing
					writetocache(mem_access, info.main_setcount, maincache, false, cacheinfo);
					writetocache(mem_access, info.victim_setcount, victimcache, true, cacheinfo);
				}
			}
		}
		
		maincache->time_stamp = maincache->time_stamp + 2;
		if(victim_on){
			victimcache->time_stamp = victimcache->time_stamp + 2;
		}
	}

	/*Free up the cache so you can have your memory back :)*/
	free(maincache->blocks);
	free(maincache);

	if(victim_on){
		free(victimcache->blocks);
		free(victimcache);
	}

	infile.close();
	return stats;
}

/**
 * When a hit within the victim cache, this function is called to swap data between main cache and victim.
 * @param cache1 a cache of which to swap data with cache 2
 * @param cache2 a cache of which to swap data with cache 1
 * @param mem_access the unsigned long int representation of the memory address
 * @return true when finished
 */
bool swapcaches(cacheset_t *cache1, cacheset_t *cache2, unsigned long int mem_access){

	unsigned long int index1 = (mem_access >> info.offset_size) & (info.main_setcount - 1);
	unsigned long int index2 = (mem_access >> info.offset_size) & (info.victim_setcount - 1);

	unsigned int temptag = cache1->blocks[index1].tag;
	cache1->blocks[index1].time_accessed = cache1->time_stamp;
	cache1->blocks[index1].valid = true;
	cache1->blocks[index1].tag = cache2->blocks[index2].tag;

	cache2->blocks[index2].time_accessed = cache2->time_stamp;
	cache2->blocks[index2].valid = true;
	cache2->blocks[index2].tag = temptag;

	return true;
}

/**
 * Used for load: this function checks to see if an element is present in the cache
 * @param mem_access the unsigned long int representation of the memory address
 * @param set_count the number of sets within the passed in cache
 * @param cur_cache the current cache to work with (main cache or victim cache)
 * @return true for hit, false for a miss
 */
bool readcache(unsigned long int mem_access, unsigned int set_count, cacheset_t *cur_cache){

	if(cur_cache == NULL || set_count == 0){
		return false;
	}

	unsigned long int index = (mem_access >> info.offset_size) & (set_count - 1);
	unsigned long int tag = ((mem_access >> info.offset_size) >> info.index_size) & (info.tag_size - 1);

	if(cur_cache->blocks[index].tag == tag && cur_cache->blocks[index].valid){
		cur_cache->blocks[index].time_accessed = cur_cache->time_stamp;

		return true; //return true if found in the current cache
	}
	return false;
}

/**
 * This function will write to the passed in cache while applying replacement policies if requested
 * @param mem_access the unsigned long int representation of the memory address
 * @param set_count the number of sets within the passed in cache
 * @param cur_cache the current cache to work with (main cache or victim cache)
 * @param policy a flag used to determine if replacement policy is to be enforced
 * @param cacheinfo the structure holding the configurations for the caches
 * @return true for successfully written to
 */
bool writetocache(unsigned long int mem_access, unsigned int set_count, cacheset_t *cur_cache, bool policy, config cacheinfo){

	if(cur_cache == NULL || set_count == 0){
		return false;
	}

	unsigned long int index = (mem_access >> info.offset_size) & (set_count - 1);
	unsigned long int tag = ((mem_access >> info.offset_size) >> info.index_size) & (info.tag_size - 1);
	int min_time = INT_MAX; //FIFO;
	srand(time(NULL)); /*For random placement, random number generation*/

	if(policy){ //if policy is to be enforced, for victim cache
		if(cacheinfo.policy == 0){ //rand
			index = rand()%set_count;
		}
		else if(cacheinfo.policy == 1){ //FIFO

			for(unsigned int i = 0; i < set_count; i++){
				if(cur_cache->blocks[i].time_stored < min_time){
					min_time = cur_cache->blocks[i].time_stored;
					index = i;
				}
			}
		}
		else if(cacheinfo.policy == 2){ //LRU
			//min_time = cur_cache->blocks[0].time_accessed;
			for(unsigned int i = 0; i < set_count; i++){
				if(cur_cache->blocks[i].time_accessed < min_time){
					min_time = cur_cache->blocks[i].time_accessed;
					index = i;
				}
			}	
		}
	}
		
	cur_cache->blocks[index].tag = tag;
	cur_cache->blocks[index].valid = true;
	cur_cache->blocks[index].time_accessed = cur_cache->time_stamp;
	cur_cache->blocks[index].time_stored = cur_cache->time_stamp;
	
	return true;
}

/**
 * This function will write to the cache while setting the dirty bit if the element is present (hence the name writeback)
 * @param mem_access the unsigned long int representation of the memory address
 * @param set_count the number of sets within the passed in cache
 * @param cur_cache the current cache to work with (main cache or victim cache)
 * @return true for found and written to, false for a miss
 */
bool writebackcache(unsigned long int mem_access, unsigned int set_count, cacheset_t *cur_cache){

	if(cur_cache == NULL || set_count == 0){
		return false;
	}

	unsigned long int index = (mem_access >> info.offset_size) & (set_count - 1);
	unsigned long int tag = ((mem_access >> info.offset_size) >> info.index_size) & (info.tag_size - 1);

	if(cur_cache->blocks[index].tag == tag && cur_cache->blocks[index].valid){

		cur_cache->blocks[index].tag = tag;
		cur_cache->blocks[index].valid = true;
		cur_cache->blocks[index].dirty = true;
		cur_cache->blocks[index].time_accessed = cur_cache->time_stamp;
		cur_cache->blocks[index].time_stored = cur_cache->time_stamp;

		return true; //return true if found in the current cache
	}
	return false;
}

/**
 * Calculate the total hitrate, load hitrate, store hitrate, and Average Memory Access Time
 * @param stats structure holding the results gathered from the simulation
 * @param final_stats the structure for holding the calcuated results from this function
 * @param cacheinfo the structure holding the cache configuration obtained from config file
 * @return the populated calcuations structure
 */
calculations get_calcuations(struct result stats ,struct calculations final_stats, config cacheinfo){

	final_stats.total_hitrate = 0;
	final_stats.load_hitrate = 0;
	final_stats.store_hitrate = 0;
	final_stats.runtime_cycles = 0;
	final_stats.AMAT = 0;

	double totalhits = (stats.total_store_hits + stats.total_load_hits + stats.total_store_misses + stats.total_load_misses); 
	double hits = (double) (stats.total_load_hits + stats.total_store_hits);
	double misses = (double) (stats.total_load_misses + stats.total_load_misses);
	double temp = (double) ((hits * 1.0) + (misses * cacheinfo.misspenalty));

	final_stats.total_hitrate = (double)((stats.total_store_hits + stats.total_load_hits) * 100 / totalhits);
	final_stats.load_hitrate = (double)(stats.total_load_hits * 100)/(stats.total_load_hits + stats.total_load_misses);
	final_stats.store_hitrate = (double)(stats.total_store_hits * 100)/(stats.total_store_hits + stats.total_store_misses);
	final_stats.runtime_cycles = stats.cycles;
	final_stats.AMAT = (double) temp *1.0/(hits + misses);

	return final_stats;	
}

/**
 * Displays the cache configuration
 * @param cacheinfo the structure holding the cache infomation
 * @param trace_name the trace file pathname
 * @param config_name the config/conf pathname
 */
void config_display(config cacheinfo, char trace_name[]){

		cout << "Trace Filename: \"" << trace_name << "\"\n";
		cout << "\nConfiguration......\n---------------------------\n";

		if ((cacheinfo.blocksize % 2) == 0) {
			cout << "Block size = " << cacheinfo.blocksize << " byte Blocks\n";
		}
		else {
			cout << "Line Size --> INCONSISTENT LINE SIZE\n";
		}
		cout << "Number of Victim Entries = " << cacheinfo.victim_entries;
		if(cacheinfo.victim_entries == 0){
			cout << " --> Victim cache off.\n";
		}
		else{
			cout << "\n";
		}
		if (cacheinfo.writemode == 1){
			cout << "Associativity --> \"Direct-Mapped\"\n";
		}
		else {
			cout << "Write Mode --> INVALID ASSOCIATIVITY\n";
		}
		cout << "Data_Size = " << cacheinfo.datasize << "KB Cache\n";
		if(cacheinfo.policy == 0){
			cout << "Replacement Policy --> \"Random Placement\"\n";
		}
		else if (cacheinfo.policy == 1){
			cout << "Replacement Policy --> \"FIFO (First-In-First-Out)\"\n";
		}
		else if (cacheinfo.policy == 2){
			cout << "Replacement Policy --> \"LRU (Least Recently Used)\"\n";
		}
		else {
			cout << "Replacement Policy --> INVALID POLICY\n";
		}
		cout << "Miss penalty = " << cacheinfo.misspenalty << " cycles\n";
		if(cacheinfo.allocatemode == 1){
			cout << "Write Mode --> \"Write-Allocate/write-back\"\n";
		}
		else if (cacheinfo.allocatemode == 0){
			cout << "Write Mode --> \"No-Write-Allocate/write-back\"\n";
		}
		else {
			cout << "Write Mode --> INVALID ALLOCATE MODE\n";
		}
		cout << "---------------------------\n\n";
}

/**
 * Displays the hit/miss/cycle results (if enabled) along with the final calcuations
 * @param stats the structure golding the hit/miss/cycles results from the simulation
 * @param final_stats the structure holding the final calculations based on stats
 */
void display_results(result stats, calculations final_stats){

	if(display_hits){ /*Present in header file*/
		cout << "\nCache Results.....\n---------------------------\n";
		cout << "Total Load Hits = " << stats.total_load_hits << "\n";
		cout << "Total Store Hits = " << stats.total_store_hits <<"\n";
		cout << "Total Load Misses = " << stats.total_load_misses << "\n";
		cout << "Total Store Misses = " << stats.total_store_misses << "\n";
		cout << "Total Cycles = " << stats.cycles << "\n\n";
		cout << "TOTAL HITS: " << stats.total_load_hits + stats.total_store_hits << "\n";
		cout << "TOTAL MISSES: " << stats.total_store_misses + stats.total_load_misses << "\n\n";
	}
	cout << "Total Hit Rate = " << setprecision(4) << final_stats.total_hitrate << "%";
	cout << "\nLoad Hit Rate = " << setprecision(4) << final_stats.load_hitrate << "%";
	cout << "\nStore Hit Rate = " << setprecision(4) << final_stats.store_hitrate << "%";
	//cout << "\nTotal Run Time = " << stats.cycles << " cycles";
	cout << "\nAverage Memory Access Latency = " << setprecision(3) << final_stats.AMAT<< "\n"; 

}

/**
 * Displays usage infomation when non sufficent input happens
 */
void display_help(){
	cout << "This program accepts command line arguments like so:\n\n";
	cout << "\tArgument 1: trace file pathname: \"path/to/file\"\n\n";
	cout << "\t\tNOTE: For proper parsing of trace name, please use pathname like so --> \"traces/somename.trace\"\n\n";
	cout << "\tArgument 2: block size (2-32) only integers divisible by 2\n";
	cout << "\tArgument 3: # of Victim Cache Entries (Must be > 0)\n";
	cout << "\tArgument 4: Replacement Policy: \"0\" for Random Placement, \"1\" for FIFO and \"2\" for LRU\n";
	cout << "\tArgument 5: Allocate mode: \"0\" for no-write-allocate \"1\" for write-allocate\n\n";
	cout << "\t\tNOTE: Write-Back is native in this program, there is no write-through\n\n"; 
	cout << "\tEx: ./victimcache traces/test.trace 8 15 0 0\n";
	cout << "\n\tThis would run a L1 (32 KB, direct-mapped, 8-byte block size), victim (15 entries, 8-byte block size) \n";
	cout << "\twith random replacement policy, no-write-allocate.\n\n";
}

/**
 * This function simply stores user input infomation into the cache config structure for later use
 * @param cacheinfo the config structure holding cache configuration infomation
 * @param argv the user input char array
 * @return the populated config structure
 */
config store_input(config cacheinfo, char *argv[]){

	unsigned long int block = 0, victim = 0, policy = 0, allocate = 0; 

	block = atoi(argv[2]);
	victim = atoi(argv[3]);
	policy = atoi(argv[4]);
	allocate = atoi(argv[5]);

	if(!((block % 2) == 0 || (block == 1)) || (block == 0) || (block > 3200)){
		cout << "NOT A VALID BLOCK SIZE INPUT! MUST BE DIVISIBLE BY 2 OR EQUAL TO 1 OR NOT GREATER THAN 3200 bits! TRY AGAIN.\n";
		display_help();
		exit(2);
	}
	if(!(policy == 0 || policy == 1 || policy == 2)){
		cout << "NOT A VALID POLICY INPUT! MUST BE 0,1, or 2! TRY AGAIN.\n";
		display_help();
		exit(2);
	}
	if(!(allocate == 0 || allocate == 1)){
		cout << "NOT A VALID ALLOCATE MODE! MUST BE 0 or 1! TRY AGAIN.\n";
		display_help();
		exit(2);
	}
	//if(victim < 1){
//		cout << "THERE MUST BE AT LEAST 1 ENTRY IN THE VICTIM CACHE! TRY AGAIN!\n";
//		display_help();
//		exit(2);
//	}
	cacheinfo.blocksize = block;
	cacheinfo.victim_entries = victim;
	cacheinfo.policy = policy;
	cacheinfo.allocatemode = allocate;
	cacheinfo.writemode = 1; //direct-mapped
	cacheinfo.datasize = 32; //fixed 32kb L1 cache
	cacheinfo.misspenalty = 100; //assume to be 100 from pdf

	return cacheinfo;
}

int main(int argc, char *argv[]){

	if(argv == NULL || argc < 5){
		cout << "NON-SUFFICENT ARGUMENTS PROVIDED\n";
		display_help();
		exit(2);
	}
	if(argv[0] == NULL || argv[1] == NULL || argv[2] == NULL || argv[3] == NULL || argv[4] == NULL || argv[5] == NULL){
		cout << "NON-SUFFICENT ARGUMENTS PROVIDED\n";
		display_help();
		exit(2);
	}

	char trace_name[50];

	config cacheinfo;
	result stats;
	calculations final_stats;

	strcpy(trace_name, argv[1]); //simply copy over trace file name.
	cacheinfo = store_input(cacheinfo, argv);

	if(display_config){
		config_display(cacheinfo, trace_name);
	}

	/*Process the Trace File*/
	stats = trace_process(cacheinfo, stats, trace_name);
	final_stats = get_calcuations(stats, final_stats, cacheinfo);

	/*Parse the trace name and concatenate ".out" for output file*/
	if(output_to_file){
		char *cstr, outname;
		istringstream temp(trace_name);
		string temp2;
		while(temp.get(outname)){
			if(outname == '/'){
				while(temp.get(outname)){
					temp2 = temp2 + outname;
				}
			}
		}
		cstr = (char*)temp2.c_str();
		strcat(cstr, ".out");
		cout << "OUTPUT FILE: " << cstr << "\n";
		freopen(cstr,"w",stdout);
	}
	/*--------------parse output file name done------------------*/

	display_results(stats, final_stats);

	return 0;
}