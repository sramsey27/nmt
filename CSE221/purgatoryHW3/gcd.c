#include <stdio.h>

int gcd(int a, int b);

int main()
{

	int a = 50, b = 20, answer = 0;
	answer = gcd(a, b);
	
	printf("%d\n", answer);
  	return 0;
}

int gcd(int a, int b)
{
	while (a != b)
    {
        if (a > b)
        {
            return gcd(a - b, b);
        }
        else
        {
            return gcd(a, b - a);
        }
    }
    return a;

}