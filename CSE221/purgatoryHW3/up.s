.section .data

# all lowercase characters are translated to uppercase using xlat command 
# upcase and justupper define a new 'ASCII' table
upcase:
        .byte 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x09, 0x0A, 0x20, 0x20, 0x20, 0x20, 0x20
        .byte 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20
        .byte 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2A, 0x2B, 0x2C, 0x2D, 0x2E, 0x2F
        .byte 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3D, 0x3E, 0x3F
        .byte 0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4A, 0x4B, 0x4C, 0x4D, 0x4E, 0x4F
        .byte 0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5A, 0x5B, 0x5C, 0x5D, 0x5E, 0x5F
        .byte 0x60, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4A, 0x4B, 0x4C, 0x4D, 0x4E, 0x4F
        .byte 0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5A, 0x7B, 0x7C, 0x7D, 0x7E, 0x20

# translate all characters except upper case characters and the newline and tab to a space        
justupper:
        .byte 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x09, 0x0A, 0x20, 0x20, 0x20, 0x20, 0x20
        .byte 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20
        .byte 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20
        .byte 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20
        .byte 0x20, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4A, 0x4B, 0x4C, 0x4D, 0x4E, 0x4F
        .byte 0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5A, 0x20, 0x20, 0x20, 0x20, 0x20
        .byte 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20
        .byte 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20

.section .bss
       .comm buf, 4096

.section .text
.globl _start
_start:

read:
        mov $3, %eax # system read
        mov $0, %ebx # stdin
        mov $buf, %ecx
        mov $4096, %edx
        int $0x80

        movl %eax, %edi # store reads return value
        cmpl $0x0, %eax
        je fini

# load translation table
       movl $upcase, %ebx
#       movl $justupper, %ebx

# get buf
        movl $buf, %edx
        movl %edi, %ecx  # number of bytes read into buf

translate:
        xor %eax, %eax
        movb (%edx, %ecx), %al
        xlat
        movb %al, (%edx, %ecx)
        decl %ecx
        jnz translate

        xor %eax, %eax
        movb (%edx, %ecx), %al
        xlat
        movb %al, (%edx, %ecx)
 

write:
        movl $4, %eax 
        movl $1, %ebx # stdout
        movl $buf, %ecx
        movl %edi, %edx
        int $0x80
        jmp read

fini:        
       mov $1, %eax
       mov $0, %ebx
       int $0x80  
