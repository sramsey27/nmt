.section .data

print_output:
	.asciz "%d\n"

.section .bss
	.comm buf, 4096
	#.comm stack, 400000000

.section .text
.globl _start
_start:

main:

	pushq	%rbp	#base pointer
	movq	%rsp, %rbp

	movq	$10, %r14
	#addq	$1, %r14 	# n value
	movq	%r14, %r12
	leaq	(,%r14, 4), %r15 #open up space for stack, 8 * n bytes
	andq	$-16, %r15
	movq	%r14, %rbx

	subq	%r15, %rsp

	#setup for array, %r13 memory incrementor, %r14 value to be stored in array
	#rsp is the base pointer for the stack.

	movq	$0, (%rsp) #first two numbers are not prime
	movq	$0,	4(%rsp)
	movq	$2, %r13
	jmp		array

array:
	cmpq	%r14, %r13
	je		print_setup

	movq	$0, (%rsp, %r13, 4) 	#mov 1 into %r13*4(%rsp)
	incq	%r13	#increment for memory spot (stack index per say)

	jmp		array	#repeat the loop between 

setup:
	movq	$2, %r15 #p
	movq	$2, %r11 #index
	#jmp		algo1

algo1:
	#main loop
	cmpq	%rbx, %r11 #%14 = n, $r11 current index
	jge		print_setup	#if greater than or equal, proceed to print array

	#if
	movq	(%rsp, %r11, 4), %r10 #move current array index in %r10
	cmpq	$1, %r10 # if index value = 1 go to next index as it is already declared not prime
	je		algo2 #increment index

	imulq 	%r15, %r11 #index = index * p

	cmpq	$1, (%rsp, %r11, 4) #check if index value is 0
	je 		algo2	#increment p restart loop

	cmpq	%rbx, %r15 #i < n
	jge		algo3 #increment index

	cmpq	%rbx, %r11
	jge		algo3 #jump to increment p

	cmpq	$1, (%rsp, %r11, 4)
	je 		algo4

algo4:
	movq	$1, (%rsp, %r11, 4)
	addq	%r15, %r11
	jmp 	algo3

algo2:
	incq	%r15
	jmp 	algo1
	
algo3:
	incq	%r11
	movq	%r11, %r15
	jmp		algo1

print_setup:
	movq 	$0, %r13 #set up incrementers
	movq	$0, %r15
	jmp print

print:
	cmpq	%r14, %r13
	je		fini

	movq	(%rsp, %r13, 4), %r10
	#cmpq	$0, %r10
	#je		print_index

	xorq	%rax, %rax 				#0 rax
	movq 	%r10, %rsi 	#print this register
	movq	$print_output, %rdi
	call 	printf

	incq	%r13
	incq	%r15

	jmp 	print

print_index:
	movq	%r15, %rsi
	#movq	$4, %rbx
	xorq	%rax, %rax
	movq	$print_output, %rdi
	call 	printf
	incq	%r15
	incq	%r13
	jmp print

fini:

	mov 	$1, %eax
    mov 	$0, %ebx
    int 	$0x80  
