.section .data

print_output:
	.asciz "%d\n"

.section .bss
	
	.comm stack, 400000000

.section .text
.globl _start
_start:

main:
	movq	$stack, %r8

	#and $-16, %rax <------ THIS LINE IS EVIL!!

	movq	$100000000, %r14 # limit number
	#leaq	(,%r14, 4), %r15 #open up space for stack, 4 * n bytes

	#addq	%r15, %r8

	#array logic
	movq	$0, (%r8) #first two numbers are not prime
	movq	$0,	4(%r8)
	movq	$2, %r13
	jmp		array

array:
	cmpq	%r14, %r13 #%r14 == n
	je		algo_setup

	imulq	$2, %r13 #2i + 1
	addq	$1, %r13

	movq	%r13, (%r8, %r13, 4) 	#mov 1 into %r13*4(%r8)
	incq	%r13	#increment for memory spot (stack index per say)

	jmp		array	#repeat the loop between 	

algo_setup:
	movq	$3, %r10 # == i
	movq	$0, %r12

algo1:
	movq	%r10, %r9
	cmpq	%r14, %r9 	#i < limit, %14 = limit
	je		print_setup #escape this shit if i == limit

	xorq 	%r12, %r12
	movq	(%rsp, %r10, 4), %r12 #primes[i]

	cmpq	$0, %r12 # primes[i] != 0
	jne		algo2 #jump to inner while

	#this coninutes to increment	
	increment:
		incq	%r10
		jmp		algo1

	algo2:
		movq	%r10, %r11 #j == i

	algo3: # while ((j * i) < limit)
		mov 	%r11, %r9 #tmp == j
		imul	%r10, %r9 #tmp = i * tmp
		cmp 	%r14, %r9 #while(i*j < limit)
		jge		increment #escape to increment if not satisifed

		movl 	$0, (%rsp, %r9, 4)

		inc 	%r11
		jmp 	algo3

print_setup:
	movq 	$0, %r13 #set up incrementers
	movq	$0, %r15

print:
	movl	(%rsp, %r15, 4), %ebx
	cmpl	$0, %ebx
	jne 	print_prime
	
	cmpq 	%r13, %r14
	je		fini

	incq	%r15
	incq	%r13

	jmp 	print

print_prime:
	xorq	%rax, %rax
	movq	$print_output, %rdi
	movl	%ebx , %esi
	call 	printf

	incq	%r15
	incq	%r13
	jmp print

fini:

	mov 	$1, %eax
    mov 	$0, %ebx
    int 	$0x80  
