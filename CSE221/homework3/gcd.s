.section .data

print_output:
	.asciz "The greatest common denominator is %d\n"

.section .bss
.section .text
.globl _start
_start:

main:
	pushq	%rbp #boilerplate
	movq	%rsp, %rbp
	subq	$16, %rsp

	movl	$8, -12(%rbp) #a
	movl	$6, -8(%rbp) #b
	movl	$0, -4(%rbp) # answer

	movl	-8(%rbp), %esi #parameters
	movl	-12(%rbp), %edi

	call	gcd

	movl	%eax, -4(%rbp) #move result into answer
	movl	-4(%rbp), %esi
	movl	$print_output, %edi
	xorl	%eax, %eax

	call	printf #print result

	jmp fini #end it all

gcd:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$16, %rsp

	movl	%edi, -4(%rbp) #keep stack logic, move parameters in stack
	movl	%esi, -8(%rbp) #move b on stack

	movl	-4(%rbp), %eax #cmp complains when using too many memory references
	cmpl	-8(%rbp), %eax
	je	abequal

	movl	-4(%rbp), %eax
	cmpl	-8(%rbp), %eax #a > b test
	jle	a_greater
	jmp else_case

	a_greater: #a > b case gcd(a-b, b)
		movl	-8(%rbp), %edx #sub complains if too many memory references
		subl	-4(%rbp), %edx 
		movl	-4(%rbp), %eax
		movl	%edx, %esi
		movl	%eax, %edi
		call	gcd
		jmp	to_leave

	else_case: #else case
		movl	-8(%rbp), %eax
		movl	-4(%rbp), %edx
		subl	%eax, %edx #sub complains if too many memory references
		movl	-8(%rbp), %eax
		movl	%eax, %esi
		movl	%edx, %edi
		call	gcd
		jmp to_leave

	abequal: #a == b case, just move a into %eax, call it good
		movl	-4(%rbp), %eax

	to_leave: #return call
		leave
		ret

fini: #terminate program, prologue
	mov 	$1, %eax
    mov 	$0, %ebx
    int 	$0x80 
