/**
 * @file hex2dec.c
 * 
 * @brief contains a hex to decimal conversion program
 *
 * @details As the title shows; this program has the ability to convert
 *          hex representation to decimal.
 *
 * @author Steven Ramsey
 *
 * @date 8/25/15
 *  
 * @todo nothing
 *
 * @bug none
 *
 * @remark Try it out :)
 * 
 */

#include <stdio.h>
#include <errno.h>
#include <stdlib.h> 

long hex2dec(char *argv[], int i, int check);

#define EXIT 27
#define LEN 256

/**
 * Utitlizes strtol to convert hex to decimal
 * @param argv command line argument input
 * @param i current position (argument)
 * @param check a check for 0x
 */
long hex2dec(char *argv[], int i, int check){
	if(check == 0){
		return (long)strtol(argv[i], NULL, 16);
	}
	else{
		return (long)strtol(argv[i], NULL, 0);
	}
	return 0;
}

int main(int argc, char *argv[])
{
	if (argv[1] == NULL){ /*Produce usage if no input*/
		printf("Usage: hex2dec hexnum [hexnum2] . . .\n");
		exit(EXIT);
	}

	int i = 0, check = 0; long result = 0; char error[LEN]; /*No hating on the one liners*/

	for(i = 1; i < argc; i++, check = 0){
		if(argv[i][2] == 'x'){ /*Check for 0x*/
			check = 1;
		}

		result = hex2dec(argv, i, check);

		if(errno){ /*Produce error message if needed*/
			sprintf(error, "Error converting %s to a decimal value", argv[i]);
			perror(error);
			continue; /*Skip the error*/
		}

		printf("%ld\n", result);;
	}
	return 0;
}