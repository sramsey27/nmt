/**
 * @file dec2hex.c
 * 
 * @brief contains a decimal to hex conversion program
 *
 * @details As the title shows; this program has the ability to convert
 *          decimal representation to hex.
 *
 * @author Steven Ramsey
 *
 * @date 8/25/15
 *  
 * @todo nothing
 *
 * @bug none
 *
 * @remark Try it out.... again :)
 * 
 */

#include <stdio.h>
#include <errno.h>
#include <stdlib.h> 

long hex2dec(char *argv[], int i);
#define EXIT 27
#define LEN 256

/**
 * Utitlizes strtol to convert decimal to hex
 * @param argv command line argument input
 * @param i current position (argument)
 */
long hex2dec(char *argv[], int i)
{
	return (long)strtol(argv[i], NULL, 10);
}

int main(int argc, char *argv[])
{
	if (argv[1] == NULL){ /*Produce usage if no input*/
		printf("Usage: dec2hex num1 [num2] . . .\n");
		exit(EXIT);
	}

	int i = 0; long result = 0; char error[LEN]; /*No hating on the one liners*/

	for(i = 1; i < argc; i++){

		result = hex2dec(argv, i);

		if(errno){ /*Produce error message if needed*/
			sprintf(error, "Error converting %s to a hexadecimal value", argv[i]);
			perror(error);
			continue; /*Skip the error*/
		}

		printf("0x%lX\n", result); /*Converts long to hex*/
	}
	return 0;
}