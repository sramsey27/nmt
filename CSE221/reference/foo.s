.section .data
answer1:
	.asciz "The answer is %d\n"
answer2:
	.asciz "No, the answer is %d\n"
.section .text
.globl foo

foo:
#Prologue
	pushq	%rbp	#base pointer
	movq	%rsp, %rbp

	#parameters
	movl	%edi,-20(%rbp) #=a
	movl	%esi,-24(%rbp) #=b
	movl	%edx,-28(%rbp) #=c

	#local variables
	movl	$29, -32(%rbp)	#=x
	movl	$50, -36(%rbp)	#=y

	movq	-24(%rbp), %rax #follow order of operations
	imulq	-28(%rbp), %rax
	movq 	%rax, %rdx

	movq	-20(%rbp), %rax
	addq	%rdx, %rax

	subq	-32(%rbp), %rax

	#movq	%rax, -40(%rbp)

	movq	%rax, %rdi
	divq	%rdi

	movq	%rdi, %rax

#Epologue
	popq	%rbp
	ret

.globl _start
_start:

#main
	#prologue
	pushq	%rbp	#base pointer
	movq	%rsp, %rbp

	movq	$10, -4(%rbp)
	movq	$20, -8(%rbp)
	movq	$3,	-12(%rbp)
	movq	$0,	-16(%rbp)

	subq	$16, %rsp #stack pointer (shown in prologue), goes back $16 bytes

	movq	-4(%rbp), %rdi
	movq	-8(%rbp), %rsi
	movq	-12(%rbp), %rdx

	call foo #foo returns a value to %rax

	#movq	%rax, -16(%rbp)
	#movq	-16(%rbp), %rax

	movq 	%rax, %rsi #out expects input to be %rdi
	movq 	$answer1, %rdi #printf always expects %rdi to be the string

	xorq	%rax, %rax #printf segfaults if something is in %rax, xor itself will become 0

	call printf

	#xorq	%rdi, %rdi
	addq	$1, %rsi 
	#movq	%rsi, %rax
	movq	$answer2, %rdi
	xorq	%rax, %rax

	call printf 

	movq $0, %rdi
	call exit
