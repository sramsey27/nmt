.section .data
before:
        .asciz "before %s, the value is %d\n"
after:
        .asciz "after %s, the value is %d\n"
before_hex:
        .asciz "before %s, the value is %#.8x\n"
after_hex:
        .asciz "after %s, the value is %#.8x\n"
increment:
        .asciz "increment"
decrement:
        .asciz "decrement"
negate:
        .asciz "negate"
complement:
        .asciz "complement"
addition:
        .asciz "addition"
subtraction:
        .asciz "subtraction"
# two operand printf
before2:
        .asciz "before %s, src is %d and dest is %d\n"
after2:
        .asciz "after %s, src is %d and dest is %d\n"

.section .bss

.section .text
.globl _start
_start:
        #setup stack
        subq $8, %rsp
        movq $0x10, 8(%rsp)

        # functions that return integers use %rax as the return value

# increment
        # function call setup
        movq 8(%rsp), %rdx
        movq $increment, %rsi
        movq $before, %rdi
        movq $0, %rax
        call printf  # this returns a int -- number of chars printed, stored in %rax
       
        incq 8(%rsp)

        # function call setup
        movq 8(%rsp), %rdx
        movq $decrement, %rsi
        movq $after, %rdi
        movq $0, %rax
        call printf 

#decrement        
#movq 8(%rsp), %rax

        # function call setup
        movq 8(%rsp), %rdx
        movq $decrement, %rsi
        movq $before, %rdi
        movq $0, %rax
        call printf 
       
        decq 8(%rsp)

        # function call setup
        movq 8(%rsp), %rdx
        movq $increment, %rsi
        movq $after, %rdi
        movq $0, %rax
        call printf 

## negate
        # function call setup
        movq 8(%rsp), %rdx
        movq $negate, %rsi
        movq $before, %rdi
        movq $0, %rax
        call printf
       
        negq 8(%rsp)

        # function call setup
        movq 8(%rsp), %rdx
        movq $negate, %rsi
        movq $after, %rdi
        movq $0, %rax
        call printf 

## not - one's complement negation bitwise not
        # function call setup
        movq 8(%rsp), %rdx
        movq $complement, %rsi
        movq $before_hex, %rdi
        movq $0, %rax
        call printf 
       
        notq 8(%rsp)

        # function call setup
        movq 8(%rsp), %rdx
        movq $complement, %rsi
        movq $after_hex, %rdi
        movq $0, %rax
        call printf 

## two operand instruction, will use 5 and 10 for all ops
## add
        movq $0xa, 8(%rsp)
        movq $0x5, %rbx 

        # function call setup
        movq 8(%rsp), %rcx
        movq %rbx, %rdx
        movq $addition, %rsi
        movq $before2, %rdi
        movq $0, %rax
        call printf 
       
        addq %rbx, 8(%rsp)

        # function call setup
        movq 8(%rsp), %rcx
        movq %rbx, %rdx
        movq $addition, %rsi
        movq $after2, %rdi
        movq $0, %rax
        call printf 

# subtract
        #reset
        movq $0xa, 8(%rsp)
        movq $0x5, %rbx 

        # function call setup
        movq 8(%rsp), %rcx
        movq %rbx, %rdx
        movq $subtraction, %rsi
        movq $before2, %rdi
        movq $0, %rax
        call printf 
       
        subq %rbx, 8(%rsp)

        # function call setup
        movq 8(%rsp), %rcx
        movq %rbx, %rdx
        movq $subtraction, %rsi
        movq $after2, %rdi
        movq $0, %rax
        call printf 

# exit        
        movq $0, %rdi
        call exit


