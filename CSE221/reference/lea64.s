.section .data
in:
        .asciz "before: rax = %d\n"
in2:
        .asciz "before: rbx = %d\n"
out:
        .asciz "after: rax = %d\n"
out2:
        .asciz "after: rbx = %d\n"
.section .bss
.section .text
.globl _start
_start:
        pushq %rbp
        movq %rsp, %rbp
        
        subq $16, %rsp
        movq $10, 8(%rsp)
        movq 8(%rsp), %rax
       
# 6 + 10 = 18
        movq $in, %rdi        
        movq %rax, %rsi
        call print
        
        #reset rax
        movq 8(%rsp), %rax
        leaq 0x6(%rax), %rax # 6 + 10 = 16

        movq $out, %rdi        
        movq %rax, %rsi
        call print

# 5 * 10 = 60
        #reset rax
        movq 8(%rsp), %rax
        
        movq $in, %rdi        
        movq %rax, %rsi
        call print
        
        #reset rax
        movq 8(%rsp), %rax

        leaq (%rax, %rax, 4), %rax #5 * rax 
        
        movq $out, %rdi        
        movq %rax, %rsi
        call print

        
# say you want to multiply a number by 3
# 3x = x << (2 + 1)x = (x << 1) + x        
# one way

        movq $5, 16(%rsp)
        movq 16(%rsp), %rbx
       
        movq $in2, %rdi        
        movq %rbx, %rsi
        call print
        
 
#movq %rbx, %rdi
#        call before
        
        sal $1, %rbx 
        addq 16(%rsp), %rbx 
        
        movq $out2, %rdi        
        movq %rbx, %rsi
        call print

# lea is faster than shifts
        movq 16(%rsp), %rbx
        
        movq $in2, %rdi        
        movq %rbx, %rsi
        call print
        
        leaq (%rbx, %rbx, 2), %rbx
        
        movq $out2, %rdi        
        movq %rbx, %rsi
        call print

#exit        
        movq $0, %rdi
        call exit

print:    
        pushq %rbp
        movq %rsp, %rbp
 
        movq $0, %rax
        call printf  # this returns a int -- number of chars printed, stored in %rax
        
        movq %rbp, %rsp
        pop %rbp
        ret

    
