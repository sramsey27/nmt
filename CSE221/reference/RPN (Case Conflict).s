# Steven Ramsey

.section .bss
.section .text
.globl _start
_start:

	pushq 	$0x8 	#Push the number 8 on the stack
	pushq	$0x2 	#Push the number 2 on the stack
	popq	%rax 	#rax corresponds to the value 8
	popq	%rbx 	#rbx corresponds to the value 2
	addq	%rbx, %rax 	#add the two values together, store in rax
	pushq	%rax 	#push rax (10) back on the stack

	pushq	$6		#Push 6 on the stack 
	popq	%rax	#pop rax (10) off the stack
 	popq	%rbx	#pop rbx off the stack, corresponds to 6
	mulq	%rbx	#multiply by %rax
	pushq	%rax	#push %rax (60) back on stack

	pushq 	$12		#push 12 on the stack
	popq	%rbx	#pop %rbx off the stack, corresponds to 12
	popq	%rax	#pop %rax (60) off the stack
	addq	%rbx, %rax	#add %rbx(12) to %rax(60), then store in %rax
	pushq	%rax	#push %rax (72) back on the stack

	pushq	$2		#push 2 on the stack
	popq	%rbx 	#pop largest value first
	popq	%rax	#pop %rax (72) back on the stack
	divq	%rbx 	#put number your diving by first
	movq	%rax, %rax	#move result onto %rax
	pushq	%rax	#put %rax (36) back on the stack

	pushq	$1		#push 1 onto the stack
	popq	%rax	#pop %rax (36) off the stack
	popq	%rbx	#pop %rbx off the stack, = 1
	addq	%rbx, %rax	#add %rbx (1) to %rax (36)
	pushq	%rax	#put %rax (37) back on the stack

	pushq	$3		#put 3 on the stack
	popq	%rbx	#pop %rbx off the stack, = 3
	popq	%rax	#pop %rax (37) off the stack
	subq	%rbx, %rax	#subtract %rbx (3) from %rax (37) 
	pushq 	%rax	#put %rax (34) back on the stack

	pushq	$4		#put 4 on the stack
	popq	%rbx	#pop %rbx off the stack, = 4
	popq	%rax	#pop %rax (34) off the stack
	divq	%rbx	#register %rbx contains the remainder
	movq 	%rbx, %rax	#move remainder into %rax
	pushq	%rax	#put rax (2) on the stack

	#Epilogue
	movq 	%rax, %rdi	#exit the program
	movq 	$60, %rax
	syscall
