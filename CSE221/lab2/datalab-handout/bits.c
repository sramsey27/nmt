/* 
 * CSE 221 - Lab 2 -- Data Lab Floating Point
 * 
 * Steven Ramsey 900312622
 * 
 * bits.c - Source file with your solutions to the Lab.
 *          This is the file you will hand in to your instructor.
 *
 * WARNING: Do not include the <stdio.h> header; it confuses the dlc
 * compiler. You can still use printf for debugging without including
 * <stdio.h>, although you might get a compiler warning. In general,
 * it's not good practice to ignore compiler warnings, but in this
 * case it's OK.  
 */

#if 0
/*
 * Instructions to Students:
 *
 * STEP 1: Read the following instructions carefully.
 */

You will provide your solution to the Data Lab by
editing the collection of functions in this source file.

FLOATING POINT CODING RULES

For the problems that require you to implent floating-point operations,
the coding rules are less strict.  You are allowed to use looping and
conditional control.  You are allowed to use both ints and unsigneds.
You can use arbitrary integer and unsigned constants.

You are expressly forbidden to:
  1. Define or use any macros.
  2. Define any additional functions in this file.
  3. Call any functions.
  4. Use any form of casting.
  5. Use any data type other than int or unsigned.  This means that you
     cannot use arrays, structs, or unions.
  6. Use any floating point data types, operations, or constants.


NOTES:
  1. Use the dlc (data lab checker) compiler (described in the handout) to 
     check the legality of your solutions.
  2. Each function has a maximum number of operators (! ~ & ^ | + << >>)
     that you are allowed to use for your implementation of the function. 
     The max operator count is checked by dlc. Note that '=' is not 
     counted; you may use as many of these as you want without penalty.
  3. Use the btest test harness to check your functions for correctness.
  4. Use the BDD checker to formally verify your functions
  5. The maximum number of ops for each function is given in the
     header comment for each function. If there are any inconsistencies 
     between the maximum ops in the writeup and in this file, consider
     this file the authoritative source.

/*
 * STEP 2: Modify the following functions according the coding rules.
 * 
 *   IMPORTANT. TO AVOID GRADING SURPRISES:
 *   1. Use the dlc compiler to check that your solutions conform
 *      to the coding rules.
 *   2. Use the BDD checker to formally verify that your solutions produce 
 *      the correct answers.
 */


#endif
/* Copyright (C) 1991-2015 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */
/* This header is separate from features.h so that the compiler can
   include it implicitly at the start of every compilation.  It must
   not itself include <features.h> or any other header that includes
   <features.h> because the implicit include comes before any feature
   test macros that may be defined in a source file before it first
   explicitly includes a system header.  GCC knows the name of this
   header in order to preinclude it.  */
/* glibc's intent is to support the IEC 559 math functionality, real
   and complex.  If the GCC (4.9 and later) predefined macros
   specifying compiler intent are available, use them to determine
   whether the overall intent is to support these features; otherwise,
   presume an older compiler has intent to support these features and
   define these macros by default.  */
/* wchar_t uses ISO/IEC 10646 (2nd ed., published 2011-03-15) /
   Unicode 6.0.  */
/* We do not support C11 <threads.h>.  */
/* 
 * float_abs - Return bit-level equivalent of absolute value of f for
 *   floating point argument f.
 *   Both the argument and result are passed as unsigned int's, but
 *   they are to be interpreted as the bit-level representations of
 *   single-precision floating point values.
 *   When argument is NaN, return argument..
 *   Legal ops: Any integer/unsigned operations incl. ||, &&. also if, while
 *   Max ops: 10
 *   Rating: 2
 */
unsigned float_abs(unsigned uf) {

	/*When argument is NaN, return argument..*/
	if((uf & 0x7FFFFFFF) >= 0x7F800001){
		return uf;
	}
	else if((uf & 0x7FFFFFFF) < 0x7F800001){
		return (uf & 0x7FFFFFFF);
	}
	return uf;
}
/* 
 * float_f2i - Return bit-level equivalent of expression (int) f
 *   for floating point argument f.
 *   Argument is passed as unsigned int, but
 *   it is to be interpreted as the bit-level representation of a
 *   single-precision floating point value.
 *   Anything out of range (including NaN and infinity) should return
 *   0x80000000u.
 *   Legal ops: Any integer/unsigned operations incl. ||, &&. also if, while
 *   Max ops: 30
 *   Rating: 4
 */
int float_f2i(unsigned uf) {

	int mask = (uf &  0x007FFFFF), mask2 = ((mask) | 0x00800000);
	int temp = (-0x7F + ((uf & 0x7F800000) >> 0x17)), answer = 0;

	/* Anything out of range (including NaN and infinity)*/
	if (((uf >> 0x17) & 0xFF) == 0x7F800000){
  		return 0x80000000u;
  	}
	else if ((temp - 0x17) > 0x7) {	
		return 0x80000000u;
	}
  	else if (temp < 0x0){
  		return 0x0;
  	}
  	else if ((temp - 0x17) < 0x0){
  		answer = mask2 >> -(temp - 0x17);
  	}
  	else if ((temp - 0x17) >= 0x0){	
  		answer = mask2 <<  (temp - 0x17);
  	}
  	
  	if ((uf & 0x80000000) == 0x80000000){
  		answer = (~answer + 0x1);
  	}
  	return answer;
}

/* 
 * float_half - Return bit-level equivalent of expression 0.5*f for
 *   floating point argument f.
 *   Both the argument and result are passed as unsigned int's, but
 *   they are to be interpreted as the bit-level representation of
 *   single-precision floating point values.
 *   When argument is NaN, return argument
 *   Legal ops: Any integer/unsigned operations incl. ||, &&. also if, while
 *   Max ops: 30
 *   Rating: 4
 */
unsigned float_half(unsigned uf) {

	unsigned temp = (uf & 3) ^ 3;
	unsigned mask = (uf & 0x7FFFFFFF) >> 1, mask2 = (uf & 0x007FFFFF) >> 1;
	unsigned answer = 0;

 	/*When Argument is NaN or infinity, return argument*/
  	if ((uf & 0x7F800000) == 0x7F800000){ 
  		return uf;
  	}
  	else if ((uf & 0x7F800000) == 0x00800000){
  		answer = (uf & 0x80000000);
  		answer = answer | (!temp + mask);;
  	}
  	else if ((uf & 0x7F800000) == 0x00000000) {
  		answer = (uf & 0x80000000);
  		answer = answer | ((mask2) + !temp);
  	}
  	else {
  		answer = ((uf & 0x7F800000) >> 23);
  		answer = ((answer - 1) << 23) | (uf & 0x807FFFFF);;
  	}
  	return answer;
}

/* 
 * float_neg - Return bit-level equivalent of expression -f for
 *   floating point argument f.
 *   Both the argument and result are passed as unsigned int's, but
 *   they are to be interpreted as the bit-level representations of
 *   single-precision floating point values.
 *   When argument is NaN, return argument.
 *   Legal ops: Any integer/unsigned operations incl. ||, &&. also if, while
 *   Max ops: 10
 *   Rating: 2
 */
unsigned float_neg(unsigned uf) {

	/* NaN Check*/
  	if (uf == 0x7FC00000){
  		return uf;
  	}
  	/* Inifnity Check */
  	else if (uf == 0xFFC00000){
  		return uf;
  	}
  	/* Flip sign otherwise */
  	else {
  		return uf ^ 0x80000000;
  	}

}
/* 
 * float_i2f - Return bit-level equivalent of expression (float) x
 *   Result is returned as unsigned int, but
 *   it is to be interpreted as the bit-level representation of a
 *   single-precision floating point values.
 *   Legal ops: Any integer/unsigned operations incl. ||, &&. also if, while
 *   Max ops: 30
 *   Rating: 4
 */
unsigned float_i2f(int x) {

	unsigned temp = x, temp2 = 0, temp3, temp_sign = 0;
	unsigned s = 0, answer = 0, count = 0;

	/* Check input value for special cases such as x = 0 or x < 0 */
	if (x == 0){
    	return 0;
    }
	else if (x < 0) { /*Collect the sign if less than 0*/
    	temp_sign = 0x80000000;
    	temp = -x;	
  	}
  	/* Temporary place holder*/
  	temp3 = temp;

  	/* count is the number of shifts needed */
  	for (temp3 = temp; (temp2 & 0x80000000) == 0; count++){
    	temp2 = temp3;
    	temp3 = (temp3 << 1);
  	}
  	s = ((0x9F - count) << 0x17);

  	/* Float expression */
  	if ((temp3 & 0x01FF) > 0x0100){
    	answer = (temp_sign + (temp3 >> 0x9) + s +  0x1);
  	}
  	else if (((temp3 & 0x03ff) == 0x0300)){
      	answer = (temp_sign + (temp3 >> 0x9) + s + 0x1);
  	}
    else {
      	answer = (temp_sign + (temp3 >> 0x9) + s + 0x0);
    }
  	return answer;
}
  
/* 
 * float_twice - Return bit-level equivalent of expression 2*f for
 *   floating point argument f.
 *   Both the argument and result are passed as unsigned int's, but
 *   they are to be interpreted as the bit-level representation of
 *   single-precision floating point values.
 *   When argument is NaN, return argument
 *   Legal ops: Any integer/unsigned operations incl. ||, &&. also if, while
 *   Max ops: 30
 *   Rating: 4
 */
unsigned float_twice(unsigned uf) {

	unsigned temp = 0, temp2 = 0, mask = 0;
 
	/* NaN and infinity Check */
  	if ((uf & 0x7F800000) == 0x7F800000){ 
  		return uf;
  	}
  	else if(uf == 0xFFC00000){
  		return uf;
  	}

  	temp = (uf & 0x7FFFFF);
  	mask = ((uf >> 31) & 1);
  	temp2 = (uf >> 23) & 0xFF;

	/* Float expression */ 
  	if(!(temp2)){
  		temp = temp << 1;
  		return ((mask) << 31) | ((temp2) << 23) | temp;
  	}
  	else if (temp2){
   		return ((mask) << 31) | (((temp2) + 1) << 23) | temp;
  	}

  	return 0;
}
