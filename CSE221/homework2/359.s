.section .data
.section .text

.globl _start
_start

	movq	%rdx, %rax
	cqto				#convert from quad to oct
	movq	%rsi, %rcx	
	sarq	$63, %rcx	#shift over 63 bits
	imulq	%rax, %rdx	#perform a 64bit signed multiply
	#a 128 bit product placed in rdx and rax, most significant bits are places in rdx
	addq	%rdx, %rcx  #computes the sum of two signed 64-bit values
	mulq	%rsi		#computes the product of two signed 64-bit values.
	addq	%rcx, %rdx  #computes the sum of two signed 64-bit values
	movq	%rax, (%rdx) #Places one half of the product in memory (%rdx) 
	movq	%rdx, 8(%rdi) #other half placed in next place over in memory

	ret

	#result was produced due to a union of the two products
	#assembly will split 128 bit registers in two
