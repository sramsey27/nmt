.section .data
answer1:
	.asciz "The answer is %d\n"
answer2:
	.asciz "No, the answer is %d\n"
.section .text
.globl foo

.globl _start
_start:

#main
	#prologue
	pushq	%rbp	#base pointer
	movq	%rsp, %rbp
       
#set up local variables first

    subq $16, %rsp
    movl $10, 12(%rsp) #x,
    movl $20, 8(%rsp) #y 
    movl $3, 4(%rsp) #z 
    movl $0, (%rsp) #answer

	movl	4(%rsp), %edi #z = c
	movl	8(%rsp), %esi #y = b
	movl	12(%rsp), %edx #x = a

	call foo #foo returns a value to %rax

	subq	$4, %rsp
	movl	%eax, (%rsp) #wierdness occurred so I rerouted %eax into (%rsp) for use later
	movl 	%eax, %esi #out expects input to be %rdi
	movq 	$answer1, %rdi #printf always expects %rdi to be the string

	xorq	%rax, %rax #printf segfaults if something is in %rax, xor itself will become 0

	call printf

	movl	(%rsp), %esi #Simple increment and print
	addq	$4, %rsp
	incl	%esi 
	movq	$answer2, %rdi
	xorq	%rax, %rax

	call printf 

	movq $0, %rdi
	call exit

foo:
	#Prologue
	pushq	%rbp	#base pointer
	movq	%rsp, %rbp

    #only two local variables
    #set up stack for local variables
    subq 	$8, %rsp
    movl	$29, (%rsp) # x
    movl	$50, 4(%rsp)# y
    movl	%esi, %eax
    movl	%edi, %r11d #move c into r11d
    imull	%edi, %eax #multiply
    addl	%edx, %eax #add
    movl	%eax, %r12d
    movl	(%rsp), %eax
    xorl	%edx, %edx #set up for mod
    divl	4(%rsp) #mod y
   	movl	%r12d, %eax 
   	subl	%edx, %eax #subtract both ends

	#Epologue
	addq	$8, %rsp
	movq	%rbp, %rsp
	popq	%rbp
	ret
