#Steven Ramsey
#10/31/15

.section .data
output:
	.asciz "The processor Vendor ID is 'xxxxxxxxxxxx'\n"
.section .text
.globl _start
_start:

	#Changed registers to x64 'rax, rdi, etc...' and added syscall
	movq 	$0, %rax
	cpuid
	movq 	$output, %rdi
	movl 	%ebx, 28(%rdi)
	movl 	%edx, 32(%rdi)
	movl 	%ecx, 36(%rdi)
	movq 	$1, %rax #syscall write acquired by 1
	movq 	$1, %rdi
	movq 	$output, %rsi
	movq 	$42, %rdx
	syscall
	movq 	$1, %rax
	movq 	$0, %rdi
	int		$0x80
