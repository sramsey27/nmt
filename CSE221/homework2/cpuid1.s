#Steven Ramsey
#10/31/15

#cpuid2.s View the CPUID Vendor ID string using C library calls
.section .data
output:
.asciz "The processor Vendor ID is ‘%s’\n"

print1:
	.asciz "cpuid eax value 0x%x\n"

print2:
	.asciz "extended family id: 0x%x\n"

print3:
	.asciz "extended model id: 0x%x\n"

print4:
	.asciz "processor: 0x%x\n"

print5:
	.asciz "family: 0x%x\n"

print6:
	.asciz "model: 0x%x, %d\n"

print7:
	.asciz "stepping_id: 0x%x\n"

.section .bss
.lcomm buffer, 12
.section .text
.globl _start
_start:

	movl 	$1, %eax
	cpuid

	#done by moving along the bits stored in eax

	#eax value
	movl	%eax, %r12d #move eax out of harms way
	movq 	$print1, %rdi
	movl	%r12d, %esi
	movl 	$0, %eax
	call 	printf

	#extended family id
	movq	$print2, %rdi
	movl	%r12d, %esi
	sarl	$20, %esi
	call 	printf

	#extended model id
	movq	$print3, %rdi
	movl	%r12d, %esi
	sarl	$16, %esi
	call   	printf

	#processor
	movq	$print4, %rdi
	movl	%r12d, %esi
	sarl	$12, %esi
	call   	printf

	#family - required 
	movq	$print5, %rdi
	movl	%r12d, %esi
	sarl	$8, %esi
	movl	%r12d, %r14d
	sarl	$20, %r14d
	andl	$0xF, %r14d #masking
	andl	$0xF, %esi
	sall	$4,	%r14d
	xorl	%r14d, %esi
	call   	printf

	#model
	movq	$print6, %rdi
	movl	%r12d, %esi
	sarl	$4, %esi
	movl	%r12d, %r14d
	sarl	$16, %r14d
	andl	$0xF, %r14d	#masking
	andl	$0xF, %esi
	sall	$4,	%r14d
	xorl	%r14d, %esi #zero out
	movl	%esi, %edx
	call   	printf

	#stepping_id
	movq	$print7, %rdi
	movl	%r12d, %esi
	andl	$0xF, %esi
	call 	printf

	call 	exit
