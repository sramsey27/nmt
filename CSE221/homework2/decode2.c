//Steven Ramsey
//10-31-15

long decode2(long x, long y, long z){

	/*when rdi = x, rsi = y, rdx = z */

	long answer; //return value

	/* subq	%rdx, %rsi */
	y = z - y;

	/*imulq	%rsi, %rdi*/
	x = y * x;

	/*movq	%rsi, %rax */
	answer = y;

	/* salq	$63, %rax */
	answer = (answer << 63);

	/*sarq	$63, %rax */
	answer = (answer >> 63);

	/* xorq	%rdi, %rax */
	answer = x ^ answer;

	return answer;

}