/**
 * @file lisp.cpp
 *
 * @author Steven Ramsey
 *
 * @date 4/13/16
 * 
 * @brief this program acts as an alternate lisp interpreter
 * 
 * @details Similar to the actual lisp interpreter with some personal refinements/
 * The goal of this is create a interactive lisp interpreter without any memory leaks
 * and aimed at total functionality similar to the lisp interpreter that is also equiped
 * with error checking.
 * 
 * @todo implement proper nesting
 *
 * @bugs listed in README
 */

using namespace std;

int statement = 0; //debugmode

#include "lisp.h"

/**
 * This function reads the input char by char to check for any mismatches in parenthesis
 * and returns a string that has been processed.
 * @return the processed input, blank string if the parenthesis is mismatched
 */
string readinput(){
	string input;
	char temp_char;
	int parleft = 0, parright = 0, i = 0;

	while(true){
		cin.get(temp_char); //get the char
		
		if(temp_char != '\n') //don't include a newline char to avoid errors
			input = input + temp_char; //add on the the current string

		/*Parenthesis check*/
		if(temp_char == '('){
			parleft++;
		}
		else if(temp_char == ')'){
			parright++;
		}
		else if(temp_char == '\n')
			break;                
	}

	/*Check if number of left and right parenthesis match, else return a blank string*/
	if(parleft != parright){
                cout << "Parenthesis mismatch...check your input\n";
                return "";
        }
	return input;
}

/** 
 * This function splits the user input into tokens for later processing and evaluation
 * @param input the user input string obtained from readinput()
 * @return a list of tokens made from input
 */
deque<string> tokenizer(string input) {

        deque<string> tokens;
        const char *cur = input.c_str(), *temp_char;
        string temp_string;
        while (*cur) {
                if(*cur == ' '){
                        *cur++;
                }
                else if (*cur == '('){
                        tokens.push_back("(");
                        *cur++;
                }
                else if (*cur == ')'){
                        tokens.push_back(")");
                        *cur++;
                }
                else {
                        temp_char = cur;
                        while(*temp_char && *temp_char != ' ' && *temp_char != '(' && *temp_char != ')'){
                                temp_char++;
                        }
                        tokens.push_back(string(cur, temp_char));
                        cur = temp_char;
                }
        }

        if(statement){
        	deque<string>::iterator j;
	        cout << "Tokens: ";
	        for(j = tokens.begin(); j != tokens.end(); j++){
	        	cout << *j << " ";
	        }	
	        cout << "\n";
	}

        return tokens;
}


/**
 * Handles the quote command and returns the corresponding arg to quote
 * @param original user input
 * @return the quote output
 */
string quote_handler(string u_input){

	string output;
	for(int i = 1; i < u_input.length() - 1; i++){
		if(u_input[i] == ' '){
			for(i++; i < u_input.length() - 1; i++){
				output += u_input[i];
			}
		}	          
	}
	return output;
}

string NumberToString (double Number){ //inspired by stack overflow reference [6]
     	ostringstream ss;
     	ss << Number;
     	return ss.str();
}

bool number_check(string u_input) {

	char temp = u_input[0], *check;
	strtod(u_input.c_str(), &check);

	if (temp != (*check)){
		return true;
	}
	else {
		return false;
	}
}

string get_number(string u_input){
	string output;

	for(int i = 1; i < u_input.length() - 1; i++){
		output += u_input[i];
	}
	return output;
}

int contained_in_mem(string curname, variables* vars){
	// -1 for empty , if found index for tracking, 0 for something went wrong
	for(int index = 0; index != vars->instance.size(); index++){
		if(vars->instance[index]->name == curname){
			return 1;
		}
		else{
			continue;
		}
	}
	return 0;
}

void insert_in_memory(string name, string val, variables *vars){
	vars->name = name;
	vars->var_values = val;
}

/**
 * Acts as a prefix calculator for math operations
 */
double operand_handler(stack_block *tree, variables *var_values){

	double result = 0.0;
	deque<string> cur = tree->lvl;
	for(int i = 0; cur[i] != ")"; i++){
		if(cur[i] == "+"){
			for(i++;cur[i] != ")"; i++){ //working
				if(statement) 
					cout << "check: " << cur[i];
				if(contained_in_mem(cur[i], var_values)){
					result += atof(get_element(cur[i], var_values).c_str());
				}
				else {
					result += atof(cur[i].c_str());
				}
				if(statement)
					cout << "\tresult: " << result << "\n";	
			}
			break;
		}
		else if(cur[i] == "*"){ //working

			if(contained_in_mem(cur[i + 1], var_values)){
				result = atof(get_element(cur[i + 1], var_values).c_str());
			}
			else {
				result = (atof(cur[i + 1].c_str()));
			}

			for(i = i + 1, i++ ;cur[i] != ")"; i++){
				if(statement) 
					cout << "check: " << cur[i];
				if(contained_in_mem(cur[i], var_values)){
					result *= atof(get_element(cur[i], var_values).c_str());
				}
				else {
					result *= (atof(cur[i].c_str()));
				}
				if(statement) 
					cout << "\tresult: " << result << "\n";
			}
			break;
		}
		else if(cur[i] == "/"){ //working

			if(contained_in_mem(cur[i + 1], var_values)){
					result = atof(get_element(cur[i + 1], var_values).c_str());
			}
			else {
				result = (atof(cur[i + 1].c_str()));
			}

			for(i = i + 2; cur[i] != ")"; i++){
				if(statement) 
					cout << "check: " << cur[i];
				if(contained_in_mem(cur[i], var_values) && i < 2){
					result /= atof(get_element(cur[i], var_values).c_str());
				}
				else {
					result /= (atof(cur[i].c_str()));
				}
				if(statement) 
					cout << "\tresult: " << result << "\n";
			}
			break;
		}
		else if(cur[i] == "-"){ //working
			if(contained_in_mem(cur[i + 1], var_values)){
					result = atof(get_element(cur[i + 1], var_values).c_str());
			}
			else {
				result = (atof(cur[i + 1].c_str()));
			}
			for(i = i + 2; cur[i] != ")"; i++){
				if(statement) cout << "check: " << cur[i];
				if(contained_in_mem(cur[i], var_values)){
					result -= atof(get_element(cur[i], var_values).c_str());
				}
				else {
					result -= (atof(cur[i].c_str()));
				}
				if(statement)cout << "\tresult: " << result << "\n";
			}
			break;
		}
		else if(cur[i] == "sqrt"){
			if(cur[i + 1] == "("){
				result = sqrt(operand_handler(tree->branches.back(), var_values));
				return result;
			}
			else{
				result = sqrt((atof(cur[i + 1].c_str())));
			}
			
			break;
		}
	}
	return result;
}

string get_element(string cur_elm, variables *var_values){

	deque<variables*>::iterator g;
	for(g = var_values->instance.begin(); g != var_values->instance.end(); g++){
		if((*g)->name == cur_elm){
			return (*g)->var_values;
		}
		else{
			continue;
		}
	}
	return "something went wrong";
}

string change_element(deque<string> cur, variables *var_values, stack_block *tree){

	deque<variables*>::iterator g;
	double result = 0.0;
	for(g = var_values->instance.begin(); g != var_values->instance.end(); g++){
		if((*g)->name == cur[1]){
			if(cur[2] == "("){
				(*g)->var_values.clear();
				(*g)->var_values = NumberToString(operand_handler(tree->branches.back(), var_values));
				return (*g)->name;

			}
			else {
				(*g)->var_values.clear();
				(*g)->var_values = cur[2];
				return (*g)->name;
			}
		}
		else{
			continue;
		}
	}
	return "something went wrong";

}

string conditional_check(string cur_command, deque<string> cur, string u_input){
	int i = 1;
	int parleft = 0, parright = 0;
	bool cond = false;
	string temp_str;
	if(cur_command == "if"){
		for(; i < cur.size(); i++){
			if(cur[i] == "("){
				if(cur[i + 1] == "<"){
					i++;
					if((atof(cur[i + 2].c_str())) < (atof(cur[i + 3].c_str()))){
						i = i + 5;
						cond = true;
						return "t";
					}
				}
				else if(cur[i + 1] == ">"){
					if((atof(cur[i + 2].c_str())) > (atof(cur[i + 3].c_str()))){
						i = i + 5;
						cond = true;
						return "t";
					}

				}
			}
			
		}
		if(cond){
			if(cur[i] == "("){
				for(int j = i; cur[j] != ")";){
					temp_str = temp_str + cur[i];
				}
				//push to tree?
			}
		}
		
	}
	else if(cur_command == "<"){
		if((atof(cur[2].c_str())) < (atof(cur[3].c_str()))){
			return "t";
		}
		else{
			return "f";
		}
	}
	else if(cur_command == ">"){
		if((atof(cur[2].c_str())) > (atof(cur[3].c_str()))){
			return "t";
		}
		else{
			return "f";
		}
	}
	else if(cur_command == "cond"){
		return "not yet implemented";
	}

	return "f";
}


string evaluate(deque<string> tokens, variables *functions, string u_input, stack_block *tree){

	deque<stack_block*>::iterator j;
	deque<stack_block*>::iterator n;
	deque<string>::iterator k;
	int r = 0;
	int w = 1;
	int i = 0;
	double curnum = 0; 
	stringstream val;
	string temp_str;

	if(statement){
		for(n = tree->branches.begin(); n != tree->branches.end(); n++, w++){
			cout << "Level " << w << ": ";
			for(k = (*n)->lvl.begin(); k != (*n)->lvl.end(); k++){
				cout << *k << " ";
			}
			cout << "\n";
		}	
	}
	for(j = tree->branches.begin(); j != tree->branches.end(); j++){
		deque<string> cur = (*j)->lvl;
		for(i = 0; i != (*j)->lvl.size(); i++){
			if(cur[i] == ")" || cur[i] == ") "){
				if(tree->branches.empty()){
					return "memory problem...";
				}
				tree->branches.pop_back();
				evaluate(tokens, functions, u_input, tree->branches.back());
			}
			else if(cur[i] == "+" || cur[i] == "-" || cur[i] == "/" || cur[i] == "*" || cur[i] == "sqrt"){
					if(statement) 
						cout << "Operation detected!\n";
					return NumberToString(operand_handler(tree, functions));

			}
			else if(cur[i] == "(" || cur[i] == "( "){
				evaluate(tokens, functions, u_input, tree->branches.front());
			}
			else if(cur[i] == "quote" || cur[i] == "quote "){ //working
				if(cur[i + 1] == ")" || cur[i + 1] != "("){
					return "invalid argument for quote!";
				}
				else{
					return quote_handler(u_input);
				}
			}
			else if(cur[i] == "define" || cur[i] == "define "){
				if(cur[i + 1] == ")"){
					return "invalid argument for define!";
				}
				r = contained_in_mem(cur[i + 1], functions);
				if(r == 1){
					temp_str ="variable '" + cur[i + 1] + "' already exist, use set! for changing value!";
					return temp_str;
				}
				else if(r == 0){
					if(cur[i + 1] == ")"){
						return "insufficent number of arguments for define";
					}
					functions->instance.push_back(new variables);

					insert_in_memory(cur[i + 1], cur[i + 2], functions->instance.back());
					return cur[i + 1];
				}
				else{
					return "Error -- Could not define variable";
				}
			}
			else if(number_check(cur[i].c_str()) && !(contained_in_mem(cur[i], functions))){
				goto number;
			}		
			else if(contained_in_mem(cur[i], functions) == 1){ /*Print any known variables*/
				return get_element(cur[i], functions);
			}
			else if(cur[i] == "<" || cur[i] == ">" || cur[i] == "if" || cur[i] == "cond"){
				return conditional_check(cur[i], tokens, u_input);
			}
			else if(cur[i] == "set!"){
				if(cur[i + 1] == ")"){
					return "invalid argument for define!";
				}
				r = contained_in_mem(cur[i + 1], functions);
				if(r == 0){
					temp_str ="variable '" + cur[i + 1] + "' does not exist, use 'define'";
					return temp_str;
				}
				else if(r == 1){
					change_element(cur, functions, tree);
					return cur[i + 1];
				}
				else{
					return "Error -- Could not set variable";
				}
			}
			
		}
	}

number:
	/*The wierd, but working number check to output constant literals*/
	if(number_check(u_input) == true || u_input[0] == '(' && u_input[1] != 'q' || u_input[1] != 's'){
		if(number_check(u_input) == true && tokens.size() == 1){
			return u_input;
		}
      		if(u_input[0] == '('){
			return get_number(u_input);
		}
		else {
			return "Invalid input expression!";
		}
	}	
}

void store_info(stack_block *tree, deque<string> tokens){
	while(!(tokens.empty())){
		if(tokens.front() == "("){
			tree->lvl.push_back(tokens.front());
			tokens.pop_front();
			tree->branches.push_back(new stack_block);
			store_info(tree->branches.back(), tokens);
		}
		else if(tokens.front() == ")"){
			tree->lvl.push_back(tokens.front());
			tokens.pop_front();
			return;
		}
		else {
			tree->lvl.push_back(tokens.front());
			tokens.pop_front();
		}
	}
}

/** 
 * This is where the fun begins, the user get prompted as a interpreter would do
 * and the appropreiate functions are called accordingly
 */
void promptuser_process(variables *vars, stack_block *tree){
        string u_input, answer;

        cout << "Opening output file...";
        ofstream outfile;
      	outfile.open("results.txt", ofstream::out);
        outfile << "/**\n *Interpreter output\n*/\n\nBEGIN:\n\n"; 
	cout << "Done!\n\nReady. Go ahead, enter some lisp commands.\n";

	while(true){
		cout << "Prompt> ";
                u_input = readinput(); //here is where the user actually get prompted
                if((u_input == "(quit)") || u_input == "(exit)" || u_input == "quit" || u_input == "q"){
                        break; //when user wants to exit gracefully
                }
                else if(u_input == ""){
                	outfile << "none\n";
                        continue;
                }
                else if(u_input == "debugmode"){ /*Personal error checking mechanism*/
                	if(statement){
                		statement--;
                		answer = "debug prints off";
                	}
                	else{
                		statement++;
                		answer = "debug prints on";
                	}
                }
                else{
                        deque<string> tokens = tokenizer(u_input);  //split user input into tokens
                        deque<string> temp_tokens = tokens; //retain token infomation
                        store_info(tree, temp_tokens); //store the infomation in a tree/block structure
                        /*if(tokens.size() == 3 && number_check(u_input)){
                        	tree->branches.clear();
               			tree->lvl.clear();
                        	cout << u_input << "\n";
                        }*/
                        answer = evaluate(tokens, vars, u_input, tree); //evaluate
                        tokens.clear();
                }
                cout << answer <<"\n";
                outfile << answer << "\n";
               	tree->branches.clear();
               	tree->lvl.clear();
               	answer.clear();
	}
	vars->instance.clear();
	tree->lvl.clear();
	tree->branches.clear();
	outfile << "\nEND. ";
        outfile.close(); //close output file
        cout << "Goodbye\n";
}

int main (){
	cout << "Welcome to my lisp interpreter, Version: It works for the most part :)\n";
	cout << "Initialzing Lisp Environment...";
	variables vars;
	stack_block tree;
	cout << "Done!\n";
        promptuser_process(&vars, &tree); //the environment will get passed in here once things are good
	return 0;

}