#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <fstream>
#include <list>
#include <map>
#include "stdlib.h"
#include <algorithm> 
#include <deque>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h> 

struct variables {
	//variable declarions
	string name;
	string var_values;
	deque<variables*> instance;

};

struct stack_block{ //holds the nested situations
	deque<string> lvl;
	deque<stack_block*> branches;
};

string readinput();
deque<string> tokenizer(string input);
void promptuser_process(variables *vars, stack_block *tree);
string evaluate(deque<string> tokens, variables *functions, string u_input, stack_block *tree);
string change_element(deque<string> cur, variables *var_values, stack_block *tree);
string get_element(string cur_elm, variables *var_values);
void store_info(stack_block *tree, deque<string> tokens);
double operand_handler(stack_block *tree, variables *var_values);
string get_number(string u_input);
bool number_check(string u_input);
string NumberToString (double Number);