Psuedo Lisp Interpreter (C++98 Standards)
By Steven Ramsey

Successfully Compiles on Ada!

Based on Table in Document:

	Variable reference: Functioning
	Constant literal: Functioning
	Definition: Functioning
	Procedure call: Functioning?
	Assignment: Functioning

Everything functions as expected except:

1). Conditional (incomplete)
	- It is semi-functioning as mentioned in the report, 
	  but unable to carry execution based on t/f.
	  
	- Only functional like so:

	  Prompt> (if (< 40 20))
	  f
	  Prompt> (if (> 20 10))
	  t

2). Procedure (it is simply not implemented due to time constrictions).

There is a predefined function:

	- Square Root Procedure call

		Prompt> (sqrt (* 2 8))
		4

Known Issues:
	When an unknown variable is requested, a segmentation fault occurs...
	
		Prompt> (known_variable)
		known_variable value

		Prompt> (unknown_variable)
		Segmentation Fault

	Divison doesn't function with variables...
		
	  
