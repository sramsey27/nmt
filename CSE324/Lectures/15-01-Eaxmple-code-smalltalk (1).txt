"------------This file is saved in C:\TEMP\VBE----------------------------------------"
"for with downto"
     | a|  
     a := 0.
    10 to:4 by:-2 do:[:i| a := i + a]. ^a.   28
"----------------------------------------------------------------------------------------------------"
    "Orderedcollection initialization"
  |x|  

   x := OrderedCollection newFrom: {1. 2. 3}. ^x.


"-------------------------------------------------------------------------------------------------------"
"write a program to collect all even numbers in 'a'  and all odd numbers in 'b'"
  
 |a b |
  a := OrderedCollection new.
  b := OrderedCollection new.
  
  1 to:10  do:[:j| (j odd) ifFalse: [a add:j ]
                           ifTrue: [b add:j]]. ^a. an OrderedCollection(2 4 6 8 10)   
               
                   "**************************************"

"another way: receiver is acollection "

|a b |
  a := OrderedCollection new.
  b := OrderedCollection new.
  
  {7. 2. 3. 4. 5. 6. 1. 8. 9. 10}  do:[:j| (j odd) ifFalse: [a add:j ]
                           ifTrue: [b add:j]].
   ^b.  an OrderedCollection(7 3 5 1 9)an OrderedCollection(1 3 5 7 9)
    ^a. an OrderedCollection(2 4 6 8 10)"
               
                   "**************************************"
"another way: receiver is acollection "

|a b |
  a := SortedCollection new:10.
  b := SortedCollection new:10.
  
  {7. 8. 3. 4. 5. 6. 1. 2. 9. 10}  do:[:j| (j odd) ifFalse: [a add:j ]
                           ifTrue: [b add:j]].
   ^a.  a SortedCollection(2 4 6 8 10) 
"************************************************************************************

" another way: receiver is a collection name 'c' initialized with acollection "
|a b c|
  a := OrderedCollection new.
  b := OrderedCollection new.
c := #(63 55 93 62 24 35 67 98 10 34 2 4 56 7 8 90 75 63 52 63 10 63 92 95).
 
  c do:[:j| (j odd) ifFalse: [a add:j ]
                           ifTrue: [b add:j]]. 
"^b. an OrderedCollection(63 55 93 35 67 7 75 63 63 63 95)
^a. an OrderedCollection(62 24 98 10 34 2 4 56 8 90 52 10 92)"

"--------------------------------------------------------------------------------------------------------"

  |s c|
 c := #(63 55 93 62 24 35 67 98 10 34 2 4 56 7 8 90 75 63 52 63 10 63 92 95).
   s := SortedCollection new: 10.

   s addAll:c. 
   s add:-5.  
   s := s * 2. 
   s := s + 2. 
   s := s / 2. ^s. an OrderedCollection(-4 3 5 8 9 11 11 25 35 36 53 56 57 63 64 64 64 64 68 76 91 93 94 96 99)
   
 
"----------------------------------------------------------------------------------------------------------"

  |x|
   x := 0.
   10 timesRepeat:[x := x +1].

"---------------------------------------------------------------------------------------------------------"
| q1 |

  q1 := OrderedCollection new:100.
  q1 addAll:  #(8  1 2 3 4 5 77 99 33 10 11 22 33 44 55 66 7 2 3 4 7 8 0).        
   ^q1.
"----------------------------------------------------------------------------------------------------"

 |o a b v quad|
           quad := [v := 0.].
           o := SortedCollection new:30.
          b := OrderedCollection new:100.
           a := #(100  -20  10   -13   55   -9  -110   -10   -34   90   13   20  30  -15  77).
           quad value.
           a collect: [:j| (j > 10 negated) ifTrue:[ b add:j.  v := v + j]] from:5 to:10.
           o addAll: b. 
           o sortBlock:[:x :y| y <= x]. 
 "------------------------------------------------------------------------------------------------------------"

 | a |
  a := FloatArray new:10.
  a at:1 put:1.3. 
  a at:2 put:8.0.
  a at:3 put:2.5.
 ^a. a FloatArray(1.299999952316284 8.0 2.5 0.0 0.0 0.0 0.0 0.0 0.0 0.0)


"-----------------------------------------------------------------------------------------------------------"
  |a b c e|
  a := OrderedCollection new.
  b := OrderedCollection new.
  e := #(2 90).
 
c := #(63 55 93 62 24 35 67 98 10 34 2 4 56 7 8 90 75 63 52 63 10 63 92 95).
 e do:[:h| c do: [:j| (j odd) ifFalse: [a add:j ]
                           ifTrue: [b add:j]] without: h]. 
  
^a. an OrderedCollection(62 24 98 10 34 4 56 8 90 52 10 92 62 24 98 10 34 2 4 56 8 52 10 92)

"here is the class 'Array' inheritance hierarchy"
"ProtoObject #()
	Object #()
		Collection #()
			SequenceableCollection #()
				ArrayedCollection #()

					Array #()
						FlippyArray2 #()
						WeakArray #() "  