;1 - Finished
(defun insert-atom-list(x L)
	(cond ((null L) nil)
		(t (if (atom x) (cons x L) (append L (list x))))))

;2 - Finished
(defun insert-special(x y z L)
	(cond ((null L) nil)
		(t (cond ((eq (car L) y) (cond ((eq (cadr L) z) (append (append (list y) (list x)) (insert-special x y z (cdr L)))) 
			(t (cons (car L) (insert-special x y z (cdr L))))))
		(t (cons (car L) (insert-special x y z (cdr L))))))))


;3 - Finished
(defun remove-duplicate-item(L)
	(cond ((null L) nil)
	(t (cond ((eq (car L) (cadr L)) (remove-duplicate-item(cdr L)))
		(t (cons (car L) (remove-duplicate-item (cdr L))))))))
	
;4 - Finished
(defun remove-list(L)
	(cond ((null L) nil)
	(t (cond ((listp (car L)) (remove-list(cdr L)))
		(t (cons (car L) (remove-list (cdr L))))))))

;5 - Finished
(defun count-match-atom(L1 L2)
	(cond ((null L1) 0) ;check the first list for null, return 0 if true
		(t (cond ((null L2) 0) ;check the second list for null, return 0 if true
			(t (cond ((eq (car L1) (car L2)) (+ 1 (count-match-atom (cdr L1) (cdr L2))))
			(t (count-match-atom (cdr L1) (cdr L2)))))))))

;6 - Finished
(defun filter(P L)
	(cond ((null L) nil)
		(t (cond ((funcall P (car L)) (cons (car L) (filter P (cdr L)))) 
			(t (filter P (cdr L)))))))

;7 - Finished
(defun my-reverse(L)
	(cond ((null L) nil)
		(t (append (my-reverse(cdr L)) (List (car L))))))