package polymorphism;

class Vehicle{

    public void move(){

    System.out.println(�gVehicles can move!!�h);

    }

}

class MotorBike extends Vehicle{

    public void move(){

    System.out.println(�gMotorBike can move and accelerate too!!�h);

    }

}

class SailBoat extends Vehicle{
	
	public void move(){
		
		System.out.prinln("SailBoat is purrty");
	}
}

class Test{

    public static void main(String[] args){

    Vehicle vh=new MotorBike();

    vh.move();    // prints MotorBike can move and accelerate too!!
    
    vh=new SailBoat();
    
    vh.move();    // SailBoat is purrty

    vh=new Vehicle();

    vh.move();    // prints Vehicles can move!!

    }

}