package com.jhendricks.hw8.one;

public interface QueueInterface {
	public void enque(Object x);
	public Object deque();
	public boolean isEmpty();
	public Object peek();
	public int size();
}
