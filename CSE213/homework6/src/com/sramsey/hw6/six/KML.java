package com.sramsey.hw6.six;

/**
 * Takes in infomation from location.txt and converts it to KML format
 * 
 * @author Steven Ramsey, Jaeden Hendricks, Jimmy Alexander
 * 
 * @version homework6 #2
 * 
 * @bugs none
 */

import java.io.*;
import java.util.*;

public class KML {
	
	public static void main(String[] args) throws FileNotFoundException {
			
			String filename = "location.txt";
			
			try{
				BufferedReader infile = new BufferedReader(new FileReader(filename));
				readdata(infile); //start reading the data from the file
				
			}catch(IOException e){
				System.out.println("File could be read! Infomation below:");
				e.printStackTrace();
			}		
		}
	
	/**
	 * Reads location infomation from file
	 * @param infile the input file buffer
	 * @throws IOException and NumberFormatException
	 */
	private static void readdata(BufferedReader infile) throws NumberFormatException, IOException{
		
		String currline;
		String[] temp = null;
		int linecount = 0;
		List<Double> lat = new ArrayList<Double>(); //latitude values
		List<Double> lng = new ArrayList<Double>(); //longitude values

		try {
			while((currline = infile.readLine()) != null){
				
				if(linecount >= 1){ //ignore first line
					
					temp = currline.split("\\|");
					
					lat.add(linecount - 1, Double.parseDouble(temp[0]));
					lng.add(linecount - 1, Double.parseDouble(temp[1]));	
				}			
				linecount++;
			}
			
			processoutput(lat, lng, linecount-1); //call processoutput method
			
		} catch (NumberFormatException e) {
			System.out.println("Something went wrong while processing values...");
			e.printStackTrace();
		}
		
		 //process KML file
	}
	/**
	 * Output the KML infomation
	 * @param lat latitiude values
	 * @param lng longitude values
	 * @param index how many times to repeat process
	 */
	private static void processoutput(List<Double> lat, List<Double> lng, int index) throws IOException{
		
		BufferedWriter out = null;
		String filename = "location.kml";
		
		/*Hardcoded strings*/
		String header = "<?xml version = \"1.0\" encoding=\"UTF-8\"?>\n"; //intital kml header
		header = header + "<kml xmlns =\"http://earth.google.com/kml/2.1\">\n<Document>\n";
		header = header + "\t<name>Mystery Locations</name>\n";
		String info1 = "\t<Placemark>\n\t\t<name> Location ";
		String info2 = "</name>\n\t\t<Point>\n\t\t\t<coordinates>";
		String info3 = "</coordinates>\n\t\t</Point>\n\t</Placemark>\n";
		String end = "</Document>\n</kml>";
		
		try{		
			File outfile = new File(filename);
			
			if (outfile.exists() == false) {
			     outfile.createNewFile();
			}
			
			out = new BufferedWriter(new FileWriter(outfile));
			
			/*Start writing to the file*/
			out.write(header);
			for(int i = 1; i <= index; i++){
				out.write(info1);
				out.write(Integer.toString(i)); //location number
				out.write(info2);
				out.write(Double.toString(lng.get(i - 1))); //longitude number
				out.write("," + Double.toString(lat.get(i - 1)) + ",0"); //latitude number
				out.write(info3);
			}
			out.write(end); //end of file heading
			/*Finish writing*/
				
		}catch (IOException e){
			System.out.println("Problem with output file...");
			e.printStackTrace();
		}
		finally{	
			if(out != null){
				out.close();
			}
		}	
	}
}