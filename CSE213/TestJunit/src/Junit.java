import org.junit.*;
import static org.junit.Assert.*;
	
public class Junit{

	private static void main(String[] args) {
		
    int a = 5;
    int b = 10;
    int c = 13;
    int sum = 0;
    int sum2 = 0;
    
    /*say we have 'a' =5 and 'b' = 10 which will be added, you expect it to be 15 as the result*/
    
    sum = a + b;
    System.out.println("The sum of a and b is" + sum);
    
    //now let's add b and c which is 23
    
    sum2 = b + c;
    System.out.println("The sum of b and c is" + sum2);
    
    assertEquals(sum, sum2);  //This will throw an exception error because they are not the same. Good for testing results
	}
}
