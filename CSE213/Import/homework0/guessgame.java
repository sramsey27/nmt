package com.sramsey.hw0.one;

import java.util.Scanner;

/**
 * Guesses the number the user is guessing using divide and conquer method
 * 
 * This program takes in a given interval to work within (X,Y) and uses
 * the divide and conquer strategy to ask questions to reach a conclusion.
 * Also checks for X >= Y case.
 * 
 * @author Steven Ramsey, Jaden Hendricks
 * 
 * @version homework0 #3 
 * 
 * @bugs 1. The code doesn't take wrong input kindly (throws a mismatch exception)
 * 		 2. May ask the same question twice, but doesn't experience error.
 */

public class guessgame {

	public static void main(String[] args) {
		
		Scanner num_input = new Scanner(System.in);
		
		/*Gather X and Y through user prompt*/
		System.out.printf("Welcome to the Guessing Game! The Interval is [X, Y]\nInput a number for X: ");
		int X = num_input.nextInt();
		
		System.out.printf("Input a number for Y: ");
		int Y = num_input.nextInt();
		
		/*Error Check: X <= Y*/
		if (X >= Y){
			System.out.printf("\nX [%d] cannot be greater than or equal to Y [%d]!\n", X, Y);
			System.out.printf("Please try again...\n");
			System.exit(0);
		}
		
		/*Begin asking questions*/
		int midpoint;
		System.out.printf("\nOkay let's begin:\n");
			
		while (X != Y){
			
			Scanner Question = new Scanner(System.in);
		
			midpoint = ((X + Y) / 2);
			
			System.out.printf("Okay, is your number greater than %d? [Y/N]: ", midpoint);
			
			String user_answer = Question.next();
			String l_case = user_answer.toLowerCase(); /*Prevent Case errors*/
	
			/*20 Questions*/
			if(l_case.equals("y")){
				if (X == midpoint){
					midpoint++;
				}
				X = midpoint;
				continue;
			}
			else if(l_case.equals("n")){
				if (Y == midpoint){
					X = midpoint;
				}
				Y = midpoint;
				continue;
			}
			Question.close();//Fix warnings
		}
		/*Bonus Question/Complement*/
		Scanner Question = new Scanner(System.in);
		System.out.printf("\nIs %d the number you were thinking about? [Y/N]: ", X);
		String user_answer = Question.next();
		String l_case = user_answer.toLowerCase();
		
		if(l_case.equals("y")){
			System.out.printf("\nSweet. My work is done. Goodbye :)\n");
		}
		else if(l_case.equals("n")){
			System.out.printf("\nWhat? I know I'm right, don't you lie to me :(\n");
		}
		Question.close(); //Fix warnings
		num_input.close(); 
	}
}