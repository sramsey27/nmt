package com.sramsey.hw0.one;

/**
 * Prints FizzBuzz, Fizz, and Buzz depending on number division
 * 
 * This program prints "FizzBuzz" if number is divisible by 3 and 5,
 * prints Fizz if number is divisible by 3, and prints Buzz if number
 * is divisible by 5. Utilizing an array numbers 1-100 are stored and 
 * checked.
 * 
 * @author Steven Ramsey, Jaden Hendricks
 * 
 * @version homework0 #1 
 * 
 * @bugs 1. The code doesn't take wrong input kindly (throws a mismatch exception)
 * 		 2. May ask the same question twice, but doesn't experience error.
 */

public class fizzbuzz {
	
	public static void main(String[] args) {
		
		/*Initialize the array*/
		int[] Array;
		Array = new int[100];
		
		/*Populate the Array 1-100*/
		for(int i = 1; i < 100; i++){
			Array[i] = i;
		}	
		
		/*Run through the array testing modulus conditions*/
		for(int i = 1; i < 100; i++){
			
			if ((Array[i] % 3 == 0) && (Array[i] % 5 == 0)){
				System.out.printf("FizzBuzz\n");
			}
			else if (Array[i] % 3 == 0){
				System.out.println("Fizz");
			}
			else if (Array[i] % 5 == 0){
				System.out.print("Buzz\n");
			}
			else{
				System.out.println(Array[i]);
			}
		}
	}
}
