package com.sramsey.hw0.one;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Processes the file "alice.txt" and alternates letter case per line
 * 
 * This class changes the letter case to upper and lower case of every
 * other line while ignoring blank lines. The final result out is put in
 * "alice_in_mixed_case.txt"
 * 
 * @author Steven Ramsey, Jaden Hendricks
 * 
 * @version homework0 #4 
 * 
 * @bugs none
 */

public class alice {

	public static void main(String[] args) {
		
		Scanner inFile;
		
		/*Prevent file not found error with try*/
		try {
			inFile = new Scanner(new File("alice.txt"));//located in folder "homework0"
			PrintWriter OutFile = new PrintWriter("alice_in_mixed_case.txt");
			int i = 1; /*Counter*/
			
			System.out.printf("Processing file...");
			
			/*Check if next line exist to continue*/
			while(inFile.hasNextLine()){
			
	            String cur_line = inFile.nextLine();
	            
	            /*When line is occupied*/
				if (!(cur_line.isEmpty())){
					
					/*Reset alternating counter*/
					if(i == 3)
						i = 1;
					
					/*Lower case*/
					if (i == 1){
						OutFile.println(cur_line.toLowerCase());
						i++;
					}
					/*Upper Case*/
					else if (i == 2){
						OutFile.println(cur_line.toUpperCase());
						i++;
					}
				}
				/*If line is blank*/
				else {
					OutFile.printf("\n");
				}
			}
			OutFile.close();
		/*Catch error and print*/
		} catch (FileNotFoundException e) {
			System.out.printf("File Not Found! Please place it in the same directory as this file!\n");
			e.printStackTrace();
		}	
		System.out.println("Finshed!\nOutput File: alice_in_mixed_case.txt");
	}
}
