package com.sramsey.hw9.nine;

import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import org.jgrapht.traverse.BreadthFirstIterator;

public class Problem1 {

	public static void main(String[] args){
        SimpleGraph<String, DefaultEdge> g = new SimpleGraph<String, DefaultEdge>(DefaultEdge.class);
        BreadthFirstIterator<String, DefaultEdge> i;

        // add the vertices
        g.addVertex("A");
        g.addVertex("B");
        g.addVertex("C");
        g.addVertex("D");
        g.addVertex("E");
        g.addVertex("E");
        g.addVertex("F");
        g.addVertex("G");
        g.addVertex("F");
        g.addVertex("H");
        g.addVertex("I");
        g.addVertex("J");
        g.addVertex("K");
        g.addVertex("L");

        // add edges to create a circuit
        g.addEdge("A", "B");
		g.addEdge("A", "C");
		g.addEdge("A", "D");
		g.addEdge("B", "E");
		g.addEdge("B", "F");
		g.addEdge("C", "D");
		g.addEdge("C", "G");
		g.addEdge("D", "E");
		g.addEdge("D", "H");
		g.addEdge("E", "F");
		g.addEdge("E", "I");
		g.addEdge("F", "J");
		g.addEdge("G", "H");
		g.addEdge("G", "K");
		g.addEdge("H", "K");
		g.addEdge("H", "I");
		g.addEdge("I", "L");
		g.addEdge("I", "J");
		g.addEdge("K", "L");
		g.addEdge("J", "L");

		i = new BreadthFirstIterator<String, DefaultEdge>(g);
		while(i.hasNext()){
				System.out.print(i.next());
				if(i.hasNext()){
					System.out.print(" - ");
				}
		}
    }
}
