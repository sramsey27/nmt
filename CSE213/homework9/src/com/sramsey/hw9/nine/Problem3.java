package com.sramsey.hw9.nine;

import org.jgrapht.alg.PrimMinimumSpanningTree;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleWeightedGraph;

public class Problem3 {

	public static void main(String[] args){
        SimpleWeightedGraph<String, DefaultWeightedEdge> g = new SimpleWeightedGraph<String, DefaultWeightedEdge>(DefaultWeightedEdge.class);
        PrimMinimumSpanningTree<String, DefaultWeightedEdge> minitree;
        
        // add the vertices
        g.addVertex("A");
        g.addVertex("B");
        g.addVertex("C");
        g.addVertex("D");
        g.addVertex("E");
        g.addVertex("E");
        g.addVertex("F");
        g.addVertex("G");
        g.addVertex("F");
        g.addVertex("H");
        g.addVertex("I");
        g.addVertex("J");
        g.addVertex("K");
        g.addVertex("L");

        // add edges to create a circuit
        g.setEdgeWeight(g.addEdge("A", "B"), 3);
		g.setEdgeWeight(g.addEdge("A", "C"), 5);
		g.setEdgeWeight(g.addEdge("A", "D"), 4);
		g.setEdgeWeight(g.addEdge("B", "E"), 3);
		g.setEdgeWeight(g.addEdge("B", "F"), 6);
		g.setEdgeWeight(g.addEdge("C", "D"), 2);
		g.setEdgeWeight(g.addEdge("C", "G"), 4);
		g.setEdgeWeight(g.addEdge("D", "E"), 1);
		g.setEdgeWeight(g.addEdge("D", "H"), 5);
		g.setEdgeWeight(g.addEdge("E", "F"), 2);
		g.setEdgeWeight(g.addEdge("E", "I"), 4);
		g.setEdgeWeight(g.addEdge("F", "J"), 5);
		g.setEdgeWeight(g.addEdge("G", "H"), 3);
		g.setEdgeWeight(g.addEdge("G", "K"), 6);
		g.setEdgeWeight(g.addEdge("H", "K"), 7);
		g.setEdgeWeight(g.addEdge("H", "I"), 6);
		g.setEdgeWeight(g.addEdge("I", "L"), 5);
		g.setEdgeWeight(g.addEdge("I", "J"), 3);
		g.setEdgeWeight(g.addEdge("K", "L"), 8);
		g.setEdgeWeight(g.addEdge("J", "L"), 9);

		minitree = new PrimMinimumSpanningTree<String, DefaultWeightedEdge>(g);
		System.out.print(minitree.getMinimumSpanningTreeEdgeSet());
		System.out.println("\nThe total cost of the cost is " + minitree.getMinimumSpanningTreeTotalWeight());	
    }

}
