package com.sramsey.lastexam;

import java.io.*;
import java.util.*;

public class Question6 {

	public static void main(String[] args) {
		
		String fileinput = "cars.txt", curline;
		String[] temp;
		
		ArrayList<String> carlist = new ArrayList<String>();
		ArrayList<String> people = new ArrayList<String>();
		
		try{ /*read in from the file and add to ArrayList*/
			BufferedReader infile1 = new BufferedReader(new FileReader(fileinput));
			try {
				while((curline = infile1.readLine()) != null){
					temp = curline.split("\t");
					carlist.add(temp[1]);
					people.add(temp[0]);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}catch(IOException e){
			System.out.println("File could not be read! Infomation below:");
			e.printStackTrace();
		}	
		
		ArrayList<Integer> carcount = new ArrayList<Integer>();
		int jcount = 0, alexcount = 0, maxcount = 0, petercount = 0, annacount = 0;
		
		for(int i = 0; i < people.size(); i++){
			if(people.get(i).equals("John")){
				jcount++;
			}
			else if(people.get(i).equals("Peter")){
				petercount++;
			}
			else if(people.get(i).equals("Alex")){
				alexcount++;
			}
			else if(people.get(i).equals("Max")){
				maxcount++;
			}
			else if(people.get(i).equals("Anna")){
				annacount++;
			}	

		}
		carcount.add(jcount);
		carcount.add(petercount);
		carcount.add(alexcount);
		carcount.add(maxcount);
		carcount.add(annacount);
		
		int max = carcount.get(0);
		
		for(int i = 1; i < carcount.size(); i++){
			if(carcount.get(i) > max){
				max = carcount.get(i);
			}
		}
		int index = 0;
		for(int i = 0; i < carcount.size(); i++){
			if(carcount.get(i) == max){
				index = i;
			}
		}
		
		if(index == 0){
			System.out.println("John has the most cars");
		}
		if(index == 1){
			System.out.println("Peter has the most cars");
		}
		if(index == 2){
			System.out.println("Alex has the most cars");
		}
		if(index == 3){
			System.out.println("Max has the most cars");
		}
		if(index == 4){
			System.out.println("Anna has the most cars");
		}	
		
		System.out.printf("Cars he/she owns: ");
		for(int i = 0; i < people.size(); i++){
			if(people.get(i).equals("John")){
				System.out.printf(carlist.get(i));
				System.out.printf(", ");
			}
			else if(people.get(i).equals("Peter")){
				if(people.get(i).equals("John")){
					System.out.printf(carlist.get(i));
					System.out.printf(", ");
				}
			}
			else if(people.get(i).equals("Alex")){
				if(people.get(i).equals("John")){
					System.out.printf(carlist.get(i));
					System.out.printf(" ");
				}
			}
			else if(people.get(i).equals("Max")){
				if(people.get(i).equals("John")){
					System.out.printf(carlist.get(i));
					System.out.printf(" ");
				}
			}
			else if(people.get(i).equals("Anna")){
				if(people.get(i).equals("John")){
					System.out.printf(carlist.get(i));
					System.out.printf(" ");
				}
			}		
		}
		System.out.printf("\n");

	}
}
