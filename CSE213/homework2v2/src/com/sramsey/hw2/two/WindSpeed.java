package com.sramsey.hw2.two;

/**
 * Converts between mph and meters per second for the windspeed
 * 
 * @author Jaeden Hendricks, Steven Ramsey, Jimmy Alexander
 * 
 * @version homework2 #2
 * 
 * @bugs none
 */

public class WindSpeed {

	double mph;
	
	/**
	 * Store the wind speed in mph
	 * @param mph, input for wind speed
	 */
	WindSpeed(double mph) {
		this.mph = mph;
	}
	
	/**
	 * Converts wind speed to meters per second
	 * @return Calculated wind speed in meters per seconds
	 */
	double toMetersPerSecond() {
		return mph * 0.44704;
	}
	
	/**
	 * Returns the string representation of wind speed
	 * @return wind speed string representation
	 */
	public String toString() {
		return String.valueOf(mph);
	}
	
	/*Setters and Getters*/
	
	/**
	 * Return the wind speed
	 * @return the wind speed in mph
	 */
	double getSpeed(){
        return mph;
    }
	/**
	 * Sets the wind speed
	 * @param mph, user input wind speed
	 */
    void setSpeed(double mph){
        this.mph = mph;
    }
}

