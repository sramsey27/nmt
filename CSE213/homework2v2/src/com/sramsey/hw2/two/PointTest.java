package com.sramsey.hw2.two;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Test the point class
 * 
 * @author Jaeden Hendricks, Steven Ramsey, Jimmy Alexander
 * 
 * @version homework2 #1
 * 
 * @bugs none
 */

public class PointTest {
        Point p = Point.cartesianInstance(1, 1);// 1.4142135623731 r 45 deg
        Point q = Point.polarInstance(1, 90); //(1,0) cartesian
        Point d = Point.cartesianInstance(0, 0);
        Point e = Point.cartesianInstance(2, 0);
        
        @Test
        public void testEuclideanDistancePoint() {
                assertEquals(d.euclideanDistance(e), 2, 1);
        }

        @Test
        public void testEuclideanDistancePointPoint() {
                assertEquals(Point.euclideanDistance(d, e), 2, 1);
        }

        @Test
        public void testManhattanDistancePointPoint() {
                System.out.println(d.manhattanDistance(e));
                assertEquals(d.manhattanDistance(e), 2, .1);
        }

        @Test
        public void testManhattanDistancePoint() {
                assertEquals(Point.manhattanDistance(d, e), 2, .1);
        }

        @Test
        public void testGetX() {
                assertEquals(p.getX(), 1,1);
        }

        @Test
        public void testGetY() {
                assertEquals(p.getY(), 1,1);
        }

        @Test
        public void testGetR() {
                assertEquals(q.getR(), 1,1);
        }

        @Test
        public void testGetTheta() {
                assertEquals(q.getTheta(), 90,0);
        }

        @Test
        public void testSetX() {
                p.setX(0);
                assertEquals(p.getR(),1,1);
                assertEquals(p.getTheta(),90,1);
        }

        @Test
        public void testSetY() {
                p.setX(0);
                p.setY(1);
                assertEquals(p.getR(),1,1);
                assertEquals(p.getTheta(),90,1);
        }

        @Test
        public void testSetR() {
                p.setR(1);
                p.setTheta(0);
                assertEquals(p.getX(),0,1);
                assertEquals(p.getY(),0,1);
        }

        @Test
        public void testSetTheta() {
                p.setR(1);
                p.setTheta(0);
                assertEquals(p.getX(),0,1);
                assertEquals(p.getY(),0,1);
        }

}