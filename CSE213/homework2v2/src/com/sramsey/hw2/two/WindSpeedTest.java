package com.sramsey.hw2.two;

/*J-unit imports*/
import static org.junit.Assert.*;
import org.junit.*;

public class WindSpeedTest {

	WindSpeed Test1 = new WindSpeed(7.2);
	
	@Test
	public void WindTest(){
		assertNotNull(Test1); //Check to see if it went through okay
	}
	
	@Test
	public void MetersTest(){
		assertEquals(3.218688, (Test1.toMetersPerSecond()), 1); //Epsilon offset
	}
	
	@Test
	public void tostringTest(){
		assertEquals("7.2", Test1.toString());
	}
	
	@Test
	public void SpeedTest(){
		assertEquals(7.2, Test1.getSpeed(), 1);
	}
	
	/*Set Speed is Void, this won't work...*/
	
}
