package com.sramsey.hw2.two;

import java.text.DecimalFormat;
import java.util.Scanner;

/**
 * Main class for the weather classes, prints the wind chill and
 * collects user input for windspeed etc.
 * 
 * @author Jaeden Hendricks, Steven Ramsey, Jimmy Alexander
 * 
 * @version homework2 #2
 * 
 * @bugs none
 */


public class Weather {
	
	public static void main(String[] args) {
		double temp, mph;
		String unit;
		
		Scanner scan = new Scanner(System.in);
		DecimalFormat dec = new DecimalFormat("#.00");
		
		System.out.printf("Enter a temperature: ");
		temp = scan.nextDouble();
		
		System.out.printf("Enter temperature units [C/F/K]: ");
		unit = scan.next();
		unit.toUpperCase();
		//System.out.println("Unit = " + unit);//Case won't matter for user
		
		System.out.printf("Enter wind speed (mph): ");
		mph = scan.nextDouble();
		
		if(unit.equals("C")){
			Temperature.celsiusInstance(temp);
		}
		else if(unit.equals("F")){
			Temperature.fahrenheitInstance(temp);
		}
		else if(unit.equals("K")){
			Temperature.kelvinInstance(temp);
		}
		else{
			System.out.println("Invalid temperature units given. Ending process...\n");
			System.exit(0);
		}
		
		// From the previous switch, you should be able to somehow call up Temperature's fahrenheit, celsius, & kelvin values
		// but I don't know the call to do so.
		
		// All that needs to be done is throw the temperature value and wind speed into WindChill.getWindChill
		// and convert the wind speed using WindSpeed.toMetersPerSecond before throwing them into WindChill.GetWindChillWatts
		
		//The next line tries using Temperature's getter/setters, but they are incompatible since WindChill's methods are static
		//WindChill.getWindChill(Temperature.getFahrenheit(), mph);
		
		//I did not see another way to get this done, so static it is for now... - Steven
		
		double F = Temperature.getFahrenheit();
		System.out.println("The current wind chill is " + dec.format(WindChill.getWindChill(F, mph)));
		double C = Temperature.getCelsius();
		System.out.println("or in Watts per meter squared the wind chill is " + dec.format(WindChill.getWindChillWatts((C), mph)));
		scan.close();
	}
}
