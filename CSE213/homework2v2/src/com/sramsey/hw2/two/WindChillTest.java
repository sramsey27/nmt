package com.sramsey.hw2.two;

import static org.junit.Assert.*;
import org.junit.*;

/**
 * Test the WindChill class
 * 
 * @author Jaeden Hendricks, Steven Ramsey, Jimmy Alexander
 * 
 * @version homework2 #1
 * 
 * @bugs none
 */

public class WindChillTest {
	
	@Test 
	public void chillTest() {
		Object Chill = WindChill.getWindChill(30, 30);
		assertEquals(Chill, 14.880302092245604);
	}
	
	@Test
	public void watt_Test(){
		Object Watt = WindChill.getWindChillWatts(30, 30);
		assertEquals(Watt, 0.0);
	}
}
