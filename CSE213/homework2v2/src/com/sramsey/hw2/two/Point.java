package com.sramsey.hw2.two;

/**
 * Stores both the Cartesian and polar coordinates of the point.
 * 
 * @author Jaeden Hendricks, Steven Ramsey, Jimmy Alexander
 * 
 * @version homework2 #1
 * 
 * @bugs none
 */

public class Point {
    private double x;
    private double y;
    private double r;
    private double theta;
    private enum Type{Cartesian, Polar};
    private Type type;
    
    private Point(double a, double b, Type type){
        this.type = type;
        switch (type) {
        case Cartesian:
            this.x = a;
            this.y  = b;
            this.r = Math.sqrt(x*x + y * y);
            this.theta = Math.atan(y/x);
            break;
        case Polar:
            this.r = a;
            this.theta = b;
            this.x = Math.cos(theta) * r;
            this.y = Math.sin(theta) * r;
            break;
        default:
            break;    
        }          
    }
  
    /**
     * Return object of Cartesian Instance
     * @param x, coordinate value of x
     * @param y, coordinate value of y
     * @return
     */
    public static Point cartesianInstance(double x, double y){
        return new Point(x, y, Type.Cartesian);
    }
    
    /**
     * Return object for polar instance
     * @param r, 
     * @param theta
     * @return
     */
    public static Point polarInstance(double r, double theta){
        return new Point(r, theta, Type.Polar);  
    }
    
    /**
     * Returns the Euclidean Distance
     * @param q, first point
     * @return the object of eudlidean distance
     */
    public double euclideanDistance(Point q){
        return Point.euclideanDistance(this, q);
    }

    /**
     * Calculates the Euclidean Distance
     * @param p, First X coordinate
     * @param q, Second Y coordinate 
     * @return calcuated Euclidean Distance
     */
    
    public static double euclideanDistance(Point p, Point q) {
        return Math.sqrt(Math.pow((p.getX() - q.getX()), 2) + Math.pow((p.getY() - q.getY()),2)); 
    }

    /**
     * Calculates the Manhattan Distance
     * @param p, First X point
     * @param q, Second Y point
     * @return the calcuated Manhattan distance
     */
    public static double manhattanDistance(Point p, Point q){
        return Math.abs(p.x - q.x) + Math.abs(p.y - q.y);
    }
    
    /**
     * Returns the Manhattan Distance
     * @param q, First point
     * @return the manhattan distance
     */
    public double manhattanDistance(Point q){
        return manhattanDistance(this,  q);
    }
    
    /**
     * Returns the value of x
     * @return the x value
     */
    public double getX() {
        return this.x;
    }
    
    /**
     * Returns the value of y
     * @return value of y
     */
    public double getY() {
        return this.y;
    }
    
    /**
     * Return the value for R
     * @return value of R
     */
    public double getR() {
        return this.r;
    }
    
    /**
     * Return the value of theta
     * @return value of theta
     */
    public double getTheta(){
        return this.theta;
    }
    
    /**
     * Sets the value for x, r and theta
     * @param x, is the x coordinate
     */
    public void setX(double x){
        this.x = x;
        this.r = Math.sqrt(x*x + y * y);
        this.theta = Math.atan(y/x);
        
    }
    
    /**
     * Sets the value for Y, r, and theta
     * @param y, the Y coordinate
     */
    public void setY(double y){
        this.y = y;
        this.r = Math.sqrt(x*x + y * y);
        this.theta = Math.atan(y/x);
    }
    
    /**
     * Sets values for r, x and y
     * @param r, the value of r /radius
     */
    public void setR(double r){
        this.r = r;
        this.x = Math.cos(theta) * r;
        this.y = Math.sin(theta) * r;
    }
    
    /**
     * Sets the values for theta, x and y
     * @param theta, the value of theta
     */
    public void setTheta(double theta){
        this.theta = theta;
        this.x = Math.cos(theta) * r;
        this.y = Math.sin(theta) * r;
    }
    
    /**
     * Returns a string representation of the point data
     * @Return point data in string format
     */
    public String toString(){
        switch (type){
        case Cartesian:
            return "Cartesian Point: (" + x + "," +  y + ")";
        case Polar:
            return "Polar Point: (" + r + "," + theta + ")";
        default:
            return "invalid type";   
        }
    }    
}
