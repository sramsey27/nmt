package com.sramsey.hw2.two;

import static org.junit.Assert.*;
import org.junit.*;

public class TemperatureTest {
	
    enum Type{FAHRENHEIT, CELSIUS, KELVIN};
    public Type type;
    
    @Test
    public void Fehernhiet_InstanceTest(){
    	System.out.println(Temperature.fahrenheitInstance(50));
    	assertEquals(50.0 ,Temperature.getFahrenheit(), 1);
    	assertNotNull(Temperature.getFahrenheit());
		assertEquals(50.0 ,Temperature.getFahrenheit(), 1);
    }
    
    @Test
    public void Celsius_InstanceTest(){
    	System.out.println(Temperature.celsiusInstance(50));
		assertNotNull(Temperature.celsiusInstance(50));
		assertNotNull(Temperature.getCelsius());
		assertEquals(50.0 ,Temperature.getCelsius(), 1);
    }
    
    @Test
	public void Kelvin_InstanceTest(){
		System.out.println(Temperature.kelvinInstance(50));
		assertNotNull(Temperature.kelvinInstance(50));
		assertNotNull(Temperature.getKelvin());
		assertEquals(50.0 ,Temperature.getKelvin(), 1);
	}
}
