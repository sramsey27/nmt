package com.sramsey.hw2.two;

/**
 * Calcuates the wind chill and wind chill in watts
 * 
 * @author Jaeden Hendricks, Steven Ramsey, Jimmy Alexander
 * 
 * @version homework2 #2
 * 
 * @bugs none
 */

public class WindChill {

		/**
		 * Takes in temperature (t) in Fahrenheit and windspeed (w) in miles/hour
		 * and calculates the wind chill.
		 * @param t, temperature
		 * @param w, wind speed (mph)
		 * @return the calculated wind chill
		 */
		static Object getWindChill(double t, double w) {
			if (t > 50 || w < 3)
				return 0.0;
			else
				return 35.74 + (0.6215 * t) - (35.75 * Math.pow(w, 0.16)) + (0.4275 * t * Math.pow(w, 0.16));
		}
		
		/**
		 * Takes in temperature (t) in Celsius and windspeed (w) in meters/sec 
		 * and calculates the wind chill in watts
		 * @param t, temperature value
		 * @param w, wind speed (mph)
		 * @return calculated wind chill in watts
		 */
		static Object getWindChillWatts(double t, double w) {
			if (t > 10 || w < 1.34112)
				return 0.0;
			else
				return (12.1452 + 11.6222 * Math.sqrt(w) - 1.16222 * w) * (33 - t);
		}	
}
