package com.sramsey.hw2.two;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Test the rectangle class
 * 
 * @author Jaeden Hendricks, Steven Ramsey, Jimmy Alexander
 * 
 * @version homework2 #1
 * 
 * @bugs none
 */

public class RectangleTest {
        Rectangle r = new Rectangle();
        
        @Test
        public void testArea() {
                assertEquals(r.area(),1,1);
        }

        @Test
        public void testPerimeter() {
                assertEquals(r.perimeter(), 4,1);
        }

        @Test
        public void testDiagonalLength() {
                assertEquals(r.diagonalLength(),Math.sqrt(2), .1);
        }

        @Test
        public void testDistanceFromOrigin() {
                assertEquals(r.distanceFromOrigin(),0,1);
        }

        @Test
        public void testManhattanDistanceFromOrigin() {
                assertEquals(r.manhattanDistanceFromOrigin(),0,1);
        }

        @Test
        public void testGetHeight() {
                assertEquals(r.getHeight(), 1,1);
        }

        @Test
        public void testGetWidth() {
                assertEquals(r.getWidth(), 1,1);
        }

        @Test
        public void testGetX() {
                assertEquals(r.getX(), 0,1);
        }

        @Test
        public void testGetY() {
                assertEquals(r.getY(), 0,1);
        }

}
