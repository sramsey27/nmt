package com.sramsey.hw2.two;

/**
 * Stores the infomation of a rectangle and does various
 * calculations such as area, perimeter, etc...
 * 
 * @author Jaeden Hendricks, Steven Ramsey, Jimmy Alexander
 * 
 * @version homework2 #1
 * 
 * @bugs none
 */

public class Rectangle {

    private double height; //the height of the rectangle
    private double width; //the width of the rectangle
    private Point p;
    private Point origin = Point.cartesianInstance(0,0);
    
    /**
     * Initializes a unit cube
     */
    Rectangle(){
        this(0, 0, 1.0, 1.0);
    }
    
    /**
     * Initializes a rectangle given width and height
     * @param width width of a rectangle
     * @param height height of a rectangle
     */
    Rectangle(double width, double height){
        this(0, 0, width, height);
    }
    
    /**
     * Inititializes a rectangle with given criteria
     * @param x x offset from origin
     * @param y y offste from origin
     * @param width of a rectangle
     * @param height of a rectangle
     */
    Rectangle(double x, double y, double width, double height){
        p = Point.cartesianInstance(x,y);
        this.height = height;
        this.width = width;
    }
    
    /**
     * calculates area of the rectangle
     * @return area
     */
    double area(){
        return width * height;
    }
    
    /**
     * calculates perimeter of the rectangle
     * @return perimeter
     */
    double perimeter(){
        return 2 * width + 2 * height;
    }
    
    /**
     * Calculates diagonalLength of the rectangle
     * @return calcuated diagonalLength
     */
    double diagonalLength(){
        return Math.sqrt(width * width + height * height);
    }
    
    /**
     * Calculates distanceFromOrigin of the rectangle
     * @return the calcuated distanceFromOrigin
     */
    double distanceFromOrigin(){
        return p.euclideanDistance(origin);
    }
    
    /**
     * Calculates manhattanDistance
     * @return the calculated manhattanDistance
     */
    double manhattanDistanceFromOrigin() {
        return p.manhattanDistance(origin);    
    }
    
    /**
     * Outputs the results as a string
     * @return finished strings 
     */
    public String toString() {
        String mystring = String.format("width = %.2f\theight = %.2f\tx = %2f\ty = %.2f\n\n",
width, height, p.getX(), p.getY());
        return mystring;
    }
    
    /**
     * Receieve Height
     * @return the height
     */
    double getHeight(){
        return height;
    }
    /**
     * Set the height variable
     * @param height 
     */
    void setHeight(double height){
        this.height = height;
    }
    /**
     * Set the width variable
     * @return width
     */
    double getWidth(){
        return width;
    }
    /**
     * setter of width
     * @param width of rectangle
     */
    void setWidth(double width){
        this.width = width;
    }
    
    /**
     * return the x coordinate setter
     * @return x coordinate
     */
    double getX(){
        return p.getX();
    }
    
    /**
     * Set the x coordinate setter
     * @param x coordinate
     */
    void setX(double x){
        p.setX(x);
    }
    
    /**
     * Return the y coordinate setter
     * @return y coordinate
     */
    double getY(){
        return p.getY();
    }
    /**
     * Set the y coordinate
     * @param y coordinate
     */
    void setY(double y){
        p.setY(y);
    }
}