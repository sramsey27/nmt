package com.sramsey.hw2.two;

/**
 * Sets the appropreiate values for temperatures.
 * 
 * @author Jaeden Hendricks, Steven Ramsey, Jimmy Alexander
 * 
 * @version homework2 #2
 * 
 * @bugs none
 */

public class Temperature {
	
	private static double kelvin;
	private static double fahrenheit;
	private static double celsius;
    enum Type{FAHRENHEIT, CELSIUS, KELVIN};
    public Type type;
    
    Temperature(double value, Type type){
    	this.type = type;
    	switch (type) {
    	case FAHRENHEIT:
    		Temperature.fahrenheit = value;
    		Temperature.kelvin = (fahrenheit + 459.67) * (5/9);
    		Temperature.celsius = (fahrenheit - 32.0) * (5/9);
    		break;
    	case CELSIUS:
    		Temperature.celsius = value;
    		Temperature.fahrenheit = (celsius * (9/5)) + 32.0;
    		Temperature.kelvin = celsius + 273.15;
    		break;
    	case KELVIN:
    		Temperature.kelvin = value;
    		Temperature.fahrenheit = (kelvin * (9/5)) - 459.67;
    		Temperature.celsius = kelvin - 273.15;
    		break;
    	default:
    		System.out.printf("Invalid type given\n");
    		break;
    	}
    }        
    /**
     * Returns a new object of Temperature with the appropreiate
     * Type and value corresponding to the type.
     * @param value, the corresponding temperature value
     * @return the new temperature object
     */
    public static Temperature kelvinInstance(double value) {
    	return new Temperature(value, Type.KELVIN);
    }
    /**
     * Returns a new object of Temperature with the appropreiate
     * Type and value corresponding to the type.
     * @param value, the corresponding temperature value
     * @return the new temperature object
     */
    public static Temperature celsiusInstance(double value) {
    	return new Temperature(value, Type.CELSIUS);
    }
    /**
     * Returns a new object of Temperature with the appropreiate
     * Type and value corresponding to the type.
     * @param value, the corresponding temperature value
     * @return the new temperature object
     */
    public static Temperature fahrenheitInstance(double value) {
    	return new Temperature(value, Type.FAHRENHEIT);
    }
    /**
     * Returns the string representation of the temperature
     * @return appropreiate string
     */
    public String toString(){
        switch (type){
	        case FAHRENHEIT:
	            return fahrenheit + " degrees Fahrenheit";
	        case CELSIUS:
	            return celsius + " degrees Celsius";
	        case KELVIN:
	        	return kelvin + " degrees Kelvin";
	        default:
	            return "invalid type given\n";
	        }
        }
    /*Setters and Getters*/
    
    /**
     * Sets the value for Kelvin
     */
    public void setKelvin(double kelvin) {
    	Temperature.kelvin = kelvin;
    }
    
    /**
     * Sets the value for fahrenheit
     * @param fahrenheit, passed in value for fahreheit 
     */
    public void setFahrenheit(double fahrenheit) {
    	Temperature.fahrenheit = fahrenheit;
    }

    /**
     * Sets the value for celsius
     * @param celsius, the value for celsius
     */
    public void setCelsius(double celsius) {
    	celsius = Temperature.celsius;
    }
    
    /**
     * Returns the value for Kelvin
     * @return Kelvin Value
     */
    public static double getKelvin() {
    	return kelvin;
    }

    /**
     * Return value for Fahrenheit
     * @return Fahrenheit Value
     */
    public static double getFahrenheit() { //
    	return fahrenheit;
    }

    /**
     * Return value for Celsius
     * @return Celsius Value
     */
    public static double getCelsius() {
    	return celsius;
    }
}