package com.sramsey.hw8.eight.com.jhendricks.hw8.one;

/**
 * Implements a depth first search, keeping track of visited nodes
 * 
 * @author Jaeden Hendricks, Steven Ramsey, Jimmy Alexander
 * 
 * @version Homework 8 #1
 * 
 * @bugs none
 */

import java.util.ArrayList;

public class DFS {
	
	static ArrayList<Vertex> visited = new ArrayList<>();
	
	public static ArrayList<Vertex> dfs(Graph g) {
		
		for (Vertex u : g.getVertices()) {
			u.setColor(Vertex.color.White);
			u.setPred(null);
		}
		for (Vertex u : g.getVertices()) {
			if (u.colorString() == "White") {
				dfsVisit(g, u);
				//visited.add(u);
			}
		}
		return visited;
	}
	
	public static void dfsVisit(Graph g, Vertex u) {
		u.setColor(Vertex.color.Gray);
		for (Vertex v : g.adjacentTo(u.getName())) {
			if (v.colorString() == "White") {
				//visited.add(v);
				v.setPred(u.getName());
				dfsVisit(g, v);
			}
		}
		u.setColor(Vertex.color.Black);
		visited.add(u);
	}
	
	public static ArrayList<Vertex> dfsAll(Graph g) {
		return DFS.dfs(g);
	}
}
