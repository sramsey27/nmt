package com.sramsey.hw8.eight.com.jhendricks.hw8.one;

import java.util.ArrayList;

public class BFS {
	public static ArrayList<Vertex> bfs(String v, Graph g, String end){
		Vertex strt = g.getVertex(v);
		Vertex u;
		ArrayList<Vertex> visited = new ArrayList<Vertex>();
		for(Vertex w : g.adjacentTo(strt.getName())){
			w.setColor(Vertex.color.White);
			w.distance = -1;
			w.setPred(null);
		}
		strt.setColor(Vertex.color.Gray);
		strt.distance = 0;
		strt.setPred(null);
		
		QueueInterface Q = new Queue();
		
		Q.enque(strt);
		u = strt;
		while (!Q.isEmpty() && !u.getName().equals(end)){
			u = (Vertex) Q.deque();
			for(Vertex w : g.adjacentTo(u.getName())){
				if (w.getColor() == Vertex.color.White){
					w.setColor(Vertex.color.Gray);
					w.distance = u.distance + 1;
					w.setPred(u.getName());
					Q.enque(w);
					
				}
			}
			u.setColor(Vertex.color.Black);
			visited.add(u);
		}
		return visited;
	}
	
	public static ArrayList<Vertex> bfsAll(String v, Graph g){
		return BFS.bfs(v, g, null);
	}
}
