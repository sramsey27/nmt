package com.sramsey.hw8.eight.com.jhendricks.hw8.one;

import java.util.ArrayDeque;

public class Queue implements QueueInterface{
	private int count;
	private ArrayDeque<Object> list;
	
	Queue(){
		list = new ArrayDeque<Object>();
	}
	
	@Override
	public void enque(Object x) {
		list.add(x);
		count++;
		
	}

	@Override
	public Object deque() {
		// TODO Auto-generated method stub
		count--;
		return list.removeLast();
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return list.isEmpty();
	}

	@Override
	public Object peek() {
		// TODO Auto-generated method stub
		return list.peek();
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return count;
	}

}
