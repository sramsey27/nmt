import java.util.Scanner;

public class Student {

	@SuppressWarnings("null")
	public static void main(String[] args) {
		
		int N = 0, i = 0;
	
		System.out.println("Enter the number of Students : ");
		Scanner Input_int = new Scanner(System.in);
		N = Input_int.nextInt();
		
		String[] firstname = new String[N];
		String[] lastname = new String[N];
		double[] gpa = new double[N];
			
		Scanner Input_fname = new Scanner(System.in);
		Scanner Input_lname = new Scanner(System.in);
		Scanner Gpa = new Scanner(System.in);
			
		for (i = 1; i <= N; i++){
			System.out.printf("Enter the first name of student[#%d]: ", i);
			firstname[i] = Input_fname.next();
			System.out.printf("Enter the last name of student[#%d]: ", i);
			lastname[i] = Input_lname.next();
			System.out.printf("Enter the GPA of student[#%d]: ", i);
			gpa[i] = Gpa.nextInt();
		}
		
		for(i = 1; i <= N; i++){
			System.out.printf("Student[#%d]\nFirstname: %s\nLastName: %s\nGPA: %lf", firstname[i], lastname[i], gpa[i]);
		}
	}
}
