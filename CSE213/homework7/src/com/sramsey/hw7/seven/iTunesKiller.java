package com.sramsey.hw7.seven;

import java.io.*;
import java.util.*;

public class iTunesKiller {

	String title, artist; /*Instance Variables*/
	
	iTunesKiller(){
		this.artist = "no artist";
		this.title = "no title";
	}
	iTunesKiller(String title, String artist){
		this.artist = artist;
		this.title = title;
	}
	
	/**
	 * sets the current song title
	 * @param title the song title to be set
	 */
	@SuppressWarnings("unused")
	private void setTitle(String title){
		this.title = title;
	}
	
	/**
	 * sets the current artist
	 * @param artist the artist to be set
	 */
	@SuppressWarnings("unused")
	private void setArtist(String artist){
		this.artist = artist;
	}
	
	/**
	 * return the current title
	 * @return the current title
	 */
	@SuppressWarnings("unused")
	private String getTitle(){
		return title;
	}
	
	/**
	 * returns the current artist
	 * @return the current artist
	 */
	@SuppressWarnings("unused")
	private String getArtist(){
		return artist;
	}
	
	/**
	 * Takes in elements from arraylist and adds to Playlist Deque
	 * @param tunes the current song
	 * @param playlist the arraydeque for playlist
	 */
	private static void addSong(iTunesKiller tunes, ArrayDeque<iTunesKiller> playlist){
		playlist.add(tunes);
	}
	
	/**
	 * Goes back a song by adding front of playlist to top of history stack
	 * @param history the arraydeque for history
	 * @param playlist the arraydeque for playlist
	 */
	private static void Back(ArrayDeque<iTunesKiller> history, ArrayDeque<iTunesKiller> playlist){
		playlist.addFirst(history.peek());
	}
	
	/**
	 * Plays the song by popping from playlist and adding to history stack also prints it
	 * @param playlist
	 * @param history
	 */
	private static void Play(ArrayDeque<iTunesKiller> playlist, ArrayDeque<iTunesKiller> history){
		System.out.println(playlist.peek());
		history.add(playlist.peek());	
		playlist.pop();
	}

	/**
	 * Skips the song at the head of the playlist, pops it from playlist then places it
	 * on the skipped stack
	 * @param playlist the arraydeque/stack for playlist
	 * @param skipped the arraydeque/stack for skipped songs
	 */
	private static void skipSong(ArrayDeque<iTunesKiller> playlist, ArrayDeque<iTunesKiller> skipped){
		skipped.add(playlist.peekFirst());
		playlist.pop();
	}
	
	/**
	 * Undos the skipped action by adding the top skipped song back to the top of playlist
	 * @param playlist the playlist arraydeque/stack
	 * @param skipped the skipped arraydeque/stack
	 */
	private static void undoSkippedSong(ArrayDeque<iTunesKiller> playlist, ArrayDeque<iTunesKiller> skipped){
		playlist.addFirst(skipped.pop());
	}
	
	/**
	 * Displays the History ArrayDeque using an iterator
	 * @param history the history arraydeque/stack
	 */
	private static void viewHistory(ArrayDeque<iTunesKiller> history){
		Iterator<iTunesKiller> hist = history.iterator();
		while(hist.hasNext()){
			System.out.println(hist.next().toString());
		}
	}
	
	/**
	 * Displays the Playist arraydeque using an iterator
	 * @param Playlist the playlist arraydeque/stack
	 */
	private static void viewPlayList(ArrayDeque<iTunesKiller> Playlist){
		Iterator<iTunesKiller> Plist = Playlist.iterator();
		while(Plist.hasNext()){
			System.out.println(Plist.next().toString());
		}
	}
	
	/**
	 * Displays the Skipped arraydeque using an iterator
	 * @param skipped the skipped arraydeque
	 */
	private static void viewSkipped(ArrayDeque<iTunesKiller> skipped){
		Iterator<iTunesKiller> Slist = skipped.iterator();
		while(Slist.hasNext()){
			System.out.println(Slist.next().toString());
		}
	}
	
	/**
	 * Outputs title and artist in requested format
	 */
	public String toString(){
		return artist + "\n\t" + title;
	}
	
	public static void main(String[] args) {
		
		ArrayDeque<iTunesKiller> Playlist = new ArrayDeque<iTunesKiller>(); //playlist
		ArrayDeque<iTunesKiller> History = new ArrayDeque<iTunesKiller>(); //history list
		ArrayDeque<iTunesKiller> Skipped = new ArrayDeque<iTunesKiller>(); //Skipped List
		ArrayList<iTunesKiller> inlist = new ArrayList<iTunesKiller>(); //ArrayList from file
		
		String fileinput = "songs.txt", curline;
		String[] temp = null; /*for split*/
		
		try{ /*read in from the file and add to ArrayList*/
			BufferedReader infile = new BufferedReader(new FileReader(fileinput));
			try {
				while((curline = infile.readLine()) != null){
					temp = curline.split("- ");
					inlist.add(new iTunesKiller(temp[0], temp[1]));
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}catch(IOException e){
			System.out.println("File could be read! Infomation below:");
			e.printStackTrace();
		}	
		
		/*Start adding Songs to PlayList the way the PDF describes*/
		int inlist_mid = inlist.size() / 2; //midpoint
		addSong(inlist.get(inlist_mid), Playlist);
		for(int i = 1; i <= 5; i++){ //right 5
			addSong(inlist.get(i + inlist_mid), Playlist);
			addSong(inlist.get(inlist_mid - i), Playlist);
		}
		
		/*Requested Calls*/
		iTunesKiller.viewPlayList(Playlist);
		iTunesKiller.Play(Playlist, History);
		iTunesKiller.Play(Playlist, History);
		iTunesKiller.viewHistory(History);
		iTunesKiller.skipSong(Playlist, Skipped);
		iTunesKiller.viewSkipped(Skipped);
		iTunesKiller.undoSkippedSong(Playlist, Skipped);
		iTunesKiller.Play(Playlist, History);
		iTunesKiller.Back(History, Playlist);
		iTunesKiller.Play(Playlist, History);
		iTunesKiller.viewHistory(History);
		iTunesKiller.addSong(new iTunesKiller("The Letter", "The Boxtops"), Playlist);
		iTunesKiller.Play(Playlist, History);
		iTunesKiller.Play(Playlist, History);
		iTunesKiller.Play(Playlist, History);
		iTunesKiller.Play(Playlist, History);
		iTunesKiller.viewHistory(History);
		iTunesKiller.skipSong(Playlist, Skipped);
		iTunesKiller.skipSong(Playlist, Skipped);
		iTunesKiller.skipSong(Playlist, Skipped);
		iTunesKiller.viewSkipped(Skipped);
		iTunesKiller.viewPlayList(Playlist);
	}
}