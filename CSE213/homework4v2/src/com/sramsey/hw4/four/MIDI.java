package com.sramsey.hw4.four;

/**
 * Sets the appropriate values for MIDI
 * 
 * @author Jaeden Hendricks, Steven Ramsey, Jimmy Alexander
 * 
 * @version homework4 #1
 * 
 * @bugs none
 */

public class MIDI {

	static NoteValue length;
	private static int midiNumber = 0;
	private static double frequency;
	
	public MIDI(){
		this("F2", NoteValue.HALF);
	}
	private MIDI(String note, NoteValue length){
		int offset = 0;
		int octave = (Character.getNumericValue((note.charAt(note.length() - 1))));
		if(note.matches("[A-G]{1}[#]?[-]?[0-9]{1}")){
			
			if(note.startsWith("A")){
				offset = 0;
				if(isSharp(note)){
					offset++;
				}	
				MIDI.midiNumber = (69 + offset) + (octave - 4) * 12; 
			}
			if(note.startsWith("B")){
				offset = 2;
				MIDI.midiNumber = (69 + offset) + (octave - 4) * 12; 
			}
			if(note.startsWith("C")){
				offset = -9;
				if(isSharp(note)){
					offset++;
				}
				MIDI.midiNumber = (69 + offset) + (octave - 4) * 12;
			}
			if(note.startsWith("D")){
				offset = -7;
				if(isSharp(note)){
					offset++;
				}
				MIDI.midiNumber = (69 + offset) + (octave - 4) * 12;
			}
			if(note.startsWith("E")){
				offset = -5;
				MIDI.midiNumber = (69 + offset) + (octave - 4) * 12;
			}
			if(note.startsWith("F")){
				offset = -4;
				if(isSharp(note)){
					offset++;
				}
				MIDI.midiNumber = (69 + offset) + (octave - 4) * 12; 
			}
			if(note.startsWith("G")){
				offset = -2;
				if(isSharp(note)){
					offset++;
				}
				MIDI.midiNumber = (69 + offset) + (octave - 4) * 12;
			}
		}
		else {
			System.out.printf("The input does not match the SNF Form\n");
			System.exit(0);
		}
	}
	public MIDI(int midiNumber, NoteValue length){
		MIDI.frequency = (Math.pow(2, (midiNumber - 69)/12)) * 440;
		MIDI.length = length;
	}
	
	/**
	 * @param newfrequency
	 */
	public void setFrequency(double newfrequency){
		MIDI.frequency = newfrequency;		
	}
	
	/**
	 * @return the calculated frequency
	 */
	public static double getFrequency(){
		return frequency;
	}
	
	/**
	 * @return the calculated length
	 */
	public NoteValue getLength(){
		return length;
	}
	
	/**
	 * @return the calculated midi number
	 */
	public static int getMidiNumber() {
		return midiNumber;
	}
	
	/**
	 * @return the calculated frequency
	 */
	public String getNote(String note){
		return note.substring(1);
	}
	
	/**
	 * @return the calculated octave
	 */
	public int getOctave(){
		if(midiNumber < 13)
			return -1;
		else if (midiNumber > 11 && midiNumber < 24)
			return 0;
		else if (midiNumber > 23 && midiNumber < 36)
			return 1;
		else if (midiNumber > 35 && midiNumber < 48)
			return 2;
		else if (midiNumber > 47 && midiNumber < 60)
			return 3;
		else if (midiNumber > 59 && midiNumber < 72)
			return 4;
		else if (midiNumber > 71 && midiNumber < 84)
			return 5;
		else if (midiNumber > 83 && midiNumber < 96)
			return 6;
		else if (midiNumber > 95 && midiNumber < 108)
			return 7;
		else if (midiNumber > 107 && midiNumber < 210)
			return 8;
		else 
			return 9;
	}
	
	/**
	 * @param note the current note
	 * @return boolean true of false
	 */
	public boolean isSharp(String note){
		char temp = note.charAt(1);
		if(temp == '#'){
			return true;
		}
		else{
			return false;
		}
	}
	/**
	 * @param length the length of the note to set
	 */
	public void setLength(NoteValue length){
		MIDI.length = length;
	}
	/**
	 * @param midi the midinumber to set
	 */
	public void setMidiNumber(int Midi){
		MIDI.midiNumber = Midi;
	}
	
	/**
	 * @param octave the current octave
	 * @return nothing
	 */
	public void setOctave(int Octave){
		int i = 0, temp = 0;
		if(Octave < -1 || Octave > 9){
			MIDI.midiNumber = 60;
			return;
		}
		if(Octave == -1){
			MIDI.midiNumber = 0; 
			return;
		}
		if(Octave >= 0){
			for(;i < Octave; i++){
				temp = temp + 12;
				MIDI.midiNumber = MIDI.midiNumber + temp;
			}
		}	
	}
	/**
	 * Increments midinumber if called
	 */
	public void setSharp(){
		MIDI.midiNumber++;
	}
	
	/**
	 * @param note the SNF note
	 */
	public void toString(String note){
		System.out.printf("%s", note);
		System.out.println(midiNumber);
	}
	
}

