package com.sramsey.hw4.four;

public class Tempo {

	private double bpm;
	NoteValue noteValue;
	
	
	Tempo() { // default bpm sets to 60
		bpm = 120.0;
	}
	
	Tempo(double new_bpm) {
		bpm = new_bpm;
	}
	
	public static double noteLength(double bpm, NoteValue noteValue) {
		switch (noteValue) {
			case WHOLE:
				return (60.0 / bpm) * 4.0;
			case HALF:
				return (60.0 / bpm) * 2.0;
			case QUARTER:
				return (60.0 / bpm) * 1.0;
			case EIGHT:
				return (60.0 / bpm) * (1/2);
			case SIXTEEN:
				return (60.0 / bpm) * (1/4);
			default:
				System.out.println("Invalid MIDI NoteValue given\n");
				return 0;
		}
	}
	
	public double getBpm() {
		return bpm;
	}

	public void setBpm(double bpm) {
		this.bpm = bpm;
	}
	
}
