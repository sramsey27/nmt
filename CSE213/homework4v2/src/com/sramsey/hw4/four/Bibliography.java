package com.sramsey.hw4.four;

/**
 * Bibliography Class
 * 
 * @author Jaeden Hendricks, Steven Ramsey, Jimmy Alexander
 * 
 * @version homework4 #2
 * 
 * @bugs none
 */

import java.util.ArrayList;

public class Bibliography {
	protected ArrayList<Author> author;
	protected int year;
	protected String title;
	protected String puborowner;
	
	Bibliography(){
		;
	}
	
	void print(){
		System.out.println("This is a default bibliography and cannot be printed");
	}
	
	ArrayList<Author> toAuthor(String author){
		ArrayList<Author> authors = new ArrayList<Author>(); 
		author = author.toUpperCase();
		if (author.contains(", AND ")){
			author = author.replace(", AND ", ", ");
		}
		else if (author.contains(" AND ")){
			author = author.replace(" AND ", ", ");
		}
		for (String element : author.split(", ")) {
			String[] name = element.split(" ");
			if (name.length == 2){
				authors.add(new Author(name[0], name[1]));
			}
			else if(name.length == 3){
				authors.add(new Author(name[0], name[1], name[2]));
			}
			else{
				authors.add(new Author(" "," "," "));
			}
		}
		
		return authors;
	}

	/**
	 * @return the author
	 */
	public ArrayList<Author> getAuthor() {
		return author;
	}

	/**
	 * @param arrayList the author to set
	 */
	public void setAuthor(String author) {
		this.author = toAuthor(author);
		
	}

	/**
	 * @return the year
	 */
	public int getYear() {
		return year;
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(int year) {
		this.year = year;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the puborowner
	 */
	public String getPuborowner() {
		return puborowner;
	}

	/**
	 * @param puborowner the puborowner to set
	 */
	public void setPuborowner(String puborowner) {
		this.puborowner = puborowner;
	}
}
