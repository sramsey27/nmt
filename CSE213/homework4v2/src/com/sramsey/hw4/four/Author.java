package com.sramsey.hw4.four;

/**
 * Author Class
 * 
 * @author Jaeden Hendricks, Steven Ramsey, Jimmy Alexander
 * 
 * @version homework4 #1
 * 
 * @bugs none
 */

public class Author {
	
	private String first, last, middle;
	
	Author(String f, String l){
		first = f;
		last = l;
		middle = " ";
	}
	
	Author(String f, String m, String l){
		first = f;
		last = l;
		middle = m;
	}

	/**
	 * @return the first
	 */
	public String getFirst() {
		return first;
	}

	/**
	 * @param first the first to set
	 */
	public void setFirst(String first) {
		this.first = first;
	}

	/**
	 * @return the last
	 */
	public String getLast() {
		return last;
	}

	/**
	 * @param last the last to set
	 */
	public void setLast(String last) {
		this.last = last;
	}

	/**
	 * @return the middle
	 */
	public String getMiddle() {
		return middle;
	}

	/**
	 * @param middle the middle to set
	 */
	public void setMiddle(String middle) {
		this.middle = middle;
	}
}

