package com.sramsey.hw4.four;

public enum NoteValue {
		WHOLE, HALF, QUARTER, EIGHT, SIXTEEN;
}
