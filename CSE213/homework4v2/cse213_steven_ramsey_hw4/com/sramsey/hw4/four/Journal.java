package com.sramsey.hw4.four;

/**
 * Jounral Class
 * 
 * @author Jaeden Hendricks, Steven Ramsey, Jimmy Alexander
 * 
 * @version homework4 #2
 * 
 * @bugs none
 */

public class Journal extends Bibliography{
	int volume, beginPage, endPage;
	
	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

	public int getBeginPage() {
		return beginPage;
	}

	public void setBeginPage(int beginPage) {
		this.beginPage = beginPage;
	}

	public int getEndPage() {
		return endPage;
	}

	public void setEndPage(int endPage) {
		this.endPage = endPage;
	}
	
	Journal(String author, int year, String title, String journalName, int volume, int  beginPage, int Endpage){
		this.setAuthor(author);
		this.setYear(year);
		this.setTitle(title);
		this.setPuborowner(journalName);
		this.setVolume(volume);
		this.setBeginPage(beginPage);
		this.setEndPage(Endpage);
		
	}
	
	void print(){
		
		int count = 0;
		for(Author element: this.getAuthor()){
			if (count == this.getAuthor().size() -1){
				System.out.printf("and ");
			}
			System.out.printf("%s, ", element.getLast());
			System.out.printf("%s.%s. ", element.getFirst().charAt(0), this.getAuthor().get(0).getMiddle().charAt(0));
			count++;
		}
		
		
		System.out.printf("%d. %s. %s. ", year, title, puborowner);
		System.out.printf("vol. %d. pp. %d-%d.\n", volume, beginPage, endPage);
	}
}
