package com.sramsey.hw4.four;

/**
 * demo Class, test the various classes
 * 
 * @author Jaeden Hendricks, Steven Ramsey, Jimmy Alexander
 * 
 * @version homework4 #2
 * 
 * @bugs none
 */

public class demo {

	public static void main(String[] args) {
		
		Book dog = new Book("Morgan Ginsberg", 1987, "Readings in Nonmonotonic Reasoning", "Morgan Kaufmann", "Los Altos", "CA", 454);
		dog.print();
	}
}