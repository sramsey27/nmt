package com.sramsey.hw4.four;

/**
 * Chapter Class extends Book
 * 
 * @author Jaeden Hendricks, Steven Ramsey, Jimmy Alexander
 * 
 * @version homework4 #2
 * 
 * @bugs none
 */

public class Chapter extends Book{
	
	Chapter(String author, int year, String title, String publisher,
			String publicationCity, String publicationState, int numberOfPages) {
		super(author, year, title, publisher, publicationCity, publicationState,
				numberOfPages);
	}

}