package com.sramsey.hw4.four;

/**
 * Book Class
 * 
 * @author Jaeden Hendricks, Steven Ramsey, Jimmy Alexander
 * 
 * @version homework4 #2
 * 
 * @bugs none
 */

public class Book extends Bibliography{
	private String publicationCity, publicationState;
	//private int numberOfPages;
	
	Book(String author, int year, String title, String publisher, String publicationCity, String publicationState, int numberOfPages){
		this.setAuthor(author);
		this.setYear(year);
		this.setTitle(title);
		this.setPuborowner(publisher);
		this.setPublicationCity(publicationCity);
		this.setPublicationState(publicationState);
		this.numberOfPages = numberOfPages;
	}
	
void print(){
		
		int count = 0;
		for(Author element: this.getAuthor()){
			if (count == this.getAuthor().size() -1){
				System.out.printf("and ");
			}
			System.out.printf("%s, ", element.getLast());
			System.out.printf("%s.%s. ", element.getFirst().charAt(0), this.getAuthor().get(0).getMiddle().charAt(0));
			count++;
		}
		
		
		System.out.printf("%d. %s. %s, ", year, title, puborowner);
		System.out.printf("%s, %s, %d pp.\n", publicationCity, publicationState, numberOfPages);
	}
	
	int numberOfPages;
	/**
	 * @return the publicationCity
	 */
	public String getPublicationCity() {
		return publicationCity;
	}
	/**
	 * @param publicationCity the publicationCity to set
	 */
	public void setPublicationCity(String publicationCity) {
		this.publicationCity = publicationCity;
	}
	/**
	 * @return the publicationState
	 */
	public String getPublicationState() {
		return publicationState;
	}
	/**
	 * @param publicationState the publicationState to set
	 */
	public void setPublicationState(String publicationState) {
		this.publicationState = publicationState;
	}
	/**
	 * @return the numberOfPages
	 */
	public int getNumberOfPages() {
		return numberOfPages;
	}
	/**
	 * @param numberOfPages the numberOfPages to set
	 */
	public void setNumberOfPages(int numberOfPages) {
		this.numberOfPages = numberOfPages;
	}
	
}
