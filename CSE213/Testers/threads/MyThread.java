package threads;

// Explanation:
// Each MyThread object tries to print numbers from 0 to 300, but are only
// responsible for certain regions of that range (split by indices in this case)

public class MyThread extends Thread
{
   private int startIdx, nThreads, maxIdx;

   public MyThread(int s, int n, int m)
   {
      this.startIdx = s;
      this.nThreads = n;
      this.maxIdx = m;
   }

   @Override
   public void run()
   {
      for(int i = this.startIdx; i < this.maxIdx; i += this.nThreads)
      {
         System.out.println("[ID " + this.getId() + "] " + i);
         // t1, t2, t3 should split by indices, so t1 does index 0, 3, 6, 9...
         // no threads executing right now because they aren't waiting on each other.
         // uncomment next line to have threads work:
         Thread.yield();
      }
   }
}