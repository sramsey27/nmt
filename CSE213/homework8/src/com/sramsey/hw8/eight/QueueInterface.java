package com.sramsey.hw8.eight;

public interface QueueInterface {
	public void enque(Object x);
	public Object deque();
	public boolean isEmpty();
	public Object peek();
	public int size();
}
