package com.sramsey.hw8.eight;

public class Vertex implements Comparable<Vertex> {

	/**
	 * label for Vertex
	 */
	private String name;  
	public static enum color {White, Gray, Black};
	private String pred;
	int distance;
	color c;

	
	
	
	
	public Vertex(String v)
	{
		this.name = v;
		this.pred = null;
		this.c = color.White;
		this.distance = -1;
		
	}
	
	public void setPred(String name){
		this.pred = name;
	}
	
	public String getPred(){
		return this.pred;
	}
	
	public void setColor(color c){
		this.c =c;
	}
	
	public color getColor(){
		return c;
	}
	
	public String colorString(){
		switch (c) {
		case Black:
			return "Black";
		case Gray:
			return "Gray";
		case White:
			return "White";
		default:
			return "ehhh";
		
		}
	}

	public String toString()
	{ 
		return this.name;
	}

	public String getName() {
		return this.name;
	}

	public void print(){
		System.out.printf("%s (%s, %d, %s)\n", this.name, this.colorString(), this.distance, this.pred);
	}
	
	public void print2() {
		System.out.printf("%s (%s, %s)\n", this.name, this.colorString(), this.pred);
	}

	public int compareTo(Vertex other)
	{
		return this.name.compareTo(other.getName());
	}
}