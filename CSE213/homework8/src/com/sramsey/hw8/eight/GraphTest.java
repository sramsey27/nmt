package com.sramsey.hw8.eight;

import java.util.ArrayList;

public class GraphTest {

	public static void main(String[] args) {
		/*graph 1*/
		Graph G1 = new Graph();
		G1.addEdge("A", "B");
		G1.addEdge("A", "E");
		G1.addEdge("B", "F");
		G1.addEdge("E", "F");
		G1.addEdge("B", "C");
		G1.addEdge("F", "G");
		G1.addEdge("C", "G");
		G1.addEdge("C", "D");
		G1.addEdge("G", "H");
		G1.addEdge("D", "H");

		System.out.println("graph 1 BFS starting at A");
		ArrayList<Vertex> vis = BFS.bfsAll("A", G1);
		for (Vertex w: vis){
			w.print();
		}
		/*graph 2*/
		Graph G2 = new Graph();
		G2.addEdge("A", "B");
		G2.addEdge("A", "E");
		G2.addEdge("B", "C");
		G2.addEdge("B", "F");
		G2.addEdge("C", "D");
		G2.addEdge("C", "G");
		System.out.println("graph 2 DFS");
		ArrayList<Vertex> alv = DFS.dfsAll(G2);
		for (Vertex u : alv)
			u.print2();
		
		/*graph 3*/
		Graph G3 = new Graph();
		G3.addEdge("A", "B");
		G3.addEdge("A", "C");
		G3.addEdge("A", "D");
		G3.addEdge("B", "E");
		G3.addEdge("B", "F");
		G3.addEdge("C", "D");
		G3.addEdge("C", "G");
		G3.addEdge("D", "E");
		G3.addEdge("D", "H");
		G3.addEdge("E", "F");
		G3.addEdge("E", "I");
		G3.addEdge("F", "J");
		G3.addEdge("G", "H");
		G3.addEdge("G", "K");
		G3.addEdge("H", "K");
		G3.addEdge("H", "I");
		G3.addEdge("I", "L");
		G3.addEdge("I", "J");
		G3.addEdge("K", "L");
		G3.addEdge("J", "L");
		System.out.println("graph 3 BFS starting at D");
		vis = BFS.bfsAll("D", G1);
		for (Vertex w: vis){
			w.print();
		}
		
		System.out.println("graph 3 BFS starting at C Stop at J");
		vis = BFS.bfs("C", G1, "J");
		for (Vertex w: vis){
			w.print();
		}
		
		System.out.println("graph 3 DFS");
		alv = DFS.dfsAll(G3);
		for (Vertex u : alv)
			u.print2();
	}
}