package com.jhendricks.hw0.one;

import java.util.Scanner;

public class RockyIV {
	public static void main(String[] args) {
		System.out.printf("Let's play rock paper scissors!\n");
		Scanner input = new Scanner(System.in);
		System.out.printf("Select rock, paper, or scissors: ");
		String n = input.next();
	
		String choice = n.toLowerCase();
		switch (choice) {
		case "rock":
			System.out.printf("Computer chose... rock\nIt's a tie!\n");
			break;
		case "paper":
			System.out.printf("Computer chose... rock\nPaper covers rock. You win!\n");
			break;
		case "scissors":
			System.out.printf("Computer chose... rock\nRock crushes scissors. You lose!\n");
			break;
		default:
			System.out.printf("Invalid input. Choose rock, paper, or scissors!\n");
		}
	
	}
	
}
