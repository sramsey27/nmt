package com.sramsey.hw0.one;

import java.util.Scanner;

/**
 * This class emulates a game of rock, paper, scissors with the computer.
 * 
 * Assuming the computer always chooses rock, the following results are
 * displayed depending on user input (utilizing Scanner Objects)
 * 
 * @author Steven Ramsey, Jaden Hendricks
 * 
 * @version homework0 #2 
 * 
 * @bugs none
 */

public class RockyIV {
	
	public static void main(String[] args) {
		System.out.printf("Let's play rock, paper, scissors!\n");
		Scanner Input_Word = new Scanner(System.in);
		System.out.printf("\nEnter [rock, paper, or scissors].\nYour Choice: ");
		
		String n = Input_Word.next();
		
		/*Close the scanner object*/
		Input_Word.close(); //Fix warning
	
		/*Ignore letter case*/
		String choice = n.toLowerCase();
		
		/*Process choice*/
		switch (choice) {
			case "rock":
				System.out.printf("\nThe computer chose...Rock.\nIt's a tie!\n");
				break;
			case "paper":
				System.out.printf("\nThe computer chose...Rock.\nPaper covers rock. You win!\n");
				break;
			case "scissors":
				System.out.printf("\nThe computer chose...Rock.\nRock crushes scissors. You lose!\n");
				break;
			default:
				System.out.printf("\nInvalid input. Please enter \"rock\", \"paper\", or \"scissors\"!\n");
			}
	}
}
