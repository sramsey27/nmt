package com.sramsey.hw3.three;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;


public class TimeDisplay {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		GregorianCalendar date = new GregorianCalendar();
		SimpleDateFormat format = new SimpleDateFormat();

		format.setCalendar(date);
		
		String buf = format.format(date.getTime());
		
		System.out.println(buf);
		
		format = new SimpleDateFormat("EEEEEEEEEEE, MM.dd.yyyy hh:mm:ss a");
		
		format.setCalendar(date);
		
		buf = format.format(date.getTime());
		System.out.println(buf);
		
		int dayOfYear = date.get(Calendar.DAY_OF_YEAR);
		
		System.out.println("Which is the " + TimeDisplay.dayEnd(dayOfYear) +" day of the year.");
		
		
	}
	
	/**
	 * Converts a number to the correct suffix
	 * @param day the number to convert
	 * @return a string with the correc suffix appended
	 */
	static String dayEnd(int day){
		int ending = day % 10;
		switch (ending) {
		case 1:
			return day + "st";
		case 2:
			return day + "nd";
		case 3:
			return day + "rd";
		default:
			break;
		
		
		
		}
		
		return day + "th";
	}

}
