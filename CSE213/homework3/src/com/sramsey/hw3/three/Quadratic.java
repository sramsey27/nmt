package com.sramsey.hw3.three;

/**
* @author Jimmy Alexander, Jaeden Hendricks, Steven Ramsey
*
*/
public class Quadratic {

	private double a, b, c;
	
	/**
	 * Default constructor
	 */
	Quadratic(){
		this(0.0,0.0,0.0);
	}
	/**
	 * More specific constuctor
	 * @param a
	 * @param b
	 * @param c
	 */
	Quadratic(double a, double b, double c){
		this.a = a;
		this.b = b;
		this.c = c;
	}
	/**
	 * 
	 * @return array of coeeficients
	 */
	double[] getCoefficents(){
		double[] coef = new double[3];;
		coef[0] = a;
		coef[1] = b;
		coef[2] = c;
		return coef;
	}
	
	void setCoefficents(double a, double b, double c){
		this.a = a;
		this.b = b;
		this.c = c;
	}

	public double getA() {
		return a;
	}

	public void setA(double a) {
		this.a = a;
	}

	public double getB() {
		return b;
	}

	public void setB(double b) {
		this.b = b;
	}

	public double getC() {
		return c;
	}

	public void setC(double c) {
		this.c = c;
	}
	
	double valueAt(double x){
		return a * x * x + b * x + c;
	}
	
	/**
	 * @param scalar
	 */
	void scale(double scalar){
		setCoefficents(scalar * a, scalar * b, scalar * c);
	}
	
	static Quadratic sum(Quadratic p, Quadratic q){
		return new Quadratic(p.a + q.a, p.b + q.b, p.c + q.c);
	}
	
	double[] roots(){
		double[] rts;
		double determinant = b * b - 4 * a * c;
		if (determinant < 0){
			return new double[0];
		}
		else if (determinant == 0){
			rts = new double[1];
			rts[0] = (-1 * b)/(2 * 1);
			return rts;
		}
		rts = new double[2];
		rts[0] = (-1 * b + Math.sqrt(determinant))/(2 * 1);
		rts[1] = (-1 * b - Math.sqrt(determinant))/(2 * 1);
		return rts;
	}
	
	public String toString(){
		return a +"x^2 + " + b + "x + " + c;
	}
}
