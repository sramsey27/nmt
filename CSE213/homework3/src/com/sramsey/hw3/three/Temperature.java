package com.sramsey.hw3.three;

/**
 * Sets the appropriate values for temperatures.
 * 
 * @author Jaeden Hendricks, Steven Ramsey, Jimmy Alexander
 * 
 * @version homework3 #1
 * 
 * @bugs none
 */

abstract class Temperature {
	
	static double temperature;
	
    abstract double getKelvin();
    abstract double getCelsius();
    abstract double getFahrenheit();
    
    public void setTemperature(double temperature) {
    	Temperature.temperature = temperature;
    }
    
    public static double getTemperature() {
    	return temperature;
    }
}

