package com.sramsey.hw3.three;

import com.sramsey.hw3.three.MIDI.Length;

/**
 * Calculates, sets and return variations of note length and tempo
 * 
 * @author Jaeden Hendricks, Steven Ramsey, Jimmy Alexander
 * 
 * @version homework4 #1
 * 
 * @bugs none
 */

public class Metronome {

	private double bpm;
	
	Metronome() { // default bpm sets to 60
		bpm = 60.0;
	}
	
	Metronome(double new_bpm) {
		bpm = new_bpm;
	}

	/**
	 * @param length, current note length
	 * @return calculated note length
	 */
	public double noteLength(Length length) {
		switch (length) {
		case WHOLE:
			return (bpm / 60.0) * 4.0;
		case HALF:
			return (bpm / 60.0) * 2.0;
		case QUARTER:
			return (bpm / 60.0) * 1.0;
		case EIGHT:
			return (bpm / 60.0) * (1/2);
		case SIXTEEN:
			return (bpm / 60.0) * (1/4);
		default:
			System.out.println("Invalid MIDI length given\n");
			return 0;
		}
	}
	
	/**
	 * @return the calcuated bpm 
	 */
	public double getBpm() {
		return bpm;
	}

	/**
	 * @param bpm the bpm to set
	 */
	public void setBpm(double bpm) {
		this.bpm = bpm;
	}
	
}

