package com.sramsey.hw3.three;

public class MIDI {

	enum Length {WHOLE, HALF, QUARTER, EIGHT, SIXTEEN};
	Length length;
	int midiNumber = 0;
	private double frequency; //I feel this was impossible to NOT use a instance variable for frequency
	
	private MIDI(){
		this("C4", Length.QUARTER);
	}
	private MIDI(String note, Length length){
		
		if(note.matches("[A-G]{1}[#]?[-]?[0-9]{1}")){
			
			if(note.startsWith("A")){
				this.midiNumber += 9;
			}
			if(note.startsWith("B")){
				this.midiNumber += 11;
			}
			if(note.startsWith("C")){
				this.midiNumber += 0;
			}
			if(note.startsWith("D")){
				this.midiNumber += 2;
			}
			if(note.startsWith("E")){
				this.midiNumber += 4;
			}
			if(note.startsWith("F")){
				this.midiNumber += 5;
			}
			if(note.startsWith("G")){
				this.midiNumber += 7;
			}
			setFrequency(this.midiNumber);
			
			if(isSharp(note)){
				setSharp();
			}
			setOctave(Integer.parseInt(note));
			
		}
		else {
			System.out.printf("The input does not match the SNF Form\n");
			System.exit(0);
		}
	}
	private MIDI(int midiNumber, Length length){
		this.midiNumber = midiNumber;
		this.length = length;

	}
	private MIDI(double frequency, Length length){
		setFrequency(Math.pow(2, (midiNumber - 69)/12));
		this.length = length;
	}
	
	public void setFrequency(double frequency){
		this.frequency = frequency;		
	}
	
	public double getFrequency(){
		return this.frequency;
	}
	
	public Length getLength(){
		return this.length;
	}
	
	public int getMidiNumber(){
		return this.midiNumber;
	}
	
	public String getNote(String note){
		return note;
	}
	
	public int getOctave(int octave){
		return octave;
	}
	public boolean isSharp(String note){
		
		char temp = note.charAt(2);
		if(temp == '#'){
			return true;
		}
		else{
			return false;
		}
		
	}
	public void setLength(Length length){
		this.length = length;
	}
	public int setMidiNumber(int Midi){
		return Midi;
	}
	public void setOctave(int Octave){
		int i = 0, temp = 0;
		if(Octave < -1 || Octave > 9){
			this.midiNumber = 60;
		}
		if(Octave == -1){
			return;
		}
		if(Octave >= 0){
			for(;i < Octave; i++){
				temp = temp + 12;
				this.midiNumber = this.midiNumber + temp;
			}
		}	
	}
	public void setSharp(){
		this.midiNumber++;
	}
	public String toString(){
		System.out.println("");
		System.out.println("");
		return null;
	}
}
