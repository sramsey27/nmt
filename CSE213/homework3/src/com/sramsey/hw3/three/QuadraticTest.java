package com.sramsey.hw3.three;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * @author decjimmy
 *
 */
public class QuadraticTest {

	/**
	 * Test method for {@link com.jalexander.hw3.four.Quadratic#getCoefficents()}.
	 */
	
	Quadratic q = new Quadratic(1,1,0);
	Quadratic r = new Quadratic(3,1,0);
	
	@Test
	public void testGetCoefficents() {
		assertArrayEquals(q.getCoefficents(), new double[]{1,1,0}, .1);
	}

	/**
	 * Test method for {@link com.jalexander.hw3.four.Quadratic#getA()}.
	 */
	@Test
	public void testGetA() {
		assertEquals(q.getA(), 1, .1);
	}

	/**
	 * Test method for {@link com.jalexander.hw3.four.Quadratic#getB()}.
	 */
	@Test
	public void testGetB() {
		assertEquals(q.getB(), 1, .1);
	}

	/**
	 * Test method for {@link com.jalexander.hw3.four.Quadratic#valueAt(double)}.
	 */
	@Test
	public void testValueAt() {
		assertEquals(q.valueAt(0), 0, .1);
	}

	/**
	 * Test method for {@link com.jalexander.hw3.four.Quadratic#scale(double)}.
	 */
	@Test
	public void testScale() {
		q.scale(3);
		assertArrayEquals(q.getCoefficents(), new double[]{3,3,0}, .1);
		q.scale(1/3);
		
	}

	/**
	 * Test method for {@link com.jalexander.hw3.four.Quadratic#sum(com.jalexander.hw3.four.Quadratic, com.jalexander.hw3.four.Quadratic)}.
	 */
	@Test
	public void testSum() {
		Quadratic t = Quadratic.sum(r, q);
		assertArrayEquals(t.getCoefficents(), new double[]{4,2,0}, .1);
		
	}

	/**
	 * Test method for {@link com.jalexander.hw3.four.Quadratic#roots()}.
	 */
	@Test
	public void testRoots() {
		fail("Not yet implemented");
	}

}
