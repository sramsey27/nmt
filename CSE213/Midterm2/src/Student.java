import java.util.Scanner;

/*Steven Ramsey*/

public class Student {

	public static void main(String[] args) {
		
		int N = 0, i = 0;
	
		System.out.printf("Enter the number of Students : ");
		Scanner Input_int = new Scanner(System.in);
		N = Input_int.nextInt();	
		
		String[] firstname = new String[N];
		String[] lastname = new String[N];
		double[] gpa = new double[N];
			
		Scanner Input_fname = new Scanner(System.in);
		Scanner Input_lname = new Scanner(System.in);
		Scanner Gpa = new Scanner(System.in);
			
		for (i = 0; i < N; i++){
			System.out.printf("\nStudent #%d: ", i + 1);
			System.out.printf("\nEnter the first name of student[#%d]: ", i + 1);
			firstname[i] = Input_fname.next();
			System.out.printf("Enter the last name of student[#%d]: ", i + 1);
			lastname[i] = Input_lname.next();
			System.out.printf("Enter the GPA of student[#%d]: ", i + 1);
			gpa[i] = Gpa.nextDouble();
			if(gpa[i] > 4.5){
				System.out.printf("GPA cannot be over 4.5! Try the program again...\n");
				System.exit(0);
			}
		}	
		System.out.println("\nSTUDENT INFO:\n-----------");
		for(i = 0; i < N; i++){
			System.out.println("Student #" + (i + 1)+ ":\nFirst Name: "+ firstname[i]);
			System.out.println("Last Name: " + lastname[i] + "\nGPA Score: " + gpa[i] + "\n");
		}
	}
}
