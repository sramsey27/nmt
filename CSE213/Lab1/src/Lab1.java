/**
 * CSE/IT 213 - Lab 1 -- Welcome to Methods.
 *
 * Implement all the methods described below.
 *
 * Student name: JAEDEN HENDRICKS & STEVEN RAMSEY
 */

public class Lab1 {

    /**
     * Returns the sum of its arguments.
     * @param x First argument
     * @param y Second argument
     * @return Sum of x and y
     */
    public static int sumTwo( int x, int y ) {
    	return x + y;
    }

    /**
     * Is argument negative?
     * @param x Value to check.
     * @return True is x is less than 0, false otherwise.
     */
    public static boolean isNegative( int x ) {
        if (x < 0)
            return true;
        else
            return false;
    }

    /**
     * Does the given string contain the letter M (either upper or
     * lower case)?
     * @param x String to check
     * @return True if x contains M, false otherwise.
     */
    public static boolean containsM( String x ) {
        if (x.contains(Character.toString('M')))
            return true;
        if (x.contains(Character.toString('m')))
        	return true;
        else
            return false;
    }
    /**
     * Where is the location of the letter M (upper or lower case) in
     * the given string?
     * @param x String to check
     * @return 0 based location of first occurance of M in x,
     *         -1 if M is not present.
     */
    public static int mIndex( String x ) {
    	
        for(int i = 0; i < x.length(); i++){
        	char m = x.charAt(i);
        	if(m == 'm' || m == 'M'){
        		return i;
        	}
        }
        return -1;
    }

    /**
     * This method returns a response based on the string input:
     *     "Apple" => "Orange"
     *     "Hello" => "Goodbye!"
     *     "meat" => "potatoes"
     *     "Turing" => "Machine"
     *     "Special" => "\o/"
     * Any other input should be responded to with:
     *     "I don't know what to say." 
     * @param input The input string
     * @return Corresponding output string.
     */
    public static String respond( String input ) {
        switch (input){
            case "Apple": 
                return "Orange";
                //break;
            
            case "Hello": 
                return "Goodbye!";
                //break;
            
            case "meat": 
                return "potatoes";
                //break;
            
            case "Turing": 
                return "Machine";
                //break;
            
            case "Special": 
                return "\\o/";
                //break;
            
            default:
                return "I don't know what to say.";
        }
        
    }
    
	// REPLACE THE METHOD BODY
	// This method returns a response based on the string input:
	// "Apple" => "Orange"
	// "Hello" => "Goodbye!"
	// "meat" => "potatoes"
	// "Turing" => "Machine"
	// "Special" => "\o/"
	// Any other input should be responded to with:
	// "I don't know what to say." 
    

    /**
     * Average up to six positive numbers. Any non-positive values are
     * not included in the average.
     * @param a First value
     * @param b Second value
     * @param c Third value
     * @param d Fourth value
     * @param e Fifth value
     * @param f Sixth value
     * @return Average of the positive input values. If none are positive, returns -1.
     */
    public static double averagePositives( int a, int b, int c, int d, int e, int f ) {
	   double avg = 0.0;
	   double count = 0;
	   if (a <= 0 && b <= 0 && c <= 0 && d <= 0 && e <= 0 && f <= 0)
            return -1;
       if (a <= 0) {
        	a = 0;
        	count++;
        }
       if (b <= 0) {
        	b = 0;
        	count++;
        }
       if (c <= 0) {
        	c = 0;
        	count++;
        }
       if (d <= 0) {
        	d = 0;
        	count++;
        }
       if (e <= 0) {
        	e = 0;
        	count++;
        }
       if (f <= 0) {
        	f = 0;
        	count++;
        }
        avg = (a + b + c + d + e + f) / (6.0 - count);
        return avg;
    }
        
    // REPLACE THE METHOD BODY
	// This method averages six POSITIVE integers
	// values which are negative or zero should not 
	// be included in the average
	// If all the numbers are invalid (not positive), return -1

    // WRITE A METHOD FROM SCRATCH
    // Write a method called squareOddIgnoreEven that
    // returns the square of odd numbers and 
    // returns even numbers untouched.
    // The method should have a single int argument
    // and return an int
    // Don't forget to use the public static modifiers
	public static double squareOddIgnoreEven(int a) {
		if (a % 2 == 0)
			return a;
		else
			return a*a;
	}
		
    // WRITE A METHOD FROM SCRATCH
    // Write a method called calculatePayment
    // that takes two arguments, 
    // an int meal
    // which is the cost of a meal,
    // and a double tip which
    // represents the tip one would add to the bill.
    // The method must return a double
    // which equals how much should be paid.
    // int meal must be greater than 0
    // double tip must be 0 or greater and .5 or less (50%)
    // If either number is invalid, return -1;
    // Don't forget to use the public static modifiers
	public static double calculatePayment( int meal, double tip) {
		if (meal == 0)
			return -1;
		if (tip == 0)
			return -1;
		if (tip > .5)
			return -1;
		else
			return meal + (tip * meal);
	}
	
    // This code tests your program's completeness.
    public static void main(String[] args) {
	int completeness = 0;
	if( sumTwo(2,3) == 5 ) { completeness++; }
	if( sumTwo(2,-3) == -1 ) { completeness++; }

	if( isNegative(-3) ) { completeness++; }    
	if( !isNegative(2) ) { completeness++; }    
	if( !isNegative(0) ) { completeness++; }    

	if( containsM( "man" ) ) { completeness++; }
	if( !containsM( "dog" ) ) { completeness++; }    
	if( containsM( "EXCLAIMS!" ) ) { completeness++; } 

	if( mIndex( "man" ) == 0 ) { completeness++; } 
	if( mIndex( "EXCLAIMS!" ) == 6 ) { completeness++; } 
	if( mIndex( "dog" ) == -1 ) { completeness++; } 
	if( mIndex( "klmmMmmM" ) == 2 ) { completeness++; }    // THIS ONE
        if( mIndex( "klMMmMMm" ) == 2 ) { completeness++; }

	if( respond( "Apple" ).equals( "Orange" ) ) { completeness++; } 
	if( respond( "Hello" ).equals( "Goodbye!" ) ) { completeness++; } 
	if( respond( "meat" ).equals( "potatoes" ) ) { completeness++; } 
	if( respond( "Turing" ).equals( "Machine" ) ) { completeness++; } 
	if( respond( "Special" ).equals( "\\o/" ) ) { completeness++; } 
	if( respond( "xxx" ).equals( "I don't know what to say." ) ) { completeness++; } 

	if( averagePositives( 12,13,12,13,12,13 ) == 12.5 ) { completeness++; }    // THIS ONE
	if( averagePositives( 0,0,0,0,0,0 ) == -1 ) { completeness++; } 
	if( averagePositives( 0,0,15,0,-2,0 ) == 15 ) { completeness++; } 
	if( averagePositives( 100,-3,4021,-2,13,-6 ) == 1378 ) { completeness++; }    // THIS ONE

	// UNCOMMENT AFTER IMPLEMENTING squareOddIgnoreEven method
	if( squareOddIgnoreEven( 4 ) == 4 ) { completeness++; } 
	if( squareOddIgnoreEven( 0 ) == 0 ) { completeness++; } 
	if( squareOddIgnoreEven( 3 ) == 9 ) { completeness++; } 


	// UNCOMMENT AFTER IMPLEMENTING calculatePayment
	if( calculatePayment( 0, .3 ) == -1 ) { completeness++; } 
	if( calculatePayment( 10, .2 ) == 12.0 ) { completeness++; } 
	if( calculatePayment( 100, .6 ) == -1 ) { completeness++; }
	if( calculatePayment( 120, .32 ) == 158.4 ) { completeness++; }
    
	System.out.println( "Your program's completeness is currently: " + completeness + "/30" );
    }

}
