package com.sramsey.hw1.one;

import java.lang.Math;

/**
 * Creates a sphere and returns data based on its radius and its center
 * 
 * creates a sphere with a radius and x,y,z coordinates of the center and
 * used constructors to return the volume, surface area, and distance from origin
 * 
 * @author Jaeden Hendricks, Steven Ramsey
 * 
 * @version homework1 #1
 * 
 * @bugs none
 */

public class Sphere {
	public double radius, x, y, z;
	public Sphere() {
		radius = 1.0;
		x = 0.0;
		y = 0.0;
		z = 0.0;
	}
	
	public Sphere(double rad) {
		radius = rad;
	}

	public Sphere(double x_val, double y_val, double z_val) {
		x = x_val;
		y = y_val;
		z = z_val;
		radius = 1.0;
	}
	public Sphere(double x_val, double y_val, double z_val, double rad) {
		radius = rad;
		x = x_val;
		y = y_val;
		z = z_val;
	}
	
	/**
	 * returns the volume of the sphere
	 * @param radius the radius of the sphere
	 * @return the volume of the sphere
	 */
	public double volume() {
		return (4/3)*Math.PI*Math.pow(radius, 3.0);
	}
	
	/**
	 * returns the surface area of the sphere
	 * @param radius the radius of the sphere
	 * @return the surface area of the sphere
	 */
	public double surfaceArea() {
		return 4*Math.PI*Math.pow(radius, 2.0);
	}
	
	/**
	 * returns the distance the center sphere is from the origin (0,0,0)
	 * @param x the x-coordinate of the sphere's center
	 * @param y the y-coordinate of the sphere's center
	 * @param z the z-coordinate of the sphere's center
	 * @return the numerical distance the center sphere is from the origin
	 */
	public double distanceFromOrigin() {
		return Math.abs(Math.sqrt(Math.pow(x, 2.0) + Math.pow(x, 2.0) + Math.pow(z, 2.0)) - radius);
	}
	
	// prints out the values of radius and x,y,z for testing
	public void printVals() {
		System.out.printf("radius = %.2f\t", radius);
		System.out.printf("x = %.2f\t", x);
		System.out.printf("y = %.2f\t", y);
		System.out.printf("z = %.2f\n\n", z);
	}
}