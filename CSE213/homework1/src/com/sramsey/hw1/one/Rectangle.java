package com.sramsey.hw1.one;
import java.lang.Math;

/**
 * Creates a rectangle and returns data based on its width and height
 * 
 * creates a rectangle with width and height and coordinates of the center
 * using constructors to return the area, perimeter, and distance from origin
 * 
 * @author Jaeden Hendricks, Steven Ramsey
 * 
 * @version homework1 #1
 * 
 * @bugs none
 */

public class Rectangle {
	public double width,height,x ,y;
	
	public Rectangle() { /*Default Constructor*/
		width = 1.0;
		height = 1.0;
		x = 0.0;
		y = 0.0;
	}
	
	public Rectangle(double in_width, double in_height) { /*Second Constructor*/
		width = in_width;
		height = in_height;
		x = 0.0;
		y = 0.0;
	}
	
	public Rectangle(double in_width, double in_height, double in_x, double in_y){ /*Third Constructor*/
		width = in_width;
		height = in_height;
		x = in_x;
		y = in_y;
	}

	//Methods
	
	/**
	 * Calculates the area of the rectangle with given parameters
	 * 
	 * @param width the width of the rectangle
	 * @param height the height of the rectangle
	 * @return the calculated area
	 * 
	 */
	public double area() {
		return width * height;
	}
	
	/**
	 * Calculates perimeter of the rectangle with given parameters
	 * 
	 * @param width, the width of the rectangle
	 * @param height, the height of the rectangle
	 * @return the calculated perimeter of the rectangle
	 * 
	 */
	public double perimeter() {
		return (2 * (width)) + (2 * (height));
	}
	
	/**
	 * Calculates the diagonal length of the rectangle
	 * 
	 * @param width, the width of the rectangle
	 * @param height, the height of the rectangle
	 * @return the calculated diagonal length
	 */
	public double diagonalLength() {
		return ((Math.sqrt((Math.pow(width, 2)) + (Math.pow(height, 2)))));
	}
	/**
	 * Calculates the distance from the origin with the given
	 * parameters
	 * 
	 * @param x, the x coordinate
	 * @param y, the y coordinate
	 * @return the calculated distance from the origin
	 */
	public double distanceFromOrgin(){
		return Math.sqrt((Math.pow(x, 2) + Math.pow(y, 2)));
	}
	
	// prints out the values of width, height, x, and y for testing
	public void printVals() {
		System.out.printf("width = %.2f\t", width);
		System.out.printf("height = %.2f\t", height);
		System.out.printf("x = %.2f\t", x);
		System.out.printf("y = %.2f\n\n", y);
	}
}
