package com.sramsey.hw1.one;

/**
 * Tests the data from the sphere and rectangle classes
 * 
 * Prints out data regarding constructors and methods for classes
 * Sphere and Rectangle
 * 
 * @author Jaeden Hendricks, Steven Ramsey
 * 
 * @version homework1 #1
 * 
 * @bugs none
 */

public class Test {

	public static void main(String[] args) {
		
		/*Rectangle*/
		System.out.printf("Rectangle\n----------------------\n");
		
		Rectangle rect1 = new Rectangle(); // width & height of 1, corner at (0,0)
		System.out.printf("Default constructor:\n");
		rect1.printVals();
		
		Rectangle rect2 = new Rectangle(5.0, 5.0); // sets width and height, keeps corner
		System.out.printf("2nd Constructor:\n");
		rect2.printVals();
		
		System.out.printf("3rd Constructor:\n"); // sets width, height, and corner
		Rectangle rect3 = new Rectangle(3.0, 4.0, 5.0, 6.0);
		rect3.printVals();
		
		System.out.printf("Stats from 3rd constructor:\n");
		System.out.println("Area = " + rect3.area()); //Area
		System.out.println("Parameter = " + rect3.perimeter()); //Parameter
		System.out.println("Diagonal Length = " + rect3.diagonalLength()); //Diagonal Length
		System.out.println("Distance from Origin = " + rect3.distanceFromOrgin()); //Distance from Origin
		
		/*Sphere*/
		System.out.println("\nSphere\n----------------------");
		
		System.out.printf("Default constructor:\n"); // radius 1, centered at origin (0,0,0)
		Sphere sphere1 = new Sphere();
		sphere1.printVals();
		
		System.out.printf("2nd constructor:\n"); // sets radius but keeps center position
		Sphere sphere2 = new Sphere(3.0);
		sphere2.printVals();
		
		System.out.printf("3rd constructor:\n"); // sets center position but keeps radius
		Sphere sphere3 = new Sphere(2.0, 2.0, 2.0);
		sphere3.printVals();
		
		System.out.printf("4rd constructor:\n"); // sets center position and radius
		Sphere sphere4 = new Sphere(3.0, 2.0, 3.0, 2);
		sphere4.printVals();
		
		System.out.printf("Stats from 4rd constructor:\n");
		System.out.println("Volume = " + sphere4.volume());
		System.out.println("Surface area = " + sphere4.surfaceArea());
		System.out.println("Distance from origin = " + sphere4.distanceFromOrigin());
	}
}
