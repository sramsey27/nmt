package com.sramsey.hw5.five;

public interface BinarySearchInterface<E extends Comparable<? super E>> {

	public void makeEmpty();
	
	public boolean isEmpty();
	
	public boolean find(E x);
	
	public E findMin();
	
	public E findMax();
	
	public void add(E x);
	
	public void delete(E x);
	
	public void print(Transversal mode);
	
}
