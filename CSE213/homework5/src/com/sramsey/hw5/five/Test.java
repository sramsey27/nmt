package com.sramsey.hw5.five;

import java.util.*;

public class Test {

	public static void main(String[] args) {
		
		int i = 0;
		BinarySearchInterface<Integer> Integertree = new BST<Integer>();
		
		for(i = 1; i < 11; i++){
			Integertree.add(i);
		}
	
		System.out.println("Printing in INORDER Transversal...");
		Integertree.print(Transversal.INORDER);
		System.out.println("Printing in PREORDER Transversal...");
		Integertree.print(Transversal.PREORDER);
		System.out.println("Printing in POSTORDER Transversal...");
		Integertree.print(Transversal.POSTORDER);
		
		System.out.printf("Emptying Integer Tree...");
		Integertree.makeEmpty();
		System.out.printf("Success!\n Processing Random numbers...");
		
		Random rand = new Random();
		int max = 9, min = 0;
		int range = max - min + 1;
		
		for(i = 1; i < 21; i++){
			Integertree.add(rand.nextInt(range) + min);
		}
		System.out.println("Printing in INORDER Transversal...");
		Integertree.print(Transversal.INORDER);
		System.out.println("Printing in PREORDER Transversal...");
		Integertree.print(Transversal.PREORDER);
		System.out.println("Printing in POSTORDER Transversal...");
		Integertree.print(Transversal.POSTORDER);

		BinarySearchInterface<String> Techname = new BST<String>();
		BinarySearchInterface<Long> TechID = new BST<Long>();
		
		/*TechID.add(10);
		Techname.add("Google");
		TechID.add(20);
		Techname.add("Apple");
		TechID.add(5);
		Techname.add("Amazon");
		TechID.add(30);
		Techname.add("Twitter");
		TechID.add(1);
		Techname.add("Facebook");
		TechID.add(25);
		Techname.add("C");
		TechID.add(28);
		Techname.add("C++");
		TechID.add(3);
		Techname.add("Python");
		TechID.add(7);
		Techname.add("Linux");
		TechID.add(15);
		Techname.add("Unix");
		TechID.add(22);
		Techname.add("Java");*/
		
		Techname.print(Transversal.INORDER);
		TechID.print(Transversal.INORDER);
		
	}
}
