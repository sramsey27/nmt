package com.sramsey.hw5.five;

import java.util.*;

@SuppressWarnings("unused")
public class BST<E extends Comparable<? super E>> implements BinarySearchInterface<E> {
	
	private Node<E> root;
	private Transversal mode;
	
	private static class Node<T>{
		private T element;
		private Node<T> left;
		private Node<T> right;
		
		Node(T element){
			this(element,null,null);
		}

		public Node(T element2, Node<T> object, Node<T> object2) {
			element = element2;
			left = object;
			right = object2;
		}	
	}
	
	public void makeEmpty() {
		this.root = null;
	}

	public boolean isEmpty() {
		if(root == null){
			return true;
		}
		else{
			return false;
		}
	}
	
	public boolean find(E x){
		return find(x, this.root);
	}

	private boolean find(E x, Node<E> T) {
		if(T != null){
			if(x.compareTo(T.element) < 0){
				return find(x, T.left);
			}
			else if(x.compareTo(T.element) > 0){
				return find(x, T.right);
			}
			else if(x.compareTo(T.element) == 0){
				return true;
			}
		}
		return false;
	}

	public E findMin() {
		if(isEmpty()){
			//throw exception???
		}
		return findMin(this.root).element;
	}

	public E findMax() {
		if(isEmpty()){
			//throw exception???
		}
		return findMax(this.root).element;
	}

	public void add(E x) {
		this.root = add(x, root);	
	}

	public void delete(E x) {
		this.root = delete(x, root);
	}

	public void print(Transversal mode) {
		if(isEmpty()){
			System.out.println("The Tree is empty, nothing to print!");
		}
		else{
			print(mode, root);
		}	
	}
	
	private void print(Transversal mode ,Node<E> T){
		if(T == null){
			return;
		}
		else{
			switch(mode){
				case INORDER:
					print(mode, T.left);
					System.out.println(T.element);
					print(mode, T.right);
					break;
				case PREORDER:
					System.out.println(T.element);
					print(mode, T.left);
					print(mode, T.right);
					break;
				case POSTORDER:
					print(mode, T.left);
					print(mode, T.right);
					System.out.println(T.element);
					break;
					
				default:
					System.out.println("Transversal mode not recognized!");
			}
		}
	}
	
	private Node<E> findMax(Node<E> T){
		if(T != null){
			while(T.right != null){
				T = T.right;
			}
		}
		return T;
	}
	
	/**
	 * Finds the minimal item in the tree
	 * @param T the current node
	 * @return the node that contains the minimal item
	 */
	private Node<E> findMin(Node<E> T){
		if(T == null){
			return null;
		}
		else if(T.left == null){
			return T;
		}
		return findMin(T.left);
	}
	
	/**
	 * Deletes node at x
	 * @param x current element to remove
	 * @param T current node
	 * @return new tree structure
	 */
	private Node<E> delete(E x, Node<E> T){
		if(T == null){
			return T;
		}
		if(x.compareTo(T.element) < 0){
			T.left = delete(x, T.left);
		}
		else if(x.compareTo(T.element) > 0){
			T.right = delete(x, T.right);
		}
		else if(T.left != null && T.right != null){
			T.element = findMin(T.right).element;
			T.right = delete(T.element, T.right);
		}
		else{
			if(T.left != null){
				return T.left;
			}
			else{
				return T.right;
			}
		}
		return T;
	}
	/**
	 * Adds elements to tree
	 * @param x the item to add to the tree
	 * @param T the current node 
	 * @return the new node for the tree
	 */
	private Node<E> add(E x, Node<E> T){
		if(T == null){
			return new Node<E>(x, null, null);
		}	
		if(x.compareTo(T.element) < 0){
			T.left = add(x, T.left);
		}
		else if(x.compareTo(T.element) > 0){
			T.right = add(x, T.right);
		}
		return T;
	}
}	