package com.sramsey.hw5.five;

@SuppressWarnings("unused")
public class Tech {

	private String name = new String();
	private long ID;
	
	private Tech(){
		this("Default name", 50);
	}

	private Tech(String name, long ID) {
		this.name = name;
		this.ID = ID;
	}
		
	private void SetName(String name){
		this.name = name;
	}
	
	private void SetID(Long ID){
		this.ID = ID;
	}
	
	private String getname(){
		return name;
	}
	
	private long getID(){
		return ID;
	}
	
	private void tostring(){
		System.out.println("Name = " + name);
		System.out.println("ID = " + ID);
	}
}
