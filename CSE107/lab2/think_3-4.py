#!/usr/bin/env python

#Author: Steven Ramsey

def do_twice(f, value):
    
    """Basically uses print_twice, two times"""

    f(value)
    f(value)

def print_twice(value):

    """Prints value twice"""

    print value
    print value

def do_four(f, value):

    """Calls do_twice, two times then calls print_twice as an argument for do_twice
    then prints spam"""

    do_twice(f, value)
    do_twice(f, value)

    do_twice(print_twice, 'spam')
    print 'spam'

def main():

    print ''
    do_four(print_twice, 'spam')

if __name__ == '__main__':
    main()

#Author: Steven Ramsey
