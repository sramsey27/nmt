#!/usr/bin/env python

def check_fermat(a , b , c , n):

	"""Takes four parameters-a, b, c and n-and that checks to see
	if Fermat's theorem holds. If n is greater than 2 and it turns
	out to be true that an + bn = cn """

	if (a ** n + b ** n) == (c ** n):
		print ("\nHoly smokes! Fermat Was Wrong! They do equal each other!\n")
	else:
		print ("\nNo, that doesn't work, try a different set of intergers.\n")

def main():

	print "\nFind what values of a,b,c, and n makes the equation true?"
	print "\n(a**n + b**n) = (c**n)"
	print "\nNote: 'n' must be greater than or equal to 2.\n"

	a = int(raw_input ('Input your value for a = '))
	b = int(raw_input ('Input your value for b = '))
	c = int(raw_input ('Input your value for c = '))
	n = int(raw_input ('Input your value for n = '))

	check_fermat(a , b , c , n)

if __name__ == '__main__':
	main()

