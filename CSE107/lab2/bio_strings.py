#!/usr/bin/env python

#Author: Steven Ramsey

from random import randint


def create_random_dna(n):
    """Return a random string of dna of length n"""
    s = str()
    for i in range(n):
        s += s.join(('ATCG')[randint(0, 3)])
    return s


def create_random_rna(n):
    """Return a random string of dna of length n"""
    r = str()
    for i in range(n):
        r += r.join(('AUGC')[randint(0, 3)])
    return r


def reverse(s):
    """Return a new string that is the reverse of string s"""
    t = s.upper()[::-1]
    return t


def complement(s):
    """Return a new string that is the DNA complement of string s"""
    c = s.replace('A', 't').replace('T', 'a').replace('t','T').replace('a', 'A').replace('C', 'g').replace('G', 'c').replace('c', 'C').replace('g','G')
    return c


def reverse_complement(s):
    return reverse(complement(s))


def transcription(s):
    """Return a new string that is the transcription (mRNA) of string s"""
    c = s.replace('A', 'U').replace('T', 'a').replace('a', 'A').replace('C', 'g').replace('G', 'c').replace('c', 'C').replace('g','G')
    return c


def print_codons(s):
    """Prints codons as a space separated list contained in the string s"""
    if len(s) % 3 == 0:
        for i in range(0, len(s), 3):   
            print s[i : i+3],
    else:
        print 'ERROR: DNA STRING IS NOT DIVISIBLE BY 3'


def count(c, s):
    """return the count of the number of times c, a character,
       appears in string s"""
    counter = 0    
    for char in s:
        counter += (c == char)
    return counter


def gc_content(s):
    """Return the percentage of g and c base pairs in the string s"""
    x = float((count('C',s))+count('G',s))/(count('A',s)+count('T',s)+count('G', s)+count('C',s)) * 100
    return x
    

def main():
    
    r = create_random_rna(9)
    s = create_random_dna(9)
    t = reverse(s)
    print 'DNA =', s
    print 'RNA =', r
    print 'Reverse of DNA =', t
    print 'Complement of DNA =', complement(s)
    print 'Reverse-complement of DNA =', reverse_complement(s)
    print 'mRNA =', transcription(s)
    print 'Number of A =', count('A', s)
    print gc_content(s)    
    print_codons(s)
    
if __name__ == '__main__':
    main()

#Author: Steven Ramsey