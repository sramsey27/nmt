#!/usr/bin/env python

#Author: Steven Ramsey

def first(word):
    return word[0]
    
def last(word):
    return word[-1]

def middle(word):
    return word[1:-1]

def is_odd_char(word):
    if len(word) % 2 == 1:
        return True
    else:
        return False
    
def first_equals_last(a):
    if first(a) == last (a):
        return True
    else:
        return False

def is_palindrome(word):

    """Takes a string argument and returns True if it is a palindrome
    and False otherwise."""

    if not first_equals_last(word) or not is_odd_char(word):
        return "is not a Palindrome."
    else:
        if len(word) > 3:
            word = middle(word)
            return is_palindrome(word)
        elif len(word) == 3:
            return "is a Palindrome."
        else:
            return "is not a Palindrome."

def main():

    print '\nBob ' + str(is_palindrome('bob'))
    print 'Serenity ' + str(is_palindrome('serenity'))
    print 'Steven ' + str(is_palindrome('steven'))
    
if __name__ == '__main__':
    main()
    
#Author: Steven Ramsey
