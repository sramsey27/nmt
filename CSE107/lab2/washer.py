#!/usr/bin/env python

#Author: Steven Ramsey

from math import pi

def area_circle(radius):

	"""Basic evaluation of the area of the circle formula."""

	return pi * radius ** 2

def washer_area(inner,outter):

	"""Applies the inputted radi to evaluate by calling area_circle"""

	return area_circle(outter) - area_circle(inner)

def main():

	print "\nInput the Radius of both the Inner and Outter Circles of the washer.\n"
	radius1 = raw_input('Enter the inner radius: ')
	print "\nGot it! The inner radius is:",radius1
	radius2 = raw_input("\nNow Enter the outer radius: ")
	print "\nThe outer radius is:",radius2
	print "\nThe area of the wash is:", washer_area (int(radius1),int(radius2))

if __name__ == '__main__':
	main()

#Author: Steven Ramsey