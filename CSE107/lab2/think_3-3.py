#!/usr/bin/env python

#Author: Steven Ramsey

def right_justify(s):
    
    """Creates a formula to put strings in the right justify
    format"""
    
    print (' ' * (70 - len(s)) + s)

def main():
    
    right_justify("To be, or not to be, that is the question:")
    right_justify("Whether 'tis Nobler in the mind to suffer")
    right_justify("The Slings and Arrows of outrageous Fortune,")

if __name__ == '__main__':
    main()

#Author: Steven Ramsey