#!/usr/bin/env python

#Author: Steven Ramsey

def gcd(a,b):

    """Takes parameters a and b and returns their greatest common divisor."""

    if b == 0:
        return a
    else:
        remainder = a%b
    if remainder == 0:
            return b
    else:
            return gcd(b, remainder)

def main():

    print '\nThe GCD for 46322 and 71162 is:'
    print gcd(46332 , 71162)
    print '\nThe GCD for 128 and 32 is:'
    print gcd(128 , 32)
    print '\nThe GCD for 100 and 101 is:'
    print gcd(100 , 101)
    print '\nThe GCD for 97 and 97 is:'
    print gcd(97 , 97)
    print ''

if __name__ == '__main__':
    main()

#Author: Steven Ramsey
