#!/usr/bin/env python

#Author: Steven Ramsey

from sys import argv

def ack(m, n):

	"""Evaluates Ackermann's function"""

	if m == 0:
		return n + 1
	elif n == 0:
		return ack(m - 1, 1)
	else:
		return  ack(m - 1, ack(m, n - 1))

def main():

	m = int(argv[1])
	n = int(argv[2])
	print ack(m, n)

if __name__ == '__main__':
    main()
    
#Author: Steven Ramsey