#!/usr/bin/env python

#Author: Steven Ramsey

#Rewitten to higher lab standards.

from math import pi

def area_circle(radi):

	"""Simply implements the standard formula for finding
	the area of a circle then returns."""

	return pi * radi ** 2

def main():

	print ("\nHello, please enter the radius of your circle, I'll do the rest.\n")
	radius = int(raw_input('Radius = '))
	print "Area of circle =",area_circle(radius)

if __name__ == '__main__':
	main()

#Author: Steven Ramsey

