#!/usr/bin/env python

def mcarthy(n):

    if n > 100:
        return n - 10
    elif n <= 100:
        return mcarthy(mcarthy(n + 11))
    

def main():
    
    for i in range(1, 102):
        print 'M({0}) = {1}'.format(i, mcarthy(i))

if __name__ == '__main__':
    main()
