import random


def winner(move, computer):
    if move == 'spock':
        if computer == 'scissors':
            return '\nUser wins! Spock smashes scissors!\n'
        elif computer == 'rock':
            return '\nUser Wins! Spock vaporizes rock!\n'
        elif computer == 'lizard':
            return '\nComputer wins! Lizard poisons Spock!\n'
        elif computer == 'paper':
            return '\nComputer wins! Paper disproves Spock!\n'
        elif computer == 'spock':
            return '\nIt is a tie!\n'
    if move == 'scissors':
        if computer == 'scissors':
            return '\nIt is a tie!\n'
        elif computer == 'rock':
            return '\nComputer Wins! Rock crushes scissors!\n'
        elif computer == 'lizard':
            return '\nUser wins! Scissors decapitates lizard!\n'
        elif computer == 'paper':
            return '\nUser wins! Scissors cuts paper!\n'
        elif computer == 'spock':
            return '\nIt is a tie!\n'
    if move == 'rock':
        if computer == 'scissors':
            return '\nUser Wins! Rock crushes scissors!\n'
        elif computer == 'rock':
            return '\nIt is a tie!\n'
        elif computer == 'lizard':
            return '\nUser wins! Rock crushes lizard!\n'
        elif computer == 'paper':
            return '\nComputer wins! Paper covers rock!\n'
        elif computer == 'spock':
            return '\nComputer wins! Spock vaporizes rock!\n'
    if move == 'paper':
        if computer == 'scissors':
            return '\nComputer wins! Scissors cut paper!\n'
        elif computer == 'rock':
            return '\nUser Wins! Paper covers rock!\n'
        elif computer == 'lizard':
            return '\nComputer wins! Lizard eats paper!\n'
        elif computer == 'paper':
            return '\nIt is a tie!\n'
        elif computer == 'spock':
            return '\nUser Wins! Paper disproves Spock!\n'
    if move == 'lizard':
        if computer == 'scissors':
            return '\nComputer wins! Scissors decapitates lizard!\n'
        elif computer == 'rock':
            return '\nComputer wins! Rock crushes lizard!\n'
        elif computer == 'lizard':
            return '\nIt is a tie!\n'
        elif computer == 'paper':
            return '\nUser wins! Lizard eats Paper!\n'
        elif computer == 'spock':
            return '\nUser wins! Paper disproves Spock!\n'


def computer():

    """This will return a string of the computer's move"""

    number = random.randint(1, 5)
    if(number == 1):
        name = 'rock'
    elif(number == 2):
        name = 'spock'
    elif(number == 3):
        name = 'paper'
    elif(number == 4):
        name = 'lizard'
    elif(number == 5):
        name = 'scissors'
    return name


def move_is_valid(move):

    """This Returns true if the move is rock, paper, scissors, lizard, or spock,
       but false if otherwise"""

    n = ['rock','paper','scissors','lizard','spock']
    print '\nYour move was ' + move
    for i in n:
        if move == i:
           return True

    return False
        

def main():

    move = 'play'
    print "\nWelcome!"
    while move != 'quit':
        
        move = raw_input("Enter a move (Press 'h' if you need help): ")
        move = move.lower()
        if move == 'h':
            print "\nHelp:\n"
            print 'This program allows you to play rock, paper,'
            print "scissors, lizard, Spock against the machine!\n"
            print "How Exciting!\n"
            print "Now Here's how it works:"
            print 'Valid moves are "rock", "paper", "scissors",'
            print '"lizard", or "Spock".\n'
            print 'To escape this maddness enter "quit".\n'
            print "\nIt's your move."
        elif move_is_valid(move):
            print 'MOVE WAS VALID'
            print winner(move, computer())
        elif move != 'quit':
            print "\nInvalid move, try again\n"

if __name__ == '__main__':
    main()
