#!/usr/bin/env python

def russian(a,b):
    if b == 0:
        return 0
    elif b % 2 == 0:
        return russian(a * 2, b / 2)
    elif b % 2 == 1:
        return a + russian(a * 2, b / 2)

def main():
    print russian(1, 17)

if __name__ == '__main__':
    main()
