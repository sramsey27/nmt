#!/usr/bin/env python
from sys import argv

def luhn_multiply(lst):
    for i in range(len(lst)):
        if i % 2 ==0:
            lst[i] *= 2
            
                      
def luhn_total(lst):
    total = 0
    for i in range(len(lst)):
        total += lst[i]%10
        total += lst[i]/10
    if total % 10 == 0:
        print "Card is Valid"
    else:
        print "Card is Invalid"

    
def main():
    
    
    lst =[int(argv[1])]
    
    luhn_multiply(lst)
    luhn_total(lst)
    
    
    
if __name__ == '__main__':
    main()
