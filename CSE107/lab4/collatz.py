#!/usr/bin/env python

from sys import argv
import matplotlib.pyplot as plt


def hailstone(n):
    lines = [n]
    while n > 1:
        if n % 2 == 0:
           n = n / 2
           lines.append(n)
        else:
            n = (n * 3) + 1
            lines.append(n)
    return lines


def hailstone_plot(n):
    a = hailstone(n)
    plt.plot(a, 'bo-')
    plt.show()


def sequence(n):
    lines = [n]
    while n > 1:
        if n % 2 == 0:
           n = n / 2
           lines.append(n)
        else:
            n = (n * 3) + 1
            lines.append(n)
    print lines


def stats(n):
    lines = [n]
    while n > 1:
        if n % 2 == 0:
           n = n / 2
           lines.append(n)
        else:
            n = (n * 3) + 1
            lines.append(n)
    print 'The length of the sequence =', len(lines)
    print 'The max term =', max(lines)


def evens(n):
    a = [i for i in hailstone(n) if i % 2 == 1]
    print 'The number of even terms', a


def odds(n):
    a = [i for i in hailstone(n) if i % 2 == 1]
    print 'The number of odd terms', a


def main():
    
    n = int(argv[1])
    if argv[2] == '-p' and argv[3] == '-s':
        
        sequence(n)
        stats(n)
        evens(n)
        odds(n)
        
    elif argv[2] == '-p':
        
        sequence(n)
        
    elif argv[3] == '-s':
        
        stats(n)
        evens(n)
        odds(n)
        

if __name__ == '__main__':
    main()
