#! /usr/bin/env python

#Author: Steven Ramsey

from collections import deque

"""Evaluates prefix notation expressions"""

def parse(x):
    token=x.popleft()
    if token=='+':
            return parse(x)+parse(x)
    elif token=='-':
            return parse(x)-parse(x)
    elif token=='*':
            return parse(x)*parse(x)
    elif token=='/':
            return parse(x)/parse(x)
    else:
            #must return as a interger.
            return int(token)


def main():
    
    expression="* 5 5"
    print "\nExpression Input:",expression
    print "Evaluated:",parse(deque(expression.split()))

if __name__=='__main__':
	main()
	
#Author: Steven Ramsey