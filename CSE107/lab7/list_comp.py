#!/usr/bin/env python

#Author: Steven Ramsey

def sum_cubes(n):
	
	"""Finds the sum for the first n cubes"""
	
	lst_sum = sum([n ** 3 for n in range(1,9)])
	return lst_sum

def sum_even(numlist):
	
	"""List comprehension to find sum of even numbers
	in list"""
	
	product = sum([i for i in numlist if i % 2 ==0])
	return product

def sum_factors(n):
	
	"""List comprehension to find the sum of all the
	factors of n"""
	
	factor = sum([t for t in range(1,n+1) if n % t == 0])
	return factor

def main():
	
	"""Multiple print statements with list comprehension."""
	
	print "\n7.\n",[2**i for i in range (9)]
	
	print "\n8.\n",[i**2 + 3*i + 2 for i in range (-1, 9)]
	
	print "\n9.\n",[chr(x) for x in range(ord("a"), ord("z") + 1)]
	
	print "\n10.\n",[chr(x).upper() for x in range(ord("a"), ord("z") + 1)][::-1]

	#cube changeable at will.
	cube = 15
	print "\n11.\n","The sum of the cubes of",cube,"is",sum_cubes(cube)
	
	#lst changeable at will.
	lst = [15,20,25,30,35,40,45,50,55,60]
	print '\n12.\n', lst
	print "All the even numbers add up to",sum_even(lst),'.'
	
	#Configure s for sum_factors(s).
	s = 12
	print '\n13.\n',"The sum of the factors of",s,"is", sum_factors(s)
	
if __name__ == '__main__':
	main()

#Author: Steven Ramsey