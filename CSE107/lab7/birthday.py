#!/usr/bin/env python

#Author: Steven Ramsey

from sys import argv
import random

def has_duplicates(input_list):
	"""Checks for duplicates"""

	check_duplicate = {}
	
	for item in input_list:
		if check_duplicate.has_key(item):
			print "Day",item,"is the duplicate."
			return True
		else:
			check_duplicate[item] = 1
	return False

def main():

	#n is the number of people in the room.
	n =int(argv[1])
	dup_count = 0
	for i in range(1,1000): #Number of run throughs.
		my_bday_list = []
		for i in range(n):
			my_bday_list.append(random.randint(1,365))

		if has_duplicates(my_bday_list):
			print my_bday_list
			dup_count += 1
			print

	print "Probability of having duplicates out of",n,"people is:", dup_count/100.0 
if __name__ == '__main__':
	main()

#Author: Steven Ramsey
