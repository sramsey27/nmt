#!/usr/bin/env python

#Author: Steven Ramsey

"""Simple shifting to remove the first and last elements of the list"""

def middle(llist):
    return llist[1:-1]

def main():
    
    lst = [1,2,3,4]
    
    print '\n', lst
    print '\n','The middle numbers of the list is:', middle(lst)

if __name__ == '__main__':
	main()

#Author: Steven Ramsey