#!/usr/bin/env python

#Author: Steven Ramsey

def abc_lst(lst):
		
	s = []
	
	for i in range(len(lst)-1,-1,-1):
		s.append(lst[i:])
	return s		
	

def main():
	
	lst =  ['a','b','c','d','e','f','g',	
		'h','i','j','k','l','m',
		'n','o','p','q','r','s',
		't','u','v','w','x','y','z']
	
	print '\n',abc_lst(lst)
	
	print '\nThat is a lot of list and letters.\n'
	print 'MIND BLOWN! 0_0'
	
if __name__ == '__main__':
	main()

#Author: Steven Ramsey
