#!/usr/bin/env python

#Author: Steven Ramsey

"""Adds the elements from all nested list"""

def nested_sum(slist):
	sums = 0
	for i in slist:
		if type(i) is list:
			sums+= sum(i)
		else:
			sums += i
	return sums		
		
def main():
	
	"""Basic function call and variable assignment"""
	
	list = [[5,5,5],[5,5,5]]
	
	print  "\nThe list is equal to:",nested_sum(list)
	print "You happy now?"
	
	
	
if __name__ == '__main__':
	main()

	
