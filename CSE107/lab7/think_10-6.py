#!/usr/bin/env python

#Author: Steven Ramsey

def is_sorted(lst):
	
	"""Checks to see if defined list is ascending
	or descending."""
	
	lst1 = lst[0]
	for c in lst[1:]:
		if lst1 > c:
			print "The list is descending."
			return False
		lst1 = c
	print "The list is ascending."
	return True

def main():
	
	print "\nInput a list to detemine if it is"
	print 'ascending or descending.'
	print "\nExample: 4,5,6 or s,n,r"
	
	s = raw_input("\nInput = ")
	first_list = s.split(',',)

	print '\n',first_list 
	print "\n",is_sorted(first_list), "is returned."
	
if __name__ == '__main__':
	main()

#Author: Steven Ramsey
