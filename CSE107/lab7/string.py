#!/usr/bin/env python

#Author: Steven Ramsey

from sys import argv

def string(w):
   
   """Simply takes a list, sorts it, then joins them to make
   a new string."""
   
   word_list = list(w)
   print '\nOriginal:\n',word_list
   
   word_list.sort()
   print 'Sorted:\n',word_list
   
   str1 = ''.join(word_list)
   print "\nThe new sorted string is",str1

def main():
    
    word = argv[1]
    string(word)

if __name__ == '__main__':
	main()

#Author: Steven Ramsey