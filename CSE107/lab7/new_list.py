#!/usr/bin/env python

#Author: Steven Ramsey

def new_list(lst):
    
    """Takes a list of numbers, creates a new list
    for each number then opposite ends of the list
    are added together."""
    
    newlst = []
    while len(lst)>1:
        first = lst.pop(0)
        last = lst.pop()
        sums = first + last
        newlst.append(sums)
    if len(lst) == 1:
        newlst.append(lst[0])
    return newlst


def main():
    
    lst =[2,4,6,8,9,1]
    print "\nOriginal:\n",lst
    print "\nNew List:\n",new_list(lst)
 
if __name__ == '__main__':
    main()

#Author: Steven Ramsey