#!/usr/bin/env python

#Author: Steven Ramsey

def chop(x):
	
	"""Deletes the indexes and return none"""
	
	del x[:1], x[-1:]
	#Pretty simple.

def main():

	lst = ['a','b','c']
	print chop(lst)

if __name__ == '__main__':
	main()

#Author: Steven Ramsey
