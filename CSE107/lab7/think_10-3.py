#!/usr/bin/env python

from sys import argv

#Author: Steven Ramsey

def c_sum(numlist):
	
	"""Takes a list of numbers and return the cumulative
	sum (numlist)"""

	for i in range(len(numlist)):
		if i == 0:
			numlist[i] = numlist[i]
		else:
			numlist[i] = numlist[i]+numlist[i-1]
	return numlist

def main():
	
	first_list = [1,2,3]
	#Changeable
	
	print '\nOriginal list =',first_list 
	print '\nCumulative sum =',c_sum(first_list)

if __name__ == '__main__':
	main()

#Author: Steven Ramsey