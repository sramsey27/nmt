#!/usr/bin/env python

#Author: Steven Ramsey

def capitalize_all(t):
	
	"""Function provided by Think Python that takes a list of
	strings and returns a list that only contains the uppercase
	strings."""
		
	res = []
	for s in t:
		res.append(s.capitalize())
	return res

def capitalize_nested(t):
	
	"""Takes a nested list of strings and returns a new nested
	list with all strings capitalized."""
	
	lst = []
	for innerlist in t:
		if isinstance(innerlist, list):
			newList = []
			for element in innerlist:
				newList.append(element.capitalize())
				res.append(newList)
		else:
			res.append(innerlist.capitalize())
	return res

def main():

	listone = ['a','b','c','d']
	print capitalize_all(listone)
	
	listtwo = [['a','b','c'],['x','Y','Z'],'z','q']
	print capitalize_nested(listtwo)

if __name__ == '__main__':
	main()
