#!/usr/bin/env python
 
from datetime import date

def sundays():
        D=[31,28,31,
            30,31,30,
            31,31,30,
            31,30,31]
        d,n=0,0
        for i in range(0,101):
            if (not (1900+i)%4 and (1900+i)%100) or not (1900+i)%400: D[1]=29
            else: D[1]=28
            for t in D:             
                if not (d-1)%7 and i>=1:
                    n+=1
                d+=t
		
	return n

def main():
 
	print "\nThe number of Sundays at the first" 
	print "of each month from 1900 to 2000 is", sundays()
	print ' '
 
if __name__ == '__main__':
	main()

	
