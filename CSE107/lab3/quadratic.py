#!/usr/bin/env python

from sys import argv
from math import sqrt


def quadratic(a, b, c):
    if (b ** 2 - 4 * a * c) < 0:
            print "Error: No Real Parts."
            return
    if a == 0:
            print "Error: Divison by Zero."
            return
    print"x = "
    print (-b + sqrt(b ** 2 - 4 *a * c)) / 2 * a
    print "x = "
    print (-b - sqrt(b ** 2 - 4 * a * c)) / 2 * a 

def main():

    a_set = False
    b_set = False
    c_set = False

    # simple for loop to demo enumerate
    # enumerate lets you keep track of the numerical index as well
    # as the data value
    # In this example, enumerate creates tuples (0, argv[0]), (1, argv[1]), etc
    # comment these lines out once you understand what enumerate does
    for i, x in enumerate(argv):
        print 'i = ', i, 'x = ', x

    # process command line args
    # does it matter in what order they are entered?
    for i, x in enumerate(argv):
        if x == 'a':
            # grab the next element after 'a' in the list argv
            a = float(argv[i + 1])  # convert string to a float
            a_set = True
        elif x == 'b':
            b = float(argv[i + 1])  # what happens if user doesn't enter a num?
            b_set = True
        elif x == 'c':
            c = float(argv[i + 1])
            c_set = True
        else:
            pass

    #check to see if all three needed values where set
    if a_set is True and b_set is True and c_set is True:
        #todo find the roots of the quadratic equation
        print a, b, c
        quadratic(a, b, c)

    else:
        print 'usage: quadratic.py a [real] b [real] c [real]'
        print 'a the coefficient of x ** 2 term'
        print 'b the coefficient of the x term'
        print 'c the constant'


#Standard boilerplate to call the main() function to begin
# the program.
if __name__ == '__main__':
    main()
