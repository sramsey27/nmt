#!/usr/bin/env python
from sys import argv


def square_root(a, x, epsilon=1e-12):
	
	while True:
		y = (x + a/x) /2
		if abs(y-x) < epsilon:
		    break
		x = y
	return x


def main():


    print argv[1]
    a = int(argv[1])
    x = a-1
    print square_root(a, x)
    
if __name__== '__main__':
    main()
