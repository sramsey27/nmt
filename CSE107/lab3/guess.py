#!/usr/bin/env python

def guess(a, b, n):    
   n = (( a + b ) / 2)
   #if the guess is yes or greater
   if raw_input('Is the number greater than {0}? (Y/N) '.format(n)) == 'y':
      # this checks for repetitions
      if a == n:
         n = n + 1
      # sets the midpoint as a, but only if greater
      a = n
      
   else:
      # this checks for repetitions
      if b == n:
         a = n
      # sets the midpoint as b but only if not greater than guess
      b = n
      
   return (a, b, n)

def main():
   a=1
   b=100
   n= b / 2
   print "\nWelcome, this is The guessing game. \n\nThink of a number between 1 and 100 and I will guess it!\n".format(a)
   while a!=b:
      a, b ,n = guess(a, b, n)
   print "\nYou were thinking of the number {0} weren't you?\n".format (a)
   return 0

if __name__== '__main__':
    main()
