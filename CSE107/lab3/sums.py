#!/usr/bin/env python

from random import randint


def zeros(n):
    """return a list of len(n) of zeros"""
    return [0 for i in range(n)]

def ones(n):
	return [1 for i in range(n)]

def sum(b):
    total = 0
    for i in range(1, 11):
        total += i
    return total

def sum_even():
    total=0
    for i in a:
        if i % 2 ==0:
            total += +1
        return total

def sum_odd():
    total = 0
    for i in a:
        if i % 1 == 1:
            total=total + 1
    return total

def sum_multiple():
    for i in range(1001):
        if i % 3 == 0 or i % 5 ==0:
            total += i
    return total

def idiot_king():
    wheat = 1
    for i in range(64):
        wheat *= 2
    return wheat

def bushel():
    wheat = idiot_king()
    wheat * 50 * 0.0000022046 / 60
    return wheat

    
def random_list(n, a, b):
    """return a list of len(n) of random integers between [a, b]"""
    lst = []
    for i in range(n):
        lst.append(randint(1, b))
    return lst
    # note you could write this as a one liner but the goal here
    # is to show you the append method for lists
    # return [randint(a, b) for i in range(n)]

def main():
    x = zeros(100)
    print x

    x = random_list(100, 20, 30)
    print x

    print sum(zeros(4))
    print sum(ones(4))
    print sum(i for i in range(1, 11))
    print sum(random_list(10, 0, 1000))
                
#Standard boilerplate to call the main() function to begin
# the program.
if __name__ == '__main__':
    main()
