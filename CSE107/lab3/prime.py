#!/usr/bin/env python

def is_prime(n):
	for i in range(2, n):
		if n % i == 0:
			return False
	
	return True

def main():

#Prints only the Prime numbers.

	for n in range (2,1000):
		if is_prime(n):
			print n, "is Prime."

if __name__ == '__main__':
	main()

	
