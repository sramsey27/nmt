#!/usr/bin/env python

def print_n_recur(s, n):
	if n <= 0:
		return
	print s
	print_n_recur(s, n-1)
	

def print_n_for(s, n):
	for n in range(n):
		print s
		

def print_n_while(s, n):
	while n > 0:
		print s
		n = n-1
	
def main():
	n = 10
	s = "It's just a flesh wound."
	print_n_recur(s, n)
	print_n_for(s, n)
	print_n_while (s, n)
	print ' '
	print "Copyright Monty 'Python'"
	print ' '

	
if __name__ == '__main__':
	main()

	
