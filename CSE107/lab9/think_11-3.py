#!/usr/bin/env python

#Author: Steven Ramsey

from sys import argv

def print_hist(s):

	"""Simply takes in a string, converts to dictionary	
	and prints out the keys with their corresponding
	value in sorted order."""

	diction = dict()
	for c in s:
		dict_value = diction.get(c,0)
		diction[c] = dict_value + 1 

	key = diction.keys()
	dict1 = list(key)

	print "\nUnsorted:"
	for e in dict1:
		print e,':', diction[e]

	dict1.sort()
	print '\nSorted order:'
	for e in dict1:
		print e,':', diction[e]

def main():

	#t = 'Supercalifragilisticexpialidocious'
	t = str(argv[1])
	print_hist(t)

if __name__ == '__main__':
    main()
    
#Author: Steven Ramsey