#!/usr/bin/env python

#Author: Steven Ramsey

import sys
import os
import time
from sys import argv

known = {0:0, 1:1}

def fibonacci1(n):
	
	if n == 0:
		return 0
	elif n == 1:
		return 1
	else:
		return fibonacci1(n-1) + fibonacci1(n-2)

def fibonacci2(n, f):
	
	known = [0, 1]
	for i in range(n):
		newNum = known[0] + known[1]
		known[0] = known[1]
		known[1] = newNum
		f.write(str(known[0]) + " ")
	return known[0]

def main():

	g = (int(argv[1]))
	
	f = open('fib500.txt', 'w')
	r = "Sequence is as follows:\n"
	f. write(r)
	
	time_start = time.time()
	fibonacci1(g)
	time_end = time.time()
	print "\nEnd Result is:",fibonacci1(g)
	print "\nTime for Fibonacci 1 to complete ", time_end - time_start
	
	time_start = time.time()
	fibonacci2(g, f)
	time_end = time.time()
	print "Time for Fibonacci 2 to complete ", time_end - time_start

	f.close()
	print "\nThe Fibonacci Sequence written to fib500.txt within the same directory."	
	
if __name__ == '__main__':
        main()

#Author: Steven Ramsey