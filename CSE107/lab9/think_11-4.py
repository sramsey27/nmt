#!/usr/bin/env python

#Author: Steven Ramsey

from sys import argv

def histogram(s):

	diction = dict()
	for c in s:
		dict_value = diction.get(c,0)
		diction[c] = dict_value + 1 
	return diction

def reverse_lookup(d,v):

	result_list = list()
	for k in d:
		if d[k] == v:
			result_list.append(k)
	#raise ValueError - This could have been put here, but who likes errors?
	return result_list

def main():

	word = str(argv[1])

	h = histogram(word)
	k = reverse_lookup(h, 2)

	if k == []:
		print "\nNope, Nothing."
	else:
		print "\nHere is what I found:"
	print k

if __name__ == '__main__':
    main()
    
#Author: Steven Ramsey