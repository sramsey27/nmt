#!/usr/bin/python -tt

#Since some of the code was provided.

#Partial Author: Steven Ramsey

import random
import sys


def mimic_dict(filename):

    """Returns mimic dict mapping each word to list of words which follow it."""
    # +++your code here++

    str = filename.read()
    strs = str.split()
    dict = {}
    for word in strs:
        if word == strs[0]:
            prev = word
        else:
            if prev in dict:
                dict[prev].append(word)
            else:
                dict[prev] = []
                dict[prev].append(word)
    return dict

def print_mimic(mimic_dict, word):
  
    """Given mimic dict and start word, prints random words."""
    # +++your code here+++
    count = 0
    for count in range(2000):
        count += 1
        if word in mimic_dict.keys():
            print random.choice(mimic_dict[word]),
        elif word == '':
            temp = random.choice(mimic_dict.keys())
            print random.choice(mimic_dict[temp]),
        else:
            print 'The Keyword does not exist in Dictionary!'
    return

# Provided main(), calls mimic_dict() and mimic()
def main():
    
    
    file1 = open(str(sys.argv[1]), 'r') 
    print ''
    if len(sys.argv) != 2:
        print 'Mimic.py needs a file argument to mimic properly.'
        sys.exit(1)
    dict = mimic_dict(file1)
    print_mimic(dict, '')
    #I written the code to where it would work with what was provided.
    
    #Just for fun
    if sys.argv[1] == 'words.txt':
      print "\n\nIt makes no sense. There is a chance a inappropriate word is sitting in there."
    elif sys.argv[1] == 'mphg.txt':
      print '''\n\nKing Author: "Look, you stupid Bastard. You've got no arms left."'''
      print '''\nBlack Knight: "It's just a Flesh Wound."'''
    elif sys.argv[1] == 'alice.txt':
      print '''\n\n"I can't go back to yesterday because I was a different person then."''' 
    else:
      print "\n\n",sys.argv[1],"has been mimicked!"

if __name__ == '__main__':
    main()

#Partial Author: Steven Ramsey