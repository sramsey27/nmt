#!/usr/bin/env python

#Author: Steven Ramsey

from sys import argv

def histogram(s):

	""" Using the .get function, provides a histogram from user
	command line argument."""

	diction = dict()
	for e in s:
		dict_value = diction.get(e,0)
		diction[e] = dict_value + 1 
	return diction

def main():

	t = str(argv[1])
	print '\nHistogram results are as follows for',t,':\n',histogram(t)

if __name__ == '__main__':
	main()

#Author: Steven Ramsey