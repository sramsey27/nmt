#!/usr/bin/env python

#Author: Steven Ramsey

import time

def has_duplicates_slow(t):
	"""Using dictionaries, this function checks for elements that
	appear more than once in a sequence."""

	dict = {}
	for x in t:
		if x in dict:
			return True
		dict[x] = True
	return False

def has_duplicates_fast(e):
	"""Using the built in set and length functions to check for
	elements that appear more than once in a sequence. Which is
	faster than dictionaries"""

	return len(set(e)) < len(e)

def main():

	lst = [1,2,3,4,5,6,7,8,9]

	time_start1 = time.time() #to time the functions.
	print "\n", has_duplicates_slow(lst)
	lst.append(1)
	print has_duplicates_slow(lst), "\n"
	time_end1 = time.time()

	print "It took has_duplicates_slow:",time_end1 - time_start1

	time_start2 = time.time()
	print "\n",has_duplicates_fast(lst)
	lst.append(1)
	print has_duplicates_fast(lst),"\n"
	time_end2 = time.time()

	print "It took has_duplicates_fast:", time_end2 - time_start2

	if time_end1 > time_end2:
		print "\nThe function has_duplicate_slow is faster."
	else:
		print "\nThe function has_duplicate_fast is faster."

if __name__ == '__main__':
	main()

#Author: Steven Ramsey