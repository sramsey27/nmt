#!/usr/bin/env python

#Author: Steven Ramsey

from sys import argv

def main():

	"""Goes through words.txt and splits each line
	and puts it into a dictionary. Search through
	dictionary is done with command line argument."""

	file1 = open('words.txt', 'r')

	diction = {}

	for line in file1:
		words = line.split()
		for i in range(len(words)):
			diction[words[i]] = i

	a = str(argv[1]) #Define word to search
	s = a.lower()

	if s in diction: #dictionary search
		print'\n', s,"is in the dictionary."
	else:
		print '\n', s,"is not in the dictionary"

	file1.close()

if __name__ == '__main__':
	main()

#Author: Steven Ramsey