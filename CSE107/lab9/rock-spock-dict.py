#!/usr/bin/env python

#Rewritten Rock-Spock

#Author: Steven Ramsey

import random

def winner(move, computer):
    
    """The winner function written in dictionary format, depending on move, the key is searched for
    and the value is returned"""
    
    wind = {'scissors':{'scissors':"\nIt's a tie!\n",
				'rock':'\nComputer wins, Rock crushes scissors!\n',
				'paper':'\nYou win, Scissors cuts paper!\n',
				'spock':'\nComputer wins, Spock smashes scissors!\n',
				'lizard':'\nYou win, Scissors decapitates lizard!\n'
						},
		'rock':{'rock':"It's a tie!\n",
				'scissors':'\nYou win, Rock crushes scissors!\n',
				'paper':'\nComputer wins, Paper covers rock!\n',
				'spock':'\nComputer wins, Spock vaporizes rock!\n',
				'lizard':'\nYou win, Rock crushes lizard!\n'},
		'paper':{'paper':"\nIt's a tie!\n",
				'scissors':'\nComputer wins, Scissors cuts paper!\n',
				'rock':'\nComputer wins, Paper covers rock!\n',
				'spock':'\nYou win, Paper disproves Spock!\n',
				'lizard':'\nComputer wins, Lizard eats paper!\n'
				 },
		'spock':{'spock':"\nIt's a tie!",
				'scissors':'\nYou win, Spock smashes scissors!\n',
				'rock':'\nYou win, Spock vaporizes rock!\n',
				'paper':'\nYou win, paper disproves spock!\n',
				'lizard':'\nComputer wins, Lizard poisons Spock!\n'
				 },
		'lizard':{'lizard':"\nIt's a tie!\n",
				'scissors':'\nComputer wins, Scissors decapitates lizard!\n',
				'rock':'\nComputer wins, rock crushes lizard!\n',
				'paper':'\nYou win, Lizard eats Paper!\n',
				'spock':'\nYou win, Lizard poisons Spock!\n'}
}
    return wind[move][computer]

def computer():
    
    """Computer choice rewritten to dictionary, depending on randint, the key is searched
    for and the value is returned"""

    move = random.randint(1,5)
    cd = {1:'rock',2:'scissors',
    	3:'paper',4:'lizard',5:'spock'}
    
    print '\nThe Computer chose:', cd[move]
    return cd[move]

def move_is_valid(move):
    
    """Returns true if the move is rock, paper, scissors, lizard, or spock
       false otherwise"""
       
    if move == 'rock'or move == 'scissors' or move == 'paper' or move == 'spock' or move == 'lizard':
        return True
    else:
        return False

def main():

    #play rock-paper-scissors-lizard-Spock against the machine

    move = 'play'
    print "\nWelcome to Rock-Paper-Scissors-Lizard-Spock!\n"
    while move != 'quit':
        
        move = raw_input("Enter a move (Press 'h' if you need help): ")
        move = move.lower()
        
        if move == 'h':
            print "\nHelp:\n"
            print 'This program allows you to play rock, paper,'
            print "scissors, lizard, Spock against the machine!\n"
            print "How Exciting!\n"
            print "Now Here's how it works:"
            print 'Valid moves are "rock", "paper", "scissors",'
            print '"lizard", or "Spock".\n'
            print 'To escape this maddness enter "quit".\n'
            print "\nIt's your move."
            
        elif move_is_valid(move):
            print "\nYou chose:", move
            print winner(move, computer())
            
        elif move != 'quit':
            print "\n",move, "is an invalid move, please try again.\n"

if __name__ == '__main__':
    main()

#Author: Steven Ramsey