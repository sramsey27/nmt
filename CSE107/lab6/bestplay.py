#!/usr/bin/env python

#Author: Steven Ramsey

from sys import argv

def use_only(word,jc):
    for c in word:
        word_char = c.strip()
        match = True
        if word_char in jc and word_char != '':
            match = True
            jc = jc.replace(word_char,'')
        else:
            match = False
        if match == False:
            return False
    return True
    
    
    """Assigns score to each letter and adds apon each letter
    assigned score."""
    
def score(string):
    dic = {'a':1,'b':3,'c':3,'d':2,'e':1,'f':4,'g':2,'h':4,
           'i':1,'j':8,'k':5,'l':1,'m':3,'n':1,'o':1,
           'p':3,'q':10,'r':1,'s':1,'t':1,'u':1,'v':4,
           'w':4,'x':8,'y':4,'z':10}
    scr = 0
    for c in string:
        scr += dic[c]
    return scr

def main():
    
    """Assigns s the file 'scrabble.txt', then appends and strips
    the words as they are added to scrabbelist."""
    
    print ''
    s = open('scrabble.txt')
    scrabblelist = []
    for word in s:
        scrabblelist.append(word.strip())
    s.close()

    done = False
    while not done:
        test = raw_input('Enter a word: ')
        print ''
        if test.isdigit():
            done = True
        else:
            bestmatch = {}
            for word in scrabblelist:
                if use_only(word, test):
                    bestmatch[word] = score(word)
            c = 0
            for w in sorted(bestmatch, key=bestmatch.get,reverse = True): 
                print w, 'is worth:',bestmatch[w],'points.'
                
                c += 1 
                if c >= 10:
                    break
            print ''
if __name__ == '__main__':
    main()

#Author: Steven Ramsey
