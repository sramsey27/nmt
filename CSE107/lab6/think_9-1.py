#!/usr/bin/env python

#Author: Steven Ramsey

def word_find(word):
	
	"""Basically prints all words with 20 or more characters"""

	for line in word:
		words = line.strip()
		if len(words) >= 20:
			print words

def main():
	
	"""First prints out message for terminal"""
	
	print "\nThese words have twenty or more characters:\n"
	
	"""Opens and by default reads words.txt"""	
	
	infile = open('words.txt')
	
	#as long as words.txt is present within think_9-1.py's
	#directory.
	
	#argv[1] could have been used if this program was used for more
	#than just words.txt. Like so:

	#infile = open(argv[1], 'r')
	
	"""Basic function call, argument assignment, and closure of words.txt"""
	
	word_find(infile)
	infile.close
	
if __name__ == '__main__':
	main()

#Author: Steven Ramsey
	
