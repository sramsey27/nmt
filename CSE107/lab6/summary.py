#!/usr/bin/env python

#Author: Steven Ramsey

def output_file(l):
    
    outfile = open('summary.txt', 'w')
    outfile.write('number of items = {0}\n'.format(len(l)))
    outfile.write('sum = {0}\n'.format((sumx(l))))
    outfile.write('mean = {:.3f}\n'.format((mean(l)))) 
    outfile.write('median = {}\n'.format((median(l))))  
    mod = 0
    mod = mode(l)
    outfile.write('mode = {} '.format((mod)))
    outfile.write('appears in file {} times\n'.format(l.count(mod)))
    outfile.write('min = {}\n'.format((minimum(l)))) 
    outfile.write('max = {}\n'.format((maximum(l)))) 
    outfile.write('%s' % '\n'.join(map(str, l)))

def mean(l):
    mean = float(sumx(l) / len(l))
    return mean

def sumx(l):
    sumx = 0.0
    for i in range(len(l)):
        sumx = sumx + l[i]
    return sumx

def median(l):
    median = 0.0
    n = len(l)
    l.sort()
    if n%2 == 0: 
        # n is even
        median = (l[n/2]+l[(n/2)+1])/2
    else:
        # n is odd
        i = (n+1)/2
        median = l[i]

    #print (n)
    #print ('[%s]' % ', '.join(map(str, l)))
    return median

def mode(l):
    mode = 0
    cnt = {}
    l2 = set(l)   
    l.sort()
    for i in l2:
       cnt[i] = l.count(i)
    oldval = 0
    for w in sorted(cnt,key=cnt.get,reverse=True):
        #print w, cnt[w]
        if oldval == 0:
            oldval = cnt[w]
            mode = w
            break
    return mode

def minimum(l):
    return min(l)

def maximum(l):
    return max(l)

def main():
    l = []
    f = open('integers.txt')
    for line in f:
        l.append(int(line))
    f.close()
    output_file(l)  

if __name__ == '__main__':
    main()
    
#Author: Steven Ramsey
