#!/usr/bin/env python
from sys import argv

#Author: Steven Ramsey

"""Strips every word that has the length of 2 characters"""

def word_count(c):
    for line in c:
        if len(line.strip()) == 2:
            print line
    
def main():
    
    """Basic function call and variable assignment"""
    
    # Written for universal use
    # Scabble.txt is used originally
    
    s = open('scrabble.txt')
    
    word_count(s)


if __name__ == '__main__':
    main()

#Author: Steven Ramsey