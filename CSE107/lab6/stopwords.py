#!/usr/bin/env python
from sys import argv

#Author: Steven Ramsey

def file_open():
    infile = open(argv[1],'r')
    data = infile.read()
    infile.close()
    return data

def uniquelist(wl):
    output = []
    dic = {}
    for word in wl:
        dic[word] = 0

    for key in dic:
        output.append(key)
    return output

def write_file(uni_word_list,filename):
    
    outfile = open(filename, 'w')
    for word in uni_word_list:
        outfile.write(word +'\n')
    outfile.close()

def strip_char(str):
    str = ''.join(c for c in str if c.isalnum())
    return str

def remove_from_list(l1,l2):
    newlist = []
    for o in l1:
        found = False
        for p in l2:
            if o == p:
                found = True
        if not found:
            newlist.append(o)
    return newlist

def main():
    functionlist = []
    s = open('function_words.txt')
    for line in s:
        functionlist.append(line.strip())
    s.close()

    stoplist = []
    s = open('stopwords.csv')
    for line in s:
        stoplist= line.strip().split(',')
    s.close()

    analist = []
    s = open(argv[1])
    for line in f:
        t = line.strip().split(' ')
        for x in t:
            analist.append(strip_char(x.lower())) 
    s.close()

    print len(analist)
    unique = uniquelist(analist)
    print len(unique)
    write_file(unique, argv[1]+'_unique.txt')
    without_stop = remove_from_list(unique, stoplist)
    print len(without_stop)
    write_file(without_stop,argv[1]+'_stop.txt')
    
    without_func = remove_from_list(unique, functionlist)
    print len(without_func)
    write_file(without_func,argv[1]+'_function.txt')
    
    # print statements that output percentages.

    print 'percent of unique words to total words',float(len(unique))/len(analist)*100
    print 'percent of unique words without stop words to total words',float(len(without_stop))/len(analist)*100
    print 'percent of unique words without stop words to unique words',float(len(without_stop))/len(unique)*100
    print 'percent of unique words without function words to total words',float(len(without_func))/len(analist)*100
    print 'percent of unique words without function words to unique words',float(len(without_func))/len(unique)*100

if __name__ == '__main__':
    main()

#Author: Steven Ramsey

