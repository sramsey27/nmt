#!/usr/bin/env python

#Author Steven Ramsey

def avoids(word, avoid_list):
    
    """Searches through the "Avoid_list" argument for forbidden letters"""
    
    for char in word:
        if char in avoid_list:
	    
	    """Returns False if defined characters are found in the defined word"""
	    
	    print "\nI have found letters you defined in this word, so it's:"
            return False
    
    """Returns true if no characters in the list are found in the word"""
    
    print "\nThere is no letters you defined in this word, this makes it:"        
    return True
    
    
def chars_to_avoid(letters):
    
    #Start off with a empty list for chars_to_avoid.
    chars_to_avoid = []

    #This runs through the list defined by the user.
    for char in letters:

        if char not in chars_to_avoid:

	    """If the char is not in our list append it"""
            chars_to_avoid.append(char)

    """Then Returns"""
    return chars_to_avoid

def main():

    print "Welcome!\n"
    
    """Defines the list and word also calls the functions with new arguments"""
    
    word = raw_input("Please enter the word of your choice: ").strip()
    letters = raw_input("Now enter a list of letters to avoid: ").strip()
    print "\nYou chose the word:", word
    list_to_avoid = chars_to_avoid(letters)
    print "You wish to avoid", list_to_avoid
    print avoids(word, list_to_avoid)
    
	
if __name__ == '__main__':
	main()

#Author: Steven Ramsey