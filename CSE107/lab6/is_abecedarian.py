#!/usr/bin/env python

#Author: Steven Ramsey

"""Determines if the word is a abecedarian then returns True or False
depending if the word/line passes"""

def is_abecedarian(word):

	#Starts off by giving previous_letter a blank string.

	previous_letter = ''
	for letter in word:

		letter = letter.lower()
		if letter < previous_letter:

			return False
		previous_letter = letter

	return True


"""Everytime a valid abecedarian passes though is_abecedarian it is
added to the abecedarian count in this function."""

def abecedarian_count(infile):
	
	#Starts the count at zero.
	
	abecedarian_count = 0
	for line in infile:
		line = line.strip()
		
		#Checks if is_abecedarian is True, if so it adds 1 to the count.
		
		if is_abecedarian(line):
			abecedarian_count = abecedarian_count + 1
		
		#Then returns for later printage.
		
	return abecedarian_count

def main():

	"""Reads words.txt (if present in current directory) and reads it line by line.
	Then starts a for loop for repeated access of words, and if the word/line passes
	is_abecedarian, it prints the word and adds to the count witin abecedarian_count
	then prints it for each word"""

	#Usual opening of the file and reading line by line.

	infile = open('words.txt', 'r')
	words = infile.readlines()

	#The for loop for repeated access of lines/words.
	
	for word in words:
		word = word.strip
		
		#Checks to see if word/line passes is_abecedarian.
		
		if is_abecedarian(word):
			print word
		
	print "\nThere are",abecedarian_count(words), "abecedarians in word.txt"
	
if __name__ == '__main__':
    main()
    
#Author: Steven Ramsey