#!/usr/bin/env python

#Author: Steven Ramsey

def uses_only(word, use_only_lst):
	
	for letter in word:
		
		"""Checks if defined letters are present in defined word"""
		
		if letter not in use_only_lst and letter != " ":
			print "Assigned letters not in defined word, it is:"
			return False
		
	print "Assigned letters found in defined word, it is:"
	return True

def letters_lst(letters):
	
	"""Makes variable 'letters' into a list for future use"""
	
	letters_lst = []
	for char in letters:
		letters_lst.append(char)
	return letters_lst
	

def main():
	
	print "Welcome!"
	
	"""Defines the word and letters that can only be in defined word"""
	
	word = raw_input("\nEnter a word of your choice: ").strip()
	letters = raw_input("Now enter a list of letters to use only: ").strip()
	
	"""Failsafe that changes letters to lower case to prevent errors"""
	
	letters.lower
	
	""" Self explainitory for future use in the code"""
	
	use_only_lst = letters_lst(letters)
	
	"""Just gestures to point out the user inputs within the terminal"""
	
	print "\nYou chose the word:",word
	print "You chose to use these letters only: ", use_only_lst, '\n'
	
	print uses_only(word, use_only_lst)
	

if __name__ == '__main__':
	main()

#Author: Steven Ramsey
	
