#!/usr/bin/env python
from sys import argv

#Author: Steven Ramsey

"""Binary Search"""

def valid_word(word,slist):
    low = 0
    high = len(slist)
    while low < high:
        mid = (low + high)/2
        if slist[mid] < word:
            low = mid + 1
        elif slist[mid] > word:
            high = mid
        else:
            return mid
    return -1

def main():
    
    """Opens file and assigns to s. Then adds the words
    to a list"""
    print ''
    s = open('scrabble.txt')
    scrablist = []
    for word in s:
        scrablist.append(word.strip())
    s.close()
    
    """While loop to make the program continously ask
    for words and check if they are valid or not through the
    valid_word function"""
    
    done = False
    while not done:
        test = raw_input('Enter a word: ')
        if test.isdigit():
            done = True
        else:
            if valid_word(test.lower(),scrablist) != -1:
               print test,'is in the scrabble dictionary'
            else:
                print 'The word is not valid, please try again.'


if __name__ == '__main__':
    main()

#Author: Steven Ramsey