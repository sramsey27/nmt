#!/usr/bin/env python


#Author: Steven Ramsey


def use_all(word, use_all_lst):
	
	"""Checks if defined letter is present in defined word"""
	
	for needed_letter in use_all_lst:
		needed_letter.lower
		if needed_letter not in word and needed_letter != " ":
			print "\nSome/all letters not present in defined word, it is: "
			return False
		
	print "\nLetters are present in defined word, it is:"
	return True

def letter_lst(letters):
	
	"""Starts the list off empty then puts 'letters' inside the list"""
	
	letter_lst = []
	
	for char in letters:
		letter_lst.append(char)
	return letter_lst

def main():
	
	"""Assigns the word and list by user input and calls the functions with assigned
	arugments"""
	
	print "Welcome!"
	
	word = raw_input("\nEnter the word of your choice: ").strip()
	letters = raw_input ("Now, enter the letters you would like to use: ").strip()
	
	#for the words.txt you can uncomment the loine below and comment the raw input above:
	#word = open('words.txt', 'r')
	
	#Failsafe, sets the case for letters to lower to prevent errors.
	
	letters.lower
	
	#Makes use_all_string equal to letter_lst(letters) function.
	
	use_all_string = letter_lst(letters)
	
	print "\nThe word you chose is: " ,word
	print "The letters you chose is: ",use_all_string
	
	#Prints out 'true' or 'false' depending on input.
	
	print use_all(word, use_all_string)
	
	

if __name__ == '__main__':
	main()

#Author: Steven Ramsey
	
