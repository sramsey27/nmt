#!/usr/bin/env python

#Author: Steven Ramsey

def has_no_e(word):
	
	"""Lowers Case of characters and checks for e.
	Returns true if e is found within the text file"""
	
	char = ''
	char.lower
	for char in word:
		if char == 'e':
			print "\nThe character 'e' was not found in the current text."
			return False
		else:
			print "\nThe Character 'e' is present in the current text."
			return True

def main():
	
	"""Opens 'gadbsy_updated.txt' and assigns it to infile"""
	
	infile = open('gadbsy_updated.txt')
	
	#again, argv[1] could have been used if this program was used for more
	#than just gadbsy_updated.txt. Like so:

	#infile = open(argv[1], 'r')
	
	"""Calls functions, assigns arguments, and prints functions (if availible)"""
	
	print has_no_e(infile)
	
	"""Closes file assigned to infile"""
	
	infile.close
	
if __name__ == '__main__':
	main()

#Author: Steven Ramsey
	
