#!/usr/bin/env python
from sys import argv

#Author: Steven Ramsey

"""Runs through the words and filters according to the
Vowels and prints the words that only consist of consonants"""

def consonants(word,vowels):
    for c in vowels:
        if c in word:
            return False
    print word
    return True
    
def main():
    
    """Assigns s the scrabble.txt file and send each line
    to the consonants function to be filtered according to
    the """
    
    s = open('scrabble.txt')
    for line in s:
        consonants(line,'aeiouy')

if __name__ == '__main__':
    main()

#Author: Steven Ramsey