#!/usr/bin/env python

#Author: Steven Ramsey

import sys

def balance_parenthesis(input):
    
    """Starts with a blank stack and predefined dictionary
    for the sumbols then scans the input left to right to
    check for balenced parenthesis"""
    
    stack = []
    dic = {')':'(','}':'{',']':'[','\'':'\''}
    for c in input:
        if c in dic.values():
            stack.append(c)
            print '\nFile Found'  + c
        elif c in dic.keys():
            if len(stack) == 0:
                return False
            x = stack.pop()
            if x != dic[c]:    
                return False
    if len(stack):
        return False
    else:
        return True

def main():
    
    try: #Attempt function
        print '\nThe File is: ' + sys.argv[1]
        infile = open(sys.argv[1],'r')
        if balance_parenthesis(infile.readline()):
            print '\nThe File',sys.argv[1], "is balanced."
        else:
            print '\nThe File', sys.argv[1], "is not balanced."
        infile.close()
    except IOError as e: #Ignore IOError
        print '\nFile does not Exist ({0}): {1}'.format(e.errno, e.strerror)
    
if __name__ == '__main__':
    main()

#Author: Steven Ramsey