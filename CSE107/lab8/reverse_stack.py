#!/usr/bin/env python

#Author: Steven Ramsey

from collections import deque
 
def main():
    
    """Assigns stack intergers 1 through 50 and utilizes
    deque to reverse the elements in a stack"""
    
    stack = []
    for i in range(1,51):
        stack.append(i)
    print "\nOriginal Stack\n",stack
    que = deque()
    for i in range(len(stack)):
        v = stack.pop()
        que.append(v)   
    for i in range(len(que)):
        v = que.popleft()
        stack.append(v)
    print "\nReversed Stack:\n",stack
    
if __name__ == '__main__':
    main()

#Author: Steven Ramsey