#!/usr/bin/env python

#Author: Steven Ramsey

from sys import argv

def dec2hex(m):
    
    """Using dictionaries, Numbers are assigned values
    then proceeds to convert and appends to stack then pops
    off then formatted"""
    
    hexdigit= {1:'1',2:'2',3:'3',4:'4',
             5:'5',6:'6',7:'7',8:'8',9:'9',0:'0',10:'A',
             11:'B',12:'C',13:'D',14:'E',15:'F'}
    q = m
    stack = []
    while q != 0:
        q, r = divmod(q, 16)
        stack.append(r)

    s = str()
    while stack:
        digit = stack.pop()
        s += (hexdigit[digit])
    print '\n{} = {}'.format(m, s)

def main():
    
    # demo stack
    # convert decimal to hex
    dec2hex(0xff)

if __name__ == '__main__':
    main()

#Author: Steven Ramsey