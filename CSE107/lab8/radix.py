#!/usr/bin/env python

#Author: Steven Ramsey

import random

def random_num(n,r):
    
    """Applys random numbers into a list that is in
    range of the previous assignment of r then returns."""
    
    lst = []
    for i in range(0,n):
        lst.append(str(random.randint(0,r)).zfill(5))
    return lst


def radix_sort(l):
    for i in range(len(l[0])-1,-1,-1):
        array_dict = {'0':[],'1':[],'2':[],'3':[],'4':[],
                 '5':[],'6':[],'7':[],'8':[],'9':[]}
        
        for n in range(0,len(l)):
            v = l.pop()
            c = v[i]
            array_dict[str(c)].append(v)
        for x in range(0,10):
            for y in range(len(array_dict[str(x)])-1, -1, -1):
                l.append(array_dict[str(x)][y]) 
    return l

def main():
    
    lst = random_num(100,100000)
    
    print lst
    print radix_sort(lst)
    
if __name__ == '__main__':
    main()
    
#Author: Steven Ramsey
