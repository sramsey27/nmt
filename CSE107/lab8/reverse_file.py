#!/usr/bin/env python

#Author: Steven Ramsey

from sys import argv

def reverse_file(infile,outfile):
    
    """Takes alice.txt and appends each word in a stack
    then proceeds to a while loop that pops from the
    stack and puts the words in a file (output.txt)"""
    
    stack = []
    for i in infile:
        stack.append(i)
    while len(stack) != 0:
        outfile.write(stack.pop(-1))
            
def main():
    
    outfile = open('output.txt','w')
    file1 = open('alice.txt','r') 
    reverse_file(file1,outfile)
    print "\nAlice.txt has been reversed and outputed"
    print "to output.txt within the same directory."
    file1.close()
    outfile.close()
    
if __name__ == '__main__':
    main()

#Author: Steven Ramsey