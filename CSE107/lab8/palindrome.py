#!/usr/bin/env python

#Author: Steven Ramsey

from sys import argv

def is_palindrome(word):

    """Determines if word is a palindrome. Returns
    True or False"""

    stack = []
    half = len(word)/2
    mid = half 
    if len(word) % 2:
        mid += 1
    for i in range(0,half):
        stack.append(word[i])
    for i in range(mid, len(word)):
        c = stack.pop()
        if c != word[i]:
            return False
    return True


def main():
    
    """Creates a file and stores Palindromes within
    also produces a percentage of words that are
    Palindromes out of words.txt""" 
    
    file1 = open('words.txt')
    newfile = open('palindrome.txt', 'w')
    word_count = 0
    palindrome_count = 0
    for word in file1.readlines():
        word = word.strip()
        word_count += 1
        if len(word) > 1:
            print word
            if is_palindrome(word):
                palindrome_count += 1
                newfile.write(word + '\n') 
    newfile.write(str((palindrome_count*100.0)/word_count) + ' Percent')
    print "\nAll palindromes outputted to 'palindrome.txt'"
    print "within the same directory."

    file1.close()
    newfile.close()
    
if __name__ == '__main__':
    main()

#Author: Steven Ramsey
