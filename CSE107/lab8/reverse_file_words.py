#!/usr/bin/env python
from sys import argv

def reverse_file(infile,outfile):
    
    """Reverses the lines and the words as well
    then outputs to out_reverse.txt."""
    
    stack = []
    for i in infile:
        string1 = ''
        stack1 = []
        for words in i.split():
            stack1.append(words)
        while len(stack1) != 0:
            string1 = string1 + ' ' +stack1.pop(-1)
        stack.append(string1)
    while len(stack) != 0:
        outfile.write(stack.pop(-1)+ '\n')
    
def main():
    
    outfile = open('output_reverse.txt','w')
    file1 = open('alice.txt','r') 
    reverse_file(file1,outfile)
    file1.close()
    outfile.close()
    
    print "\nAlice.txt has been reversed and has been outputted"
    print "to output_reverse.txt."
    print "\nCheck it! It works!"

if __name__ == '__main__':
    main()
