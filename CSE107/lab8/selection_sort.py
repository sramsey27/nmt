#!/usr/bin/env python

from sys import argv
 
#Author: Steven Ramsey

def selection_sort(stack):
    
    """Makes use of two stacks (one original
    the other a temporary), unsorted is
    moved to temp, then goes through a loop
    until stack is properly sorted"""
    
    tmp =  []
    length = len(stack)
    for i in range(0,length):
        max = 0
        for j in range(i,length):
            t = stack.pop()
            if t > max:
                max = t 
            tmp.append(t)
        stack.append(max)
        for k in range(0,len(tmp)):
            t = tmp.pop()
            if t is not max:
                stack.append(t)
    return stack

def main():
    
    #Changeable
    stack = [1,9,6,5,4,2,3,11]
    #stack = [int(argv[1])] #Didn't work haha
    print "\nOriginal Unsorted Stack:\n",stack
    print "\nNewly sorted stack:\n",selection_sort(stack)
    
if __name__ == '__main__':
    main()

#Author: Steven Ramsey