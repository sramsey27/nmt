#Author: Steven Ramsey

import turtle
import argparse
import sys
import random

def main():
    print '___________________________________________'
    p = argparse.ArgumentParser()

    # help strings dictionary
    help_str = {'pencolor': 'The color of the pen to draw the turtle with. A rgb hex string , e.g., #FFFFFF.',
                'penwidth': 'The width of the pen to start with. An interger.',
                'random': 'The number of times to repeat a color for the F operation before changing to a new random color. An integer. If set than the pencolor option is ignored.',
                'initpos': 'Set the initial position (x, y) of the turtle. This takes two integer arguments. The x-coordinate is the first value.',
                'initheading': 'Set the initial heading of the turtle. A float. The turn is to the left.',
                'scale': 'Set the scale factor for > and < operations. A float.',
                'bgcolor': 'Set the background color of the screen. A rgb hex string, e.g., #FFFFFF.',
                'distance': 'Change the distance that the turtle travels forward.',
                'width': 'Set the width of the screen in pixels. An integer.',
                'angle': 'Change the angle the turle will turn.',
                'height': 'Set the height of the screen in pixels. An integer.',
                'start': 'Initalize the turtle.',
                'startx': 'Set the x coordinate of the upper left corner of the screen. An integer.',
                'starty': 'Set the y coordinate of the upper left corner of the screen. An integer.',
                'rule': 'The input of the axiom string.',
                'iterations': 'The number of times the code will execute.',
                }

    # turtle color
    p.add_argument('-c', action="store", type=str, dest='pencolor', help=help_str['pencolor'])
    p.add_argument('--pencolor', action="store", type=str, dest='pencolor')
    p.add_argument('--random', action="store", type=int, dest='random', help=help_str['random'])
    p.add_argument('--scale', action="store", type=float, dest='scale', help=help_str['scale'])

    # init turtle placement
    p.add_argument('-p', action="store", type=int, nargs=2, dest='initpos', help=help_str['initpos'])
    p.add_argument('--initpos', action="store", type=int, nargs=2, dest='initpos')
    p.add_argument('--initheading', action="store", type=int, dest='initheading', help=help_str['initheading'])

    # Project command line options
    p.add_argument('-a',action ="store", type=float, dest='angle', help=help_str['angle'])
    p.add_argument('-d',action ="store", type=int, dest='distance', help=help_str['distance'])
    p.add_argument('-s',action ="store", type=str, dest='start', help=help_str['start'])
    p.add_argument('-r',action ="store", type=str, dest='rule', help=help_str['rule'])
    p.add_argument('-n',action ="store", type=int, dest='iterations', help=help_str['iterations'])
    p.add_argument('--angle',action ="store", type=float, dest='angle', help=help_str['angle'])
    p.add_argument('--distance',action ="store", type=int, dest='distance', help=help_str['distance'])
    p.add_argument('--start',action ="store", type=str, dest='start', help=help_str['start'])
    p.add_argument('--axiom',action ="store", type=str, dest='start', help=help_str['start'])
    p.add_argument('--rule',action ="store", type=str,dest='rule', help=help_str['rule'])
    p.add_argument('--iterations',action ="store", type=int,dest='iterations', help=help_str['iterations'])

    #Pen Width
    p.add_argument('-w',action="store", type=int, dest='penwidth', help=help_str['penwidth'])
    p.add_argument('--penwidth', action="store", type=int, dest='penwidth', help=help_str['penwidth'])

    #screen setup
    p.add_argument('--bgcolor', action="store", type=str, dest='bgcolor', help=help_str['bgcolor'])
    p.add_argument('--width', action="store", type=int, dest='width', help=help_str['width'])
    p.add_argument('--height', action="store", type=int, dest='height', help=help_str['height'])
    p.add_argument('--startx', action="store", type=int, dest='startx', help=help_str['startx'])
    p.add_argument('--starty', action="store", type=int, dest='starty', help=help_str['starty'])
    p.add_argument('--foo', action="store_true", dest='foo')

    #set defaults for options
    p.set_defaults(startx=100,
                   starty=100,
                   distance=10,
                   angle=90,
                   width=500,
                   height=500,
                   bgcolor='#FAF0E6',
                   pencolor='#000000',
                   penwidth=1,
                   initpos=[0, 0],
                   initheading=0,
                   random=0,
                   scale=1,
                   iterations=1,
                   rule ='',
                   )

    #parse the command line
    #arguments are stored in args.dest fields
    args = p.parse_args()
    p = args.rule #rule
    s = args.start
    n = args.iterations
    x = rule_dict_process(p)

    #Calls expand_rule n times.
    for i in range(n):
        s = expand_rule(s, x)
    #print s
    axiom = s

    print "\nImage Statistics:\n"
    print 'startx =', args.startx
    print 'starty =', args.starty
    print 'angle =', args.angle
    print 'width =', args.width
    print 'distance =', args.distance
    print 'height =', args.height
    print 'bgcolor =', args.bgcolor
    print 'pencolor =', args.pencolor
    print 'penwidth =', args.penwidth
    print 'initpos =', args.initpos
    print 'iterations =', args.iterations
    print 'random =', args.random
    print 'scale =', args.scale
    #print 'foo =', args.foo
    print 'axiom =',args.start
    if args.rule == '':
        print "rule = No Input Detected. Exiting...."
        sys.exit()
    else:
        print 'rule =',args.rule

    if args.initheading == 90:
        print "Turtle Points North"
    elif args.initheading == 180:
        print "Turtle Points West"
    elif args.initheading == 270:
        print "Turtle Points South"
    elif args.initheading == 0:
        print "Turtle Points East"
    else:
        pass

    print'___________________________________________'
    
    print "\nWelcome!"

    #Random color generation.
    q = raw_input("\nWould you like to Enable random pen color generation? Y/N: ").upper()
    if q == 'Y':
        j = "Enabled"
    else:
        j = "Disabled"
    print "Random pen color generation is", j, "\n"
    x = raw_input('Would you like to Enable random background color generation? Y/N: ').upper()
    if x == 'Y':
        rt = "Enabled"
    elif x =='N':
        rt = "Disabled"
    print "Random background generation is", rt

    print '\nOpening Python Turtle Graphics Window.....'
    print "\nProcessing Image. Please Wait....."
    
    #Used in Turtle Commands
    d = args.distance
    o = args.scale
    z = args.penwidth
    
    #Prepare Stacks for use.
    push_stack = []
    pop_stack = []

    #Initialize Turtle settings.
    t = turtle.Turtle()
    turtle.bgcolor(args.bgcolor)
    t.pensize(args.penwidth)
    t.setheading(args.initheading)
    t.pencolor(args.pencolor)

    #Turtle Commands.
    for c in axiom:
        if q == 'Y':
            t.pencolor(random.random(),random.random(), random.random())
        if x == 'Y':
            turtle.bgcolor(random.random(),random.random(), random.random())
        if c == 'F':
            t.forward(d)
        elif c == '-':
            t.right(args.angle)
        elif c == '+':
            t.left(args.angle)
        elif c == 'A':
            t.forward(d)
        elif c == 'B':
            t.forward(d)
        elif c == '<':
            t.forward(d/o)
        elif c == '>':
            t.forward(d*o)
        elif c =='[':
            pop_stack.append(t.pos())
            push_stack.append(t.heading())
        elif c ==']':
            t.setposition(pop_stack.pop())
            t.setheading(push_stack.pop())
        elif c == 'X':
            pass
        elif c == 'Y':
            pass
        elif c == '}':
            a = z - 1
            if a < 0:
                print "Negative, nothing will be drawn."
            t.pensize(a)
        elif c == '{':
            a = z + 1
            if a > 10:
                print "The Pen is rather large, the drawing will not be drawn properly."
            t.pensize(a)

    print "\nDone."

def rule_dict_process(p):
    """Processing function which converts argument 'rule' into a dictionary"""

    rules = {}
    f = p.split('&')

    for c in f:
            d = c.split('=')
            rules[d[0]] = d[1]
    #print rules
    return rules

def expand_rule(x, s):
    """Expands the rule depending on axiom input."""
    a = []
    for c in x:
        if c in s:
            a.append(s.get(c))
        else:
            a.append(c)
    return ''.join(c for c in a)

if __name__ == '__main__':
    main()

#Author: Steven Ramsey
