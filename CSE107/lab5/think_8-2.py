#!/usr/bin/env python
print "\nThe Duckling's names are:\n "

def duckling(prefixes, suffix):
    for letter in prefixes:
        if letter in 'OQ':
            print letter + 'u' + suffix 
        else:        
            print letter + suffix
    
def  main():
    prefixes = 'JKLMNOPQ'
    suffix = 'ack'
    duckling(prefixes, suffix)
    
if __name__ == '__main__':
    main()  
