#!/usr/bin/env python

from sys import argv


def line_count(a):
    l = []
    count = 0
    for line in a:
        l.append(line)
    for i in l:
        if i == '\n':
            count += 1
    return count


def word_count(a):
    count = 0
    a = a.split()
    for word in a:
        if word != ' ':
            count += 1
    return count


def char_count(a):
    count = 0
    for char in a:
        count += 1
    print count
            


def main():
    infile = open(argv[1], 'r')
    data = infile.read()
    print line_count(data)
    print word_count(data)
    char_count(data)
    infile.close()


#Standard boilerplate to call the main() function to begin
#the program.
if __name__ == '__main__':
    main()
