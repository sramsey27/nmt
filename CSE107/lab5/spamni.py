#!/usr/bin/env python

def  main():
    t = 'ni'
    s = 'spam'
    print t.upper()
    print t + '!' + (s+t)
    print (s.capitalize() + ' ' + t.capitalize() + '!' + 2 * ' ') * 3
    print s
    print '[' + s[0:2] + ',' + ' ' + s[3] + ']'
    print s[0:2] + s[3]
    print s[0:2] + t + s[2:4]
if __name__ == '__main__':
    main()
