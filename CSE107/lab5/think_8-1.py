#!/usr/bin/env python

from sys import argv

def backwards(word):
    
    index = len(word) - 1  
    while index >= 0:
	print "\nBackward Index: "
        print word[index]
        index = index - 1

def reverse_index(word):
    
    index = -1
    while index >= -len(word):
        print "\nReverse Index: "
	print word[index]
        index = index - 1

def  main():
	backwards(argv[1])
	reverse_index(argv[1])

if __name__ == '__main__':
    main()
