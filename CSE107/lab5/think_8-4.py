#!/usr/bin/env python

from sys import argv

def find(word, letter, index):
    while index < len(word):
        if word[index] == letter:
            print index
            return 0
        index = index + 1
    print 'error'    
    return -1

def main():
    find (argv[1], argv[2], int(argv[3]))
    
if __name__== '__main__':
    main()