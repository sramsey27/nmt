#!/usr/bin/env python

"""This function looks for the assigned letters in the assigned word"""

def count(word, letter):
	i = 0
	for x in word:
		if x == letter:
			i += 1
	print i
	
def  main():
	
# Im not sure if Raw_input counts as an argument, but instead of raw input
# Argv[1] and Argv[2] could be used instead, but Importing would be needed.
	
	# count(argv[1], argv[2]) would be used to call the function
	# as well as provide the arguments.
	
	# I used raw_input instead to provide the user more convinence.
	
	word = raw_input("\nEnter Word: ")
	letter = raw_input("Enter a single letter: ")
	count(word,letter)  	

if __name__ == '__main__':
    main()
