#ifndef BMP_H_
#define BMP_H_

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "apue.h"
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <assert.h>
#include <sys/uio.h>

#define ERR_RET(s) err_ret(#s " %s:%d\n", __FILE__, __LINE__) //need apue to use
#define PERM_644 S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH 
#define BYTE4K 4096

struct HEADER {
	   char type[2];                            /* Magic identifier            */
	   unsigned int size;                       /* File size in bytes          */
	   unsigned short int reserved1, reserved2;
	   unsigned int offset;                     /* Offset to image data, bytes */
};

struct INFOHEADER {
        unsigned int size;               /* Header size in bytes      */
        int width,height;                /* Width and height of image */
        unsigned short int planes;       /* Number of colour planes   */
        unsigned short int bits;         /* Bits per pixel            */
        unsigned int compression;        /* Compression type          */
        unsigned int imagesize;          /* Image size in bytes       */
        int xresolution,yresolution;     /* Pixels per meter          */
        unsigned int ncolours;           /* Number of colours         */
        unsigned int importantcolours;   /* Important colours         */
};

void usage();
void process_files(int red, int green, int blue, char* infile, char* outfile, int r_mode, int d_header, int dump_h, int out_present, int b, int g, int r);
struct HEADER *bmp_header(int infd, struct HEADER *headinfo);
struct INFOHEADER *bmp_infoheader(int infd, struct INFOHEADER *bitmapinfoheader);
int bmpverify(int infd, char temp[]);
int openbmpfile(char *infile);
int open_outfile(char *outfile);
void display_info(int displayheader, int dumpinfo, struct INFOHEADER *infoheader, struct HEADER *mainheader, int infd);
ssize_t read_all(int fd, void *buf, size_t nbytes); //because the PDF said so...
int write_bmpfile(struct INFOHEADER *headerinfo, struct HEADER *mainheader, int infd, int outfd, int red, int green, int blue, int r, int g, int b);
int write8bit(int infd, int outfd, struct INFOHEADER *infoheader, struct HEADER *mainheader);
int process24bit(int infd, int outfd, struct INFOHEADER *infoheader, struct HEADER *mainheader, int red, int green, int blue, int r, int g, int b);


#endif