#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "apue.h"
#include <errno.h>

ssize_t read_all(int fd, void *buf, size_t nbytes) 
{
        ssize_t nread = 0;
        ssize_t n;

        do {
                if ((n = read(fd, &((char *)buf)[nread], nbytes - nread)) == -1) {
                        if (errno == EINTR)
                                continue;
                        else
                                return -1;
                }

                if (n == 0)
                        return nread;
                nread += n;
        } while (nread < nbytes);

        return nread;
}