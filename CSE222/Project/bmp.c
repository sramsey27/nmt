/**
 * @file hexdump.c
 * 
 * @author Steven Ramsey
 *
 * @date 3/5/2016
 *  
 * Assignment: Project 
 *
 * @brief Process BMP file with various effects
 *
 * @details Reads in 8bit and 24bit bmp files and has the ability to modify them according to help guide.
 *  
 * @bugs N/A
 *
 * @todo N/A
 */ 

#include "bmp.h"

int main(int argc, char *argv[]){

	char infile[40], outfile[40] = {0};
	int rev = 0, dis_h = 0, dump_h = 0, red = 0, green = 0, blue = 0, c = 0, out_present = 0;
	int color_set = 0, r = 0, b = 0, g = 0; //

	/*Get user requested infomation and set flags accordingly for checking*/
	while((c = getopt(argc, argv, "i:o:hHDRr:g:b:")) != -1) {
		switch (c) {
			case 'h':
				usage();
				exit(EXIT_SUCCESS);
				break;
			case 'i':
				strcpy(infile, optarg);
				break;
			case 'o':
				strcpy(outfile, optarg);
				out_present = 1;
				break;
			case 'H':
				dis_h = 1;
				break;
			case 'D': 
				dump_h = 1;
				break;
			case 'R':
				rev = 1;
				break;
			case 'r':
				red = atoi(optarg);
				if(red > 255 || red < 0){
					printf("Red cannot be < 0 or > 255!\n");
					exit(EXIT_FAILURE);
				}
				r = 1;
				color_set++; //fixes warnings that color_set = 1 would provoke
				break;
			case 'g':
				green = atoi(optarg);
				if(green > 255 || green < 0){
					printf("Green cannot be < 0 or > 255!\n");
					exit(EXIT_FAILURE);
				}
				color_set++; //fixes warnings that color_set = 1 would provoke
				g = 1;
				break;
			case 'b':
				blue = (atoi(optarg));
				if(blue > 255 || blue < 0){
					printf("Blue cannot be < 0 or > 255!\n");
					exit(EXIT_FAILURE);
				}
				b = 1;
				color_set++; //fixes warnings that color_set = 1 would provoke
				break;
			default:
				printf("Something weird happened\n");
		}
		//unknown command check
		if(isprint(optopt)){ 
			fprintf (stderr, "\nUnknown option `-%c'.\nClosing Program just to be safe...\n", optopt);\
			exit(EXIT_FAILURE);
		}
	}
	/*Mode presence check*/
	if((rev == 0) && (dis_h == 0) && (dump_h == 0) && (r == 0) && (b == 0) && (g == 0)){
		printf("\nNO PRIMARY MODE WAS PROVIDED!\n");
		printf("Use -h flag to open the user guide for help.\n");
		exit(EXIT_FAILURE);
	}
	/*Color value check and out file and appropriate condition check*/
	else if ((outfile[0] == 0) && (color_set)){ //bit logic, fun stuff
		printf("Error: OUTPUT FILE is not provided, please provide an output file \nwhen using flags -R, -r, -g, -b.");
		printf(" Use the -h flag to open the \nuser guide for help.\n");
		exit(EXIT_FAILURE);
	}
	else if ((outfile[0] == 0) && (rev)){ //bit logic, fun stuff
		printf("Error: OUTPUT FILE is not provided, please provide an output file \nwhen using flags -R, -r, -g, -b.");
		printf(" Use the -h flag to open the \nuser guide for help.\n");
		exit(EXIT_FAILURE);
	}

	/*let the processing fun begin*/
	process_files(red, green, blue, infile, outfile, rev, dis_h, dump_h, out_present, b, g, r);

	/*Done*/
	return 0;
}

/**
 * This is where all the fun happens, this function is responsible for most functions involving the bmp files
 * error checking, etc also happen here and takes appropiate action for various errors
 * @param red the red value for 24bit files
 * @param green the green value for 24 bit files
 * @param blue the blue value for 24bit files
 * @param infile the string of the input file (path of file)
 * @param outfile the string of the output file (path of file)
 * @param r_mode flag for reverse mode
 * @param d_header flag for display header
 * @param dump_h flag for dump infomation
 * @param out_present flag for presence of out string
 * @return void
 */
void process_files(int red, int green, int blue, char* infile, char* outfile, int r_mode, int d_header, int dump_h, int out_present, int b, int g, int r){
	
	struct HEADER *headinfo = NULL;
	struct INFOHEADER *bitmapinfoheader = NULL;

	int infd = 0, outfd = 0; /*file discriptors*/

	/*Open the input file discriptor*/
	if((infd = openbmpfile(infile)) == -1)
		goto filecleanup;

	/*allocate space for structures*/
	headinfo = malloc(sizeof(struct HEADER)); //don't allocate structures until files are open
	bitmapinfoheader = malloc(sizeof(struct INFOHEADER));

	/*Assert malloc errors*/
	assert(headinfo);
	assert(bitmapinfoheader);

	/*Read in the intial bmp header, store in structure, and check for verification*/
	if((headinfo = bmp_header(infd, headinfo)) == NULL){ //process header
		goto filecleanup; //if not verified as compatatible BMP
	} 

	/*Read in the DIB header and store in structure*/
	if((bitmapinfoheader = bmp_infoheader(infd, bitmapinfoheader)) == NULL){ //process infoheader DIB
		goto filecleanup; //if something goes wrong
	} 

	/*If requested, display the bmp file infomation*/
	if((d_header == 1) || (dump_h == 1)){ //display infomation at user request
		display_info(d_header, dump_h, bitmapinfoheader, headinfo, infd);
	}

	/*Don't write to file if not necessary*/
	if((bitmapinfoheader->bits == 8) && (r_mode == 0)){ //end it if no writing has to be done for 8 bit
		goto filecleanup;
	}

	/*Finally open the out file discriptor for writing*/
	if(out_present == 1) {//open file given, create if not present
		if((outfd = open_outfile(outfile)) == -1)
			goto filecleanup;
	}
	
	/*If requested, write to provided output file*/
	if(out_present == 1){
		if((write_bmpfile(bitmapinfoheader, headinfo, infd, outfd, red, green, blue, r ,g ,b)) == -1){
			printf("function: write_bmpfile failed\n");
			goto filecleanup;
		}
	}

	/*Done, proceed to filecleanup*/

/*Close open file discriptors*/
filecleanup: 
	printf("\nFreeing memory...");
	if(infd != -1){
		if(close(infd) == -1)
			perror("error closing input file\n");
	}
	if(outfd != -1){
		if(close(outfd) == -1)
			perror("error closing output file\n");
	}
	goto memcleanup; //just to fix the warning...

/*Free allocated variables*/
memcleanup: 
	if(headinfo != NULL){
		free(headinfo);
	}
	if(bitmapinfoheader != NULL){
		free(bitmapinfoheader);
	}
	printf("Success!!! Goodbye!\n\n");	
}

/**
 * Writing magic happens here based on a 24bit or 8bit file.
 * @param headerinfo DIB infomation of the bmp file structure
 * @param mainheader initial header of bmp file info structure
 * @param infd current input file discriptor
 * @param outfd current output file discriptor
 * @param red passed in user color for red
 * @param green passed in user color for green
 * @param blue passed in user color for blue
 * @param r flag to show that color red is set by user
 * @param g flag to show that color green is set by user
 * @param b flag to show that color blue is set by user
 * @return 1 on success, -1 on failure
 */
int write_bmpfile(struct INFOHEADER *headerinfo, struct HEADER *mainheader, int infd, int outfd, int red, int green, int blue, int r, int g, int b){

	unsigned char *buffer = malloc(sizeof(char*) * 54);
	assert(buffer);

	lseek(infd, 0, SEEK_SET); //move to the beginning of the file

	/*If 8bit bmp*/
	if(headerinfo->bits == 8){
		printf("Writing reverse bmp...");
		if(read_all(infd, buffer, 54) == -1){
			return -1;
		}
		if(write(outfd, buffer, 54) == -1){ //rewrite the headers
			perror("8bit write in 'write_bmpfile' failed");
			return -1;
		}
		if(write8bit(infd, outfd, headerinfo, mainheader) == -1){ //write colors
			printf("Write 8 bit function failed\n");
			return -1;
		} 
		printf("Success!!!\n");
		if(buffer != NULL)
			free(buffer);
		return 1;
	}
	/*If 24bit bmp*/
	else if(headerinfo->bits == 24){
		printf("Processing 24bit....");
		read_all(infd, buffer, 54); //rewrite the headers
		write(outfd, buffer, 54);
		if(process24bit(infd, outfd, headerinfo, mainheader, red, green, blue, r, g, b) == -1){
			if(buffer != NULL)
				free(buffer);
			return -1;	
		}
		if(buffer != NULL)
			free(buffer);
		printf("Success!!!\n");
		return 1;
	}
	if(buffer != NULL)
		free(buffer);

	return 1;
}

/**
 * process and write the pixel data for 24bit files along with user color set
 * @param infd input file discriptor
 * @param outfd output file discriptor
 * @param infoheader DIB infomation of the bmp file structure
 * @param mainheader initial header of bmp file info structure
 * @param red passed in user color for red
 * @param green passed in user color for green
 * @param blue passed in user color for blue
 * @param r flag to show that color red is set by user
 * @param g flag to show that color green is set by user
 * @param b flag to show that color blue is set by user
 * @return 1 on success, 0 if failure occurs
 */
int process24bit(int infd, int outfd, struct INFOHEADER *infoheader, struct HEADER *mainheader, int red, int green, int blue, int r, int g, int b) {

	int i = 0, x = 0, readbytes = 0;
	unsigned char *buffer = malloc(sizeof(unsigned char*) * (1536));
	int padding = ((4 - (infoheader->width * 3)) % 4) % 4;

	while((readbytes = (read_all(infd, buffer, 1536))) != 0){
		for(i = 0; i <= readbytes; i++){ //curse this line for causing a misalignment
			if(x <= ((infoheader->width) * 3)){
				if((x % 3) == 0){
					if(b == 1){
						buffer[i] = (char)blue;
					}
				}
				if((x % 3) == 1){
					if(g == 1){
						buffer[i] = (char)green;
					}
				}
				if((x % 3) == 2){
					if(r == 1){
						buffer[i] = (char)red;
					}
				}
			}
			else{
				//buffer[i] == 0;
			}
			if(x > ((infoheader->width * 3) + padding))
				x = 0;
			x++;
		}
		write(outfd, buffer, readbytes);
			//return 0;
	}
       	if(buffer != NULL)
		free(buffer);
	return 1;

}

/**
 * Displays infomation at user request
 * @param displayheader flag to display header
 * @param dumpinfo flag to dump bmp infomation
 * @param infoheader DIB infomation of the bmp file structure
 * @param mainheader initial header of bmp file info structure
 * @param infd input file discriptor
 */
void display_info(int displayheader, int dumpinfo, struct INFOHEADER *infoheader, struct HEADER *mainheader, int infd){

	unsigned char *buffer = malloc(sizeof(unsigned char*) * BYTE4K);
	assert(buffer);

	int i = 0, j = 0, rowcounter = 0, k = 0, rowsize = ((infoheader->bits * infoheader->width) / 32) * 4;

	if(displayheader){ //-H
		printf("\nheader: %s\n", mainheader->type);
		printf("size of bitmap (bytes: %d\n", mainheader->size);
		printf("offset (start of image data): %d\n", mainheader->offset);
		printf("\nsize of dib: %d\n", infoheader->size);
		printf("bitmap width in pixels: %d\nbitmap height in pixels: %d\n", infoheader->width, infoheader->height);
		printf("number of color planes: %d\n", infoheader->planes);
		printf("number of bits per pixel: %d\n", infoheader->bits);
		printf("compression method: %d\n", infoheader->compression);
		printf("image size: %d\n", infoheader->imagesize);
		printf("horizontal resolution (pixel per meter): %d\n", infoheader->xresolution);
		printf("vertical resolution (pixel per meter): %d\n", infoheader->yresolution);
		printf("number of colors: %d\n", infoheader->ncolours);
		printf("number of important colors: %d\n", infoheader->importantcolours);
	} 
	if(dumpinfo){ //-D
		printf("\nheader: %s\n", mainheader->type);
		printf("size of bitmap (bytes: %d\n", mainheader->size);
		printf("offset (start of image data): %d\n", mainheader->offset);
		printf("\nsize of dib: %d\n", infoheader->size);
		printf("bitmap width in pixels: %d\nbitmap height in pixels: %d\n", infoheader->width, infoheader->height);
		printf("number of color planes: %d\n", infoheader->planes);
		printf("number of bits per pixel: %d\n", infoheader->bits);
		printf("compression method: %d\n", infoheader->compression);
		printf("image size: %d\n", infoheader->imagesize);
		printf("horizontal resolution (pixel per meter): %d\n", infoheader->xresolution);
		printf("vertical resolution (pixel per meter): %d\n", infoheader->yresolution);
		printf("number of colors: %d\n", infoheader->ncolours);
		printf("number of important colors: %d\n", infoheader->importantcolours);

		if(infoheader->bits == 24){ //24 bit NEEDS WORK!

			if(infoheader->ncolours == 0){
				printf("\nNo color table\n");
			}

			lseek(infd, 0, SEEK_END); //seek to pixel data in 8bit
	       		lseek(infd, -rowsize, SEEK_END);

			printf("\nPixel Data:\n(row, col)\tred\tgreen\tblue\n---------------------------------\n");

			for(i = 0; i < infoheader->height; i++){
	       			read_all(infd, buffer, rowsize);
	       			j = 0; 
	       			for(j = 0; j < rowsize / 3; j++){ 
	       				printf("(%d, %d)\t\t%d\t%d\t%d\n", i, j, buffer[(j * 3) + 2], buffer[(j * 3) + 1], buffer[(j * 3)]);
	       			}
	       			rowcounter++; //row counter
	       			k = (rowcounter + 1) * rowsize;
	       			lseek(infd, -k, SEEK_END);
	       		}	
	        }
	       	else if(infoheader->bits == 8){ //8bit
	       		
	       		if(infoheader->ncolours == 0){
				printf("\nNo color table\n");
			}

			//Color Table
	       		if(infoheader->ncolours != 0){
	       			lseek(infd, 54, SEEK_SET); //go to color table bit offset
	       			read_all(infd, buffer, infoheader->ncolours * 4); //read color table

	       			printf("\nColor Table\nindex\tred\tgreen\tblue\talpha\n------------------------------------\n");
	       			for(i = 0; i < infoheader->ncolours; i++){
	       				printf("%d\t%d\t%d\t%d\t%d\n", i, buffer[i * 4], buffer[i * 4 + 1], buffer[i * 4 + 2], buffer[i * 4 + 3]);
	       			}
	       		}
	       		printf("\nPixel Data\n");

	       		lseek(infd, 0, SEEK_END); //seek to pixel data in 8bit
	       		int rowsize = ((infoheader->bits * infoheader->width) / 32) * 4;
	       		lseek(infd, -rowsize, SEEK_END);

	       		int rowcounter = 0, k = 0;

	       		printf("(row , col) index\n----------------------\n");
	       		
	       		//Working
	       		for(i = 0; i < infoheader->height; i++){
	       			read_all(infd, buffer, rowsize);// read current line
	       			j = 0; //reset row index
	       			for(j = 0; j < rowsize; j++){ //end until row 
	       				printf("(%d, %d) %d\n", i, j, buffer[j]);
	       			}
	       			rowcounter++; //row counter
	       			k = (rowcounter + 1) * rowsize;
	       			lseek(infd, -k, SEEK_END);
	       		}	
	       	}
	if(buffer != NULL)
		free(buffer);
	}
}

/**
 * If called, this function rewrites the color portion of the 8 bit only if reverse is
 * requested.
 * @param infd the current file discriptor of the read in file
 * @param outfd the current file discriptor of the output file
 * @param infoheader the structure of infomation related to bmp DIB header
 * @param mainheader the structure of infomation related to bmp intial header
 * @return 1 is returned if writing is successfull, otherwise -1 if not
 */ 
int write8bit(int infd, int outfd, struct INFOHEADER *infoheader, struct HEADER *mainheader){

	int i = 0, readbytes = 0;
	unsigned char *buffer = malloc(sizeof(unsigned char *) * BYTE4K);

	lseek(infd, 54, SEEK_SET); /*go to color*/
       	if((read_all(infd, buffer, infoheader->ncolours * 4)) == -1){ //read color table
       		return -1;
       	} 

       	/*inverse algorithim*/
       	for(i = 0; i < infoheader->ncolours; i++){
       		buffer[i * 4 + 0] = 255 - buffer[i * 4 + 0]; //red 
       		buffer[i * 4 + 1] = 255 - buffer[i * 4 + 1]; //green
       		buffer[i * 4 + 2] = 255 - buffer[i * 4 + 2]; //blue
       		buffer[i * 4 + 3] = 255 - buffer[i * 4 + 3]; //alpha
       	}
       	/*rewrite the inversed color table*/
       	if((write(outfd, buffer, infoheader->ncolours * 4)) == -1){
       		return -1;
       	}

 	/*rewrite the pixel data to new file (colors inversed by inverse of color table)*/
       	lseek(infd, mainheader->offset, SEEK_SET);
       	while((readbytes = read_all(infd, buffer, BYTE4K)) == BYTE4K){
       		write(outfd, buffer, readbytes);
   	}
   	if(buffer != NULL)
		free(buffer);
   	return 1;
}

/**
 * When called, this function will intialize the file discriptor for the input file
 * @param infile the string of the input file passed in by user
 * @return the file discriptor if successful, otherwise -1 if not
 */ 
int openbmpfile(char *infile){

	int infd = 0;

	if ((infd = open(infile, O_RDONLY)) == -1) {
            // capture an error -- file doesn't exist
	        if (errno == ENOENT) {
	                printf("%s file does not exist; not opening file\n", infile);
	                return -1;
	        }
	        ERR_RET("from open failed");
        	return -1;
    	}
    	return infd;
}

/**
 * When called, this function will open the output file discriptor
 * @param outfile the sting of the output file passed in by user
 * @return the file discriptor is returned if successful, otherwise -1 if not
 */ 
int open_outfile(char *outfile){
	int outfd = 0;

   	while ((outfd = open(outfile, O_WRONLY | O_CREAT, PERM_644)) == -1 && errno == EINTR);
  
    	if (outfd == -1) {
                switch (errno) {
	                case EACCES:
	                        printf("you don't have permission to open %s\n", outfile);
	                        return -1;
	                case ENOENT: //with O_CREAT -- one of the dirs in the path
	                             //doesn't exist or symlink point to a non-
	                             //existent pathname
	                        printf("a directory in %s does not exist\n", outfile);
	                        return -1;
	        }
	        err_sys("failed to open file\n");
                return -1;
        }
        return outfd;
}

/**
 * When called, this function will read in the initial BMP header and store it in HEADER structure
 * @param infd the input file's file discriptor
 * @param headerinfo the intial header structure for storage
 * @return the new infomation filled structure HEADER
 */
struct HEADER *bmp_header(int infd, struct HEADER *headerinfo){;

	int header_size = sizeof(struct HEADER), n = 0, pos = 0;
	char buffer[40], temp[2], temp2[10]; //the buffer acts as a byte[]

	lseek(infd, 0, SEEK_SET);
	read_all(infd, buffer, header_size);

	for (n = 0; n != 2; n++) { //identifier
	   temp[n] = buffer[n] & 0xFF;
	   pos++; //increment buffer index position or current byte, whichever you prefer
	}
	temp[2] = '\0'; //just in case

	if(bmpverify(infd, temp) == 0){
		return NULL;
	} //verify compatitible bmp

	strncpy(headerinfo->type, temp, 2);

	for(n = 0; n <= 4; n++){ //size in bytes
		temp2[n] = buffer[pos + n];
	}
	
	pos += 4 + (4) ; //go up 4 bytes and skip 4 bytes to get to offset 
	headerinfo->size = (temp2[0] & 0xFF) + ((temp2[1] & 0xFF) << 8) + ((temp2[2]& 0xFF) << 16) + ((temp2[3] & 0xFF) << 24);
	//to learn more about this read up about binary to decimal
	
	for(n = 0; n <= 3; n++){ //get offest bytes from buffer
		temp2[n] = buffer[pos + n];
	}
	headerinfo->offset = (temp2[0] & 0xFF) + ((temp2[1] & 0xFF) << 8) + ((temp2[2]& 0xFF) << 16) + ((temp2[3] & 0xFF) << 24);

	return headerinfo;
}

/**
 * Populates the INFOHEADER structure with DIB header infomation
 * @param infd input file discriptor
 * @param bitmapinfoheader structure to fill with infomation
 * @return newly populated structure
 */ 
struct INFOHEADER *bmp_infoheader(int infd, struct INFOHEADER *bitmapinfoheader){

	int header_size = sizeof(struct INFOHEADER), n = 0, pos = 0;
	char buffer[header_size], temp[10];

	lseek(infd, 14, SEEK_SET); //go to position of BITMAPINFOHEADER
	read_all(infd, buffer, header_size);

	for(n = 0; n <= 3; n++){ //4 bytes
		temp[n] = buffer[n];
		pos++;
	}
	bitmapinfoheader->size = (temp[0] & 0xFF) + ((temp[1] & 0xFF) << 8) + ((temp[2]& 0xFF) << 16) + ((temp[3] & 0xFF) << 24);
	
	if(bitmapinfoheader->size != 40){
		printf("Cannot process files with a header file not size of 40\n");
		return NULL;
	}

	for(n = 0; n <= 3; n++){ //4 bytes
		temp[n] = buffer[n + pos];
	}
	bitmapinfoheader->width = (temp[0] & 0xFF) + ((temp[1] & 0xFF) << 8) + ((temp[2]& 0xFF) << 16) + ((temp[3] & 0xFF) << 24);
	pos = pos + 4;

	for(n = 0; n <= 3; n++){ //4 bytes
		temp[n] = buffer[n + pos];
	}
	bitmapinfoheader->height = (temp[0] & 0xFF) + ((temp[1] & 0xFF) << 8) + ((temp[2]& 0xFF) << 16) + ((temp[3] & 0xFF) << 24);
	pos = pos + 4;

	for(n = 0; n <= 1; n++){ //2 bytes
		temp[n] = buffer[n + pos];
	}
	bitmapinfoheader->planes = (temp[0] & 0xFF) + ((temp[1] & 0xFF) << 8) + ((temp[2]& 0xFF) << 16) + ((temp[3] & 0xFF) << 24);
	pos = pos + 2;

	for(n = 0; n <= 1; n++){ //2 bytes
		temp[n] = buffer[n + pos];
	}
	bitmapinfoheader->bits = (temp[0] & 0xFF) + ((temp[1] & 0xFF) << 8) + ((temp[2]& 0xFF) << 16) + ((temp[3] & 0xFF) << 24);
	pos = pos + 2;

	for(n = 0; n <= 3; n++){ //4 bytes
		temp[n] = buffer[n + pos];
	}
	bitmapinfoheader->compression = (temp[0] & 0xFF) + ((temp[1] & 0xFF) << 8) + ((temp[2]& 0xFF) << 16) + ((temp[3] & 0xFF) << 24);
	pos = pos + 4;

	for(n = 0; n <= 3; n++){ //4 bytes
		temp[n] = buffer[n + pos];
	}
	bitmapinfoheader->imagesize = (temp[0] & 0xFF) + ((temp[1] & 0xFF) << 8) + ((temp[2]& 0xFF) << 16) + ((temp[3] & 0xFF) << 24);
	pos = pos + 4;

	for(n = 0; n <= 3; n++){ //4 bytes
		temp[n] = buffer[n + pos];
	}
	bitmapinfoheader->xresolution = (temp[0] & 0xFF) + ((temp[1] & 0xFF) << 8) + ((temp[2]& 0xFF) << 16) + ((temp[3] & 0xFF) << 24);
	pos = pos + 4;

	for(n = 0; n <= 3; n++){ //4 bytes
		temp[n] = buffer[n + pos];
	}
	bitmapinfoheader->yresolution = (temp[0] & 0xFF) + ((temp[1] & 0xFF) << 8) + ((temp[2]& 0xFF) << 16) + ((temp[3] & 0xFF) << 24);
	pos = pos + 4;

	for(n = 0; n <= 3; n++){ //4 bytes
		temp[n] = buffer[n + pos];
	}
	bitmapinfoheader->ncolours = (temp[0] & 0xFF) + ((temp[1] & 0xFF) << 8) + ((temp[2]& 0xFF) << 16) + ((temp[3] & 0xFF) << 24);
	pos = pos + 4;

	for(n = 0; n <= 3; n++){ //4 bytes
		temp[n] = buffer[n + pos];
	}
	bitmapinfoheader->importantcolours = (temp[0] & 0xFF) + ((temp[1] & 0xFF) << 8) + ((temp[2]& 0xFF) << 16) + ((temp[3] & 0xFF) << 24);
	pos = pos + 4;

	return bitmapinfoheader;
}

/**
 * Verifies that input file is bmp format
 * @param infd input file discriptor
 * @param temp header BM string
 * @return 1 on verified, 0 otherwise
 */
int bmpverify(int infd, char temp[]){ 

	printf("Checking file...");
	if(strncmp(temp, "BM", 2) != 0){ //verification
	   	printf("File is not a compatibile BMP file\n");
	   	return 0;
	}
	printf("Verified BMP.\n");
	return 1;
}

void usage(){
	printf("\nHELP GUIDE:\n\nDISCRIPTION:\n\tThis program is a simple BMP converter which");
	printf(" can\n\treverse 8 bit images and change various RGB\n\tvalues of 24bit BMP files.");
	printf(" As a bonus, it \n\tcan also print out header infomation of the BMP\n\tfile upon -D and -H.\n\n");
	printf(" AVAILABLE FLAGS:\n\t-i: input file (required)\n\t-o: output file (required **)\n\t");
	printf("-H: Display the bitmap header and the DIB header infomation\n\t");
	printf("-D: Dumps all header info, color table if availble, and pixel values one line per pixels\n\t");
	printf("-R: reverse an 8bpp bmp file and write to an output file\n\t");
	printf("-r: pass in of a red color value (cannot be less than 0 or greater than 255)\n\t");
	printf("-g: pass in of a green color value (cannot be less than 0 or greater than 255)\n\t");
	printf("-b: pass in of a red color value (cannot be less than 0 or greater than 255)\n\n");
	printf("NOTES:\n\t 1. (**) must be provided when using -R, -r, -g, and -b\n");
}
