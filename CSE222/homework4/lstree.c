/**
 * @file lstree.c
 *
 * @brief prints out infomation in the form of a ls tree
 *
 * @details The brief sums it up, but this tree is in pre order transversal
 *
 * @author Steven Ramsey
 * @date 4/13/16
 *
 * @todo None
 * @bugs None
 */

#define _XOPEN_SOURCE 500
#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ftw.h>
#include <ctype.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <libgen.h>
#include <string.h>

int print_tree(const char *fpath, const struct stat *sb, int tflag, struct FTW *ftwbuf);
void first_field_type(const char *fpath, int typeflag, struct FTW *ftwbuf, struct stat fileinfo);
void second_field_size(const char *fpath, int typeflag, struct FTW *ftwbuf, struct stat fileinfo);
void third_field_name(const char *fpath, int typeflag, struct FTW *ftwbuf, struct stat fileinfo);
void usage();

int main(int argc, char **argv) {

        if (argv[1] == NULL || argc < 1) {
                printf("No directory given.\n\n");
                usage();
                exit(EXIT_FAILURE);
        }
        
        if (argc == 2){
                if(nftw(argv[1], print_tree, 20, FTW_PHYS) == -1){
                        perror("nftw failed");
                        exit(EXIT_FAILURE);
                }
        }
        else{
                printf("Invalid amount of arguments\n");
                usage();
        }

        return 0;
}

/**
 * Passed in by nftw and actively prints out file info in the form of a ls tree
 * @param fpath the filepath of the element
 * @param sb the passed in function of removefile (weird to explain since its a struct
 * @param typeflag the type of element
 * @param ftwbuf structure of base and level
 * @return 0 for success
 */
int print_tree(const char *fpath, const struct stat *sb, int typeflag, struct FTW *ftwbuf) {

        struct stat fileinfo;

        if((lstat(fpath, &fileinfo)) < 0){
                perror("lstat failed:");
                exit(EXIT_FAILURE);
        }

        first_field_type(fpath, typeflag, ftwbuf, fileinfo);
        second_field_size(fpath, typeflag, ftwbuf, fileinfo);
        third_field_name(fpath, typeflag, ftwbuf, fileinfo);

        return 0;
}

/**
 * Simply prints the first field of the tree
 * @param fpath the filepath of the element
 * @param typeflag the type of element
 * @param ftwbuf structure of base and level
 * @param fileinfo structure that stat returned for corresponding file
 */
void first_field_type(const char *fpath, int typeflag, struct FTW *ftwbuf, struct stat fileinfo){

        switch (typeflag) {
                case FTW_D:
                        printf("D   ");
                        break;
                case FTW_F:
                        printf("F   ");
                        break;
                case FTW_SL:
                        printf("SL  ");
                        break;
                default:
                        printf("No actions can be done here");
        }
}

/**
 * Simply prints the second field of the tree
 * @param fpath the filepath of the element
 * @param typeflag the type of element
 * @param ftwbuf structure of base and level
 * @param fileinfo structure that stat returned for corresponding file
 */
void second_field_size(const char *fpath, int typeflag, struct FTW *ftwbuf, struct stat fileinfo){

        if ((S_ISBLK(fileinfo.st_mode)) || (S_ISCHR(fileinfo.st_mode)))
                printf("  %6d ", (int)fileinfo.st_dev);
        else {
                double filesize = fileinfo.st_size;
                if (filesize < 1000)
                        printf("  %6.0f ", filesize);
                else if (filesize < 1000000) {
                        fileinfo.st_size /= 1000;
                        printf("  %5.1fK ", filesize);
                }
                else if (filesize < 1000000000) {
                        filesize /= 1000000;
                        printf("  %5.1fM ", filesize);
                }
                else if (filesize < 1000000000000) {
                        filesize /= 1000000000;
                        printf("  %5.1fG ", filesize);
                }
        }
}

/**
 * Simply prints the third field of the tree
 * @param fpath the filepath of the element
 * @param typeflag the type of element
 * @param ftwbuf structure of base and level
 * @param fileinfo structure that stat returned for corresponding file
 */
void third_field_name(const char *fpath, int typeflag, struct FTW *ftwbuf, struct stat fileinfo){

        int curlevel = (ftwbuf->level) * 4, i = 0;
        char tempstring[256];

        for (i = 0; i < curlevel; i++){
                printf(" ");
        }

        if (!curlevel) { /* First line has different format */
                strcpy(tempstring, fpath);
                printf("%s/\n", basename(tempstring));
                return;
        };
        strcpy(tempstring, fpath);

        switch (typeflag){
                case FTW_D:
                        printf("\\_ %s/\n", basename(tempstring));
                        break;
                case FTW_F:
                        printf("\\_ %s\n", basename(tempstring));
                        break;
                case FTW_SL:
                        printf("\\_ %s@\n", basename(tempstring));
                        break;
        }
}

/**
 * Prints usage infomation
 */
void usage(){
        printf("\t\tlstree fileinfomation\n\nPURPOSE:\n\tThis program prints file info");
        printf(" in the\n\tform of a ls tree in a pre order fashion.\n\nUSAGE:\n\t");
        printf("./lstree [file directory]\n\n");
}