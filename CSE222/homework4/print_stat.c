/**
 * @file print_stat.c
 *
 * @brief prints out infomation given from the structure of lstat and can be used as a library file
 *
 * @details this program prints out file infomation in a formal manner similar to ls, but with 
 * the inode number as a bonus
 *
 * @author Steven Ramsey
 * @date 4/13/16
 *
 * @todo None
 * @bugs None
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <time.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <pwd.h>
#include <grp.h>

/**
 * Prints out file infomation in a formal output
 * @param infile current file to be stated
 * @param maindir the main directory of the file
 */
void print_stat(char* infile, char* maindir){

	struct stat fileinfo;
	struct passwd *user = NULL;
	struct group *group = NULL;
	char times[256];
	int rval = 0;
	strcat(maindir, "/"); //add appropriate '/' chars
	strcat(maindir, infile); //add on the file to main directory
	//basically makes it readable to to stat

	if(lstat(maindir,&fileinfo) < 0){
	      	perror("Something went wrong with lstat()");
	      	exit(EXIT_FAILURE);
	}
	
	user = (getpwuid(fileinfo.st_uid)); //for user infomation
	group = (getgrgid(fileinfo.st_gid)); //for group id infomation

	printf((S_ISDIR(fileinfo.st_mode)) ? "d" : "-");
	printf((fileinfo.st_mode & S_IRUSR) ? "r" : "-");
	printf((fileinfo.st_mode & S_IWUSR) ? "w" : "-");
	printf((fileinfo.st_mode & S_IXUSR) ? "x" : "-");
	printf((fileinfo.st_mode & S_IRGRP) ? "r" : "-");
	printf((fileinfo.st_mode & S_IWGRP) ? "w" : "-");
	printf((fileinfo.st_mode & S_IXGRP) ? "x" : "-");
	printf((fileinfo.st_mode & S_IROTH) ? "r" : "-");
	printf((fileinfo.st_mode & S_IWOTH) ? "w" : "-");
	printf((fileinfo.st_mode & S_IXOTH) ? "x" : "-");
	printf("  %d\t%d ", (int)(fileinfo.st_nlink), (int)fileinfo.st_ino);
	printf("%s %s\t", user->pw_name, group->gr_name);
	printf("%d\t", (int)fileinfo.st_size); 
	strftime(times, sizeof(times), "%b %e %H:%M:%S", localtime(&(fileinfo.st_mtime))); //get time
	printf("%s ", times);
	printf("%s ", infile);

	//symbolic link check
	char linkname[fileinfo.st_size + 1];
	if(S_ISLNK(fileinfo.st_mode)){
		rval = readlink(infile, linkname, sizeof(linkname) - 1);
		if(rval < 0){
			if(rval > fileinfo.st_size){
				fprintf(stderr, "symlink increased in size");
				exit(EXIT_FAILURE);
			}
			perror("error at readlink");
			exit(EXIT_FAILURE);
		}
		linkname[rval] = '\0';
		printf("-> %s", linkname);
	}
	
	maindir[0] = '\0'; //clean up the string

	printf("\n");
}