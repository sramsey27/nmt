/**
 * @file rm.c
 * @brief a variation of rm -rvifh
 *
 * @details This program acts like unix command rm with flags -rvifh
 *
 * @author Steven Ramsey
 * @date 4/13/16
 *
 * @todo None
 * @bugs None
 */

#define _XOPEN_SOURCE 500
#include <ftw.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdarg.h>
#include <string.h>

struct COMMANDS {
        int verbose;
        int ask;
        int force;
        int recursion;
} COMMANDS;

int removefile(const char *fpath, const struct stat *sb, int tflag, struct FTW *ftwbuf);
void runthrough(int optind, char **argv, int argc, struct stat fileinfo);
int remove_noask(const char *fpath, const struct stat *sb, int typeflag, struct FTW *ftwbuf);
void usage();

int main(int argc, char **argv) {

        struct stat fileinfo;
        int c = 0;

        /*if nothing is entered, print user guide*/
        if (argv[1] == NULL || argc < 1) {
                usage();
                exit(EXIT_FAILURE);
        }

        /*check for option*/
        while ((c = getopt(argc, argv, "hvifr")) != -1) {
                switch (c) {
                        case 'h':
                                usage();
                                exit(EXIT_SUCCESS);
                                break;
                        case 'v':
                                COMMANDS.verbose = 1;
                                break;
                        case 'i':
                                COMMANDS.ask = 1;
                                break;
                        case 'f':
                                COMMANDS.force = 1;
                                break;
                        case 'r':
                                COMMANDS.recursion = 1;
                                break;
                        //who needs recursion anyway :)

                }
                if (isprint(optopt)) {
                        fprintf(stderr, "\nUnknown option '%c'.\nClosing program\n", optopt);
                        exit(EXIT_FAILURE);
                }
                
        }
        argv++; //skip options
        argc--;

        if (argc == 2){
                if (nftw(argv[1], removefile, 20, FTW_DEPTH | FTW_PHYS) == -1) {
                        perror("nftw error encountered");
                        exit(EXIT_FAILURE);
                }
        }
        else if (argc > 2){ 
                runthrough(optind, argv, argc, fileinfo);
        }

        return 0;
}

/**
 * Actively runs through any provided directory
 * @param optind the index of the next element to be processed in argv
 * @param argv one-dimensional array of strings passed in by user
 * @param argc argument count
 * @param fileinfo stated structure infomation
 */
void runthrough(int optind, char **argv, int argc, struct stat fileinfo){

        /*FTW_DEPTH provides the pre-order transversal removal*/

        for (optind = 1; optind < argc; optind++) {
                if(lstat(argv[optind], &fileinfo) < 0) {
                        perror("runthrough: stat failed");
                        exit(EXIT_FAILURE);
                }
                if (S_ISREG(fileinfo.st_mode)){
                        if (nftw(argv[optind], removefile, 20, FTW_DEPTH | FTW_PHYS) == -1) {
                                perror("nftw error encountered");
                                exit(EXIT_FAILURE);
                        }
                }
                else if (S_ISDIR(fileinfo.st_mode)){
                        if (nftw(argv[optind], removefile, 20, FTW_DEPTH | FTW_PHYS) == -1) {
                                perror("nftw error encountered");
                                exit(EXIT_FAILURE);
                        }
                }
                else if (S_ISLNK(fileinfo.st_mode)){
                        if (nftw(argv[optind], removefile, 20, FTW_DEPTH | FTW_PHYS) == -1) {
                                perror("nftw error encountered");
                                exit(EXIT_FAILURE);
                        }
                }
                else {
                        continue;
                }
        }
}

/**
 * Passed in by nftw and actively removes any element passed in by fpath
 * @param fpath the filepath of the element
 * @param sb the passed in function of removefile (weird to explain since its a struct
 * @param typeflag the type of element
 * @param ftwbuf structure of base and level
 * @return 0 for success
 */
int removefile(const char *fpath, const struct stat *sb, int typeflag, struct FTW *ftwbuf) {

        if(COMMANDS.ask != 1 || COMMANDS.force == 1){
                remove_noask(fpath, sb, typeflag, ftwbuf);
                return 0;
        }
        else {
                char answer[20];
                if(COMMANDS.force != 1){
                        printf("remove: \"%s\"? [y/n]: ", fpath);
                        scanf("%s", answer);
                }
                if((strcmp(answer, "y")) == 0 && COMMANDS.force != 1){
                        switch (typeflag) { /*act accordingly to deletion type*/
                        case FTW_DP: /*remove directory*/
                                if (remove(fpath))
                                        perror(fpath);
                                else if(COMMANDS.verbose == 1)
                                        printf("directory removed: '%s'\n", fpath);
                                break;
                        case FTW_F: /*remove regular file*/
                                if (remove(fpath))
                                        perror(fpath);
                                else if(COMMANDS.verbose == 1)
                                        printf("file removed: '%s'\n", fpath);
                                break;  
                        case FTW_SL: /*remove symbolic link*/
                                if (unlink(fpath) == -1)
                                        perror("unlink");
                                else if(COMMANDS.verbose == 1)
                                        //printf("file unlinked: \"%s\"", fpath);
                                break;
                        default:
                                if(COMMANDS.verbose != 1)
                                        printf("No action to take.\n");
                        }
                        return 0;
                        
                }
                else if((strcmp(answer, "n")) == 0 && COMMANDS.force != 1){
                        return 0;
                }
                else if((strcmp(answer, "q")) == 0 && COMMANDS.force != 1){
                        exit(EXIT_FAILURE); //this was for my own needs
                }
                else if(COMMANDS.force != 1){
                        printf("invalid answer! Assuming no!\n");
                        return 0;
                }
        }     
        return 0;
}

/**
 * Samething as removefile, just doesn't request user input for deletion
 * @param fpath the filepath of the element
 * @param sb the passed in function of removefile (weird to explain since its a struct
 * @param typeflag the type of element
 * @param ftwbuf structure of base and level
 * @return 0 for success
 */
int remove_noask(const char *fpath, const struct stat *sb, int typeflag, struct FTW *ftwbuf) {

        switch (typeflag) { /*act accordingly to deletion type*/
                case FTW_DP: /*remove directory*/
                        if (remove(fpath))
                                perror(fpath);
                        else if(COMMANDS.verbose == 1)
                                printf("directory removed: '%s'\n", fpath);
                        break;
                case FTW_F: /*remove regular file*/
                        if (remove(fpath))
                                perror(fpath);
                        else if(COMMANDS.verbose == 1)
                                printf("file removed: '%s'\n", fpath);
                        break;  
                case FTW_SL: /*remove symbolic link*/
                        if (unlink(fpath) == -1)
                                perror("unlink");
                        else if(COMMANDS.verbose == 1)
                                //printf("file unlinked: \"%s\"", fpath);
                        break;
                default:
                        if(COMMANDS.verbose != 1)
                                printf("No action to take.\n");
        }
        return 0;
}
/**
 * Simply displays the help guide when the -h flag is given
 */
void usage() {
        printf("\n\t\trm.c removal tool\n\n");
        printf("PURPOSE:\n\tThis program deletes any given directory given\n\n");
        printf("USAGE:\n\t./rm [directory path] [optional flag]\n\n");
        printf("Optional flag:\n\t-h: Displays the user guide infomation\n\n");
        printf("NOTES:\n\t1). This program expects the optional flag\n\t    before the ");
        printf("given directory path.\n\n");
}