/**
 * @file lsinode.c
 * @brief a variation of ls -l except it prints out the inode number 
 *
 * @details this program prints out file infomation in a formal manner similar to ls, but with 
 * the inode number as a bonus, works in correspondance with print_stat.c and passes in the 
 * appropreiate infomation
 *
 * @author Steven Ramsey
 * @date 4/13/16
 *
 * @todo None
 * @bugs None
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>
#include <locale.h>
#include <langinfo.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

void print_stat(char* infile, char* maindir);

int main(int argc, char **argv){

	if(argc < 2){
		printf("Input not sufficent...\n");
		exit(EXIT_FAILURE);
	}

	struct dirent **dirlist;
    	int temp = 0, i = 0;
    	char maindir[256];
    	strcpy(maindir, argv[1]);

   	temp = scandir(argv[1], &dirlist, NULL, alphasort);
  	if (temp < 0){
        	perror("Something went wrong with scandir...");
    		exit(EXIT_FAILURE);
    	}
    	else {
    		for(i = (temp - 1); i >= 0; i--){
    			print_stat(dirlist[i]->d_name, maindir);
    			strcpy(maindir, argv[1]);
    			free(dirlist[i]);
    		}
        free(dirlist);
    	}

	return 0;
}