#include <stdio.h>
#include "zombie.h"
#include <stdlib.h>

/**
 * Holds values of various zombie infomation
 * @param zombie holds the array of data
 * @param toes holds the value for number of toes zombie had
 * @param hour holds the value for hour in given time
 * @param min holds the value for minutes in given time
 * @param sec holds value for seconds from given time
 * @param blood holds value for the amount of blood that oozed from zombie
 * @param index the index of the stored data
 * @return nothing
 */
void zombie_array(struct data_t zombie[], int toes, int hour, int min, int sec, float blood, int index)
{
	(zombie[index]).toes = toes;
	(zombie[index]).hour = hour;
	(zombie[index]).min = min;
	(zombie[index]).sec = sec;
	(zombie[index]).blood = blood;
}

/**
 * Prompts user for time zombie was found alive/dead
 * @param zombie holds the array of zombie infomation
 * @param buffer the buffer value for character use
 * @param a the index of zombie infomation
 * @return nothing
 */
void get_time(struct data_t zombie[], char buffer[], int a)
{

	printf("\nEnter time zombie was found hh:mm:ss\nTime: ");
	fgets(buffer, LEN, stdin);
	sscanf(buffer,"%d:%d:%d", &zombie[a].hour, &zombie[a].min, &zombie[a].sec);
	/*printf("%d:%d:%d", zombie[a].hour, zombie[a].min, zombie[a].sec);*/

}

/**
 * Prompts user for the day zombie was found
 * @param zombie holds the array of zombie infomation
 * @param a the index of zombie infomation
 * @param buffer the buffer value for character user
 * @return nothing
 */
void get_date(struct data_t zombie[],int a, char buffer[])
{
	printf("\nEnter date zombie was encountered.\nEnter the corresponding number to the day.\n");
	printf("1) Monday\n2) Tuesday\n3) Wednesday\n4) Thursday\n5) Friday\n6) Saturday\n7) Sunday");
	printf("\nDate: ");
	fgets(buffer, LEN, stdin);
	sscanf(buffer,"%d", (int *) &zombie[a].day);
	/*printf("%d:%d:%d", zombie[a].hour, zombie[a].min, zombie[a].sec);*/
}

/**
 * Prompts user for the number of toes dead zombie had
 * @param zombie holds the array of zombie infomation
 * @param a the index of zombie infomation
 * @param buffer the buffer value for character use
 * @return nothing
 */
void get_toes(struct data_t zombie[], int a, char buffer[])
{
	printf("\nHow many toes did the zombie have?\nToes: ");
	fgets(buffer, LEN, stdin);
	sscanf(buffer,"%d", &zombie[a].toes);
	
	if (zombie[a].toes > 10)
		printf("\nIt appears that the virus made zombies grow more toes!\n");
	/*printf("%d",zombie[a].toes);*/
}

/**
 * Prompts user for amount of blood that oozed from zombie
 * @param zombie holds the array of zombie infomation
 * @param a the index of zombie infomation
 * @param buffer the buffer value for character user
 * @return nothing
 */
void get_blood(struct data_t zombie[], int a, char buffer[])
{
	printf("\nPlease enter the amount of blood that oozed from its body after killing it.\nExample: 12.22 mL\nBlood amount: ");
	fgets(buffer, LEN, stdin);
	sscanf(buffer,"%f", &zombie[a].blood);
	/*printf("%.2f",zombie[a].blood);*/

}

/**
 * Prints collected zombie infomation with day print
 * @param zombie holds the array of zombie infomation
 * @param a the index of zombie infomation
 * @return nothing
 */
void print_data(struct data_t zombie[], int a)
{
	printf("\nZombie Infomation:\n___________________________\n");
	int i;
	/* Prints data collected from user (depending on zombie starting condition) */
	for(i = 0; i < a; i++) {
		if (zombie[i].dead == 'Y' || zombie[i].dead == 'y') {
			printf("\n%d. Zombie was found Dead!\n", 1 + i);
			printf("The zombie had %d toes.\n", zombie[i].toes);
		}
		else {
			printf("\n%d) Zombie was found Alive!\n", 1 + i);
			printf("It was drained %.2f mL of blood once killed.\n", zombie[i].blood);
		}
	/* Prints day zombie was encountered along with the time */
	switch (zombie[i].day) {
	case MONDAY:
		printf("Zombie was sighted Monday at %d:%d:%d.\n", zombie[i].hour, zombie[i].min, zombie[i].sec);
		break;
	case TUESDAY:
		printf("Zombie was sighted Tuesday at %d:%d:%d.\n", zombie[i].hour, zombie[i].min, zombie[i].sec);
		break;
	case WEDNESDAY:
		printf("Zombie was sighted Wednesday at %d:%d:%d.\n", zombie[i].hour, zombie[i].min, zombie[i].sec);
		break;
	case THURSDAY:
		printf("Zombie was sighted Thursday at %d:%d:%d.\n", zombie[i].hour, zombie[i].min, zombie[i].sec);
		break;
	case FRIDAY:
		printf("Zombie was sighted Friday at %d:%d:%d.\n", zombie[i].hour, zombie[i].min, zombie[i].sec);
		break;
	case SATURDAY:
		printf("Zombie was sighted Saturday at %d:%d:%d.\n", zombie[i].hour, zombie[i].min, zombie[i].sec);
		break;
	case SUNDAY:
		printf("Zombie was sighted Sunday at %d:%d:%d.\n", zombie[i].hour, zombie[i].min, zombie[i].sec);
		break;
	default:
		printf("\n\nError\n\n");
	}
	}
}
