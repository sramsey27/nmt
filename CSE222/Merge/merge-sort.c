#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
 
void merge(int A[], int p, int q, int r){

    int i = 0, j = 0, k = 0, n1 = ((q - p) + 1), n2 = (r - q);
    int *L = (int*)malloc(sizeof(int) * (n1 + 1));
    int *R = (int*)malloc(sizeof(int) * (n2 + 1));

    for (i = 0; i < n1; i++){
            L[i] = A[p + i];
    }
    for (j = 0; j < n2; j++){
            R[j] = A[q + j + 1];
    }

    L[i] = INT_MAX; //infinity sentinel
    R[j] = INT_MAX; //infinity sentinel

    j = 0; i = 0;
    for(k = p; k < r + 1; k++){
        if (L[i] <= R[j]){
            A[k] = L[i];
            i = i + 1;
        }
        else {
            A[k] = R[j];
            j = j + 1;
        }
    }
    free(L);
    free(R);
}

void mergesort(int A[], int p, int r){
    
    int q = 0;

    if (p < r){
        q = ((p + r) / 2);
        mergesort(A, p, q);
        mergesort(A, (q + 1), r);
        merge(A, p, q, r);
    }
}
 
int main()
{
    int element_num = 10; //number of elements for array
    int *A, i = 0;

    A = (int*)malloc(sizeof(int) * element_num);
   
    for(i = 0; i < element_num; i++){
        A[i] = rand() % element_num; //random numbers from 1 - 100;
    }

    printf("\nBefore: [ ");
    for (i = 0; i < element_num; i++){
        printf("%d ", A[i]);
    }
    printf("]\n");
    
    mergesort(A, 0, (element_num - 1));
 
    printf("\nAfter : [ ");
    for (i = 0; i < element_num; i++){
        printf("%d ", A[i]);
    }
    printf("]\n\n");

    free(A);

    return 0;
}