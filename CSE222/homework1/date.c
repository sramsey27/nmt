#include "date.h"

void local_d(char buf[], struct tm *info){
 	/*Local Time*/
 	printf("\nNumber of seconds since the Epoch: %lld\n\nLocaltime\n", (long long)time(NULL));

 	strftime(buf, 256, "Hours: %H %nMinutes: %M %nSeconds: %S %nYear: %y %nMonth: %m", info);
 	puts(buf);
 	strftime(buf, 256, "Day of the month: %d \nDay of the week: %w %nJulian: %j", info);
 	puts(buf);

 	printf("Daylight savings time: %d\n", info->tm_isdst);

 	strftime(buf, 256, "%A, %B %e, %Y %T %n", info);
 	puts(buf);
 	return;
 }

void UTC_region(char buf[], struct tm *info){
	 	/*change to UTC*/

 	printf("Coordinated Universal Time (UTC)\n");

 	strftime(buf, 256, "Hours: %H %nMinutes: %M %nSeconds: %S %nYear: %y %nMonth: %m", info);
 	puts(buf);
 	strftime(buf, 256, "Day of the month: %d \nDay of the week: %w %nJulian: %j", info);
 	puts(buf);

 	printf("Daylight savings time: %d\n", info->tm_isdst);

 	strftime(buf, 256, "%A, %B %e, %Y %T %n", info);
 	puts(buf);
 	return;
}