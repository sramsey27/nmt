/**
 * @file secs.c
 * 
 * @author Steven Ramsey
 *
 * @date 2/14/2016
 *  
 * Assignment: homework1
 *
 * @brief Simple program to print seconds since Epoch
 *
 * @details Utilize the built in system call "time" to print
 * the numbers of seconds since Epoch.
 *  
 * @bugs N/A
 *
 * @todo N/A
 */

#include <stdio.h>
#include <errno.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>

#define END 256

int main() {

	/*makefile is default set to include the -DERR_TEST flag*/

	#ifdef ERR_TEST
		char msg [256];
		errno = EFAULT; /*purposely invoke bad address EFAULT*/
		sprintf(msg, "error in %s:%s:%d", __FILE__, "main", __LINE__);
		perror(msg);
		exit(END);
	#endif   

	#ifndef ERR_TEST
		time_t x = 0;
		printf("Number of seconds since the Epoch: %lld \n", (long long) time(&x));
	#endif

	return 0;
}
