#ifndef DATE
#define DATE

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void local_d(char buf[], struct tm *info);
void UTC_region(char buf[], struct tm *info);

#endif