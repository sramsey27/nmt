/**
 * @file secs_apue.c
 * 
 * @author Steven Ramsey
 *
 * @date 2/15/2016
 *  
 * Assignment: homework1 
 *
 * @brief utilze apue error handling 
 *
 * @details utilze the apue header to report errors
 *  
 * @bugs N/A
 *
 * @todo N/A
 */ 

#include <stdio.h>
#include <errno.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include "apue.h"

#define EXIT 256

int main(void) {
        #ifdef ERR_TEST
                err_quit("error in %s:%s:%d: %s", __FILE__, "main", __LINE__, "APUE ERROR");
        #endif

        #ifndef ERR_TEST
                time_t x = 0;
                printf("Number of seconds since the Epoch: %lld \n", (long long) time(&x));
        #endif
        return 0;
}