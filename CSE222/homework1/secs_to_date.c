/**
 * @file secs_to_date.c
 * 
 * @author Steven Ramsey
 *
 * @date 2/15/2016
 *  
 * Assignment: homework1 
 *
 * @brief pretty print the tm structure
 *
 * @details ^^^^^^
 *  
 * @bugs N/A
 *
 * @todo N/A
 */ 

#include "date.h"

int main(){

 	struct tm *info;
 	char buf [256];
 	time_t time_p; 

 	time(&time_p);

 	info = localtime(&time_p);
 	local_d(buf, info);

 	info = gmtime(&time_p);
 	UTC_region(buf, info);

 	return 0;
}
