/**
 * @file zeros.c
 * 
 * @author Steven Ramsey
 *
 * @date 3/5/2016
 *  
 * Assignment: homework3 
 *
 * @brief creates a file full of zeros
 *
 * @details Takes in a number and write n zeros in a file
 * formatted as zerosDDD
 *  
 * @bugs N/A
 *
 * @todo N/A
 */ 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include "apue.h"

#define BUFSIZE 4096
#define FLAGS O_NOCTTY|O_NONBLOCK, 0755
#define ERR_RET(s) err_ret(#s " %s:%d\n", __FILE__, __LINE__)

void write_zeros(int argc, char *argv[]);
void usage(void);

int main(int argc, char *argv[]){
        
        int c;

        if (argv[1] == NULL) {
                printf("No Arguments were provided\n");
                usage();
                exit(EXIT_FAILURE);
        }

        while((c = getopt(argc, argv, "n:h")) != -1){
                switch (c) {
                        case 'n':
                                write_zeros(argc, argv);
                                break; 
                        case 'h':
                                usage();
                                exit(EXIT_SUCCESS);
                                break;
                        
                }
                if(isprint(optopt)){
                        fprintf (stderr, "\nUnknown option `-%c'.\nClosing Program just to be safe...\n", optopt);\
                        exit(EXIT_FAILURE);
                }
        }

        return 0;
}

void write_zeros(int argc, char *argv[]) {
        char filename[27], buf[BUFSIZE] = {0};
        strncpy(filename, "zeros", 6);
        strcat(filename, argv[2]); //concatinate the number provided
        int fd, number = atoi(optarg); //convert to integer
        size_t sizebuf = sizeof(buf);

        if ((fd = open(filename, O_WRONLY|O_CREAT| FLAGS)) == -1) {
                if (errno == ENOENT) {
                        printf("'%s' does not exist\n", filename);
                        usage();
                        exit(EXIT_FAILURE);
                }
                ERR_RET("-- Open failed\n");
                goto out;
        }

        memset(buf, '0', sizebuf); //fill the buffer with zeros

        for(; number > sizebuf; number -= sizebuf){
                if((write(fd, buf, sizebuf)) == -1){
                        perror("Error in write!\n");
                        goto out;
                }  
        }

        /*Just some error checking*/
        if((write(fd, buf, number)) == -1){
                perror("Error in write!\n");
                goto out;
        }
        
        goto out; //free discriptors

out:
        if (fd != -1) {
                if (close(fd) == -1)
                        perror("-- Error in close file\n");
        }
}

void usage(void) {
        printf("USAGE GUIDE:\n\n");
        printf("PURPOSE:\n\tThis writes [number] amount of zeros to a file called \"zeros[number]\"\n\n");
        printf("\tUSAGE: ./zeros [-n -h] [number]\n");
        printf("\nRequired arguments:\t-n [number]\n");
        printf("Optional arguments:\t-h, print usage information\n\n");
}