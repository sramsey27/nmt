/**
 * @file reverse.c
 * 
 * @author Steven Ramsey
 *
 * @date 3/5/2016
 *  
 * Assignment: homework3 
 *
 * @brief Contains various options in which reverse the
 * contents of a file in differerent ways.
 *
 * @details Takes in a file (an out file is optional) and outputs
 * the corresponding mode.
 *  
 * @bugs N/A
 *
 * @todo N/A
 */ 

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "apue.h"
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>

#define ERR_RET(s) err_ret(#s " %s:%d\n", __FILE__, __LINE__)
#define BUFSIZE 4096
#define PERM_644 S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH 

ssize_t read_all(int fd, void *buf, size_t nbytes);
void process_files(char in[], char out[], int mode, int out_present);
void char_reversal(int fd, int file_out, int out_present, char out[]);
void linechar_reversal(int fd, char out[], int out_present, int file_out);
void lineonly_reversal(int fd, char out[], int out_present, int file_out);
void usage();

int main(int argc, char *argv[]){

	int mode = 0, out_present = 0, c = 0; //for each mode
	char in[25], out[25];

	if(argv[1] == NULL){ /*if nothing is passed in...*/
		printf("No Arguments were provided, redirecting to user guide\n");
		usage();

		exit(EXIT_SUCCESS);
	}

	while((c = getopt(argc, argv, "i:o:clrh")) != -1) {
		switch (c)
		{
			case 'i':
				printf("INPUT: %s\n", optarg); //input is required...
				strncpy(in, optarg, strlen(optarg));
				break;

			case 'o':
				printf("OUTPUT: %s\n", optarg); //input is required, but not the argument itself
				strncpy(out, optarg, strlen(optarg));
				out_present = 1; //flag for writing to stdout or to file...
				break;

			case 'c':
				if(mode != 0){
					printf("\nConfliction: Only one mode is allowed at a time.\n");
					usage();
					exit(EXIT_FAILURE);
				}
				printf("CHAR REV ENABLED: MODE 1\n");
				mode = 1;
				break;

			case 'l':	
				if(mode != 0){
					printf("\nConfliction: Only one mode is allowed at a time.\n");
					usage();
					exit(EXIT_FAILURE);
				}
				printf("LINE REV ENABLED: MODE 2\n");
				mode = 2;
				break;

			case 'r':
				if(mode != 0){
					printf("\nConfliction: Only one mode is allowed at a time.\n");
					usage();
					exit(EXIT_FAILURE);
				}
				printf("LINE REV W/O CHAR REV ENABLED: MODE 3\n");
				mode = 3;
				break;

			case 'h':
				usage();
				exit(EXIT_SUCCESS);
				break;

		}

		if(isprint(optopt)){
			fprintf (stderr, "\nUnknown option `-%c'.\nClosing Program just to be safe...\n", optopt);\
			exit(EXIT_FAILURE);
		}
		
	}

	if(mode == 0){
		printf("NO MANIPULATION MODE WAS PROVIDED! Closing Program and opening usage guide...\n");
		usage();
		exit(EXIT_FAILURE);
	}
	else {
		process_files(in, out, mode, out_present);
	}	

	return 0;
}

void process_files(char in[], char out[], int mode, int out_present){ //this function is for output to a file.

	int fd = 0, file_out = 0;

	if ((fd = open(in, O_RDONLY)) == -1) {
            // capture an error -- file doesn't exist
        if (errno == ENOENT) {
                printf("%s file does not exist; not opening file\n", in);
                usage();
                exit(EXIT_FAILURE);
        }
        ERR_RET("from open failed");
        goto out;
    }

    /*If file name is given, go ahead an open it (create if needed), atomically*/
    if(out_present == 1){
   		while ((file_out = open(out, O_WRONLY | O_CREAT, PERM_644)) == -1 && errno == EINTR);
  
    	if (file_out == -1) {
                switch (errno) {
                case EACCES:
                        printf("you don't have permission to open %s\n", out);
                        exit(errno);
                case ENOENT: //with O_CREAT -- one of the dirs in the path
                             //doesn't exist or symlink point to a non-
                             //existent pathname
                        printf("a directory in %s does not exist\n", out);
                        exit(errno);
                }
                err_sys("failed to open file\n");
                goto out;
        }
    }

    /*Dependant on user choice of mode*/
    if (mode == 1){
    	char_reversal(fd, file_out, out_present, out);
    }
    else if(mode == 2){
    	linechar_reversal(fd, out, out_present, file_out);
    }
    else if (mode == 3){
    	lineonly_reversal(fd, out, out_present, file_out);
    }
    else {
    	printf("Something fishy happened... some how the mode # went out of range\n");
    	if (close(fd) == -1)
            perror("error in from file close");
        exit(EXIT_FAILURE);
    }

    goto out;

out:        
        if (fd != -1){
                if (close(fd) == -1)
                        perror("error in from file close");
    	exit(EXIT_FAILURE);
    }
}

void char_reversal(int fd, int file_out, int out_present, char out[]){

	size_t readbytes;
	char buf[BUFSIZE]; //BUFSIZE == 4092
	int filesize = 0, temp = 0;

	filesize = (lseek(fd, 0, SEEK_END));
	temp = filesize - 1;

	if (out_present == 1){ //output to file
		
		for (; temp > 0; temp--) {

        	lseek(fd, temp, SEEK_SET);
        	readbytes = read_all(fd, buf, 1);
	        write(file_out, buf, readbytes);

		}
		printf("Reversal sent to: %s\n", out);
	}

	else if (out_present == 0) { /*output to stdout*/

		for (; temp > 0; temp--) {

        	lseek(fd, temp, SEEK_SET);
        	readbytes = read_all(fd, buf, 1);
	        write(1, buf, readbytes);

        }
        printf("\n"); /*to fix the last line in stdout*/
	}
}

void linechar_reversal(int fd, char out[], int out_present, int file_out){ //currently not functioning right, but midterms

	int start[4000], i = 0, cur_offset = 0, linecount = 0, temp = 0, j = 0;
	size_t readbytes1, readbytes2;
	char buf[BUFSIZE];

	printf("\n");

	start[0] = 0;

	while(1){ /*Read entire file first*/
		readbytes1 = read_all(fd, buf, 1);
		if(readbytes1 == 0){
				break; //break when done reading the file
		}
		/*Keep track of the lines*/	
		for(i = 0; i < readbytes1; i++){
			cur_offset++;
			if(buf[i] == '\n'){
				start[linecount++] = cur_offset;
			}
		}
	}
	j = 0;

	int filesize = lseek(fd, 0, SEEK_END);
	for(i = 0; i < filesize; i++){
		lseek(fd, i, SEEK_SET);
		if(buf[i] == '\n'){
				j = i;
			}
		temp = (start[i + 1] - start[j]);
		readbytes2 = read_all(fd, buf, temp) ;
		write(1, buf, readbytes2);
	}

        if((close(readbytes1)) == -1){
                perror("error closing read_all variable \"Readbytes1\"\n");
                exit(EXIT_FAILURE);
        }
        return;


}

void lineonly_reversal(int fd, char out[], int out_present, int file_out){

	int start[4000], i = 0, cur_offset = 0, linecount = 0, temp = 0;

	size_t readbytes1, readbytes2;
	char buf[BUFSIZE];

	printf("\n");

	start[0] = 0;

	while(1){ /*Read entire file first*/
		if((readbytes1 = read_all(fd, buf, sizeof(buf))) == -1){
                        perror("Read_all failed.\n");
                        exit(EXIT_SUCCESS);
                }
		if(readbytes1 == 0){
				break; //break when done reading the file
		}
		/*Keep track of the lines*/	
		for(i = 0; i < readbytes1; i++){
			cur_offset++;
			if(buf[i] == '\n'){
				start[linecount++] = cur_offset;
			}
		}
	}

	for(i = linecount - 1; i >= -1; i--){
		lseek(fd, start[i], SEEK_SET);
		temp = (start[i + 1] - start[i]);
		readbytes2 = read_all(fd, buf, temp) ;
		write(1, buf, readbytes2);
	}

        exit(EXIT_SUCCESS);
}

void usage(){
	printf("\nUSAGE GUIDE:\n\nPURPOSE:\n\tThis program will reverse given files");
        printf(" in various ways\n\tprovided by the flags defined below.\n");
        printf("\n\tUSAGE: ./reverse [-i -o -c -l -r -h]\n\nRequired Arguments:\n");
        printf("\t-i : Filename of file to be reversed\n\nOther Arguments:\n\t-o : ");
        printf("File to direct results to [output does to std if not provided]\n");
        printf("\t-c : Reverse all characters of the [input file] ***\n\t-l : Reverse");
        printf(" the contents of the lines of [input file] [WIP!/UNFINISHED!] ***\n\t-r : ");
        printf( "Reverse the lines of the [input file], no character reversal ***\n\n");
        printf("\t*** NOTE: Only one of these flags can be passed in a time\n\n");
       
}
