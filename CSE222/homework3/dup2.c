/**
 * @file hexdump.c
 * 
 * @author Steven Ramsey
 *
 * @date 3/5/2016
 *  
 * Assignment: homework3 
 *
 * @brief Implements a simplisitc version of dup2 system call
 *
 * @details This program follows the behavior as described within
 * the manpage of dup2()
 *  
 * @bugs N/A
 *
 * @todo N/A
 */ 

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/types.h>
#include <limits.h>
#include <sys/stat.h>
#include <paths.h>
#include <fcntl.h>
#include "apue.h"

void usage(void);
int dup27(int argc, char *argv[]);

int main(int argc, char *argv[]){
        int c = 0;

        if (argv[1] == NULL) {
                printf("No arguments were given. Redirecting to user guide\n");
                usage();
                exit(EXIT_FAILURE);
        }

        while((c = getopt(argc, argv, "h")) != -1){
                switch (c) {
                        case 'h':
                                usage();
                                exit(EXIT_SUCCESS);
                                break;

                }
                if(isprint(optopt)){
                        fprintf (stderr, "\nUnknown option `-%c'.\nClosing Program just to be safe...\n", optopt);\
                        exit(EXIT_FAILURE);
                }        
        }

        dup27(argc, argv);

        return 0;
}

int dup27(int argc, char *argv[]) {

        size_t oldfd, newfd;
        int i = 0, fd = -1;
        char *buf = NULL;

        oldfd = atoi(argv[1]);
        newfd = atoi(argv[2]);

        if (newfd <= oldfd) {
                printf("ERROR! The new file discriptor needs to be bigger than the old file discriptor\n");
                return -1;
        }

        buf = malloc((newfd - oldfd) * sizeof(size_t));
        if (buf == NULL) {
                printf("Error - Unable to create buf, maybe out of memory?\n");
                exit(EXIT_FAILURE);
        }

        while (1) {
                fd = dup(oldfd);
                if (fd == -1) {
                        printf("Error - could not duplicate fd %zd\n", oldfd);
                        goto out;
                }
                if (fd == newfd){
                        break;
                }
                if (fd > newfd){
                        if((close(newfd)) == -1){
                                perror("closing of new file discriptor failed...\n");
                                exit(EXIT_FAILURE);
                        }
                }
                buf[i++] = fd;
        }

out: 
        for (i = 0; i < newfd - oldfd; i++) {
                if (buf[i] == 0)
                        continue;
                if (close(buf[i]) == -1)
                        printf("Error - could not close fd %d\n", buf[i]);
        }
        free(buf);
        return fd;
}

void usage(void){
        printf("\nUSAGE GUIDE:\n\nPURPOSE:\n");
        printf("\tCreates a copy of the [file discriptor to copy] to the [new file discriptor]\n");
        printf("\n\tUSAGE: ./dup2 [file discriptor to copy] [new file discriptor out file");
        printf("\n\nRequired arguments:\n\t[file discriptor to copy] [new file discriptor ");
        printf("destination]\n\n");
}