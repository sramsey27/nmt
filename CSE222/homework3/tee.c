/**
 * @file tee.c
 * 
 * @author Steven Ramsey
 *
 * @date 3/5/2016
 *  
 * Assignment: homework3 
 *
 * @brief functions the same as tee linux command (without multiple files) 
 *
 * @details Does the same thing as the tee linux command without multiple file support,
 * but with appending support
 *  
 * @bugs N/A
 *
 * @todo N/A
 */ 

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "apue.h"
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>

#define ERR_RET(s) err_ret(#s " %s:%d\n", __FILE__, __LINE__)

ssize_t read_all(int fd, void *buf, size_t nbytes);
void process_file(int append_mode, int argc, char *argv[]);
void usage(void);

int main(int argc, char *argv[]){

	int append = 0, c = 0;

	if(argv[1] == NULL){ /*if nothing is passed in...*/
		usage();
		exit(EXIT_SUCCESS);
	}

	while((c = getopt(argc, argv, ":ah")) != -1){
		
		switch (c)	{
			case 'a': 
				append = 1; /*Set a flag for append mode*/
				break;

			case 'h':
				usage();
				exit(EXIT_SUCCESS);
				break;

		}
		/*close the program if an unknown argument is passed in*/
		if(isprint(optopt)){
			fprintf (stderr, "\nUnknown option `-%c'.\nClosing Program...\n", optopt);\
			exit(EXIT_FAILURE);
		}
	}

	/*process the infomation/file*/
	process_file(append, argc, argv);

	return 0;
}

void process_file(int append_mode, int argc, char *argv[]){

	int from_fd = -1;
	char u_input1[27], buf[8192];
	size_t nread;

	if (append_mode == 1){ //append mode

		size_t inlen1 = sizeof(argv[2]); //security reasons
		strncpy(u_input1, argv[2], inlen1);

		if ((from_fd = open(u_input1, O_RDWR | O_APPEND)) == -1) {
                // capture an error -- file doesn't exist
                if (errno == ENOENT) {
                        printf("%s file does not exist; not opening file\n", u_input1);
                        usage();
                        exit(EXIT_FAILURE);
                }
                ERR_RET("from open failed");
                goto out;
        }

        while ((nread = read_all(from_fd, buf, sizeof(buf))) != -1){
        	write(1, buf, nread);
			write(from_fd, buf, nread);
		}

    }
	
	else if (append_mode == 0) {

		size_t inlen1 = sizeof(argv[1]); //security reasons
		strncpy(u_input1, argv[1], inlen1);

		if ((from_fd = open(u_input1, O_RDONLY)) == -1) {
                if (errno == ENOENT) {
                        printf("%s file does not exist; not opening file\n", u_input1);
                        usage();
                        exit(EXIT_FAILURE);
                }
                ERR_RET("from open failed");
                goto out;
        }

		while ((nread = read_all(from_fd, buf, sizeof(buf))) != -1){
			write(1, buf, nread);
		}

	}

out:        
        if (from_fd != -1)
            if (close(from_fd) == -1)
                perror("error in from file close");

    if (close(from_fd) == -1)
        perror("error in from file close");

}

ssize_t read_all(int fd, void *buf, size_t nbytes) { /*Provided code modified to work with tee*/

        ssize_t nread = 0;
        ssize_t n;

        do {	//'buf + nread' is the same thing
                if ((n = read(fd, &((char *)buf)[nread], nbytes - nread)) == -1) {
                    if (errno == EINTR)
                        continue;
                    else
                        return -1;
                }
                nread += n;

                if (n == 0){
                    return -1; //fixes the loop problem
                }
                if ((((char *)buf)[nread - 1]) == '\n'){ //added for new line case
                	return nread;
                }

        } while (nread < nbytes);

        return nread;
        
}

void usage(void)
{
        printf("\nTEE PROGRAM\n\nPURPOSE:\n\tCurrently mimics the \"tee\" command provided in linux");
        printf("\n\twith the append to file support. Doesn't support\n\tmultiple files yet...\n");
        printf("\nUSAGE:\n\t./tee [-a] [FILENAME]\n\nFLAG(S):\n\t-a : enable append function\n\n");
}
