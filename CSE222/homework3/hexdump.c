/**
 * @file hexdump.c
 * 
 * @author Steven Ramsey
 *
 * @date 3/5/2016
 *  
 * Assignment: homework3 
 *
 * @brief outputs the hex dump of a file
 *
 * @details This program prints the hex representation of a file to stdout.
 *  
 * @bugs N/A
 *
 * @todo N/A
 */ 

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "apue.h"
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>

#define ERR_RET(s) err_ret(#s " %s:%d\n", __FILE__, __LINE__)
#define SIZE 8

ssize_t read_all(int fd, void *buf, size_t nbytes);
void process_file(char* filename);
void hexdump(int from_fd);
void usage();

int main(int argc, char *argv[]){

	if (argc != 2) {
		printf("NO ARGUMENTS PROVIDED!\n");
        usage();
        exit(EXIT_FAILURE);
        }

        process_file(argv[1]);

        return 0;
}

void process_file(char* filename){

	int from_fd = 0;

	if ((from_fd = open(filename, O_RDONLY)) == -1) {

        if (errno == ENOENT) {
                printf("%s file does not exist; not opening file\n", filename);
                usage(); 
                exit(EXIT_FAILURE);
        }

        ERR_RET("from open failed");
        goto out;

   	}
   	hexdump(from_fd);

   	goto out;

out:        
        if (from_fd != -1)
                if (close(from_fd) == -1)
                        perror("error in from file close");

}

void hexdump(int from_fd){

        size_t readbytes = 0;
	int i = -1, hex_offset = 0;
	char buf[SIZE];

        do {
            	if((readbytes = read_all(from_fd, buf, sizeof(buf))) == -1){
            		perror("Read_all() failed.\n");
            		goto out;
            	}

            	printf("%06x ", hex_offset); //mem address

            	for(i = 0; i < readbytes; i++){ //hex values
            		printf ("%04x ", buf[i]);
            	}

            	printf("\n");

            	hex_offset += readbytes;	

        } while(readbytes == sizeof(buf));

    return; //keep from going to out

out:        
    if (from_fd != -1)
        if (close(from_fd) == -1)
            perror("error in from file close");

}

void usage(){
	printf("\nHEXDUMP PROGRAM\n\nPURPOSE:\n\tOutputs the hex representation/dump of a file that is passed in.\n\n");
	printf("USAGE:\n\t./hexdump [FILENAME]\n\n");
}
