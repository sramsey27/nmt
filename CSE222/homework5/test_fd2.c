/**
 * @file test_fd2.c
 *
 * @author Steven Ramsey
 *
 * @date 4/24/16
 * 
 * @brief Test the fd.c library by creating files within the process with exec calls
 * 
 * @todo N/A
 *
 * @bugs 
 */

#include "fd.h"

#define ERR_RET(s) err_ret(#s " %s:%d\n", __FILE__, __LINE__)

int create_files(int n);
void usage();

int main(int argc, char **argv){

	if(argc < 2){
		printf("Error: Invalid user input\nOpening user guide...\n\n");
		usage();
		return 0;
	}

	int filenum = 0, c = 0, i = 0;
	struct rlimit limitation; /*struct for rlimit*/
    pid_t pid;
    char *const args[] = {NULL};

	/*get flag options*/
	while ((c = getopt(argc, argv, "n:h")) != -1) {
            	switch (c) {
                	case 'n':
                		filenum = atoi(optarg);
                		break;
                	case 'h':
                		usage();
                		return 0;
                	default:
                		printf("Something went wrong with getopt()\n\n");
                }
                if (isprint(optopt)) {
                        fprintf(stderr, "\nUnknown option '%c'.\nClosing program\n", optopt);
                        exit(EXIT_FAILURE);
                }          
    	}
    	/*If 0 files are requested, exit gracefully*/
    	if(filenum < 1){
    		printf("No directories were created.\tRequested: %d\n", filenum);
    		exit(EXIT_FAILURE);
    	}
    	/*rlimit check*/
    	if(getrlimit(RLIMIT_NOFILE, &limitation) < 0){
    		perror("getrlimit() failure");
    	}

    	/*consider other 3 file discripters*/
    	if(limitation.rlim_cur - 3 < filenum){ 
    		printf("Limitation of %lld directories is exceeded in input, Try again maybe with a smaller number.\n\n", (long long int)limitation.rlim_cur);
    		exit(EXIT_FAILURE);
    	}

    	/*Personal error checking*/
    	if(create_files(filenum) < 0){
    		printf("An error occured while making the directories...\n");
    		return 0;
    	}

    	printf("open test_fd fd\n");

    	/*Finally seek the open file discriptors*/
    	int *fd = get_open_fd();
        for(i = 0; fd[i] != -1; i++){
            if((fd[i] % 2) == 1){
                fcntl(fd[i], F_SETFD, FD_CLOEXEC);
            }
            printf("fd = %d\n", fd[i]);
        }
        /*Then clean up everything*/
        printf("\nopen test_close_on_exec fd\n");

        if ((pid = fork()) ==-1)
            perror("fork error");
        else if (pid == 0)
                execv("./test_close_on_exec", args);
        else{
            wait(&pid);
            exit(0);
        }

        return 0;
}

/**
 * Creates the foo directory and creates n number of foo files.
 * @param n the number of files to create
 * @return 1 if successful -1 otherwise
 */
int create_files(int n){

	int i = 0, curfd = 0;
	char buffer[256];

	if(mkdir("foo", 0700) < 0){
		if(errno == EEXIST) /*just continue if directory exist*/
			goto process;
		perror("Unable to make directory \"foo\"");
		return -1;
	}

process: /*start creating foo files*/
	for(i = 1; i < n; i++){
		sprintf(buffer, "foo/foo%d", i);
		if((curfd = open(buffer, O_RDWR | O_TRUNC | O_CREAT |S_IRWXU ) < 0)){
                    perror("open");
               		return -1;
        }	
    }
	return 1;
}

/**
 * Display the user guide
 */
void usage(){
	printf("\t\ttest_fd User Guide\n\nPURPOSE:\n\tTo test created libraries fd.c and fd.h\n\n");
	printf("USAGE:\n\t./test_fd -n [number of directories to create] -h\n\n");
	printf("FLAGS:\n\t-n: The number of requested directories (required)\n\t-h: open user guide (optional)\n\n");
}