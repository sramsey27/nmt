#ifndef	_fd_h_
#define _fd_h_

#include <assert.h>
#include <dirent.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/resource.h>
#include <fcntl.h>
#include <sys/wait.h>


int *get_open_fd();
int helper(char buffer[]);
void cleanup_fd(void);

#endif