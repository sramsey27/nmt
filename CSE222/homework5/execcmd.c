/**
 * @file execcmd.c
 *
 * @author Steven Ramsey
 *
 * @date 4/24/16
 * 
 * @brief a program that creates a new process and executes the command passed in on the command line
 * 
 * @todo N/A
 *
 * @bugs 
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <errno.h>

int main(int argc, char **argv) {
        pid_t pid, waitval, temp = ((pid_t) - 1);
        int status;

        pid = fork();
        switch(pid) {
                case 0:
                        printf("child executing\n");
                        execvp(argv[1], argv + 1);
                        printf("Error from %s: %s\n", argv[1], strerror(errno));
                        exit(EXIT_FAILURE);
                default:
                        waitval = waitpid(pid, &status, 0);
                        while(waitval == temp && errno == EINTR)
                                waitval = waitpid(pid, &status, 0);

                        if (waitval == temp) {
                                perror("Failed");
                                return 0;
                        }
        }
        printf("child fini\n");

        if (WIFEXITED(status)) {
                if (WEXITSTATUS(status) == 0){
                        printf("normal exit status: %d\n", WEXITSTATUS(status));
                }
                else if (WEXITSTATUS(status) != 0){
                        printf("Failure: Exit status = %d\n", WEXITSTATUS(status));
                }
        }
        return 0;
}