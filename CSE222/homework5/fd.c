/**
 * @file fd.c
 *
 * @author Steven Ramsey
 *
 * @date 4/24/16
 * 
 * @brief retrieves the open file discriptors enabled by the process of this program
 * 
 * @todo N/A
 *
 * @bugs 
 */


#include "fd.h"

int *array;

/**
 * Gets the current file discriptors
 * @return the int array with the fds
 */
int *get_open_fd(){

	DIR *dirp;
	char buffer[256];
	struct dirent *dp;
	int fdnum = 0, i = 0, curfd = 0;

	/*get the pid*/
	pid_t cur_pid = getpid();
	sprintf(buffer, "/proc/%d/fd", cur_pid);

	/*open the directory*/
	dirp = opendir(buffer);
	if (dirp == NULL){
		perror("opendir error");
		return 0;
	}

	fdnum = helper(buffer); //detemine how many fds are open
	array = malloc(sizeof(int) * (fdnum )); //n number of fds allocated
	assert(array);

	while((dp = (readdir(dirp))) != NULL){
		curfd = atoi(dp->d_name);

		if(strcmp(".", dp->d_name) == 0 || strcmp("..", dp->d_name) == 0){
			continue;
		}
		#ifdef VALGRIND
		else if(curfd >= 1024){
			array[i] = curfd;
			i++;
		}
		#endif
		#ifndef VALGRIND
		else if(curfd > 2){
			array[i] = curfd;
			i++;
		}
		#endif
	}
	array[fdnum -1] = -1; /*make last entry -1*/
	closedir(dirp);
	return array;
}

/**
 * Closes the open fds
 */
void cleanup_fd(void){
	int i = 0;
	while(array[i] != -1){
			printf("closing %d\n", array[i]);
        	close(array[i]);
        	i++;
    }
    free(array);
    printf("cleanup done!\n");
}

/**
 * Determines the number of open file discriptors
 * @param buffer the file name
 * @return number of open file discriptors
 */
int helper(char buffer[]){

	int count = 0, curfd = 0;
	struct dirent *dp;
	DIR *dirp2;

	dirp2 = opendir(buffer);
	if (dirp2 == NULL){
		perror("opendir error");
		return 0;
	}

	while((dp = (readdir(dirp2))) != NULL){

		curfd = atoi(dp->d_name);

		if(strcmp(".", dp->d_name) == 0 || strcmp("..", dp->d_name) == 0){
			continue;
		}
		#ifdef VALGRIND
		else if((curfd = (atoi(dp->d_name))) >= 1024){
			count++;
		}
		#endif
		#ifndef VALGRIND
		else if((curfd = (atoi(dp->d_name))) > 2){
			count++;
		}
		#endif
	}
	closedir(dirp2);

	return count;
}