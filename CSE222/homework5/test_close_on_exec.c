/**
 * @file test_close_on_exec.c
 *
 * @author Steven Ramsey
 *
 * @date 4/24/16
 * 
 * @brief Test the left over fds from test_fd2.c and clean them up
 * 
 * @todo N/A
 *
 * @bugs 
 */

#include "fd.h"

int main(){

	int *fd = get_open_fd(), i = 0;

	printf("only even fd!\n");

	for(i = 0; fd[i] != -1; i++){
            printf("fd = %d\n", fd[i]);
        }

    printf("\natexit() closing file descriptors\n");
    
	atexit(cleanup_fd);

	return 0;
}